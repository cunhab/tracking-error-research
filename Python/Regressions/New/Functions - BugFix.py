import pandas as pd
import numpy as np
import sqlalchemy
import scipy as sc
import importlib as imp
import glob
from pandas.tseries.offsets import BMonthEnd
# import notebookO as ntb
# import RegressLS as ls
# imp.reload(ntb)
# imp.reload(ls)
import matplotlib.pylab as plt
# %matplotlib notebook
import itertools
from itertools import combinations
import cvxopt
from cvxopt import matrix
import pyodbc
from sqlalchemy.types import FLOAT
from sqlalchemy.types import DATE
from sqlalchemy.types import INTEGER
from sqlalchemy.types import NVARCHAR
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from scipy import stats
import math
import time
from datetime import datetime, date, time, timedelta
from time import sleep
from functools import reduce
import re
import scipy.optimize as sco
from scipy.stats import norm
import sklearn
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import cross_val_predict, GridSearchCV, RandomizedSearchCV, LeaveOneOut, KFold
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn import linear_model
from sklearn.experimental import enable_hist_gradient_boosting
from sklearn.ensemble import StackingRegressor, RandomForestRegressor, HistGradientBoostingRegressor, GradientBoostingRegressor
from sklearn.linear_model import Ridge, RidgeCV, Lasso, LassoCV, LinearRegression as skOLS
from sklearn.inspection import permutation_importance
from sklearn.model_selection import cross_val_score
import eli5
from eli5.sklearn import PermutationImportance
from eli5.permutation_importance import get_score_importances
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.inspection import plot_partial_dependence
from sklearn.inspection import partial_dependence
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import kpss
from statsmodels.stats.stattools import durbin_watson

#################
### Utilities ###
#################

def square_error(x1, x2):
    return (x1 - x2) * (x1 - x2)
def abs_error(x1, x2):
    return np.abs(x1 - x2)
def sortbycolname(df):
    def get_trailing_number(s):
        # https://stackoverflow.com/questions/7085512/check-what-number-a-string-ends-with-in-python
        import re
        m = re.search(r'\d+$', s)
        return int(m.group()) if m else None

    dfcol = pd.DataFrame(index=df.columns)
    dfcol['indexer'] = [get_trailing_number(col) for col in dfcol.index]
    dfcol = dfcol.fillna(999)
    dfcol['string'] = [dfcol.index[i][:-len(str(int(dfcol.iloc[i, 0])))] for i in range(dfcol.shape[0])]
    dfcol = dfcol.sort_values(by=['string', 'indexer']).drop(['string', 'indexer'], axis=1)
    df = df.reindex(dfcol.index, axis=1)

    return df
def Datenozero(df):
    # Converts date from 01/31/2001 to 1/31/2001
    first = df['Date'].str.split('/').str[0].str[0]
    first = first.replace('0', '')
    second = df['Date'].str.split('/').str[0].str[1]
    third = df['Date'].str.split('/').str[1:].str[0]
    fourth = df['Date'].str.split('/').str[1:].str[1]
    all = pd.concat([first, second, third, fourth], axis=1)
    df['Date'] = all.iloc[:, 0] + all.iloc[:, 1] + '/' + all.iloc[:, 2] + '/' + all.iloc[:, 3]

    return df
def yearmonth(df, Date):
    if df.shape[0]>0:
        df['Year'] = pd.DatetimeIndex(df[Date]).year
        df['Month'] = pd.DatetimeIndex(df[Date]).month
        df['Year'] = df['Year'].apply(str)
        df['Month'] = df['Month'].apply(str)
        df['Month'] = '0' + df['Month']
        df['Month'] = df['Month'].str[-2:]
        df['YearMonth'] = df['Year'] + df['Month']
def yearmonthfromdate(Date):
    Year = Date.year
    Month = Date.month
    Month01 = '0' + str(Month) if len(str(Month)) == 1 else str(Month)
    YearMonth = str(Year) + Month01
    return YearMonth
def yearmonthday(df, YearMonth):
    if df.shape[0] > 0:
        df['Monthtemp'] = df[YearMonth].str[4:6]
        df['Yeartemp'] = df[YearMonth].str[:4]
        df['YearMonthDay'] = pd.to_datetime(df['Yeartemp'] + df['Monthtemp'], format='%Y%m')
        df['YearMonthDay'] = pd.to_datetime(df['YearMonthDay'], format="%Y%m") + pd.tseries.offsets.MonthEnd(1)
        df.drop(['Monthtemp','Yeartemp'], axis=1, inplace=True)
def correlogram(df):
    from datetime import datetime
    import seaborn as sns
    import matplotlib.pyplot as plt

    df = df.dropna()

    sns.set()
    # sns.set(ax=ax)
    sns.set(font_scale=0.8)
    sns.set(font_scale=0.5, rc={'figure.figsize': (10, 10)})
    g = sns.PairGrid(df)
    g = g.map_upper(plt.scatter)
    g = g.map_lower(sns.kdeplot, cmap="Blues_d")
    g = g.map_diag(sns.kdeplot, lw=3, legend=True)
    g.fig.set_size_inches(20, 18)
    plt.show()
def yearmonthshift(YearMonth,shift):
    Year = int(YearMonth[:4])
    Month = int(YearMonth[-2:])
    TotalMonth = (Year - 1900) * 12 + Month + shift
    YearShifted = 1900 + (TotalMonth-1)//12
    MonthShifted = (TotalMonth-1)%12 + 1
    MonthShifted = '0' + str(MonthShifted) if len(str(MonthShifted)) == 1 else str(MonthShifted)
    YearMonth = str(YearShifted) + MonthShifted

    return YearMonth
def yearmonthshiftdf(df,name,Shift):
    df['_Year'] = df[name].str[:4].astype(int)
    df['_Month'] = df[name].str[-2:].astype(int)
    df['TotalMonth'] = (df['_Year'] - 1900) * 12 + df['_Month'] + Shift
    df['YearShifted'] = 1900 + (df['TotalMonth']-1)//12
    df['MonthShifted'] = (df['TotalMonth']-1)%12 + 1
    for i in range(df.shape[0]):
        df.loc[i, 'MonthShifted'] = '0' + str(df.loc[i, 'MonthShifted']) \
            if len(str(df.loc[i, 'MonthShifted'])) == 1 else str(df.loc[i, 'MonthShifted'])
    df[name] = df['YearShifted'].astype(str) + df['MonthShifted']
    df.drop(['_Year','_Month','TotalMonth','YearShifted','MonthShifted'], axis=1, inplace=True)

    return df
def yearmonthrange(YearMonth,Window,Shift):
    YearMonth = yearmonthshift(YearMonth, Shift)
    YearMonthList = []
    for s in range(Window):
        YearMonthList = YearMonthList + [yearmonthshift(YearMonth, -s)]

    return YearMonthList
def YearMonthMissing(df, Date1start, Date2start, YearMonth):
    Date1 = datetime.strptime(Date1start, '%Y/%m/%d')
    Date2 = datetime.strptime(Date2start, '%Y/%m/%d')
    DateList = yearmonthrange(yearmonthfromdate(Date2), (Date2.year - Date1.year) * 12 + (Date2.month - Date1.month), 0)
    return [item for item in DateList if item not in list(df[YearMonth])]
def max_common_cols(df1,df2,min,max):
    # Find columns between two dataframes that maximizes data availability across both dataframes and number of columns selected
    # Number of columns is between min and max
    if df1.shape[0]>0 and df2.shape[0]>0:
        if max=='': max=df1.shape[1]+1
        cols = df1.columns
        df = pd.DataFrame()
        k = 0
        for i in range(min, max):
            for j, jlist in enumerate(list(combinations(cols, i))):
                df.loc[k, 'i'] = i
                df.loc[k, 'j'] = j
                df.loc[k, 'Factors'] = len(jlist) / len(cols)
                df.loc[k, 'Prediction'] = df1[list(jlist)].dropna().shape[0] / df1.shape[0]
                df.loc[k, 'Training'] = df2[list(jlist)].dropna().shape[0] / df2.shape[0]
                df.loc[k, 'Score'] = 0.2 * df.loc[k, 'Factors'] + 0.6 * df.loc[k, 'Prediction'] + 0.2 * df.loc[k, 'Training']
                k = k + 1
        if not df.empty:
            # Find max Score
            k = int(df['Score'].idxmax())
            i = int(df.loc[k, 'i'])
            j = int(df.loc[k, 'j'])
            columns = list(list(combinations(cols, i))[j])
        else:
            columns = []
            df = pd.DataFrame()
    else:
        columns=[]
        df=pd.DataFrame()

    return columns,df
def mergearray(array, cols, allCols, N, Interceptflag, Intercept, Name, suffix):
    # Expands array to allCols

    # Create dataframe out of array, cols
    arrayN = np.ones((N, len(array))) * array
    arrayNdf = pd.DataFrame(arrayN, columns=[Name + suffix + ' ' + z for z in cols])
    # Create empty dataframe with allCols
    allColumns = [Name + suffix + ' ' + z for z in allCols]
    # Add Intercept if applicable
    if Interceptflag == True:
        allColumns = ['Intercept' + suffix] + allColumns
        Intercept = pd.DataFrame(Intercept, columns=['Intercept' + suffix])
        arrayNdf = pd.concat([Intercept, arrayNdf], axis=1)
    arrayall = pd.DataFrame(np.ones((N, len(allColumns))) * np.nan, columns=allColumns)
    # Update allCols dataframe with elements from cols dataframe
    arrayall[arrayNdf.columns.to_list()] = arrayNdf[arrayNdf.columns.to_list()]
    arrayall = arrayall.to_numpy()

    return {'arrayall': arrayall, 'arrayallcols': allColumns}
def vlookup(list,df):
    import re
    for i in range(df.shape[0]):
        list=[re.sub(r'^'+df.iloc[i,0]+'$', df.iloc[i,1], s) for s in list]
    return list
def isListEmpty(inList):
    if isinstance(inList, list):  # Is a list
        return all(map(isListEmpty, inList))
    return False  # Not a list
def DateJob(Date1start,Date2start,nCPU,frequency):
    Date1start = datetime.strptime(Date1start, '%Y/%m/%d')
    # Date2start = datetime(date.today().year, date.today().month, date.today().day)
    Date2start = datetime.strptime(Date2start, '%Y/%m/%d')
    if frequency=='M' or frequency=='BM':
        # The below converts the given date to month end
        Date1start = (Date1start + pd.offsets.MonthEnd(0)).date() if frequency=='M' else BMonthEnd().rollback(Date1start).date()
        Date1start = datetime(Date1start.year, Date1start.month, Date1start.day)
        # The below converts the given date to month end
        Date2start = (Date2start + pd.offsets.MonthEnd(0)).date() if frequency=='M' else BMonthEnd().rollback(Date2start).date()
        Date2start = datetime(Date2start.year, Date2start.month, Date2start.day)
        nperiods = 12 * (Date2start.year - Date1start.year) + (Date2start.month - Date1start.month) + 1
    if frequency=='B':
        nperiods = np.busday_count(Date1start.date(), Date2start.date())
    nCPU = min(nCPU, nperiods)
    split_periods = int(round(nperiods / nCPU))
    Date1 = Date1start
    Date2 = Date2start

    return Date1, Date2, Date2start, nCPU, split_periods
def MLDDresults(N, Y, Yfits):
    d2 = np.empty((N, 1)) * np.nan
    PD = np.empty((N, 1)) * np.nan
    CPD = np.empty((N, 1)) * np.nan
    CQDF = np.empty((N, 1)) * np.nan
    d2CV = np.empty((N, 1)) * np.nan
    PDCV = np.empty((N, 1)) * np.nan
    CPDCV = np.empty((N, 1)) * np.nan
    CQDFCV = np.empty((N, 1)) * np.nan
    if Y == 'd2':
        d2 = OAStcalc(Yfits, 'Yfit')[0]
        PD = OAStcalc(Yfits, 'Yfit')[1]
        CPD = OAStcalc(Yfits, 'Yfit')[2]
        CQDF = OAStcalc(Yfits, 'Yfit')[3]
        OASt = OAStcalc(Yfits, 'Yfit')[4]
        d2CV = OAStcalc(Yfits, 'YfitCV')[0]
        PDCV = OAStcalc(Yfits, 'YfitCV')[1]
        CPDCV = OAStcalc(Yfits, 'YfitCV')[2]
        CQDFCV = OAStcalc(Yfits, 'YfitCV')[3]
        OAStCV = OAStcalc(Yfits, 'YfitCV')[4]
    else:
        OASt = np.array([np.exp(Yfits['Yfit']).to_numpy()]).T
        OAStCV = np.array([np.exp(Yfits['YfitCV']).to_numpy()]).T

    return d2, PD, CPD, CQDF, d2CV, PDCV, CPDCV, CQDFCV, OASt, OAStCV
def DataDatesCreator(Data):
    Data = Data.set_index('Date')
    # Populate all days
    start_date = Data.index.min() - pd.DateOffset(day=1)
    end_date = Data.index.max() + pd.DateOffset(day=31)
    dates = pd.bdate_range(start_date, end_date, freq='D', name='Date')
    CDates = pd.DataFrame(dates, columns=['Date'])
    dates = pd.bdate_range(start_date, end_date, freq='B', name='Date')
    BDates = pd.DataFrame(dates, columns=['Date'])

    return CDates, BDates
def MonthlyDateGeneration(DataIn, Nmonths, Direction):
    Data = DataIn.copy()
    Data['DateString'] = Data['Date'].dt.strftime('%Y-%m-%d')
    DataDates = DataDatesCreator(Data)[0]
    DataDates = pd.concat([DataDates, pd.DataFrame(np.arange(0, DataDates.shape[0]))], axis=1)
    DataShiftDates = DataDates.copy()
    DataShiftDates['Date'] = DataShiftDates['Date'] + pd.DateOffset(months=Nmonths) if Direction == 'Forward' \
        else DataShiftDates['Date'] + pd.DateOffset(months=-Nmonths)
    DataDatesMerge = pd.merge(DataShiftDates, DataDates, left_on=0, right_on=0, how='outer')
    DataDatesMerge.rename(columns={'Date_x': 'Date', 'Date_y': 'Date-' + str(Nmonths) + 'M'}, inplace=True) if Direction == 'Forward' \
        else DataDatesMerge.rename(columns={'Date_y': 'Date', 'Date_x': 'Date-' + str(Nmonths) + 'M'}, inplace=True)
    # Keep only business days of day t
    BDates = DataDatesCreator(Data)[1]
    DataDatesMerge = pd.merge(DataDatesMerge, BDates, on='Date', how='inner')
    # Move t-Nmonths to previous business day if weekend - not next business day due to many Monday bank holidays.
    DataDatesMerge['Date-' + str(Nmonths) + 'M'] = [x if x.weekday() not in [5, 6] else \
                                                        x - timedelta(days=(x.weekday() - 4)) for x in pd.to_datetime(DataDatesMerge['Date-' + str(Nmonths) + 'M'])]
    # Fill missing rates by carrying forward up to 3 consecutive business days
    DataTemp = pd.merge(DataDatesMerge[['Date-' + str(Nmonths) + 'M']], Data.iloc[:, :2], left_on='Date-' + str(Nmonths) + 'M', right_on='Date', how='left')
    DataTemp[['Date']] = DataTemp[['Date']].fillna(method='ffill', limit=3)
    DataDatesMerge[['Date-' + str(Nmonths) + 'M']] = DataTemp[['Date']]
    DataDatesMerge = DataDatesMerge.dropna().reset_index(drop=True)
    # Identify duplicates
    DataDatesMerge['DateCount'] = DataDatesMerge.groupby('Date')['Date'].transform('count')
    DatesDuplicates = DataDatesMerge[DataDatesMerge['DateCount'] > 1]
    DatesDuplicates = DatesDuplicates.drop_duplicates(['Date'], keep='first')[['Date']].reset_index(drop=True)
    # From the duplicates, keep t-1 that does not fall on a weekend
    for i in range(DatesDuplicates.shape[0]):
        # Check if any of t-1 dates have rates
        # We process one variable at a time, hence Data has just one column
        col = Data.columns[1]
        DataDupes = Data[Data['DateString'].str.contains('|'.join(list(DataDatesMerge[DataDatesMerge['Date'] == \
                                                                                      DatesDuplicates.iloc[i, 0]]['Date-' + str(Nmonths) + 'M'].dt.strftime('%Y-%m-%d'))))].loc[:, ['Date', col]]
        DataDupesNotempty = DataDupes.dropna().reset_index(drop=True)
        if not DataDupesNotempty.empty:
            # Keep last entry
            DataDatesMerge.loc[DataDatesMerge['Date'] == DatesDuplicates.iloc[i, 0], 'Date-' + str(Nmonths) + 'M'] = \
                DataDupesNotempty.loc[:, 'Date'].iloc[-1]
        else:
            # Check if last business day of month
            if DatesDuplicates.iloc[i, 0] == DatesDuplicates.iloc[i, 0] + pd.offsets.MonthEnd(0):
                # Take last available entry up to 3 days away
                PreviousMonthendDate = DatesDuplicates.iloc[i, 0] + pd.offsets.MonthEnd(-1)
                DataTemp = Data.loc[(Data['Date'].dt.month == PreviousMonthendDate.month) & \
                                    (Data['Date'].dt.year == PreviousMonthendDate.year), :].dropna().reset_index(drop=True)
                DataTemp['DayDiff'] = (PreviousMonthendDate - DataTemp['Date']).dt.days
                DataTemp = DataTemp.loc[DataTemp['DayDiff'] <= 3, :]
                if not DataTemp.empty:
                    DataDatesMerge.loc[DataDatesMerge['Date'] == DatesDuplicates.iloc[i, 0], 'Date-' + str(Nmonths) + 'M'] = DataTemp['Date'].iloc[-1]
                else:
                    DataDatesMerge.loc[DataDatesMerge['Date'] == DatesDuplicates.iloc[i, 0], 'Date-' + str(Nmonths) + 'M'] = np.nan
            else:
                # Take next available entry up to 3 days away
                DataTemp = Data
                DataTemp['DayDiff'] = (DataTemp['Date'] - PreviousMonthendDate).dt.days
                DataTemp = DataTemp.loc[(DataTemp['DayDiff'] >= 0) & (DataTemp['DayDiff'] <= 3), :]
                if not DataTemp.empty:
                    DataDatesMerge.loc[DataDatesMerge['Date'] == DatesDuplicates.iloc[i, 0], 'Date-' + str(Nmonths) + 'M'] = DataTemp['Date'].iloc[-1]
                else:
                    DataDatesMerge.loc[DataDatesMerge['Date'] == DatesDuplicates.iloc[i, 0], 'Date-' + str(Nmonths) + 'M'] = np.nan
    DataDatesMerge = DataDatesMerge.drop_duplicates(['Date'], keep='first').reset_index(drop=True)
    DataDatesMerge.drop([0, 'DateCount'], axis=1, inplace=True)

    return DataDatesMerge
def DatesAllCreator(Data, Nmonths):

        DatesForward = MonthlyDateGeneration(Data, Nmonths, 'Forward')
        DatesBackward = MonthlyDateGeneration(Data, Nmonths, 'Backward')
        DatesAll = pd.merge(DatesForward, DatesBackward, on='Date', how='outer').sort_values(by='Date').reset_index(drop=True)
        DatesAll.loc[DatesAll['Date-' + str(Nmonths) + 'M' + '_x'].isnull(), 'Date-' + str(Nmonths) + 'M' + '_x'] = \
            DatesAll.loc[DatesAll['Date-' + str(Nmonths) + 'M' + '_x'].isnull(), 'Date-' + str(Nmonths) + 'M' + '_y']
        DatesAll.loc[DatesAll['Date-' + str(Nmonths) + 'M' + '_y'].isnull(), 'Date-' + str(Nmonths) + 'M' + '_y'] = \
            DatesAll.loc[DatesAll['Date-' + str(Nmonths) + 'M' + '_y'].isnull(), 'Date-' + str(Nmonths) + 'M' + '_x']
        DatesAll['Date-' + str(Nmonths) + 'M' + '_x_days'] = (DatesAll['Date'] - DatesAll['Date-' + str(Nmonths) + 'M' + '_x']).dt.days
        DatesAll['Date-' + str(Nmonths) + 'M' + '_y_days'] = (DatesAll['Date'] - DatesAll['Date-' + str(Nmonths) + 'M' + '_y']).dt.days
        for i in range(DatesAll.shape[0]):
            DatesAll.loc[i, 'Date-' + str(Nmonths) + 'M'] = DatesAll.loc[i, 'Date-' + str(Nmonths) + 'M' + '_x'] \
                if DatesAll.loc[i, 'Date-' + str(Nmonths) + 'M' + '_x_days'] <= DatesAll.loc[i, 'Date-' + str(Nmonths) + 'M' + '_y_days'] \
                else DatesAll.loc[i, 'Date-' + str(Nmonths) + 'M' + '_y']
        DatesAll = DatesAll[['Date', 'Date-' + str(Nmonths) + 'M']]

        return DatesAll
def DatesRates(Dates, Data, Nmonths,RateType, RateLabel):
    Dates.rename(columns={'Date-' + str(Nmonths) + 'M': 'Date-' + str(Nmonths) + 'M_' + list(Data)[1]}, inplace=True)
    Dates = pd.merge(Dates, Data, left_on='Date', right_on='Date', how='left')
    Dates.rename(columns={list(Data)[1]: list(Data)[1] + '_Date_' + str(Nmonths)+'M'}, inplace=True)
    Dates = pd.merge(Dates, Data, left_on='Date-' + str(Nmonths) + 'M_' + list(Data)[1], right_on='Date', how='left')
    Dates.rename(columns={'Date_x':'Date', list(Data)[1]: list(Data)[1] + '_Date-'+str(Nmonths)+'M'}, inplace=True)
    Dates.drop('Date_y', axis=1, inplace=True)
    if RateType=='LogReturn':
        Dates[list(Data)[1] + RateLabel + str(Nmonths)+'M'] = np.log(Dates[list(Data)[1] + '_Date_' + str(Nmonths)+'M']
                                                                     / Dates[list(Data)[1] + '_Date-'+str(Nmonths)+'M'])
    if RateType=='PriceChange':
        Dates[list(Data)[1] + RateLabel + str(Nmonths) + 'M'] = Dates[list(Data)[1] + '_Date_' + str(Nmonths) + 'M'] \
                                                                - Dates[list(Data)[1] + '_Date-' + str(Nmonths) + 'M']
    return Dates
def AllDatesRatesCreator(DataOrig, Nmonthlist, Ndaylist,RateType):

    AllDatesRates = pd.DataFrame()
    # Daily
    for col in DataOrig.drop('Date', axis=1).columns:
        Data = DataOrig[['Date', col]]
        for Ndays in Ndaylist:
            Dates = Data.copy()
            if RateType=='LogReturn':
                RateLabel = '_r_'
                Dates[col + RateLabel + str(Ndays) + 'D'] = np.log(Dates[[col]] / Dates[[col]].shift(Ndays))
            if RateType=='PriceChange':
                RateLabel = '_d_'
                Dates[col + RateLabel + str(Ndays) + 'D'] = Dates[[col]] - Dates[[col]].shift(Ndays)
            Dates.drop(col, axis=1, inplace=True)
            AllDatesRates = Dates if AllDatesRates.empty else pd.merge(AllDatesRates, Dates, on='Date', how='outer').sort_values(by='Date')
    # Monthly
    for col in DataOrig.drop('Date', axis=1).columns:
        Data = DataOrig[['Date', col]]
        for Nmonths in Nmonthlist:
            Dates = DatesAllCreator(Data, Nmonths)
            Dates = DatesRates(Dates, Data, Nmonths,RateType, RateLabel)
            AllDatesRates = Dates if AllDatesRates.empty else pd.merge(AllDatesRates, Dates, on='Date', how='outer').sort_values(by='Date')

    AllDatesRates = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='left'), [AllDatesRates[['Date']], DataOrig, AllDatesRates])
    cols = AllDatesRates.select_dtypes('datetime64').columns
    AllDatesRates[cols] = AllDatesRates[cols].stack().dt.strftime('%a, %Y/%m/%d').unstack()
    Returns = AllDatesRates.loc[:, ['Date'] + list(AllDatesRates.columns[AllDatesRates.columns.str.contains('|'.join([RateLabel]))])]
    Returns['Date'] = pd.to_datetime(Returns['Date']).dt.strftime('%Y/%m/%d')
    return AllDatesRates, Returns

###########
### SQL ###
###########

def sqlcol(df, cols, type, table, override):
    # https://stackoverflow.com/questions/34383000/pandas-to-sql-all-columns-as-nvarchar
    # https://stackoverflow.com/questions/30137806/where-can-i-find-a-list-of-the-flask-sqlalchemy-column-types-and-options
    dtypedict = {}
    if len(cols) == 0:
        if checkiftableexists('','','',table)=='No' and override==False:
            for i, j in zip(df.columns,df.dtypes):
                if "object" in str(j):
                    dtypedict.update({i: sqlalchemy.types.NVARCHAR(length=250)})
                if "datetime" in str(j):
                    dtypedict.update({i: sqlalchemy.types.DateTime()})
                if "float" in str(j):
                    dtypedict.update({i: sqlalchemy.types.Float(precision=8, asdecimal=True)})
                # Integers are updated to float because converting missing values to integers in pandas fails
                if "int" in str(j):
                    dtypedict.update({i: sqlalchemy.types.Float()})
        else:
            # Update SQL and Pandas columns based on existing table schema
            dftypesSQL = sqlcoltypes('', '', '', table)
            dftypesPandas = dftypesSQL.copy()
            for i, j in zip(list(dftypesSQL.iloc[:,0]), dftypesSQL.iloc[:,1]):
                if "nvarchar" == str(j):
                    dtypedict.update({i: sqlalchemy.types.NVARCHAR(length=250)})
                if "varchar" == str(j):
                    dtypedict.update({i: sqlalchemy.types.VARCHAR(length=250)})
                if "real" == str(j):
                    dtypedict.update({i: sqlalchemy.types.Float(precision=8, asdecimal=True)})
                if "int" in str(j):
                    dtypedict.update({i: sqlalchemy.types.INT()})
            dftypesPandas.replace(['varchar','nvarchar','real','int','bigint','datetime'], ['object','object','float64','int64','int64','datetime64'], inplace=True)
            dpandastypedict = {}
            for i in range(dftypesSQL.shape[0]):
                dpandastypedict.update({dftypesPandas.iloc[i, 0]: dftypesPandas.iloc[i, 1]})
            # Below easier to debug instead of df = df.astype(dpandastypedict)
            for col in list(df.columns):
                # If Pandas column is not numeric but needs to be because the table was already saved as such,
                # replace all columns values with nan and throw warning later
                # This is usually the case if a column has 'None' on a day and numeric values on other days
                # Check for date conversion later
                if df[col].dtypes=='object' and ['float64','int64'].count(dpandastypedict.get(col))>0:
                    df[col] = np.nan
                df[col] = df[col].astype(dpandastypedict.get(col))
    else:
        # Update column types based on provided single value
        for i in cols:
            dtypedict.update({i: type})
    return df, dtypedict
def sqlcoltypes(server, port, database, table):
    server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    port = ':58508' if port == '' else ':' + port.strip(':')
    database = 'Sandbox' if database == '' else database

    query = 'SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'' + table + '\''
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + port + '/' + \
                  database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    df = pd.read_sql(query, con)
    con.close()
    return df
def deletefromsql(Yearmonth, table):
    if checkiftableexists('', '', '', table) == 'Yes':
        yearmonthquery = '' if Yearmonth=='' else ' where Yearmonth = \'' + Yearmonth + '\''
        query = 'delete from GPS.[' + table + ']' + yearmonthquery
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + 'EPGLBSQLUT08\ANALYTICS:58508' + '/' + 'Sandbox' \
                                              + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        con.execute(query)
        con.close()
def dropsql(table):
    if checkiftableexists('', '', '', table) == 'Yes':
        query = 'DROP TABLE GPS.[' + table + ']'
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + 'EPGLBSQLUT08\ANALYTICS:58508' + '/' + 'Sandbox' \
                                              + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        con.execute(query)
        con.close()
def bondexceltosql(start, end, file, analytic, returntype, table):
    for i in range(start, end + 1):

        df = pd.read_excel(file, analytic + i)
        df.iloc[5, 1] = 'Date'
        dates1t = df.iloc[5, 1:].T.reset_index(drop=True)
        df = df.replace('--', np.nan)
        # =============================================================================
        #     df.iloc[:,1] = df.iloc[:,1].str.replace(",","")
        # =============================================================================
        df = pd.concat([df.iloc[9:df.shape[0] - 2, 0], df.iloc[9:df.shape[0] - 2, 2:]], axis=1)
        dft = df.T.reset_index(drop=True)

        dfall = pd.concat([dates1t, dft], axis=1)
        dfall.columns = dfall.iloc[0].tolist()
        dfall = dfall.iloc[1:, :].reset_index(drop=True)
        dfall['Date'] = pd.to_datetime(dfall['Date'], errors='coerce').dt.date

        dfall = dfall.set_index('Date').stack()
        dfall = dfall.reset_index()
        dfall.columns = ['Date', 'Bond_name', analytic]

        if returntype == True:
            dfall[analytic + '_Return'] = np.log(
                df.groupby('Bond_name')[analytic].shift(0) / df.groupby('Bond_name')[analytic].shift(1))

        action = 'replace' if i == start else 'append'
        savetosql(dfall, table, action, cpu)
def readfromsql(table, identifier, idlist):
    # identifier=['Description','Date']
    # idlist=[['A','B'],['1/1/2000','1/2/2000']]
    data = pd.DataFrame()
    mystring = ",".join(idlist[0])
    mystring2 = ' where ' + identifier[0] + ' in (' + mystring + ')' if len(identifier) > 0 else ''
    mystring = '*' if mystring2 == '' else mystring
    query = 'select ' + mystring + ' from GPS.' + table + ' with (nolock)' + mystring2
    for i in range(1, len(identifier)):
        query = query + ' and ' + identifier[i] + ' in \(' + ",".join(idlist[i]) + '\)'
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + 'EPGLBSQLUT08\ANALYTICS:58508' + '/' + 'Sandbox' \
                                          + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    data = pd.read_sql(query, con)
    con.close()

    return data

server = 'use01azvasqlbhf003.database.windows.net'
port = 1433
database = 'alm'
table = 'te_tracking_error_by_fund_view'
columns = ''
PeriodLabel = 'balance_date'
DateStart = '12-02-2020'
YearMonth = '11-30-2020'
YearMonthList = []
DateMin = ''
DateMax = ''
Label2 = 'regression_set_id'
Label2List = ['18']
Label3 = 'efa_fund_id'
Label3List = ['284']

def getNAV(server, port, database, table, YearMonth, columns, top,YearMonthList,PeriodLabel,DateMin,DateMax,
           Label2, Label2List, Label3, Label3List, DateStart):
    serverused = 'use01azvasqlbhf003.database.windows.net' if server == '' else server
    portused = ':1433' if port == '' else ':' + port.strip()
    database = 'alm' if database == '' else database
    columnsexist = colsexist('', '', '', table, columns) if columns != '' else []
    columnsquery = '*' if len(columnsexist) == 0 else '[' + str(columnsexist)[1:-1].replace('\'', '').replace(', ', '],[') + ']'
    topused = '' if top == '' else 'top ' + str(top) + ' '
    # yearmonthlist takes priority
    yearmonthused = ' where ' + PeriodLabel + ' = \'' + YearMonth + '\'' if YearMonth != '' and len(YearMonthList)==0 else ''
    yearmonthsused = ' where ' + PeriodLabel + ' in (' + str(YearMonthList)[1:-1] + ')' if len(YearMonthList)>0 else ''
    _and = ' and ' if yearmonthused !='' and yearmonthsused!='' else ''
    where1 = ' where ' if yearmonthused == '' and yearmonthsused == '' and DateMin != '' else ''
    _and1 = ' and ' if (yearmonthused != '' or yearmonthsused != '') and DateMin != '' else ''
    dateminused = where1 + _and1 + PeriodLabel + ' >= \'' + DateMin + '\'' if DateMin != '' else ''
    where2 = ' where ' if yearmonthused == '' and yearmonthsused == '' and DateMin == '' and DateMax != '' else ''
    _and2 = ' and ' if (yearmonthused != '' or yearmonthsused != '' or DateMin != '') and DateMax != '' else ''
    datemaxused = where2 + _and2 + PeriodLabel + ' <= \'' + DateMax + '\'' if DateMax != '' else ''
    Label2Query = ' and ' + Label2 + ' in (' + str(Label2List)[1:-1] + ')' if Label2 != '' and len(Label2List)>0 else ''
    Label3Query = ' and ' + Label3 + ' in (' + str(Label3List)[1:-1] + ')' if Label3 != '' and len(Label3List)>0 else ''

    df = pd.DataFrame()

    if checkiftableexists(serverused, portused, database, table) == 'Yes':
        query = 'select ' + topused + columnsquery + ' from ' + database + '.GPS.[' + table + '] \
                with (nolock)' + yearmonthused + _and + yearmonthsused + dateminused + datemaxused
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                     database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        # df = pd.read_sql(query, con).drop_duplicates(keep='last').reset_index(drop=True)
        df = pd.read_sql(query, con)
        con.close()
        # Populate month end dates as YearMonthDay
        # if list(df.columns).count('YearMonth')>0:
        #     yearmonthday(df, 'YearMonth')
        df.replace('NULL', np.nan, inplace=True)
        # Update pandas column types based on SQL types
        df, dfcolumns = sqlcol(df, [], '', table, False)
    return df

def sqlgetbyyearmonth(server, port, database, table, YearMonth, columns, top,YearMonthList,PeriodLabel,DateMin,DateMax):
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database
    columnsexist = colsexist('', '', '', table, columns) if columns != '' else []
    columnsquery = '*' if len(columnsexist) == 0 else '[' + str(columnsexist)[1:-1].replace('\'', '').replace(', ', '],[') + ']'
    topused = '' if top == '' else 'top ' + str(top) + ' '
    # yearmonthlist takes priority
    yearmonthused = ' where ' + PeriodLabel + ' = \'' + YearMonth + '\'' if YearMonth != '' and len(YearMonthList)==0 else ''
    yearmonthsused = ' where ' + PeriodLabel + ' in (' + str(YearMonthList)[1:-1] + ')' if len(YearMonthList)>0 else ''
    _and = ' and ' if yearmonthused !='' and yearmonthsused!='' else ''
    where1 = ' where ' if yearmonthused == '' and yearmonthsused == '' and DateMin != '' else ''
    _and1 = ' and ' if (yearmonthused != '' or yearmonthsused != '') and DateMin != '' else ''
    dateminused = where1 + _and1 + PeriodLabel + ' >= \'' + DateMin + '\'' if DateMin != '' else ''
    where2 = ' where ' if yearmonthused == '' and yearmonthsused == '' and DateMin == '' and DateMax != '' else ''
    _and2 = ' and ' if (yearmonthused != '' or yearmonthsused != '' or DateMin != '') and DateMax != '' else ''
    datemaxused = where2 + _and2 + PeriodLabel + ' <= \'' + DateMax + '\'' if DateMax != '' else ''

    df = pd.DataFrame()

    if checkiftableexists(serverused, portused, database, table) == 'Yes':
        query = 'select ' + topused + columnsquery + ' from ' + database + '.GPS.[' + table + '] \
                with (nolock)' + yearmonthused + _and + yearmonthsused + dateminused + datemaxused
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                     database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        # df = pd.read_sql(query, con).drop_duplicates(keep='last').reset_index(drop=True)
        df = pd.read_sql(query, con)
        con.close()
        # Populate month end dates as YearMonthDay
        # if list(df.columns).count('YearMonth')>0:
        #     yearmonthday(df, 'YearMonth')
        df.replace('NULL', np.nan, inplace=True)
        # Update pandas column types based on SQL types
        df, dfcolumns = sqlcol(df, [], '', table, False)
    return df
def sqlgetbyyearmonthorig(server, port, database, table, YearMonth, columns, top,YearMonthList):
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database
    columnsexist = colsexist('', '', '', table, columns) if columns != '' else []
    columnsquery = '*' if len(columnsexist) == 0 else '[' + str(columnsexist)[1:-1].replace('\'', '').replace(', ', '],[') + ']'
    topused = '' if top == '' else 'top ' + str(top) + ' '
    # yearmonthlist takes priority
    yearmonthused = ' where YearMonth = \'' + YearMonth + '\'' if YearMonth != '' and len(YearMonthList)==0 else ''
    yearmonthsused = ' where YearMonth in (' + str(YearMonthList)[1:-1] + ')' if len(YearMonthList)>0 else ''
    _and = ' and ' if yearmonthused !='' and yearmonthsused!='' else ''
    df = pd.DataFrame()

    if checkiftableexists(serverused, portused, database, table) == 'Yes':
        query = 'select ' + topused + columnsquery + ' from ' + database + '.GPS.[' + table + '] \
                with (nolock)' + yearmonthused + _and + yearmonthsused
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                     database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        # df = pd.read_sql(query, con).drop_duplicates(keep='last').reset_index(drop=True)
        df = pd.read_sql(query, con)
        con.close()
        # Populate month end dates as YearMonthDay
        # if list(df.columns).count('YearMonth')>0:
        #     yearmonthday(df, 'YearMonth')
        df.replace('NULL', np.nan, inplace=True)
        # Update pandas column types based on SQL types
        df, dfcolumns = sqlcol(df, [], '', table, False)
    return df
def checkiftableexists(server, port, database, table):
    server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    port = ':58508' if port == '' else ':' + port.strip(':')
    database = 'Sandbox' if database == '' else database

    query = 'IF EXISTS (SELECT \'X\' from ' + database + '.sys.objects with (nolock) where name = \'' + table \
            + '\' and type = \'U\') SELECT \'Yes\' ELSE SELECT \'No\''
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + port + '/' + \
                                          database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    df = pd.read_sql(query, con)
    con.close()
    return df.iloc[0, 0]
def colsexist(server, port, database, table, columns):
    server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    port = ':58508' if port == '' else ':' + port.strip(':')
    database = 'Sandbox' if database == '' else database
    columnsquery = str(columns)[1:-1]

    query = 'SELECT name from syscolumns with (nolock) where object_name(id) = \'' + table + \
            '\' and name in (' + columnsquery + ')'
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + port + '/' + \
                  database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    df = pd.read_sql(query, con)
    con.close()

    return df['name'].to_list()
def colslike(server, port, database, table, columnslikeor, columnslikeand, columnsnotlike):
    server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    port = ':58508' if port == '' else ':' + port.strip(':')
    database = 'Sandbox' if database == '' else database
    columnslikeorquery = ''
    if columnslikeor!=['']:
        for i in range(len(columnslikeor)):
            andor = '' if i==0 else ' or'
            # columnslikeorquery = columnslikeorquery + andor + ' name like \'%' + columnslike[i] + '%\''
            columnslikeorquery = columnslikeorquery + andor + ' name like \'' + columnslikeor[i] + '\''
        columnslikeorquery = ' and (' + columnslikeorquery + ')'
    columnslikeandquery = ''
    if columnslikeand != ['']:
        for i in range(len(columnslikeand)):
            andor = '' if i == 0 else ' and'
            # columnslikeandquery = columnslikeandquery + andor + ' name like \'%' + columnslike[i] + '%\''
            columnslikeandquery = columnslikeandquery + andor + ' name like \'' + columnslikeand[i] + '\''
        columnslikeandquery = ' and (' + columnslikeandquery + ')'
    columnsnotlikequery = ''
    if columnsnotlike!=['']:
        for i in range(len(columnsnotlike)):
            andor = '' if i == 0 else ' and'
            # columnsnotlikequery = columnsnotlikequery + andor + ' name not like \'%' + columnsnotlike[i] + '%\''
            columnsnotlikequery = columnsnotlikequery + andor + ' name not like \'' + columnsnotlike[i] + '\''
        columnsnotlikequery = ' and (' + columnsnotlikequery + ')'

    query = 'SELECT name from syscolumns where object_name(id) = \'' + table + '\'' + columnslikeorquery + columnslikeandquery + columnsnotlikequery
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + port + '/' + \
                                          database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    df = pd.read_sql(query, con)
    con.close()

    return df['name'].to_list()
def sqlgrouporder(server, port, database, table, grouplist, orderlist, columns, columntype, identifier, idvalue, operationList):
    """
    operationList: ['Avg', 'Count']
    columntype: float
    """
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database
    df = pd.DataFrame()
    if len(columns) > 0:
        groupcolumnsquery = str([x + ' as ' + x for x in grouplist])[1:-1].replace('\'', '')
        # columntypequeryprefix = '(Try_convert(float, [' if columntype=='float' else '('
        # columntypequerysuffix = '])), 6)' if columntype == 'float' else ')'
        # roundquery = 'round(' if columntype == 'float' else ''
        # columnsquery = str([roundquery + operationList[columns.index(x)] + \
        #     columntypequeryprefix + x + columntypequerysuffix + \
        #     ' as [' + x + '_' + operationList[columns.index(x)] + ']' \
        #     for x in columns])[1:-1].replace('\'', '')

        columnsquery = str([('round(' if columntype[columns.index(x)] == 'float' else '') + \
                            operationList[columns.index(x)] + \
                            ('(Try_convert(float, [' if columntype[columns.index(x)] =='float' else '(') + x \
                            + ('])), 6)' if columntype[columns.index(x)] == 'float' else ')') + \
                            ' as [' + x + '_' + operationList[columns.index(x)] + ']' \
                            for x in columns])[1:-1].replace('\'', '')

        identifierquery = ' where ' + identifier + ' = \'' + idvalue + '\'' if identifier!='' else ''

        query = 'Select ' + groupcolumnsquery + ', ' + columnsquery + \
        ' from GPS.[' + table + ']' + identifierquery + \
        ' group by ' + str(grouplist)[1:-1].replace('\'', '') + \
        ' order by ' + str(orderlist)[1:-1].replace('\'', '') + ' desc'
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                      database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        df = pd.read_sql(query, con)
        con.close()
        # Populate month end dates as YearMonthDay
        if list(df.columns).count('YearMonth')>0:
            yearmonthday(df, 'YearMonth')
    return df
def sqldistinct(server, port, database, table, column):
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database
    df = pd.DataFrame()

    if checkiftableexists(serverused, portused, database, table)=='Yes':
        query = 'Select distinct [' + column + '] from GPS.[' + table + '] with (nolock)'
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                                              database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        df = pd.read_sql(query, con)
        con.close()
    return df
def SQLinsert(df, table):
    dbo = 'GPS'
    sql_texts = []
    columns = [x for x in list(df.columns) if x not in list(df._get_numeric_data().columns)]
    if len(columns)>0:
        df[columns] = df[columns].replace({'\'': '""'}, regex=True)
    for index, row in df.iterrows():
        sql_texts.append('INSERT INTO ' + table + ' (' + \
                         '[' + str(list(df.columns))[1:-1].replace('\'', '').replace(', ', '],[') + ']' \
                         # + ') VALUES ' + str(tuple(row.values)).replace("\\'",""))
                         # str(df.values.flatten().tolist())[1:-1].replace('\'', '"')
                        + ') VALUES (' + str(row.values.flatten().tolist())[1:-1].replace('\'', '"') + ')')
    insertquery = ' '.join(sql_texts)
    # insertquery = insertquery.replace("\"", "'").replace('\'NULL\'', 'NULL')
    insertquery = insertquery.replace("(\"", "('").replace(", \"", ", '").replace("\",", "',").replace("\")", "')").replace('\'NULL\'', 'NULL')
    # insertquery = insertquery.replace(table, '\'' + table + '\'')
    return insertquery
def savetosql(df, table, action, cpu, log, YearMonth):
    dbo = 'GPS'
    if df.shape[1]>0:
        # Remove duplicate columns. Throw warning later
        df = df.loc[:, ~df.columns.duplicated()]
        # Remove infinite numbers. Throw warning later
        num = df._get_numeric_data()
        num[abs(num) > 999999999] = 999999999.0
        # Truncate strings up to 200 characters. Throw warning later
        df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].astype(str).apply(lambda x: x.str[:200])
        # Add/Update time column
        df['SQLtime'] = datetime.now().strftime("%Y%m%d_%H%M%S")
        # Check if we are saving additional columns
        columnsexist = colsexist('', '', '', table, df.columns.to_list())
        # Replace if all columns are None to nan, so that those columns become numeric
        df.replace('None', np.nan, inplace=True)
        df.replace([None], np.nan, inplace=True)

        if len(columnsexist) < len(df.columns.to_list()) and len(columnsexist)>0:
            newcols = [item for item in df.columns.to_list() if item not in columnsexist]
            # Load, expand, merge
            temp = sqlgetbyyearmonth('', '', '', table, '', '', '', [], 'YearMonth', '', '')
            temp = pd.concat([temp, pd.DataFrame(columns=newcols)])
            df = temp.append(df)
            del [temp]
            action = 'replace'
        df, dfcolumns = sqlcol(df, [], '', table, False)

        createquery = pd.io.sql.get_schema(df.reset_index(), dbo + '.' + '[' + table + ']'). \
                          replace('"' + dbo + '.' + '[' + table + ']' + '"', dbo + '.' + '[' + table + ']'). \
                          replace('"index" INTEGER,\n  ', '').replace('TEXT', 'VARCHAR (250)') + ' \n '
        # Now that we created the table with numeric columns if such columns are full of None, we change np.nan to NULL
        # so that SQL can accept such value (it doesn't accept nan)

        # df = df.where(pd.notnull(df), 'NULL')

        # insertquery =   'IF OBJECT_ID(N\'' + dbo + '.' + '[' + table + ']' + '\', N\'U\') IS NOT NULL \n BEGIN \n ' \
        #                 'BEGIN TRAN QueryStart \n \
        #                 SELECT top 1 * FROM ' + dbo + '.' + '[' + table + ']' + ' WITH (TABLOCKX) \n ' + \
        #                 SQLinsert(df, dbo + '.' + '[' + table + ']') + ' \n ' + \
        #                 'COMMIT TRAN QueryStart \n END'
        # insertquery = SQLinsert(df, dbo + '.' + '[' + table + ']')
        # insertquery = SQLinsert(df.iloc[:20,:], dbo + '.' + '[' + table + ']')
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + 'EPGLBSQLUT08\ANALYTICS:58508' + '/' + 'Sandbox' \
                                              + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        # Create table if it doesn't already exist
        query = 'BEGIN TRY \n ' + createquery + ' END TRY \n BEGIN CATCH \n \
                SELECT top 1 * FROM ' + dbo + '.' + '[' + table + ']' + ' WITH (NOLOCK) \n END CATCH'
        trans = con.begin()
        con.execute(query)
        trans.commit()
        # Update time column for up to date timing
        df['SQLtime'] = datetime.now().strftime("%Y%m%d_%H%M%S")
        i=0
        step=500
        maxtries = 10
        # Only enable the below if the manual insert query is used below
        # df = df.where(pd.notnull(df), 'NULL')

        # pd.DataFrame.from_dict(dfcolumns, orient='index').to_clipboard()
        while i<df.shape[0]:
            # insertquery = SQLinsert(df.iloc[i:min(i + step, df.shape[0]), :], dbo + '.' + '[' + table + ']')w
            success = False
            tries = 0
            while success == False and tries < maxtries:
                success = True
                tries = tries + 1
                try:
                    # con.execute(insertquery)
                    df.iloc[i:min(i + step, df.shape[0]), :].to_sql(table, schema='GPS', con=con, if_exists='append', dtype=dfcolumns, index=False)
                except:
                    pd.DataFrame([YearMonth, i, step]).T.to_sql('ErrorSQL', schema='GPS', con=con, if_exists='append', index=False)
                    log.exception('')
                    success = False
                    sleep(10)
            i += step
        con.close()
        #

# # SQL insert test cases
# # Insert all nan column
# dropsql(Output_table)
# dropsql(Output_table_unique)
# df=Fundamentals.loc[62:62,['Security_Name','YearMonth']]
# df.iloc[:,1]=np.nan
# savetosql(df, Output_table, '', '')
# df = sqlgetbyyearmonth('', '', '', Output_table, '', '', 5000,[], 'YearMonth', '', '')
# print(df)
# print(df.dtypes)
# print(df.iloc[:, 1].std())
#
# # Insert numeric values to all nan column
# df=Fundamentals.loc[62:62,['Security_Name','YearMonth']]
# df.iloc[:, 1] = 3.0
# savetosql(df, table, '', '')
# df = sqlgetbyyearmonth('', '', '', table, '', '', 5000,[], 'YearMonth', '', '')
# print(df)
# print(df.dtypes)
# print(df.iloc[:, 1].std())

def savetosqlorig(df, table, action, cpu):
    if df.shape[1]>0:
        # Remove duplicate columns. Throw warning later
        df = df.loc[:, ~df.columns.duplicated()]
        # Remove infinite numbers. Throw warning later
        num = df._get_numeric_data()
        num[abs(num) > 999999999] = 999999999.0
        # Truncate strings up to 100 characters. Throw warning later
        df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].astype(str).apply(lambda x: x.str[:100])
        # Add/Update time column
        df['SQLtime'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        # Check if we are saving additional columns
        columnsexist = colsexist('', '', '', table, df.columns.to_list())
        # Replace if all columns are None to nan, so that those columns become numeric
        # df.replace('None', np.nan, inplace=True)
        df.replace([None], np.nan, inplace=True)
        # pd.DataFrame(df.dtypes).to_clipboard()
        # df.iloc[:1, ].transpose().to_clipboard()

        if len(columnsexist) < len(df.columns.to_list()) and len(columnsexist)>0:
            newcols = [item for item in df.columns.to_list() if item not in columnsexist]
            # Load, expand, merge
            temp = sqlgetbyyearmonth('', '', '', table, '', '', '', [], 'YearMonth', '', '')
            temp = pd.concat([temp, pd.DataFrame(columns=newcols)])
            df = temp.append(df)
            del [temp]
            action = 'replace'
        df, dfcolumns = sqlcol(df, [], '', table, False)
        # pd.DataFrame(dfcolumns.items()).to_clipboard()
        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + 'EPGLBSQLUT08\ANALYTICS:58508' + '/' + 'Sandbox' \
                                              + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        # We may have that the 2nd CPU is much faster than the first.
        # But it may still be slow. We need a start time to be written out
        # and then compare latest table write time to main start time to find out whether the table is new or not
        # For now, we assume that THE TABLE TO BE CREATED HAS ALREADY BEEN DROPPED prior or at main start time
        # UNLESS WE WANT TO PRESERVE, i.e. action is not 'replace'. Note that the main function action no longer needs to change
        # as it is now being cleverly handled below
        k = 0
        if checkiftableexists('', '', '', table)=='No' and action=='replace' and cpu==1:
            df.iloc[[0],:].to_sql(table, schema='GPS', con=con, if_exists='replace', dtype=dfcolumns, index=False)
            k=1
        if df.shape[0]>k:
            # Update time column for up to date timing
            df['SQLtime'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            df.iloc[k:,:].to_sql(table, schema='GPS', con=con, if_exists='append', dtype=dfcolumns, index=False, chunksize=1000)
        con.close()
def CreateLogTable(server, port, database, table):
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database

    if checkiftableexists(serverused, portused, database, table) == 'Yes':
        dropsql(table)
    query = 'CREATE TABLE GPS.[' + table + ']( \
            [id] [bigint] IDENTITY(1,1) NOT NULL, \
            [log_level] [int] NULL, \
            [log_levelname] [char](32) NULL, \
            [msg] [char](2048) NOT NULL, \
            [trace] [varchar](8000) NOT NULL, \
            [created_at] [datetime2](7) NOT NULL, \
            [created_by] [char](32) NOT NULL, \
            ) ON [PRIMARY]'
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                  database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    con.execute(query)
    con.close()
def SqlConnection(server, port, database):
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database
    engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    connection = engine.raw_connection()
    cursor = connection.cursor()

    return connection, cursor
def DailyEquityPrices(server, database, Identifier, minDate, maxDate, table):

    df = pd.DataFrame()
    if Identifier is not np.nan:

        server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
        database = 'Sandbox' if database == '' else database
        identifierNo = str(7) if table=='Corporate' else str(6)

        query = 'SELECT AsOfDate = fbp.p_date, SEDOL = ss.sedol, Price = fbp.p_price, PriceUSD = fbp.p_price * \
                ISNULL(fru.exch_rate_usd,1), SplitFactor = fbs.p_split_factor, SUM(fbd.p_divs_pd) AS DPS, \
                SUM(fbd.p_divs_pd * ISNULL(fru.exch_rate_usd,1)) AS DPSUSD, IsSpecialDiv = fbd.p_divs_s_pd,\
                Currency = fbp.currency, FXRate = ISNULL(fru.exch_rate_usd,1) \
                FROM fp_v2.fp_basic_prices fbp WITH(NOLOCK) \
                INNER JOIN sym_v1.sym_sedol ss WITH(NOLOCK) ON fbp.fsym_id = ss.fsym_id \
                LEFT JOIN ref_v2.fx_rates_usd fru WITH(NOLOCK) ON fbp.p_date = fru.date AND fbp.currency = \
                fru.iso_currency LEFT JOIN fp_v2.fp_basic_splits fbs WITH(NOLOCK) ON fbp.p_date = \
                fbs.p_split_date AND fbp.fsym_id = fbs.fsym_id LEFT JOIN fp_v2.fp_basic_dividends fbd \
                WITH(NOLOCK) ON fbp.p_date = fbd.p_divs_exdate AND fbp.fsym_id = fbd.fsym_id \
                WHERE LEFT(SEDOL,' + identifierNo + ') = \'' + Identifier + '\'' + \
                'AND fbp.p_date >= \'' + minDate + '\' AND fbp.p_date <= \'' + maxDate + \
                '\' GROUP BY fbp.p_date, ss.sedol, fbp.p_price, fbp.p_price * ISNULL(fru.exch_rate_usd,1), \
                fbs.p_split_factor, fbd.p_divs_s_pd, fbp.currency, ISNULL(fru.exch_rate_usd,1) \
                ORDER BY AsOfDate'

        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + '/' + \
                      database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        df = pd.read_sql(query, con)
        con.close()

    return df
def fundamentalsHistory(server, database, port, identifier, id, minDate, maxDate, table, EqSymbol):
    server = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    database = 'Sandbox' if database == '' else database
    port = ':58508' if port == '' else ':' + port.strip()
    EqSymbolquery = ' AND [EqSymbol] = \'' + EqSymbol + '\'' if EqSymbol !='' else ''

    query = 'select * from Sandbox.GPS.' + table + ' with (nolock) where \
            CONVERT(date, Date) >= \'' + minDate + '\' AND CONVERT(date, Date) ' \
           '<= \'' + maxDate + '\'' + ' AND [' + identifier + '] = \'' + id + '\'' + EqSymbolquery
    sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + server + port + '/' + \
                database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
    con = sql_engine.connect()
    df = pd.read_sql(query, con)
    con.close()

    return df
def SQLupdater(tabledict, namelist,YearMonth,filepath,action,cpu,Date):
    # For each file
    for name in namelist:
        Datacheck = sqlgetbyyearmonth('', '', '', tabledict.get(name), YearMonth, '', '', [], 'YearMonth', '', '')
        if Datacheck.shape[0] == 0:
            filename = name + YearMonth
            file = glob.glob(filepath + filename + '*.csv')
            if len(file) == 1:
                Data = pd.read_csv(file[0])
                if Data.shape[0] > 0:
                    if name == 'Equity':
                        Data = Data.drop_duplicates(['Symbol.1'], keep='last')
                    Data['Date'] = Date
                    yearmonth(Data, 'Date')
                    savetosql(Data, tabledict.get(name), action, cpu)
def SQLDDupdater(DDdatatable,YearMonth,namelist,Fundamentals,Fundamentals_Eq_unique,Fundamentals_table, \
                override,DDoverride,cpu,Parent,ResultLevel,Identifier,Output_table):
    # Update existing DDdatatable+'factors', DDdatatable+'plus' tables based on DDdatatable values
    # If DDoverride==True, remove existing DDdatatable values from such tables
    # Create enhanced Fundamentals table with ML and DD results

    df0 = sqlgetbyyearmonth('', '', '', DDdatatable, YearMonth, '', '', [], 'YearMonth', '', '')
    if df0.shape[0]>0:
        # df0['Issuer OAStfit'] = df0['Issuer OASt']
        # For each file
        for name in namelist:
            dforig = sqlgetbyyearmonth('', '', '', name, YearMonth, '', '', [], 'YearMonth', '', '')
            if dforig.shape[0] > 0:
                df = pd.merge(dforig, Fundamentals_Eq_unique[['Issuer_ID', 'SEDOL']], left_on='Issuer_ID', right_on='Issuer_ID', how='left')
                if name[-3:]=='all':
                    if df[['Model']].applymap(lambda x: x is None).sum()[0]==0 or DDoverride==True:
                        df = df[df['Model'].notnull()]
                        temp =  df[df['Model']==df['Model'].unique()[-1]]
                        temp['Model'] = 'None'
                        A = df0[[e for e in list(temp.columns.intersection(df0.columns)) if e not in ['Date', 'ISIN', 'YearMonth']]]
                        B = pd.merge(temp, A, left_on='SEDOL', right_on='SEDOL', how='left')
                        B = B.loc[:, ~B.columns.str.contains('|'.join(['SEDOL','_x']))]
                        B.columns = [s.replace('_y', '') for s in B.columns]
                        savetosql(B, name, 'append', cpu)
                else:
                    if dforig['Issuer OAStfit'].isnull().sum()==dforig['Issuer OAStfit'].shape[0] or DDoverride==True:
                        A = df0[[e for e in list(df.columns.intersection(df0.columns)) if e not in ['Date', 'ISIN', 'YearMonth']]]
                        B = pd.merge(df.drop(['Issuer OAStfit'],axis=1), A, left_on='SEDOL', right_on='SEDOL', how='left')
                        B = B.loc[:, ~B.columns.str.contains('|'.join(['SEDOL', '_x']))]
                        B.columns = [s.replace('_y', '') for s in B.columns]
                        deletefromsql(YearMonth, name)
                        savetosql(B, name, 'append', cpu)

                        collist = Fundamentals.columns.difference(B.columns).to_list()
                        if Fundamentals_table[:9] != 'Corporate':
                            collist = [e for e in collist if e not in ('YearMonth', 'SEDOL Parent')] + ['YearMonth', 'SEDOL Parent']
                            Fundamentals = pd.merge(Fundamentals[collist], Datacheck0.drop_duplicates(['SEDOL'], keep='last'),
                                                    left_on=['YearMonth', 'SEDOL Parent'], right_on=['YearMonth', 'SEDOL'], how='left')
                        else:
                            # Typically CRV results will be by bond, even if issuer has been used as the training set will still
                            # contain multiple bonds
                            key = Parent if ResultLevel == 'Parent' else Identifier
                            collist = [e for e in collist if e not in ('YearMonth', key)] + ['YearMonth', key]
                            Fundamentals = pd.merge(Fundamentals[collist], B.drop_duplicates([key], keep='last'),
                                            left_on=['YearMonth', key], right_on=['YearMonth', key], how='left')
                        if override == True or Datacheck.shape[0] > 999999:
                            deletefromsql(YearMonth, Output_table)
                        savetosql(Fundamentals, Output_table, 'replace', cpu)
def IndexFilter(IndexValue, Index_table, YearMonth, df):
    # Filter based on all IndexValues
    if len(IndexValue) > 0:
        Indexdf = sqlgetbyyearmonth('', '', '', Index_table, YearMonth, '', '', [], 'YearMonth', '', '')
        Index = pd.DataFrame()
        for indexvalue in IndexValue:
            Index = pd.concat([Index, Indexdf[Indexdf[indexvalue] == 1]], axis=0).drop_duplicates().reset_index(drop=True)
        df = pd.merge(df, Index[['CUSIP']], left_on='CUSIP', right_on='CUSIP', how='inner')

    return df
def SqlDuplicateDelete(server, port, database, table, columnlist):

    # Delete duplicates by YearMonth and column
    # https://www.sqlservertutorial.net/sql-server-basics/delete-duplicates-sql-server/
    dbo = 'GPS'
    serverused = 'EPGLBSQLUT08\ANALYTICS' if server == '' else server
    portused = ':58508' if port == '' else ':' + port.strip()
    database = 'Sandbox' if database == '' else database

    if checkiftableexists(serverused, portused, database, table)=='Yes':
        Alist = [s + ',' for s in columnlist]
        columns = str(Alist)[1:-1].replace(",'", "").replace("'", "")
        # query = 'WITH cte AS (SELECT YearMonth, ISIN, ROW_NUMBER() OVER( \
        #         PARTITION BY CONCAT(' + \
        #         'YearMonth' + ', \'_\', ' + column + ') \
        #         ORDER BY CONCAT(YearMonth' + ', \'' + '_' + '\', ' + column + ')) row_num \
        #         FROM ' + dbo + '.' + table + '  with (nolock)) \
        #         DELETE FROM cte WHERE row_num > 1'

        query = 'WITH cte AS (SELECT YearMonth, ISIN, ROW_NUMBER() OVER( \
                        PARTITION BY CONCAT(' + columns + ') \
                        ORDER BY CONCAT(' + columns + ')) row_num \
                        FROM ' + dbo + '.' + table + '  with (nolock)) \
                        DELETE FROM cte WHERE row_num > 1'

        sql_engine = sqlalchemy.create_engine('mssql+pyodbc://@' + serverused + portused + '/' + \
                      database + '?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server')
        con = sql_engine.connect()
        trans = con.begin()
        con.execute(query)
        trans.commit()
        con.close()

##############
### Charts ###
##############

def datatochart(df, Description, Date, analytics2, analytics3, Factors):
    datecount = 0
    for i in range(len(df[Description].unique().tolist())):
        for p in range(len(list(analytics2))):
            analytic = analytics2[p]
            analyticcols = [col for col in df.columns if col[:len(analytic)] == analytic \
                            and len(col) > len(analytic) and col[len(analytic) + 1:len(analytic) + 2].isdigit() == True]
            Analytic = df[analyticcols].T.reset_index(drop=True).iloc[:, i]
            if datecount == 0:
                Dates = pd.DataFrame({Date: pd.DataFrame(analyticcols).iloc[:, 0].str[-10:]})
                Dates[Date] = pd.to_datetime(Dates[Date], errors='coerce').dt.date
                Data = Dates
                datecount = 1
            Data = pd.concat([Data, Analytic], axis=1)
            cols = Data.columns.tolist()
            cols[-1] = analytic + ' ' + df[Description].unique()[i]
            Data.columns = cols
        for p in range(len(list(analytics3))):
            for kk in range(1, n + 1):
                analytic = analytics3[p] + ' ' + Factors.columns[kk + xshift]
                analyticcols = [col for col in df.columns if col[:len(analytic)] == analytic \
                                and len(col) > len(analytic) and col[
                                                                 len(analytic) + 1:len(analytic) + 2].isdigit() == True]
                Analytic = df[analyticcols].T.reset_index(drop=True).iloc[:, i]
                if datecount == 0:
                    Dates = pd.DataFrame({Date: pd.DataFrame(analyticcols).iloc[:, 0].str[-10:]})
                    Dates[Date] = pd.to_datetime(Dates[Date], errors='coerce').dt.date
                    Data = Dates
                    datecount = 1
                Data = pd.concat([Data, Analytic], axis=1)
                cols = Data.columns.tolist()
                cols[-1] = analytic + ' ' + df[Description].unique()[i]
                Data.columns = cols
    Data.iloc[:, 1:] = Data.iloc[:, 1:] * 100

    return Data
def display_simulated_ef_with_random(mean_returns, cov_matrix, num_portfolios, risk_free_rate, periods):
    results, weights = random_portfolios(num_portfolios, mean_returns, cov_matrix, risk_free_rate, periods)

    max_sharpe_idx = np.argmax(results[2])
    sdp, rp = results[0, max_sharpe_idx], results[1, max_sharpe_idx]
    max_sharpe_allocation = pd.DataFrame(weights[max_sharpe_idx], index=cov_matrix.columns, columns=['allocation'])
    max_sharpe_allocation.allocation = [round(i * 100, 2) for i in max_sharpe_allocation.allocation]
    max_sharpe_allocation = max_sharpe_allocation.T

    min_vol_idx = np.argmin(results[0])
    sdp_min, rp_min = results[0, min_vol_idx], results[1, min_vol_idx]
    min_vol_allocation = pd.DataFrame(weights[min_vol_idx], index=cov_matrix.columns, columns=['allocation'])
    min_vol_allocation.allocation = [round(i * 100, 2) for i in min_vol_allocation.allocation]
    min_vol_allocation = min_vol_allocation.T

    plt.figure(figsize=(10, 7))
    plt.scatter(results[0, :], results[1, :], c=results[2, :], cmap='YlGnBu', marker='o', s=10, alpha=0.3)
    plt.colorbar()
    plt.scatter(sdp, rp, marker='*', color='r', s=50, label='Maximum Sharpe ratio')
    plt.scatter(sdp_min, rp_min, marker='*', color='g', s=50, label='Minimum volatility')
    plt.title('Simulated Portfolio Optimization based on Efficient Frontier')
    plt.xlabel('Annualized volatility')
    plt.ylabel('Annualized returns')
    plt.legend(labelspacing=0.8)
def display_calculated_ef_with_random(Parameters, file, filename, mean_returns, cov_matrix, num_portfolios, risk_free_rate, periods,
                                      minreturn, maxreturn, prefix, h):
    results, _ = random_portfolios(num_portfolios, mean_returns, cov_matrix, risk_free_rate, periods)

    max_sharpe = max_sharpe_ratio(mean_returns, cov_matrix, risk_free_rate, periods)
    sdp, rp = portfolio_Annualized_performance(max_sharpe['x'], mean_returns, cov_matrix, periods)
    max_sharpe_allocation = pd.DataFrame(max_sharpe.x, index=cov_matrix.columns, columns=['allocation'])
    max_sharpe_allocation.allocation = [round(i * 100, 2) for i in max_sharpe_allocation.allocation]
    max_sharpe_allocation = max_sharpe_allocation.T

    min_vol = min_variance(mean_returns, cov_matrix, periods)
    sdp_min, rp_min = portfolio_Annualized_performance(min_vol['x'], mean_returns, cov_matrix, periods)
    min_vol_allocation = pd.DataFrame(min_vol.x, index=cov_matrix.columns, columns=['allocation'])
    min_vol_allocation.allocation = [round(i * 100, 2) for i in min_vol_allocation.allocation]
    min_vol_allocation = min_vol_allocation.T

    divider = 1
    if Parameters.get('WeightAvg', 1) == 2:
        divider = h
    if Parameters.get('WeightAvg', 1) == 3:
        divider = np.sqrt(h)
    rp = rp * divider / h
    rp_min = rp_min * divider / h

    plt.figure(figsize=(10, 7))
    plt.scatter(results[0, :], results[1, :], c=results[2, :], cmap='YlGnBu', marker='o', s=10, alpha=0.3)
    plt.colorbar()
    plt.scatter(sdp, rp, marker='*', color='r', s=50, label='Maximum Sharpe ratio')
    plt.scatter(sdp_min, rp_min, marker='*', color='g', s=50, label='Minimum volatility')

    # =============================================================================
    #     target = np.linspace(min(rp_min,rp), maxreturn, 50)
    # =============================================================================
    target = np.linspace(minreturn, maxreturn, 50)
    efficient_portfolios = efficient_frontier(mean_returns, cov_matrix, target * h / divider, periods)
    plt.plot([p['fun'] * divider / h for p in efficient_portfolios], target, linestyle='-.', color='black',
             label='efficient frontier')
    plt.suptitle('Simulated Efficient Frontier')
    ax.set_title(filename + prefix + hh)
    plt.xlabel('Annualized volatility')
    plt.ylabel('Annualized returns')
    plt.legend(labelspacing=0.8)

    plt.savefig(file + ' simulated_ef.png', bbox_inches='tight')
    plt.close()
def display_ef_allocations(Parameters, file, filename, mean_returns, cov_matrix, risk_free_rate, periods, minreturn, maxreturn,
                           prefix, h, dochart):
    # =============================================================================
    #     max_sharpe = max_sharpe_ratio(mean_returns, cov_matrix, risk_free_rate, periods)
    #     sdp, rp = portfolio_Annualized_performance(max_sharpe['x'], mean_returns, cov_matrix, periods)
    # =============================================================================

    min_vol = min_variance(mean_returns, cov_matrix, periods)
    sdp_min, rp_min = portfolio_Annualized_performance(min_vol['x'], mean_returns, cov_matrix, periods)

    target = np.linspace(rp_min, maxreturn, 50)
    # =============================================================================
    #     target = np.linspace(minreturn, maxreturn, 50)
    # =============================================================================
    # =============================================================================
    #     #Normalize returns to correspond to h1
    #     divider=1
    #     if Parameters.get('WeightAvg',1)==2:
    #         divider=h
    #     if Parameters.get('WeightAvg',1)==3:
    #         divider=np.sqrt(h)
    #     efficient_portfolios = efficient_frontier(mean_returns, cov_matrix, target*h/divider, periods)
    # =============================================================================
    efficient_portfolios = efficient_frontier(mean_returns, cov_matrix, target, periods)

    list_arrays = [p['x'] for p in efficient_portfolios]
    Allocations = pd.DataFrame(list_arrays, index=target, columns=mean_returns.index.tolist()) * 100
    Allocations = np.round(Allocations, 3)
    Allocations = Allocations.loc[:, (Allocations != 0).any(axis=0)]

    if dochart == True:
        hh = ' h' + str(h) if (prefix not in (' d1', ' d2')) or (prefix in (' d1', ' d2') and h > 1) else ''
        ax = Allocations.plot(kind='area', figsize=(10, 7))
        plt.suptitle('Efficient Frontier Allocations')
        ax.set_title(filename + prefix + hh)
        ax.set_xlabel('return')
        ax.set_ylabel('weight')
        plt.savefig(file + ' ef_allocations ' + prefix + hh + '.png', bbox_inches='tight')
        plt.close()

    return Allocations
def display_ef_with_selected(Parameters, file, filename, mean_returns, cov_matrix, risk_free_rate, periods, minreturn, maxreturn,
                             prefix, h):
    max_sharpe = max_sharpe_ratio(mean_returns, cov_matrix, risk_free_rate, periods)
    sdp, rp = portfolio_Annualized_performance(max_sharpe['x'], mean_returns, cov_matrix, periods)
    max_sharpe_allocation = pd.DataFrame(max_sharpe.x, index=cov_matrix.columns, columns=['allocation'])
    max_sharpe_allocation.allocation = [round(i * 100, 2) for i in max_sharpe_allocation.allocation]
    max_sharpe_allocation = max_sharpe_allocation.T

    min_vol = min_variance(mean_returns, cov_matrix, periods)
    sdp_min, rp_min = portfolio_Annualized_performance(min_vol['x'], mean_returns, cov_matrix, periods)
    min_vol_allocation = pd.DataFrame(min_vol.x, index=cov_matrix.columns, columns=['allocation'])
    min_vol_allocation.allocation = [round(i * 100, 2) for i in min_vol_allocation.allocation]
    min_vol_allocation = min_vol_allocation.T

    # =============================================================================
    #     divider=1
    #     if Parameters.get('WeightAvg',1)==2:
    #         divider=h
    #     if Parameters.get('WeightAvg',1)==3:
    #         divider=np.sqrt(h)
    # =============================================================================

    an_vol = pd.Series(np.sqrt(np.diagonal(cov_matrix)) * np.sqrt(periods), index=mean_returns.index)
    an_rt = mean_returns * periods
    # Normalize returns to correspond to h1
    # =============================================================================
    #     an_rt=an_rt*divider/h
    #     rp=rp*divider/h
    #     rp_min=rp_min*divider/h
    # =============================================================================

    fig, ax = plt.subplots(figsize=(10, 7))
    ax.scatter(an_vol, an_rt, marker='o', s=50)

    for i, txt in enumerate(cov_matrix.columns):
        ax.annotate(txt, (an_vol[i], an_rt[i]), xytext=(10, 0), textcoords='offset points')
    ax.scatter(sdp, rp, marker='*', color='r', s=50, label='Maximum Sharpe ratio')
    ax.scatter(sdp_min, rp_min, marker='*', color='g', s=50, label='Minimum volatility')

    target = np.linspace(rp_min, maxreturn, 50)
    # =============================================================================
    #     target = np.linspace(minreturn, maxreturn, 50)
    # =============================================================================
    hh = ' h' + str(h) if (prefix not in (' d1', ' d2')) or (prefix in (' d1', ' d2') and h > 1) else ''

    # =============================================================================
    #     efficient_portfolios = efficient_frontier(mean_returns, cov_matrix, target*h/divider, periods)
    # =============================================================================
    efficient_portfolios = efficient_frontier(mean_returns, cov_matrix, target, periods)
    # =============================================================================
    #     ax.plot([p['fun']*divider/h for p in efficient_portfolios], target, linestyle='-.', color='black', label='efficient frontier')
    # =============================================================================
    ax.plot([p['fun'] for p in efficient_portfolios], target, linestyle='-.', color='black', label='efficient frontier')
    plt.suptitle('Portfolio Optimization with Asset Classses')
    ax.set_title(filename + prefix + hh)
    ax.set_xlabel('Annualized volatility')
    ax.set_ylabel('Annualized returns')
    ax.legend(labelspacing=0.8)

    plt.savefig(file + prefix + hh + ' ef_with_selected.png', bbox_inches='tight')
    plt.close()
def charter(Analytics, Analytic, filename, titlename, filename2, majorlist, minorlist, precision, Results):
    fontsmall = 7
    fontlarge = 10
    figx = 18
    figy = 10
    fig, ax = plt.subplots()
    Temp = Analytics.loc[:, Analytics.columns.str.contains('|'.join(Analytic))].dropna() * 12
    Temp = pd.merge(Temp, Results, left_index=True, right_index=True)
    Temp.plot(figsize=(figx, 10), \
              kind='bar', fontsize=fontlarge, grid=True, ax=ax, title=filename + ' ' + titlename).legend(prop={'size': 6})
    ax.set_yticks(majorlist, minor=False)
    ax.set_yticks(minorlist, minor=True)
    ax.yaxis.grid(True, which='major', linestyle='--', color='r')
    ax.yaxis.grid(True, which='minor', linestyle='--', color='k')
    ax.yaxis.set_minor_formatter(FormatStrFormatter('%.' + str(precision) + 'f'))
    plt.savefig(file + ' ' + filename2 + '.png', bbox_inches='tight')
    plt.close()
def charter2(data_frames, titlename, filesave, majorlist, minorlist, precision):
    chartdf = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True), data_frames)
    fig, ax = plt.subplots()
    chartdf.plot(figsize=(figx, 10), kind='bar', fontsize=fontlarge, grid=True, ax=ax, title=titlename).legend(prop={'size': fontlarge})
    ax.set_yticks(majorlist, minor=False)
    ax.set_yticks(minorlist, minor=True)
    ax.yaxis.grid(True, which='major', linestyle='--', color='r')
    ax.yaxis.grid(True, which='minor', linestyle='--', color='k')
    ax.yaxis.set_minor_formatter(FormatStrFormatter('%.' + str(precision) + 'f'))
    plt.savefig(filepath + '\\' + filesave + '.png', bbox_inches='tight')
    plt.close()
def charter3(Analytics, Analytic, Entity, Entityexclude, filepath, filename):
    # One analytic time series, multiple entities

    fontsmall = 7
    fontlarge = 10
    figx = 30
    figy = 10
    import pylab

    for i in range(len(Analytic)):
        titlename = Analytic[i]
        fig, ax = plt.subplots()
        Temp = Analytics.loc[Analytics['AnalyticQ'] == Analytic[i], Analytics.columns.str.contains('|'.join(['Date'] + Entity))]
        Temp = Temp.loc[:, ~Temp.columns.str.contains('|'.join(Entityexclude))]
        Temp = Temp.set_index('Date', drop=True)
        Temp.plot(figsize=(figx, 10), \
                  kind='bar', fontsize=fontlarge, grid=False, ax=ax, title=filename + ' ' + titlename).legend(prop={'size': 20})
        # pylab.show()
        plt.savefig(filepath + '\\' + filename + ' ' + titlename + '.png', bbox_inches='tight')
        plt.close()
        Temp = (1 + Temp / 100).cumprod() - 1
        fig, ax = plt.subplots()
        Temp.plot(figsize=(figx, 10), \
                  fontsize=fontlarge, grid=False, ax=ax, title=filename + ' ' + titlename).legend(
            prop={'size': 20})
        plt.savefig(filepath + '\\' + filename + ' ' + titlename + ' growth.png', bbox_inches='tight')
        plt.close()
def scattersquare(df,xcolname,fontsz,legendsz,xsize,ysize):
    my_colors = plt.rcParams['axes.prop_cycle']()
    dim = int(np.ceil(np.sqrt((df.shape[1]-1))))
    fig, axes = plt.subplots(sharex=True, sharey=True, nrows=dim, ncols=dim, squeeze=True)
    k = 0
    chartcols = df.columns.to_list()
    chartcols.remove(xcolname)
    for j in chartcols:
        m = int(np.floor(k/dim))
        n = int(k - m*dim)
        df.plot(figsize=(xsize, ysize), x=xcolname, y=j, kind='scatter', ax=axes[m,n], fontsize=fontsz, label=j, **next(my_colors)).legend(prop={'size': legendsz})
        y_axis = axes[m, n].get_yaxis()
        # y_axis.set_visible(False)
        y_label = y_axis.get_label()
        y_label.set_visible(False)
        x_axis = axes[m, n].get_xaxis()
        x_label = x_axis.get_label()
        x_label.set_visible(False)
        k = k + 1
    fig.tight_layout()
    plt.show()
    plt.close()

###############
### Various ###
###############

def Scale(assets):
    scalevec = np.sum(np.multiply(assets, assets) / np.size(assets, 0), 0)

    return np.diag(scalevec.A1)
def QPSolve(Q, Y, n, T, Params):

    # Q: xxT + VtUVt, Y: yx

    # Problem data
    nT = np.size(Q, 1)
    const = Params.get('Constant', 0)
    budget = Params.get('Budget', 0)
    # Lcheck =  Params.get('Lcheck',0)
    Leverage = Params.get('Leverage', 3)
    Lcheck = 0 if Leverage == 0 else 1

    if (Lcheck):
        Params['Constraints'] = 'long'

    Constraints = Params.get('Constraints', 'long')

    S = cvxopt.matrix(Q)
    p = cvxopt.matrix(-Y)

    if (budget):
        G = cvxopt.matrix(0.0, (2 * nT + 2 * T, nT))
        h = cvxopt.matrix(0.0, (2 * nT + 2 * T, 1))
        h[2 * nT:, 0] = 1.0
    else:
        G = cvxopt.matrix(0.0, (2 * nT, nT))
        h = cvxopt.matrix(0.0, (2 * nT, 1))
    for t in range(nT):
        # lower constraint: beta >=0
        G[t, t] = -1.0
        if Constraints == 'no':
            h[t, 0] = 100
        else:
            h[t, 0] = 0
        # upper constraint: beta <= 1
        G[t + nT, t] = 1.0
        if Constraints == 'no':
            h[t + nT, 0] = 100
        else:
            # upper constraint: beta <= 1 + levarage
            h[t + nT, 0] = 1 + Leverage * Lcheck

    for t in range(T):
        if (budget):
            # lower bound for cash  1 - sum(beta) >= - lev => sum(beta) <= 1 + lev
            G[t + 2 * nT, t * n:(t + 1) * n - const] = 1.0
            h[t + 2 * nT, 0] = Leverage * Lcheck + 1
            # upper bound for cash  1 - sum(beta) <= 1 => -sum(beta) <= 0
            G[t + 2 * nT + T, t * n:(t + 1) * n - const] = -1.0
            h[t + 2 * nT + T, 0] = 0

        if (const):
            # unconstraint const
            h[(t + 1) * n - 1, 0] = 100
            h[(t + 1) * n - 1 + nT, 0] = 100
        if (Lcheck and not budget):
            # APT model levarage cash
            h[(t + 1) * n - const - 1, 0] = Lcheck * Leverage
            h[nT + (t + 1) * n - const - 1, 0] = 1

    cvxopt.solvers.options['show_progress'] = False
    # S: xxT + VtUVt, p: -yx, G: Gb + h >=0
    betas = cvxopt.solvers.qp(S, p, G, h)['x']
    Sdf = pd.DataFrame(S)
    return betas
def lambdaConvert(lam):
    return 10.0 ** (10.0 * lam - 5.0)
def LeaveOneOut(data, Params):
    assets = np.asmatrix(data.get('X'))
    fund = data.get('Y')
    fund = np.asmatrix(np.array(list(fund), dtype=float)).transpose()

    nsrParams = Params['nsrParams']
    low_theshold = Params.get('low_theshold', 0)
    bot_thresh = Params.get('bot_thresh', 100000)
    errorfunc = Params.get('errorfunc', square_error)
    mean_func = Params.get('mean_func', stats.nanmean)
    band = Params.get('band', 0)
    nsrfunc = Params.get('nsrfunc', nonstatRegress)

    n = np.size(assets, 1)
    T = np.size(assets, 0)
    outPoint = np.zeros([T, 1])
    arrR2 = np.zeros([T, 1])
    fund_loo = np.zeros([T, 1])
    var = np.ones([T, 1])
    for t in range(band, T - band):
        outPoint = np.zeros([T, 1])
        outPoint[t] = 1
        nsrParams['outPoint'] = outPoint
        # Factors at missing point t
        assets_t = assets[t, :]
        # Betas based on points excluding t
        result_t = nsrfunc(assets, fund, nsrParams)
        beta = result_t.get('beta')
        # Fund estimate based on estimated betas and factors across all points t
        fund_est = result_t.get('fund_est')
        # N/A
        var_t = result_t.get('var_t', np.ones([T, 1]))
        # Betas at missing point t
        beta_t = beta[t, :]
        # import ipdb; ipdb.set_trace()
        # Fund at missing point t
        fund_t = fund[t]
        # Fund estimated at missing point t
        fund_t_est = np.dot(beta_t, assets_t.T)
        # Predicted style at missing point t
        fund_loo[t, 0] = fund_t_est
        var[t, 0] = var_t[t, 0]
        err = errorfunc(fund_t, fund_t_est)

        arrR2[t, 0] = err

    ind = np.logical_or(arrR2 < low_theshold, arrR2 > bot_thresh)

    arrR2[ind] = np.nan
    # PR2
    r2 = mean_func(arrR2)

    return arrR2, r2, fund_loo, var
def expand_grid(data_dict):
    rows = itertools.product(*data_dict.values())
    return pd.DataFrame.from_records(rows, columns=data_dict.keys())
def GetCovarMatrix(T, K):
    Q = np.identity(T)
    for i in range(0, T):
        for k in range(0, K):
            if (i + k < T):
                Q[i, i + k] = (K - k) / (K * K)
            if (i - k > -1):
                Q[i, i - k] = (K - k) / (K * K)

    eigVec, eigVal, eigVecT = np.linalg.svd(Q)
    eigVal_inv = 1 / eigVal
    sqrt_eig_val_inv = np.sqrt(eigVal_inv)
    sqrt_eig_val = np.sqrt(eigVal)
    eigValDiaginv = np.diag(sqrt_eig_val_inv)
    eigValDiag = np.diag(sqrt_eig_val)
    S = np.matmul(eigValDiaginv, eigVecT)
    S_1 = np.matmul(eigVec, eigValDiag)
    result = {'Q': Q, 'S': S, 'S_1': S_1}
    # print(Q)
    return result
def MAWeight(span, Decay):
    if (Decay == 1):
        return 1
    else:
        return pow(Decay, span)
def Lagweights(horizon, Decay, WeightAvg):
    weights = np.ones(horizon)
    if WeightAvg == 2:
        weights = weights / horizon
    if WeightAvg == 3:
        weights = weights / np.sqrt(horizon)
    if Decay != 0:
        wsum = 0
        for j in range(0, horizon):
            jw = MAWeight(j, Decay);
            weights[j] = jw
            wsum = wsum + jw
        # Normalize
        if WeightAvg == 2:
            for j in range(0, horizon):
                weights[j] = weights[j] / wsum
        if WeightAvg == 3:
            for j in range(0, horizon):
                weights[j] = weights[j] / np.sqrt(wsum)

    return weights
def rollup(Data, Parameters, horizon, mode='All'):
    # X = data.get('X')
    # Y = data.get('Y')
    X = Data.iloc[:, list(range(2, Data.shape[1]))].values
    Y = Data.iloc[:, [1]].values
    Dataroll = pd.DataFrame()
    # Convert to log returns
    # X = np.log(1 + X)
    # Y = np.log(1 + Y)

    Tx = np.size(X, 0)
    T = np.size(Y, 0)
    n = X.shape[1]
    Weighting = Parameters.get('Weighting', 0)
    WeightAvg = Parameters.get('WeightAvg', 2)
    HalfLife = Parameters.get('HalfLife', 0)
    Decay = Parameters.get('Decay', 0)
    if Weighting == 1 and HalfLife != 0: Decay = 1 / np.power(2, 1 / HalfLife)
    # schema=1 if HalfLife !=0 else 2

    Xroll = np.zeros([Tx - abs(horizon) + 1, n])
    # if n==1: Xroll = Xroll.T

    if mode == 'All':
        Yroll = np.zeros([T - horizon + 1, 1])
        # for i in range(0,n):
        # Xroll[:,i] = np.convolve(np.ravel(X[:,i]), np.ones((horizon))/horizon, mode='valid')
        for j in range(horizon, Tx + 1):
            Xroll[j - horizon, :] = np.matmul(X[j - horizon:j, :].T, Lagweights(horizon, Decay, WeightAvg))
        for j in range(horizon, T + 1):
            Yroll[j - horizon, :] = np.matmul(Y[j - horizon:j, :].T, Lagweights(horizon, 0, WeightAvg))
        # Yroll = np.convolve(np.ravel(Y), np.ones((horizon))/horizon, mode='valid')
        # Xroll = Xroll[Xroll.shape[0] - Yroll.size:, :]
        # Convert to % returns
        # Xroll = (np.exp(Xroll) - 1)
        # Yroll = (np.exp(Yroll) - 1)
        Xroll1 = pd.concat([Data.iloc[horizon - 1:, [0]].reset_index(drop=True), pd.DataFrame(Xroll)], axis=1).reset_index(drop=True)
        # Dates are off below
        Yroll1 = pd.concat([Data.iloc[Data.shape[0] - Yroll.size:, [0]].reset_index(drop=True), pd.DataFrame(Yroll)], axis=1).reset_index(drop=True)
        Dataroll = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='inner'), [Yroll1, Xroll1])
        Dataroll = Dataroll.dropna().reset_index(drop=True)
        Xroll = Dataroll.iloc[:, list(range(2, Data.shape[1]))].values
        Yroll = Dataroll.iloc[:, [1]].values
    elif mode == 'X':
        for j in range(abs(horizon), Tx + 1):
            Xroll[j - abs(horizon), :] = np.matmul(X[j - abs(horizon):j, :].T, Lagweights(abs(horizon), Decay, WeightAvg))
        # Convert to % returns
        # Xroll = (np.exp(Xroll) - 1)
        if horizon > 0:
            Xroll1 = pd.concat([Data.iloc[horizon - 1:, [0]].reset_index(drop=True), pd.DataFrame(Xroll)], axis=1).reset_index(drop=True)
        else:
            Xroll1 = pd.concat([Data.iloc[:(X.shape[0] - horizon + 1), [0]].reset_index(drop=True), pd.DataFrame(Xroll)], axis=1).reset_index(drop=True)
        Dataroll = pd.merge(Data.iloc[:, [0, 1]], pd.DataFrame(Xroll1), left_on='Date', right_on='Date', how='inner')
        Dataroll = Dataroll.dropna().reset_index(drop=True)
        Xroll = Dataroll.iloc[:, list(range(2, Data.shape[1]))].values
        Yroll = Dataroll.iloc[:, [1]].values
    elif mode == 'Y':
        Yroll = np.zeros([T - horizon + 1])
        Yroll = np.convolve(np.ravel(Y), np.ones((horizon)) / horizon, mode='valid')
        # Convert to % returns
        # Yroll = (np.exp(Yroll) - 1)
        Xroll = X[max(X.shape[0] - Yroll.size, 0):, :]
        # for i in range(0,n):
        # Xroll[:,i] = X[horizon-1:T,i]
    else:
        # Xroll = X[horizon-1:T,:]
        # Xroll=X[X.shape[0]-Yroll.size:,:]
        # Yroll = Y[horizon-1:T]
        Yroll = np.zeros([T])
        Xroll = X[X.shape[0] - Y.size:, :]
        Yroll = Y

    # Yroll = Yroll.reshape([T-horizon+1,1])
    Yroll = Yroll.reshape([Yroll.size, 1])

    Dataroll = pd.concat([Dataroll.iloc[:, [0]], pd.DataFrame(Yroll), pd.DataFrame(Xroll)], axis=1).reset_index(drop=True)
    Dataroll.columns = Data.columns

    data = {'Xroll': Xroll, 'Yroll': Yroll, 'Dataroll': Dataroll}
    return data
def RollXY(Data, Parameters, horizonX=4.0, horizonY=4.0):

    # Y first column, then X
    dataroll = rollup(Data, Parameters, horizonX, 'X')
    Xroll = dataroll.get('Xroll')
    Yroll = dataroll.get('Yroll')
    # data = {'X': Xroll4, 'Y': Yroll4}
    # The dates don't matter below
    # Data = pd.concat([Data.iloc[:Xroll.size,[0]], pd.DataFrame(Xroll), pd.DataFrame(Yroll)], axis=1).reset_index(drop=True)
    Dataroll = dataroll.get('Dataroll')
    if horizonY > 1:
        dataroll = rollup(Dataroll, Parameters, horizonY, 'All')
        Xroll = dataroll.get('Xroll')
        Yroll = dataroll.get('Yroll')
        Dataroll = dataroll.get('Dataroll')

    result = {'Xroll': Xroll, 'Yroll': Yroll, 'Dataroll': Dataroll}
    return result
def hlLeveragedQPSolve(Q, Y, Params):
    # Problem data
    nT = np.size(Q, 1)
    const = Params.get('const', 0)
    S = cvxopt.matrix(Q)
    p = cvxopt.matrix(-Y)
    G = cvxopt.matrix(0.0, (nT, nT))
    G[::nT + 1] = -1.0
    if (const):
        G[nT - 1, nT - 1] = 0.0
    h = cvxopt.matrix(0.0, (nT, 1))
    cvxopt.solvers.options['show_progress'] = False
    # https://cvxopt.org/userguide/coneprog.html
    # S=P=XTX,p=q=-XTY,G=G,h=h
    betas = cvxopt.solvers.qp(S, p, G, h)['x']
    return betas
def ClearCrossValidation(Data, data, Params):
    X = data.get('X')
    Y = data.get('Y')
    # The dates don't matter below
    Data = pd.concat([Data.iloc[:X.size, [0]].reset_index(drop=True), pd.DataFrame(Y), pd.DataFrame(X)], axis=1). \
        reset_index(drop=True)
    Tx = np.size(X)
    n = np.size(X, 1)
    K = Params.get('horizon', 1)
    Ky = Params.get('horizonY', K)
    Kx = Params.get('horizonX', Ky)
    Model = Params.get('Model', 'DSA')
    if Kx > 1:
        firstElem = max(abs(Kx) - 1 - (X.shape[0] - Y.size), 0)
    else:
        firstElem = abs(Kx) - 1

    # nsrParams = Params['nsrParams']
    low_theshold = Params.get('low_theshold', 0)
    bot_thresh = Params.get('bot_thresh', 100000)
    errorfunc = Params.get('errorfunc', square_error)
    # mean_func = Params.get('mean_func', stats.nanmean)
    # band = Params.get('band', 0)
    # nsrfunc = Params.get('nsrfunc', nonstatRegress)

    # Roll X only as LOO will work on the rolled X prior to rolling second time, if applicable
    dataX = {'X': X, 'Y': X}
    dataroll = rollup(Data, Params, Kx, 'X')
    Xlag = dataroll.get('Xroll')

    # Reduce Y if Xlag is smaller
    if Kx > 1:
        Y = Y[firstElem:, :]
    else:
        Y = Y[:Y.size - firstElem, :]
    T = np.size(Y)

    YLOO_est = np.matrix(np.zeros([T - Ky + 1, 1]))
    beta_1_est = np.matrix(np.zeros([T - Ky + 1, n]))
    alpha_1_est = np.matrix(np.zeros([T - Ky + 1, 1]))
    # beta_1_est = 0
    start = Ky - 1
    end = T

    for t in range(start, end):

        X_1 = np.copy(Xlag)
        Y_1 = np.copy(Y)
        X_1_ = np.delete(Xlag[Xlag.shape[0] - Y.size:], t, 0)
        Y_1_ = np.delete(Y_1, [t], 0)
        data = {'X': X_1_, 'Y': Y_1_}
        # The dates don't matter below
        Data = pd.concat([Data.iloc[:X_1_.size, [0]], pd.DataFrame(Y_1_), pd.DataFrame(X_1_)], axis=1).reset_index(drop=True)
        rolldata = RollXY(Data, Params, 1, Ky)
        Xroll = (np.asmatrix(rolldata.get('Xroll')))
        Yroll = (np.asmatrix(rolldata.get('Yroll')))

        if Model != 'DSA':
            dataroll = {'X': Xroll, 'Y': Yroll}
            OLSParams = {'horizonX': Kx, 'Budget': Params.get('Budget', 0), 'Constant': Params.get('Constant', 0),
                         'Leverage': Params.get('Leverage', 0)}
            outPoint = np.zeros([Yroll.size + 1, 1])
            outPoint[t - Ky + 1] = 1
            OLSParams['outPoint'] = outPoint
            result_1 = OLS(dataroll, OLSParams)
            beta_1 = result_1['beta_est']
            alpha_1 = result_1['alpha_est']

        else:
            outPoint = np.zeros([Yroll.size + 1, 1])
            # Before:
            # outPoint[t - Ky + 1] = 1
            outPoint[t - Ky + 1] = 1
            # nsrParams['outPoint'] = outPoint
            Xroll = np.concatenate(
                (Xroll[0:t - Ky + 1, :], np.asmatrix(np.zeros([1, n])), Xroll[min(t - Ky + 1, T):T, :]), axis=0)
            Yroll = np.concatenate(
                (Yroll[0:t - Ky + 1, 0], np.asmatrix(np.zeros([1, 1])), Yroll[min(t - Ky + 1, T):T, 0]), axis=0)
            dataroll = {'X': Xroll, 'Y': Yroll}
            Params['outPoint'] = outPoint
            # Betas based on points excluding t
            result_t = nonstatRegress(dataroll, Params)
            beta_1 = result_t.get('beta')
            alpha_1 = result_t.get('alpha')
        beta_1 = np.asmatrix(beta_1[t - Ky + 1, :]).T
        beta_1_est[t - Ky + 1] = np.transpose(beta_1)
        alpha_1_est[t - Ky + 1] = alpha_1[t - Ky + 1]
        x_1 = Xlag[t + (Xlag.shape[0] - Y.size)]
        YLOO_est[t - Ky + 1] = np.dot(x_1, beta_1) + alpha_1[t - Ky + 1]

    result = {'YLOO_est': YLOO_est, 'beta_1': beta_1, 'beta_1_est': beta_1_est, 'alpha_1': alpha_1,
              'alpha_1_est': alpha_1_est}
    return result
def square_error(x1, x2):
    return (x1 - x2) * (x1 - x2)
def KPIFund(fund, fund_est, Params, Data):

    # KPIFund(Yroll[firstElem:], y_clearCVroll, KPIParams, Data)
    corrR2 = Params.get('corrR2', 0)
    vebose = Params.get('vebose', 0)
    errorfunc = Params.get('errorfunc', square_error)
    # Before
    band = Params.get('band', 1)
    # After
    band = Params.get('band', 0)
    low_theshold = Params.get('low_theshold', 0)
    bot_thresh = Params.get('bot_thresh', 10000000)
    num_outliers = Params.get('num_outliers', 0)
    mean_func = Params.get('mean_func', np.nanmean)
    rollhor = Params.get('horizon', 1)

    data = {'X': fund, 'Y': fund_est}
    # The dates don't matter below
    Data = pd.concat([Data.iloc[:fund.size, [0]].reset_index(drop=True), pd.DataFrame(fund_est), pd.DataFrame(fund)], axis=1). \
        reset_index(drop=True)

    rollfunds = RollXY(Data, Params, 1, rollhor)
    fund = (np.asmatrix(rollfunds.get('Xroll')))
    fund_est = (np.asmatrix(rollfunds.get('Yroll')))
    T = np.size(fund)
    # covres = Params.get('cov')

    # S = covres.get('S')
    # if (vebose):
    #    print(fund)

    # fund      = np.matmul(S,fund)
    # fund_est  = np.matmul(S,fund_est)
    if (corrR2):
        quality = np.corrcoef(np.ravel(fund), np.ravel(fund_est))
        result = {'errarr': np.nan, 'err2': np.nan, 'qualityarr': np.nan, 'quality': quality[0, 1]}
        # return result

    errarr = np.zeros([T, 1])
    for t in range(band, T - band):
        errarr[t] = errorfunc(fund[t], fund_est[t])

    ind = np.logical_or(errarr < low_theshold, errarr > bot_thresh)
    errarr[ind] = np.nan

    ind = np.argsort(errarr, 0)
    if num_outliers > 0:
        errarr[ind[T - num_outliers:T]] = np.nan

    # R2 is calculated via mean instead of sum of squared errors
    # as the function outputs mean results
    err = mean_func(errarr)
    sqr_return = (np.dot(fund.T, fund) / T)[0,0]
    # After
    # sqr_return = np.var(fund)

    qualityarr = 1 - err / sqr_return
    quality = 1 - err / sqr_return
    if (vebose):
        print('err: ', err, 'R2: ', quality, 'y2: ', sqr_return)
    result = {'errarr': errarr, 'err2': np.asscalar(err), 'qualityarr': qualityarr, 'quality': np.asscalar(quality)}

    return result
def Paramsnew(Paramlist, numParamSet):

    # Below is redundant as the loop now happens in the main code
    Params = Paramlist[numParamSet]

    Model = Params.get('Modelvalue', 'DSA')
    if Model == 'PE': Model = 'DSA'
    Horizons = 1 + Params.get('Horizons', 0)
    # There is no else below as the test cases should not leave fallback horizons empty
    WeightAvg = Params.get('WeightAvg', 2)
    HorizonStep = Params.get('HorizonStep', 0)
    if np.isnan(HorizonStep):
        HorizonStep = 1
    else:
        HorizonStep = int(HorizonStep)
    LagStep = Params.get('LagStep', 0)
    if np.isnan(LagStep):
        LagStep = 1
    else:
        LagStep = int(LagStep)
    if not np.isnan(Horizons):
        Horizons = int(Horizons)

    HorizonsMin = 1 + Params.get('HorizonsMin', 0)
    if np.isnan(HorizonsMin):
        HorizonsMin = Horizons
    else:
        HorizonsMin = int(HorizonsMin)

    HorizonsMax = 1 + Params.get('HorizonsMax', 0)
    if np.isnan(HorizonsMax):
        HorizonsMax = Horizons
    else:
        HorizonsMax = int(HorizonsMax)
    Lags = 1 + Params.get('Lags', 0)
    if not np.isnan(Lags):
        Lags = int(Lags)

    LagsMin = 1 + Params.get('LagsMin', 0)
    if np.isnan(LagsMin):
        LagsMin = Lags
    else:
        LagsMin = int(LagsMin)

    LagsMax = 1 + Params.get('LagsMax', 0)
    if np.isnan(LagsMax):
        LagsMax = Lags
    else:
        LagsMax = int(LagsMax)
    Budget = Params.get('Budget', 0)
    if np.isnan(Budget):
        Budget = 0
    else:
        Budget = int(Budget)
    Constraints = Params.get('Constraints', 'no')
    Leverage = Params.get('Leverage', 0)
    if np.isnan(Leverage):
        Leverage = 0
    # else:
    #     Leverage = int(Leverage) / 100
    Constant = int(Params.get('Intercept', 1))
    if np.isnan(Constant):
        Constant = 1
    else:
        Constant = int(Constant)
    Weighting = Params.get('Weighting', 1)
    if np.isnan(Weighting):
        Weighting = 'Constant'
    Threshold = Params.get('Threshold', 1)
    if np.isnan(Threshold):
        Threshold = 999999
    StyleInFirstPoint = Params.get('StyleInFirstPoint', 0)
    if np.isnan(StyleInFirstPoint):
        StyleInFirstPoint = 0
    else:
        StyleInFirstPoint = int(StyleInFirstPoint)
    MarketDynamics = Params.get('MarketDynamics', 0)
    if np.isnan(MarketDynamics):
        MarketDynamics = 0
    else:
        MarketDynamics = int(MarketDynamics)
    IndividualScaling = Params.get('IndividualScaling', 1)
    if np.isnan(IndividualScaling):
        IndividualScaling = 1
    else:
        IndividualScaling = int(IndividualScaling)
    HalfLife = Params.get('HalfLife', 0)
    if np.isnan(HalfLife):
        HalfLife = 0
    else:
        HalfLife = int(HalfLife)

    Point = Params.get('Point', 1)
    Window = int(Params.get('Window', 60))
    NoOfWindows = Params.get('NoOfWindows', np.nan)
    if np.isnan(NoOfWindows):
        NoOfWindows = np.nan
    else:
        NoOfWindows = int(NoOfWindows)
    DateStart = Params.get('DateStart', np.nan)
    DateEnd = Params.get('DateEnd', np.nan)
    OOSShift = int(Params.get('OOSShift', 0))
    Decay = Params.get('Decay', 0)
    Lamda = Params.get('Lamda', 1)
    DoRolling = Params.get('DoRolling', 0)
    rollhorstart = int(Params.get('rollhorstart', 1))
    rollhorend = int(Params.get('rollhorend', 8))
    RollingStyleInFirstPoint = int(Params.get('RollingStyleInFirstPoint', 1))
    RollingStyleInLastPoint = int(Params.get('RollingStyleInLastPoint', 1))

    Calibrate = int(Params.get('Calibrate', 0))
    LongOnly = int(Params.get('LongOnly', 0))
    ConsecutiveMissing = int(Params.get('ConsecutiveMissing', 10))
    MaxMissingPercent = Params.get('MaxMissingPercent', 0.5)
    MinReturnsDesired = int(Params.get('MinReturnsDesired', 10))
    Regime = Params.get('Regime', False)
    RegimeThreshold = Params.get('RegimeThreshold', 0)
    Direction = Params.get('Direction', 'down')
    Absolute = Params.get('Absolute', False)
    doKPI = Params.get('doKPI', True)
    MaxRows = int(Params.get('MaxRows', 10000))
    xhift = int(Params.get('xhift', 0))
    FullTimeAvg = Params.get('FullTimeAvg', True)

    result = {'Model': Model, 'Horizons': Horizons, 'HorizonsMin': HorizonsMin,
              'HorizonsMax': HorizonsMax, 'Lags': Lags, 'LagsMin': LagsMin,
              'LagsMax': LagsMax, 'Budget': Budget, 'Constraints': Constraints,
              'Leverage': Leverage, 'Constant': Constant, 'Weighting': Weighting,
              'Threshold': Threshold, 'StyleInFirstPoint': StyleInFirstPoint,
              'MarketDynamics': MarketDynamics, 'IndividualScaling': IndividualScaling,
              'HalfLife': HalfLife, 'Decay': Decay, 'lam': Lamda, 'rollhorstart': rollhorstart,
              'rollhorend': rollhorend, 'RollingStyleInFirstPoint': RollingStyleInFirstPoint,
              'RollingStyleInLastPoint': RollingStyleInLastPoint, 'HorizonStep': HorizonStep,
              'LagStep': LagStep, 'WeightAvg': WeightAvg, 'Calibrate': Calibrate,
              'LongOnly': LongOnly, 'ConsecutiveMissing': ConsecutiveMissing, 'MaxMissingPercent': MaxMissingPercent,
              'MinReturnsDesired': MinReturnsDesired, 'Regime': Regime, 'RegimeThreshold': RegimeThreshold,
              'Direction': Direction, 'Absolute': Absolute, 'doKPI': doKPI, 'MaxRows': MaxRows,
              'xhift': xhift, 'FullTimeAvg': FullTimeAvg, 'window': Window, 'DoRolling': DoRolling,
              'Point': Point, 'OOSShift': OOSShift, 'DateStart': DateStart, 'DateEnd': DateEnd,
              'NoOfWindows': NoOfWindows}

    return result
def Parameterset(Parameters, excel, filepath, filename, HorizonsMin, HorizonsMax, LagsMin, LagsMax, StyleInFirstPoint,
                 RollingStyleInFirstPoint, RollingStyleInLastPoint, Modelvalue, Modeltypevalue, Holdingsvalue,
                 HorizonStep, LagStep, WeightAvg, Horizons, Threshold, Intercept, Lamda, MarketDynamics,
                 IndividualScaling, UseHalfLife, HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                 DateStart, DateEnd, NoOfWindows):
    # Use excel (excel=1) or hardcoded (excel=0)
    if excel != 1:
        # Model inputs. Some can be entered right below, others inside the Paramsnew function.
        # Defaults are already in place to unconstrained with intercept
        columns = ['Case ID', 'Model', 'Model type', 'Intercept', 'Holdings', 'Horizons',
                   'Horizons min', 'Horizons max', 'Lags', 'Lags min', 'Lags max',
                   'Weighting', 'Threshold', 'Lamda', 'Style in first point',
                   'Market dynamics', 'Individual scaling', 'UseHalfLife', 'Decay',
                   'HalfLife', 'Calibrate', 'Leverage', 'Long only', 'RollingStyleInFirstPoint',
                   'RollingStyleInLastPoint', 'HorizonStep', 'LagStep', 'WeightAvg']
        Parameters = pd.DataFrame(data=[[1, Modelvalue, Modeltypevalue, Intercept, Holdingsvalue, Horizons, \
                                         HorizonsMin, HorizonsMax, Lags, LagsMin, LagsMax, \
                                         Weighting, Threshold, Lamda, StyleInFirstPoint, \
                                         MarketDynamics, IndividualScaling, UseHalfLife, Decay, \
                                         HalfLife, Calibrate, Leverage, LongOnly, RollingStyleInFirstPoint, \
                                         RollingStyleInLastPoint, HorizonStep, LagStep, WeightAvg]],
                                        index=[0], columns=columns)

    # Specific transformations
    if excel ==1:
        Modeltypevalue = Parameters.loc[:, 'Modeltypevalue'][0]
        Holdingsvalue = Parameters.loc[:, 'Holdingsvalue'][0]

    params = Parameters
    for a, b in {'mdAPT': '0', 'mdCAPM': '1'}.items():
        Modeltypevalue = Modeltypevalue.replace(a, b)
    Modeltype = Modeltypevalue
    params['Budget'] = int(Modeltype)
    for a, b in {'hlNoConstraints': 'no', 'hlLeveraged': 'long', 'hlLongOnly': 'long'}.items():
        Holdingsvalue = Holdingsvalue.replace(a, b)
    Holdings = Holdingsvalue
    params['Constraints'] = Holdings

    Paramlist = params.to_dict(orient='records')
    # Only one set of parameters since the loop now happens in the main code
    numParamSet = 0
    Parameters = Paramsnew(Paramlist, numParamSet)

    return Parameters
def yest(Data, nfactors, OOS):
    # Data: Date, alpha, betas, factors
    Data['ysyst'] = pd.concat([pd.Series(np.empty((OOS))),
                    pd.concat([Data.iloc[:(Data.shape[0] - OOS), x].reset_index(drop=True).
                    multiply(Data.iloc[OOS:, x + nfactors].reset_index(drop=True))
                    for x in list(range(2, 2 + nfactors))], axis=1).sum(axis=1)]) \
                    .reset_index(drop=True)
    # add alpha
    Data['yest'] = Data.iloc[:, [1, -1]].sum(axis=1)
    Data.loc[:OOS - 1, 'yest'] = np.nan
    yest = Data['yest'].values
    return yest
def Regressor(w, m, c, k, i, p, j, jlist, r, rName, MaxRows, y, Description, window, RollingStyleInFirstPoint, RollingStyleInLastPoint,
              xshift, start, end, Minreturns, Datestartmax, LamdaList, Parameters, Dependent, FactorGroupList, FactorGroupDict, FactorSetList,
              AssetreturnsOrig, ResultsAll, ResultsAllC, ResultsRC, ResultsRCAllPoints, TestCase, OOSShift, Point,
              frequency, filepath, RegimeThreshold, Direction, Absolute, doKPI, Regime):

    # For each FactorSet
    for FactorGroup in FactorGroupList:
        FactorSetLoopList = FactorSetList if len(FactorSetList) > 0 else list(FactorGroupDict[FactorGroup][0])
        for FactorSet in FactorSetLoopList:
            # FactorGroup = 'Test'
            # FactorSet = 'Factors_AB'

            # Update to not load multiple times if it's the same set
            # if k == 0 or (len(FactorGroupList) + len(list(FactorGroupDict[FactorGroup])) > 2) or w > 0:
            Factorsorig = pd.read_excel(filepath + FactorSet + '.xlsx', engine='openpyxl')
            if TestCase == False and frequency == 'Weekly':
                Factorsorig = ReturnCreator(Factorsorig, frequency)
            elif TestCase == True:
                # Transform to Date - multiple factor columns if the above day of the week didn't take place
                Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
                    ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
                Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
            Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
            # For now, keep only common dates below
            Factorsorig = Factorsorig[
                Factorsorig['Date'].isin(list(y['Date']))].reset_index(drop=True)
            Assetreturns = AssetreturnsOrig[
                AssetreturnsOrig['Date'].isin(list(Factorsorig['Date']))].reset_index(drop=True)
            Dataset = Assetreturns
            Factors = Factorsorig
            n = Factors.shape[1] - 1
            # Merge with factors based on Date
            Dataall = pd.merge(y, Factors, left_on='Date', right_on='Date', how='outer'). \
                sort_values(by=['Date']).reset_index(drop=True)
            # Drop rows where any value across columns equals nan. This is also a substitute for weekends.
            # We do not drop outside start and end range so as to keep max factor history.
            # Find first and last non NA value of the fund
            start = Dataall.iloc[:, 1].first_valid_index()
            end = Dataall.iloc[:, 1].last_valid_index()
            temp = Dataall.iloc[start:end + 1, :][
                ~(Dataall.iloc[start:end + 1, :].isna()).any(axis=1)].reset_index(
                drop=True)
            if start > 0:
                Dataall = pd.concat([Dataall.iloc[0:start - 1, :], temp], ignore_index=True,
                                    sort=False).reset_index(drop=True)
            else:
                Dataall = temp
            # Also drop zeros. Substitute for bad data.
            Dataall = Dataall[~(Dataall == 0).any(axis=1)].reset_index(drop=True)
            Dataall = Dataall.iloc[-MaxRows:].reset_index(drop=True)
            # Find again first and last non NA value of the fund
            start = Dataall.iloc[:, 1].first_valid_index()
            end = Dataall.iloc[:, 1].last_valid_index()
            # For now we only process bonds with min return history and making sure to start prior to Datestartmax
            datacheck = start != None and end != None and (
                    end - start + 2) >= Minreturns and pd.to_datetime(
                Dataall.iloc[start, 0]) < Datestartmax
            if datacheck == True:
                Yall = Dataall.loc[start:end, [Dependent]].reset_index(drop=True)
                Data = Dataall.loc[w:w + window - 1, :].reset_index(drop=True)
                # Only works with trailing window
                DataOOS = Dataall.loc[(w + window + OOSShift):(w + window + OOSShift), :]. \
                    reset_index(drop=True)
                # Results1 correspond to quantile (center, last point etc) window day
                DateSuffix = ' ' + Data.loc[round(np.quantile(Data.index.values, Point)), 'Date']
                T = Data.shape[0]

                ##############
                # Regression #
                ##############
                for Lamda in LamdaList:
                    Parameters['lam'] = Lamda
                    for Ky in range(Parameters['HorizonsMin'], Parameters['HorizonsMax'] + 1,
                                    Parameters['HorizonStep']):
                        covres = GetCovarMatrix(T, Ky)
                        for Kx in range(Parameters['LagsMin'], Parameters['LagsMax'] + 1,
                                        Parameters['LagStep']):
                            # pd.DataFrame(np.array([Ky,Kx]).T,index=['Ky','Kx']).T.to_csv('C://Data//KyKx.csv')
                            if min(Data.iloc[:, 1].size - Ky + 1,
                                   Data.iloc[:, 2].size - Ky - abs(Kx) + 2) >= Minreturns:
                                Kxx = 1 if (Kx == -1 or Kx == 0) else Kx
                                Analyticsdata = Analytics(Data, DataOOS, Dependent, n, w, window, Kxx,
                                                          Ky, Parameters, covres, Minreturns,
                                                          RegimeThreshold, Direction, Absolute, doKPI,
                                                          Regime)
                                # The below shouldn't run, it's a catch if analytics haven't been produced
                                if Parameters['KPI'] == False:
                                    pd.DataFrame(np.array([Ky, Kx]).T, index=['Ky', 'Kx']).T. \
                                        to_csv(filepath + '\\KyKx.csv')
                                else:
                                    if Kx < 0:
                                        Kxx = 1 if Kx == -1 else Kx + 2
                                    ResultsAll.loc[j, Description] = y.columns[1]
                                    ResultsAll.loc[j, 'FundID'] = i + 1
                                    ResultsAll.loc[j, 'CaseID'] = p + 1
                                    ResultsAll.loc[j, 'Window'] = str(w)
                                    ResultsAll.loc[j, 'WindowDateFrom'] = Data.loc[0, 'Date']
                                    ResultsAll.loc[j, 'WindowDateTo'] = Data.loc[
                                        Data.shape[0] - 1, 'Date']
                                    ResultsAll.loc[j, 'FactorGroup'] = FactorGroup
                                    ResultsAll.loc[j, 'FactorSet'] = FactorSet
                                    ResultsAll.loc[j, 'Lamda'] = Lamda
                                    ResultsAll.loc[j, 'Horizons'] = Ky - 1
                                    ResultsAll.loc[j, 'Lags'] = Kx - 1
                                    ResultsAll.loc[j, 'Reshuffle'] = r
                                    ResultsAll.loc[j, 'ReshuffleName'] = rName
                                    ResultsAll = Results(start, Description, ResultsAll,
                                                         Yall, y, Dataall, DataOOS, i, j,
                                                         p, Kxx, Ky, Analyticsdata, Parameters, n, T,
                                                         Factors, xshift, w,
                                                         Point, doKPI, Regime, DateSuffix)
                                    # Add residuals in sample, predicted
                                    if doKPI == True:
                                        SeriesListIn = ['Style', 'Predicted Style']
                                        SeriesListOut = \
                                            ['Smooth', 'Unsmooth', 'OOS', 'Mean', 'Stdev', 'Variance',
                                             'Mean/Stdev',
                                             'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson',
                                             'Window']
                                        NameList = [x.replace('Style', 'Residual') for x in
                                                    SeriesListIn]
                                        NameList = [x.replace('style', 'Residual') for x in NameList]
                                        ResidualListOut = ['Residual']
                                        ResultsAll = ResidualsAdd(ResultsAll, Data, SeriesListIn,
                                                                  SeriesListOut, ResidualListOut,
                                                                  NameList)
                                    # Add style, residuals OOS
                                    # OOS degradation
                                    # for o in range(OOSShift_Min, min(OOSShift_Max + 1, Yall.shape[0] - window + 1)):
                                    if DataOOS.shape[0] > 0:
                                        SeriesListIn = ['StyleOOS']
                                        SeriesListOut = \
                                            ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max',
                                             'Skewness', 'Kurtosis', 'Durbin-Watson', 'Window']
                                        NameList = [x.replace('Style', 'Residual') for x in
                                                    SeriesListIn]
                                        NameList = [x.replace('style', 'Residual') for x in NameList]
                                        ResidualListOut = ['ResidualOOS']
                                        ResultsAll = ResidualsAdd(ResultsAll, DataOOS, SeriesListIn,
                                                                  SeriesListOut, ResidualListOut,
                                                                  NameList)

                                    j = j + 1
                                    # print('ResultsAll row ' + str(j-1))

    # Calibration
    if doKPI == True:
        maxrow = int(pd.to_numeric(ResultsAll.iloc[jlist[m]:j, :]['PR2 Mean' + DateSuffix]).idxmax())
        minrow = int(pd.to_numeric(ResultsAll.iloc[jlist[m]:j, :]['PR2 Mean' + DateSuffix]).idxmin())
        # Check if calibration has taken place
        if Parameters['HorizonsMax'] != Parameters['HorizonsMin'] or \
                Parameters['LagsMax'] != Parameters['LagsMin'] or len(LamdaList) > 1 or \
                (len(FactorGroupList) + len(list(FactorGroupDict[FactorGroup])) > 2):
            if ResultsAll.loc[maxrow, 'PR2 Mean' + DateSuffix] - \
                    ResultsAll.loc[minrow, 'PR2 Mean' + DateSuffix] > Parameters['Threshold']:
                optimalrow = maxrow
            else:
                # If ky,kx not in horizons,lags, one more pass to populate results
                # Fix this to save time!!!
                # =============================================================================
                #                         if ResultsAll.loc[(ResultsAll['CaseID'] == p+1) \
                #                         & (ResultsAll['Horizons '+ str(colnumber)] == Parameters['Horizons']-1) \
                #                         & (ResultsAll['Lags'] == Parameters['Lags']-1)].shape[0] == 0:
                # =============================================================================
                Ky = Parameters['Horizons']
                Kx = Parameters['Lags']
                Lamda = Parameters['Lamda']
                Parameters['lam'] = Lamda
                Kxx = 1 if (Kx == -1 or Kx == 0) else Kx
                Analyticsdata = Analytics(Data, DataOOS, Dependent, n, w, window, Kxx, Ky, Parameters,
                                          covres, Minreturns, RegimeThreshold, Direction, Absolute,
                                          doKPI, Regime)
                if Kx < 0:
                    Kxx = 1 if Kx == -1 else Kx + 2
                ResultsAll.loc[j, Description] = y.columns[1]
                ResultsAll.loc[j, 'FundID'] = i + 1
                ResultsAll.loc[j, 'CaseID'] = p + 1
                ResultsAll.loc[j, 'Window'] = str(w)
                ResultsAll.loc[j, 'WindowDateFrom'] = Data.loc[0, 'Date']
                ResultsAll.loc[j, 'WindowDateTo'] = Data.loc[Data.shape[0] - 1, 'Date']
                ResultsAll.loc[j, 'FactorGroup'] = FactorGroup
                ResultsAll.loc[j, 'FactorSet'] = FactorSet
                ResultsAll.loc[j, 'Lamda'] = Lamda
                ResultsAll.loc[j, 'Horizons'] = Ky - 1
                ResultsAll.loc[j, 'Lags'] = Kx - 1
                ResultsAll = Results(start, Description, ResultsAll, Yall, y, Dataall,
                                     DataOOS,
                                     i, j, p, Kxx, Ky, Analyticsdata, Parameters, n, T, Factors, xshift,
                                     w,
                                     Point, doKPI, Regime, DateSuffix)
                SeriesListIn = ['Style', 'Predicted Style']
                SeriesListOut = \
                    ['Smooth', 'Unsmooth', 'OOS', 'Mean', 'Stdev', 'Variance', 'Mean/Stdev',
                     'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson', 'Window']
                NameList = [x.replace('Style', 'Residual') for x in SeriesListIn]
                NameList = [x.replace('style', 'Residual') for x in NameList]
                ResidualListOut = ['Residual']
                ResultsAll = ResidualsAdd(ResultsAll, Data, SeriesListIn, SeriesListOut,
                                          ResidualListOut, NameList)
                # Add style, residuals OOS
                # OOS degradation
                # for o in range(OOSShift_Min, min(OOSShift_Max + 1, Yall.shape[0] - window + 1)):
                if DataOOS.shape[0] > 0:
                    SeriesListIn = ['StyleOOS']
                    SeriesListOut = \
                        ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max',
                         'Skewness', 'Kurtosis', 'Durbin-Watson', 'Window']
                    NameList = [x.replace('Style', 'Residual') for x in SeriesListIn]
                    NameList = [x.replace('style', 'Residual') for x in NameList]
                    ResidualListOut = ['ResidualOOS']
                    ResultsAll = ResidualsAdd(ResultsAll, DataOOS, SeriesListIn,
                                              SeriesListOut, ResidualListOut, NameList)

                optimalrow = j
                j = j + 1
        # =============================================================================
        #                         else:
        #                             optimalrow=ResultsAll.loc[(ResultsAll['CaseID'] == p+1) \
        #                             & (ResultsAll['Horizons'] == Parameters['Horizons']-1) \
        #                             & (ResultsAll['Lags'] == Parameters['Lags']-1)].index.values.astype(int)[0]
        # =============================================================================
        else:
            optimalrow = maxrow
        ResultsAll.loc[jlist[m]:j - 1, 'PR2 min Window' + DateSuffix] = \
            ResultsAll.loc[minrow, 'PR2 Mean' + DateSuffix]
        ResultsAll.loc[jlist[m]:j - 1, 'PR2 max Window' + DateSuffix] = \
            ResultsAll.loc[maxrow, 'PR2 Mean' + DateSuffix]
    else:
        optimalrow = j - 1

    ##################
    # Window results #
    ##################

    if not ResultsAllC.empty:
        # ResultsAllC.append(ResultsAll.iloc[[optimalrow]], sort=False).reset_index(drop=True)
        ResultsAllC = pd.concat([ResultsAllC, ResultsAll.iloc[[optimalrow]]], axis=0, ignore_index=True) \
            .reset_index(drop=True)
    else:
        ResultsAllC = ResultsAll.iloc[[optimalrow]].reset_index(drop=True)

    # Pick center or quantile point to represent the window.
    # Fund is already part of ResultsAllC but it doesn't get duplicated further below
    analytics2 = ['Fund', 'Alpha', 'Lamda', 'Horizons', 'Lags', 'FundOOS', 'StyleOOS', 'ResidualOOS']
    rollhorstart = Parameters.get('rollhorstart', 1)
    rollhorend = Parameters.get('rollhorend', 8)
    predictedanalytics2 = \
        ['R2', 'R2 Smooth', 'Style Smooth', 'Residual Unsmooth', 'PR2', 'PR2 Smooth', 'Style',
         'Predicted Style', 'Residual', 'Predicted Residual', 'PR2 Smooth Avg'] \
        + ['Predicted Style Smooth' + \
           str(i) for i in range(rollhorstart, min(rollhorend + 1, Data.shape[0] - Data.shape[1] + 2))]
    analytics3 = ['Beta']
    predictedanalytics3 = ['Predicted Beta']
    if doKPI == True:
        analytics2 = analytics2 + predictedanalytics2
        analytics3 = analytics3 + predictedanalytics3
    daterange = Dataall.loc[start + w:start + w + window - 1, 'Date'].astype('str').tolist()
    ResultsRC.loc[k, Description] = y.columns[1]
    ResultsRC.loc[k, 'FundID'] = i + 1
    ResultsRC.loc[k, 'CaseID'] = p + 1
    ResultsRC.loc[k, 'Window'] = str(w)
    ResultsRC.loc[k, 'WindowDateFrom'] = Data.loc[0, 'Date']
    ResultsRC.loc[k, 'WindowDateTo'] = Data.loc[Data.shape[0] - 1, 'Date']
    ResultsRC.loc[k, 'FactorGroup'] = ResultsAll.loc[optimalrow, 'FactorGroup']
    ResultsRC.loc[k, 'FactorSet'] = ResultsAll.loc[optimalrow, 'FactorSet']
    ResultsRC.loc[k, 'Lamda'] = ResultsAll.loc[optimalrow, 'Lamda']
    ResultsRC.loc[k, 'Horizons'] = ResultsAll.loc[optimalrow, 'Horizons']
    ResultsRC.loc[k, 'Lags'] = ResultsAll.loc[optimalrow, 'Lags']
    ResultsRC.loc[k, 'Reshuffle'] = ResultsAll.loc[optimalrow, 'Reshuffle']
    ResultsRC.loc[k, 'ReshuffleName'] = ResultsAll.loc[optimalrow, 'ReshuffleName']
    # Optimal FactorSet
    Factorsorig = pd.read_excel(filepath + ResultsAll.loc[optimalrow, 'FactorSet'] + '.xlsx',
                                engine='openpyxl')
    if TestCase == False and frequency == 'Weekly':
        Factorsorig = ReturnCreator(Factorsorig, frequency)
    elif TestCase == True:
        # Transform to Date - multiple factor columns if the above day of the week didn't take place
        Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
            ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
        Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
    Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
    # For now, keep only common dates below
    Factorsorig = Factorsorig[Factorsorig['Date'].isin(list(y['Date']))].reset_index(
        drop=True)
    Factors = Factorsorig
    n = Factors.shape[1] - 1

    for s in range(1, T + 1):
        colnumber = Dataall.loc[start + s + w - 1, 'Date']
        for t in range(len(list(analytics2))):
            analytic = analytics2[t]
            if analytic[-3:] != 'OOS':
                analyticcols = [col for col in ResultsAll.columns if col[:len(analytic)] == analytic \
                                and len(col) > len(analytic) and col[len(analytic) + 1:len(
                    analytic) + 2].isdigit() == True and col[-10:] in daterange]
                Analytic = ResultsAll.loc[ResultsAll.index[j - 1], analyticcols]
                col = analytic + ' ' + str(colnumber)
                colout = analytic + ' Window ' + str(colnumber)
                # Populate results based on center or quantile window
                if s >= T - window + 1:
                    col2 = analytic + ' Mean ' + str(colnumber)
                    col3 = analytic + ' Mean' + DateSuffix
                    if col in ResultsAll.columns:
                        ResultsRCAllPoints.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                        # Not all analytics have a Mean yet
                        if col3 in ResultsAll.columns:
                            ResultsRCAllPoints.loc[k, col2] = ResultsAll.copy().loc[optimalrow, col3]
                        if (w == 0 and RollingStyleInFirstPoint == 1) or (
                                w == Yall.shape[0] - window and s - (T - window) == 1 + round(
                            np.quantile(list(range(window)), Point))) \
                                or (w == Yall.shape[0] - window and s - (T - window) > 1 + round(
                            np.quantile(list(range(window)), Point)) and RollingStyleInLastPoint == 1) \
                                or s - (T - window) == 1 + round(
                            np.quantile(list(range(window)), Point)):
                            ResultsRC.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                            # Not all analytics have a Mean yet but it's ok as it gets added in diagnostics
                            if col3 in ResultsAll.columns:
                                ResultsRC.loc[k, col2] = ResultsAll.copy().loc[optimalrow, col3]
        for t in range(len(list(analytics3))):
            for kk in range(1, n + 1):
                analytic = analytics3[t] + ' ' + Factors.columns[kk + xshift]
                analyticcols = [col for col in ResultsAll.columns if col[:len(analytic)] == analytic \
                                and len(col) > len(analytic) and col[len(analytic) + 1:len(
                    analytic) + 2].isdigit() == True and col[-10:] in daterange]
                Analytic = ResultsAll.loc[ResultsAll.index[j - 1], analyticcols]
                col = analytic + ' ' + str(colnumber)
                colout = analytic + ' Window ' + str(colnumber)
                # Populate results based on center or quantile window
                if s >= T - Analytic.shape[0] + 1:
                    col2 = analytic + ' Mean ' + str(colnumber)
                    col3 = analytic + ' Mean' + DateSuffix
                    if col in ResultsAll.columns:
                        ResultsRCAllPoints.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                        ResultsRCAllPoints.loc[k, col2] = ResultsAll.copy().loc[optimalrow, col3]
                        if (w == 0 and RollingStyleInFirstPoint == 1) or (
                                w == Yall.shape[0] - window and s - (T - Analytic.shape[0]) == 1 + round(
                            np.quantile(list(range(Analytic.shape[0])), Point))) \
                                or (w == Yall.shape[0] - window and s - (T - Analytic.shape[0]) > 1 + round(
                            np.quantile(list(range(Analytic.shape[0])),
                                        Point)) and RollingStyleInLastPoint == 1) \
                                or s - (T - Analytic.shape[0]) == 1 + round(
                            np.quantile(list(range(Analytic.shape[0])), Point)):
                            ResultsRC.loc[k, colout] = ResultsAll.copy().loc[optimalrow, col]
                            ResultsRC.loc[k, col2] = ResultsAll.copy().loc[optimalrow, col3]
    # OOS
    if not DataOOS.empty:
        colnumber = DataOOS.loc[0, 'Date']
        daterange = DataOOS.loc[0:0, 'Date'].astype('str').tolist()
        for t in range(len(list(analytics2))):
            analytic = analytics2[t]
            if analytic[-3:] == 'OOS':
                analyticcols = [col for col in ResultsAll.columns if col[:len(analytic)] == analytic \
                                and len(col) > len(analytic) and col[len(analytic) + 1:len(
                    analytic) + 2].isdigit() == True and col[-10:] in daterange]
                Analytic = ResultsAll.loc[ResultsAll.index[j - 1], analyticcols]
                col = analytic + ' ' + str(colnumber)
                colout = analytic + ' Window ' + str(colnumber)
                ResultsRCAllPoints.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                ResultsRC.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]

    ######################
    # Window Diagnostics #
    ######################

    SeriesListIn1 = []
    SeriesListIn2 = ['Fund']
    if doKPI == True:
        SeriesListIn1 = SeriesListIn1 + ['Style', 'Predicted Style']
        SeriesListIn2 = SeriesListIn2 + ['Residual', 'Predicted Residual']
    SeriesListOut = \
        ['R2', 'Correlation', 'Beta', 'Smooth', 'OOS', 'Mean', 'Stdev', 'Variance',
         'Mean/Stdev', 'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson']
    NameList = [x.replace('Style', 'Residual') for x in SeriesListIn1]
    NameList = [x.replace('style', 'Residual') for x in NameList]
    ResultsRC = DiagnosticResults(k, ResultsRC, k, ResultsRCAllPoints, Data, SeriesListIn1,
                                  SeriesListIn2,
                                  SeriesListOut, DateSuffix)
    ResultsAllC = DiagnosticResults(m, ResultsAllC, k,
                                    ResultsRCAllPoints, Data, SeriesListIn1, SeriesListIn2,
                                    SeriesListOut, DateSuffix)

    return j, jlist, analytics2, analytics3, optimalrow, ResultsAll, ResultsAllC, ResultsRC, ResultsRCAllPoints, Dataall
def Analytics(Data, DataOOS, Dependent, n, w, window, Kxx, Ky, Parameters, covres, Minreturns, RegimeThreshold, Direction, Absolute,
              doKPI, Regime):

    # Initialize
    y_smoothols = np.empty(())
    y_unsmoothols = np.empty(())
    y_clearCV = np.empty(())
    beta_1_est = np.empty(())
    y_unsmootholsOOS = np.empty(())
    alpha_1_est = np.empty(())
    PR2smooth = np.nan
    PR2SmoothAvg = np.nan
    Predictedstylesmooth = {}

    Model = Parameters['Model']
    StyleInFirstPoint = Parameters['StyleInFirstPoint']

    start = Data[[Dependent]].first_valid_index()
    end = Data[[Dependent]].last_valid_index()
    Yall = Data.loc[start:end, [Dependent]].reset_index(drop=True).to_numpy()
    Y = np.asmatrix(np.array(list(Yall), dtype=float))
    T = np.size(Y)
    Xall = Data.loc[start:end, Data.columns[-n:]].reset_index(drop=True).to_numpy()
    X = np.asmatrix(np.array(list(Xall), dtype=float))
    XallOOS = DataOOS.loc[:, DataOOS.columns[-n:]].reset_index(drop=True).to_numpy()
    XOOS = np.asmatrix(np.array(list(XallOOS), dtype=float))

    if X.shape[0] == 1: X = X.transpose()
    n = X.shape[1]
    data = {'Y': Y, 'X': X}
    KPISmoothResult = {'errarr': np.nan, 'err2': np.nan, 'qualityarr': np.nan, 'quality': np.nan}
    KPIUnsmoothResult = {'errarr': np.nan, 'err2': np.nan, 'qualityarr': np.nan, 'quality': np.nan}
    KPIClearCVResult = {'errarr': np.nan, 'err2': np.nan, 'qualityarr': np.nan, 'quality': np.nan}
    KPIClearCVResultroll = {'errarr': np.nan, 'err2': np.nan, 'qualityarr': np.nan, 'quality': np.nan}
    y_smoothols = []
    y_unsmoothols = []
    y_clearCV = []
    beta_1_est = []
    alpha_1_est = []
    y_clearCVroll = []
    PR2smooth = []
    PR2SmoothAvg = []
    Dates = []
    alphaols = []
    betaols = []
    ResidualUnsmooth = np.empty(shape=(0, 1))

    # Experimental:
    rollhorstart = Parameters.get('rollhorstart', 1)
    rollhorend = Parameters.get('rollhorend', 8)
    if Kxx > 1:
        firstElem = max(abs(Kxx) - 1 - (X.shape[0] - Y.size), 0) + Ky - 1
    else:
        firstElem = abs(Kxx) - 1 + Ky - 1
    # X.shape[0]      Kxx-1    Y.size      Start
    # 12              2       12          2
    # 13              2       12          1
    # 14              2       12          0
    # 15              2       12          0

    ##################################################
    ###################  Smooth  #####################
    ##################################################

    rolldata = RollXY(Data, Parameters, Kxx, Ky)
    # X is rolled twice, if applicable
    Xroll = (np.asmatrix(rolldata.get('Xroll')))
    # Y is rolled once
    Yroll = (np.asmatrix(rolldata.get('Yroll')))
    Dataroll = rolldata.get('Dataroll')
    # Lag X
    rolldata = RollXY(Data, Parameters, Kxx, 1)
    Xlag = (np.asmatrix(rolldata.get('Xroll')))
    Datarolllag = rolldata.get('Dataroll')

    # Regime
    if Regime == True:

        # Check if ALL factors pass the RegimeThreshold. I am using Data to roll again for convenience
        Dataroll = doroll(Data, Parameters, Ky)
        if Direction == 'up':
            if Absolute == True:
                Dataroll['Pass'] = ['Y' if x == n else 'N' for x in
                                    np.sum(abs(Dataroll[Dataroll.columns[-n:]]).values >= RegimeThreshold, 1)]
            else:
                Dataroll['Pass'] = ['Y' if x == n else 'N' for x in
                                    np.sum(Dataroll[Dataroll.columns[-n:]].values >= RegimeThreshold, 1)]
        else:
            if Absolute == True:
                Dataroll['Pass'] = ['Y' if x == n else 'N' for x in
                                    np.sum(abs(Dataroll[Dataroll.columns[-n:]]).values < RegimeThreshold, 1)]
            else:
                Dataroll['Pass'] = ['Y' if x == n else 'N' for x in
                                    np.sum(Dataroll[Dataroll.columns[-n:]].values < RegimeThreshold, 1)]
        Dataroll = Dataroll[Dataroll['Pass'] == 'Y'].iloc[:, :Dataroll.shape[1] - 1].reset_index(drop=True)
        start = Dataroll[[Dependent]].first_valid_index()
        end = Dataroll[[Dependent]].last_valid_index()
        Dataroll = Dataroll.iloc[start:end, :].reset_index(drop=True)

        Yroll = Dataroll[[Dependent]].to_numpy()
        Yroll = np.asmatrix(np.array(list(Yroll), dtype=float))
        Xroll = Dataroll[Dataroll.columns[-n:]].to_numpy()
        Xroll = np.asmatrix(np.array(list(Xroll), dtype=float))
        if Xroll.shape[0] == 1: Xroll = Xroll.transpose()
        T = np.size(Yroll)
        Dates = Dataroll[['Date']]
        Dates['Date'] = Dates['Date'].dt.strftime('%Y/%m/%d')
    Parameters['KPI'] = False

    if np.size(Yroll) >= Minreturns:

        Parameters['KPI'] = True
        data = {'X': Xroll, 'Y': Yroll}
        if Model == 'Sharpe':
            OLSresult = OLS(data, Parameters)
            alphaols = OLSresult['alpha_est']
            betaols = OLSresult['beta_est']
        else:
            Parameters['outPoint'] = np.zeros(Yroll.size)
            DSAresult = nonstatRegress(data, Parameters)
            alphaols = DSAresult['alpha']
            betaols = DSAresult['beta']
        Dataroll1 = pd.concat([Dataroll.iloc[:, [0]], pd.DataFrame(alphaols), pd.DataFrame(betaols)],
                              axis=1).reset_index(drop=True)
        Dataall = pd.merge(Dataroll1, Datarolllag, left_on='Date', right_on='Date', how='inner')
        y_smoothols = np.zeros([Yroll.size, 1])
        # ylag doesn't contain alpha
        # y_lagols = np.zeros([Dataall.shape[0], 1])
        for t in range(Yroll.size):
            y_smoothols[t, 0] = np.dot(betaols[t, :], Xroll[t, :].T) + alphaols[t]
        # Find y systematic unsmooth by first creating a dataframe of b*X for each factor and then summing up, where X is the combined factor
        Dataall['ylagolssyst'] = pd.concat([Dataall.iloc[:, x].multiply(Dataall.iloc[:, x+1+n]) for x in list(range(2, 2 + n))], axis=1).sum(axis=1)
        # add alpha
        Dataall['ylagols'] = Dataall.iloc[:,[1,-1]].sum(axis=1)
        # for t in range(Dataall.shape[0]):
        #     y_lagols[t, 0] = np.dot(Dataall.iloc[t, 2:2 + n], Dataall.iloc[t, 3 + n:])
        Datasmooth = pd.concat([Dataroll.iloc[:, [0, 1]], pd.DataFrame(y_smoothols)], axis=1, sort=False).reset_index(drop=True)
        # Ylag = pd.concat([Dataall.iloc[:, [0]], pd.DataFrame(y_lagols)], axis=1).reset_index(drop=True)
        # Datalag = pd.merge(Data.iloc[:, [0, 1]], Dataall, left_on='Date', right_on='Date', how='inner')
        # ResidualUnsmooth = (Datalag.iloc[:, 1] - Datalag['ylagols']).T.to_numpy()
        # ResidualUnsmooth = FundObserved - FundUnsmooth
        ResidualUnsmooth = (Dataall.iloc[:, Dataroll1.shape[1]] - Dataall['ylagols']).T.to_numpy()
        ResidualUnsmooth = ResidualUnsmooth.reshape([ResidualUnsmooth.size, 1])
        # Why the below ?
        # ResidualUnsmooth = ResidualUnsmooth - ResidualUnsmooth.mean()

        KPIParams = {'horizon': 1, 'cov': covres}
        KPISmoothResult = KPIFund(Yroll, y_smoothols, KPIParams, Datasmooth)

        ##################################################
        #################  Unsmooth  #####################
        ##################################################

        Xunsmooth = np.asmatrix(np.array(list(X), dtype=float))
        if X.shape[1] == 1: Xunsmooth = Xunsmooth.T

        Dataall = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='inner'),
                         [Dataroll1, Data.iloc[:, [0] + list(range(2, 2 + n))]])
        if not DataOOS.empty:
            Temp = pd.DataFrame(np.zeros((1, Dataall.shape[1])) * np.nan)
            Temp.columns = Dataall.columns
            Temp.loc[:, 'Date'] = DataOOS.loc[:, 'Date'].values[0]
            Temp.iloc[:, -n:] = DataOOS.iloc[:, 2:].values[0]
            DataallOOS = pd.concat([Dataall, Temp], axis=0).reset_index(drop=True)
        # if StyleInFirstPoint == 0:
        # y_unsmoothols = np.zeros([Y[firstElem:, ].size, 1])
        # for t in range(Dataall.shape[0]):
        #     # if Kxx > 0:
        #     #     y_unsmoothols[t, 0] = np.dot(betaols[t, :],Xunsmooth[t + (X.shape[0] - Y[firstElem:, ].size),
        #     #                             :].T) + alphaols[t]
        #     # else:
        #     #     y_unsmoothols[t, 0] = np.dot(betaols[t, :], Xunsmooth[t,:].T) + alphaols[t]
        #     y_unsmoothols[t, 0] = np.dot(Dataall.iloc[t, 2:2 + n], Dataall.iloc[t, 2 + n:]) + Dataall.iloc[t, 1]
        y_unsmoothols = yest(Dataall, n, 0)
        if not DataOOS.empty:
            y_unsmootholsOOS = np.array([yest(DataallOOS, n, 1)[-1]])
        else:
            y_unsmootholsOOS = np.array(np.nan)

        if doKPI != False:

            KPIParams = {'horizon': 1, 'cov': covres}
            DataUnsmooth = pd.concat([Dataroll.iloc[:, [0]], pd.DataFrame(y_unsmoothols)], axis=1).reset_index(drop=True)
            DataUnsmoothall = pd.merge(Data.iloc[:, [0, 1]], DataUnsmooth, left_on='Date', right_on='Date', how='inner')
            # if Kxx > 0:
            KPIUnsmoothResult = KPIFund(DataUnsmoothall.iloc[:, [1]].values, y_unsmoothols, KPIParams, DataUnsmoothall)
            # else:
            # KPIUnsmoothResult = KPIFund(Y[:-firstElem, ], y_unsmoothols, KPIParams, DataUnsmoothall)
            if not DataOOS.empty:
                DataUnsmoothOOS = pd.concat([DataallOOS.iloc[-1, [0]].reset_index(drop=True), pd.DataFrame(y_unsmootholsOOS)], axis=1).reset_index(drop=True)
                DataUnsmoothOOS.rename(columns={list(DataUnsmoothOOS)[0]: 'Date'}, inplace=True)
                DataUnsmoothallOOS = pd.merge(DataOOS.iloc[:, [0, 1]].reset_index(drop=True), DataUnsmoothOOS, left_on='Date', right_on='Date',how='inner')
            # else:
                # # Populate first betas to equal first available betas
                # if Kxx > 0:
                #     betaols = np.concatenate((np.asmatrix(np.zeros([firstElem, n])) + betaols[0, :], betaols), axis=0)
                # else:
                #     betaols = np.concatenate(betaols, (np.asmatrix(np.zeros([firstElem, n])) + betaols[-1, :]), axis=0)
                # alphafill = np.zeros([firstElem, 1])
                # if Kxx > 0:
                #     alphafill[:] = alphaols[0]
                #     alphaols = np.concatenate([alphafill, alphaols])
                # else:
                #     alphafill[:] = alphaols[-1]
                #     alphaols = np.concatenate([alphaols, alphafill])
                # y_unsmoothols = np.zeros([Y.size, 1])
                # for t in range(Y.size):
                #     if Kxx > 0:
                #         y_unsmoothols[t, 0] = np.dot(betaols[t, :], Xunsmooth[t + (X.shape[0] - Y.size), :].T) + \
                #                               alphaols[t]
                #     else:
                #         y_unsmoothols[t, 0] = np.dot(betaols[t, :], Xunsmooth[t, :].T) + alphaols[t]
                # covres = GetCovarMatrix(T, 1)
                # KPIParams = {'horizon': 1, 'cov': covres}
                # KPIUnsmoothResult = KPIFund(Y, y_unsmoothols, KPIParams)
                # KPIUnsmoothResult = KPIFund(DataUnsmoothall.iloc[:, [1]].values, y_unsmoothols, KPIParams, DataUnsmoothall)

            # Estimate PR2

            urolldata = {'X': X, 'Y': Y}
            Parameters.update({'horizonX': Kxx, 'horizonY': Ky})
            ClearCVResult = ClearCrossValidation(Data, urolldata, Parameters)
            # Check if the below should be reduced by firstElement
            # Before
            # y_clearCV = np.zeros([Y.size - Ky + 1, 1])
            y_clearCV = np.zeros([ClearCVResult['YLOO_est'].shape[0], 1])
            # for t in range(Y.size - Ky + 1):
            for t in range(ClearCVResult['YLOO_est'].shape[0]):
                y_clearCV[t, 0] = ClearCVResult['YLOO_est'][t]
            beta_1_est = ClearCVResult['beta_1_est']
            alpha_1_est = ClearCVResult['alpha_1_est']
            covresclear_1 = GetCovarMatrix(np.size(y_clearCV, 0), 1)
            KPIParams = {'horizon': 1, 'vebose': 0, 'cov': covresclear_1}
            # Data is wrong below, just needed for dates inside KPI
            if Kxx > 0:
                KPIClearCVResult = KPIFund(Y[firstElem:], y_clearCV, KPIParams, Data)
            else:
                KPIClearCVResult = KPIFund(Y[:-firstElem], y_clearCV, KPIParams, Data)

            # PR2 Smooth

            data = {'X': y_clearCV, 'Y': y_clearCV}
            # Dates don't matter below - for now.
            # Date - FundCV - Fund
            Data = pd.concat([Data.iloc[:y_clearCV.size, [0]], pd.DataFrame(y_clearCV), Data.iloc[:,[1]]], axis=1)\
                    .reset_index(drop=True)
            dataclearCVroll = RollXY(Data, Parameters, 1, Ky)
            # x_clearCVroll    = dataclearCVroll['Xroll']
            y_clearCVroll = dataclearCVroll['Yroll']
            # Before: covresclearroll = GetCovarMatrix(np.size(Yroll[firstElem:], 0), 1)
            covresclearroll = GetCovarMatrix(np.size(Yroll, 0), 1)
            # New: Change horizon below to 1 from Ky
            KPIParams = {'horizon': 1, 'vebose': 0, 'cov': covresclearroll}
            # Data is wrong below, just needed for dates inside KPI
            if Kxx > 1:
                # Before: KPIClearCVResultroll = KPIFund(Yroll[firstElem:], y_clearCVroll, KPIParams, Data)
                KPIClearCVResultroll = KPIFund(Yroll, y_clearCVroll, KPIParams, Data)
            else:
                # Before: KPIClearCVResultroll = KPIFund(Yroll[:(Yroll.shape[0]-firstElem)], y_clearCVroll, KPIParams, Data)
                KPIClearCVResultroll = KPIFund(Yroll, y_clearCVroll, KPIParams, Data)
            # Experimental. Calculate PR2 smooth across horizons to find trends.
            # PR2smooth = np.zeros([min(rollhorend,Y.shape[0]-X.shape[1])-1,1])
            PR2smooth = np.zeros([rollhorend, 1])*np.nan
            Predictedstylesmooth = {}
            for i in range(rollhorstart, min(rollhorend + 1, Y.shape[0] - X.shape[1])):
                data = {'X': X, 'Y': Y}
                # RollXY expects first Y then X but below we only care about Y
                Data = pd.concat([Data.iloc[:X.shape[0], [0]], pd.DataFrame(Y), pd.DataFrame(Y)],
                                 axis=1).reset_index(drop=True)
                rolldata = RollXY(Data, Parameters, 1, i)
                Yroll = (np.asmatrix(rolldata.get('Yroll')))
                data = {'X': y_clearCV, 'Y': y_clearCV}
                Data = pd.concat([Data.iloc[:y_clearCV.shape[0], [0]], pd.DataFrame(y_clearCV), pd.DataFrame(y_clearCV)],
                                 axis=1).reset_index(drop=True)
                dataclearCVroll = RollXY(Data, Parameters, 1, i)
                y_clearCVroll = dataclearCVroll['Yroll']
                # Before: covresclearroll = GetCovarMatrix(np.size(Yroll[firstElem:], 0), 1)
                covresclearroll = GetCovarMatrix(np.size(Yroll, 0), 1)
                KPIParams = {'horizon': 1, 'vebose': 0, 'cov': covresclearroll}
                if Kxx > 0:
                    # Before: KPIClearCVResultrollall = KPIFund(Yroll[firstElem:], y_clearCVroll, KPIParams, Data)
                    KPIClearCVResultrollall = KPIFund(Yroll, y_clearCVroll, KPIParams, Data)
                else:
                    # Before: KPIClearCVResultrollall = KPIFund(Yroll[:-firstElem], y_clearCVroll, KPIParams, Data)
                    KPIClearCVResultrollall = KPIFund(Yroll, y_clearCVroll, KPIParams, Data)
                PR2smooth[i - 1] = KPIClearCVResultrollall['quality']
                Predictedstylesmooth = {**Predictedstylesmooth, 'predictedstylesmooth'+str(i):y_clearCVroll}
            # Average PR2 across horizons
            PR2SmoothAvg = np.mean(PR2smooth[~np.isnan(PR2smooth)])

    alphaols = [np.nan] if len(alphaols) == 0 else alphaols

    result = {'KPIUnsmoothResult': KPIUnsmoothResult,
              'KPISmoothResult': KPISmoothResult,
              'KPIClearCVResult': KPIClearCVResult, 'KPIClearCVResultroll': KPIClearCVResultroll,
              'alphaols': alphaols[-1], 'betas': betaols, 'stylesmooth': y_smoothols,
              'style': y_unsmoothols, 'predictedstyle': y_clearCV, 'styleOOS': y_unsmootholsOOS, 'predictedbetas': beta_1_est,
              'predictedalphas': alpha_1_est, **Predictedstylesmooth, 'PR2 Smooth': PR2smooth,
              'PR2 Smooth Avg': PR2SmoothAvg, 'T': T, 'Dates': Dates, 'ResidualUnsmooth': ResidualUnsmooth}

    return result
def Results(start, Description, ResultsAll, Yall, y, Data, DataOOS, i, j, numParamSet, Kx, Ky, Analyticsdata,
            Parameters, n, T, Factors, xshift, w, Point, doKPI, Regime, suffix):

    analytics2 = {}
    RollingStyleInFirstPoint = Parameters['RollingStyleInFirstPoint']
    RollingStyleInLastPoint = Parameters['RollingStyleInLastPoint']

    KPI = True
    if doKPI==True:
        analytics2 = {'stylesmooth': 'Style Smooth', 'ResidualUnsmooth': 'Residual Unsmooth'}
        predictedstylesmoothlist = [x for x in list(Analyticsdata.keys()) if any(sub in x for sub in ['predictedstylesmooth'])]
        predictedstylesmoothdict = dict((k, 'Predicted Style Smooth' + k[-1])
                                        for k in predictedstylesmoothlist if k in Analyticsdata)
        # predictedstylesmoothdict = dict((k, Analyticsdata[k]) for k in predictedstylesmoothlist if k in Analyticsdata)
        predictedanalytics2 = {'style': 'Style', 'predictedstyle': 'Predicted Style', **predictedstylesmoothdict}
    analyticsOOS = {'styleOOS': 'StyleOOS'}
    analytics3 = {'betas': 'Beta'}
    predictedanalytics3 = {'predictedbetas': 'Predicted Beta'}
    if doKPI == True:
        analytics2 = {**analytics2, **predictedanalytics2}
        analytics3 = {**analytics3, **predictedanalytics3}

    # Some of this is used for calibration
    if doKPI == True and KPI == True:
        ResultsAll.loc[j, 'R2 Mean' + suffix] = Analyticsdata['KPIUnsmoothResult']['quality']
        ResultsAll.loc[j, 'R2 Smooth Mean' + suffix] = Analyticsdata['KPISmoothResult']['quality']
        ResultsAll.loc[j, 'PR2 Mean' + suffix] = Analyticsdata['KPIClearCVResult']['quality']
        ResultsAll.loc[j, 'PR2 Smooth Avg Mean' + suffix] = Analyticsdata['PR2 Smooth Avg']
        ResultsAll.loc[j, 'PR2 Smooth Mean' + suffix] = Analyticsdata['KPIClearCVResultroll']['quality']
    ResultsAll.loc[j, 'Lamda Mean' + suffix] = Parameters['lam']
    ResultsAll.loc[j, 'Horizons Mean' + suffix] = Ky - 1
    ResultsAll.loc[j, 'Lags Mean' + suffix] = Kx - 1
    ResultsAll.loc[j, 'Alpha Mean' + suffix] = Analyticsdata['alphaols'][0]

    # Expand window level results to each time point.
    # Needed in order to later pick one of them to represent each window.
    for s in range(1, T + 1):
        colnumber = Data.loc[start + s + w - 1, 'Date']
        if Regime == True:
            # Check if loop date exists in the potentially reduced analytical data due to regimes
            if sum(pd.to_datetime(Analyticsdata['Dates']['Date'], errors='coerce') == pd.Timestamp(
                    Data.loc[start + s + w - 1, 'Date'])) > 0:
                ResultsAll.loc[j, 'Lamda ' + str(colnumber)] = Parameters['lam']
                ResultsAll.loc[j, 'Horizons ' + str(colnumber)] = Ky - 1
                ResultsAll.loc[j, 'Lags ' + str(colnumber)] = Kx - 1
                if KPI == True:
                    ResultsAll.loc[j, 'R2 Smooth ' + str(colnumber)] = Analyticsdata['KPISmoothResult']['quality']
                ResultsAll.loc[j, 'Alpha ' + str(colnumber)] = Analyticsdata['alphaols']
                if doKPI == True and KPI == True:
                    ResultsAll.loc[j, 'R2 ' + str(colnumber)] = Analyticsdata['KPIUnsmoothResult']['quality']
                    ResultsAll.loc[j, 'PR2 ' + str(colnumber)] = Analyticsdata['KPIClearCVResult']['quality']
                    ResultsAll.loc[j, 'PR2 Smooth Avg ' + str(colnumber)] = Analyticsdata['PR2 Smooth Avg']
                    ResultsAll.loc[j, 'PR2 Smooth ' + str(colnumber)] = Analyticsdata['KPIClearCVResultroll']['quality']
            else:
                ResultsAll.loc[j, 'Lamda ' + str(colnumber)] = np.nan
                ResultsAll.loc[j, 'Horizons ' + str(colnumber)] = np.nan
                ResultsAll.loc[j, 'Lags ' + str(colnumber)] = np.nan
                ResultsAll.loc[j, 'R2 Smooth ' + str(colnumber)] = np.nan
                ResultsAll.loc[j, 'Alpha ' + str(colnumber)] = np.nan
                if doKPI == True:
                    ResultsAll.loc[j, 'R2 ' + str(colnumber)] = np.nan
                    ResultsAll.loc[j, 'PR2 ' + str(colnumber)] = np.nan
                    ResultsAll.loc[j, 'PR2 Smooth Avg ' + str(colnumber)] = np.nan
                    ResultsAll.loc[j, 'PR2 Smooth ' + str(colnumber)] = np.nan
        else:
            ResultsAll.loc[j, 'Lamda ' + str(colnumber)] = Parameters['lam']
            ResultsAll.loc[j, 'Horizons ' + str(colnumber)] = Ky - 1
            ResultsAll.loc[j, 'Lags ' + str(colnumber)] = Kx - 1
            if KPI == True:
                ResultsAll.loc[j, 'R2 Smooth ' + str(colnumber)] = Analyticsdata['KPISmoothResult']['quality']
            ResultsAll.loc[j, 'Alpha ' + str(colnumber)] = Analyticsdata['alphaols'][0]
            if doKPI == True and KPI == True:
                ResultsAll.loc[j, 'R2 ' + str(colnumber)] = Analyticsdata['KPIUnsmoothResult']['quality']
                ResultsAll.loc[j, 'PR2 ' + str(colnumber)] = Analyticsdata['KPIClearCVResult']['quality']
                ResultsAll.loc[j, 'PR2 Smooth Avg ' + str(colnumber)] = Analyticsdata['PR2 Smooth Avg']
                ResultsAll.loc[j, 'PR2 Smooth ' + str(colnumber)] = Analyticsdata['KPIClearCVResultroll']['quality']

    # Obtain time series results
    for p in range(len(list(analytics2.values()))):
        analytic = list(analytics2.keys())[p]
        if KPI == True:
            Analytic = Analyticsdata[analytic]
            if Regime == True:
                # Expand potentially reduced analytical data due to regimes
                temp = pd.concat([Analyticsdata['Dates'], pd.DataFrame(Analytic)], axis=1)
                Analytic = pd.merge(Data.loc[start:end, 'Date'], temp, left_on='Date', right_on='Date', how='outer').iloc[:,
                           1]
            # Average values across all time points of a given window.
            col = list(analytics2.values())[p] + ' Mean' + suffix
            ResultsAll.loc[j, col] = Analytic.mean()
            for s in range(1, T + 1):
                colnumber = Data.loc[start + s + w - 1, 'Date']
                col = list(analytics2.values())[p] + ' ' + str(colnumber)
                # The below condition is there because smooth analytics have dimension less than T
                if s >= T - Analytic.size + 1:
                    ResultsAll.loc[j, col] = Analytic[s - 1 - (T - Analytic.size)][0] if \
                    isinstance(Analytic[s - 1 - (T - Analytic.size)], np.ndarray) else \
                    Analytic[s - 1 - (T - Analytic.size)]
                else:
                    ResultsAll.loc[j, col] = np.nan
    # Add the fund
    FundSum = 0
    for s in range(1, T + 1):
        colnumber = Data.loc[start + s + w - 1, 'Date']
        # Add the fund
        ResultsAll.loc[j, 'Fund' + ' ' + str(colnumber)] = Data.loc[start + s + w - 1, Data.columns[1]]
        FundSum += Data.loc[start + s + w - 1, Data.columns[1]]
    ResultsAll.loc[j, 'Fund Mean' + suffix] = FundSum / T

    if not DataOOS.empty:
        for p in range(len(list(analyticsOOS.values()))):
            analytic = list(analyticsOOS.keys())[p]
            if KPI == True:
                Analytic = Analyticsdata[analytic]
                s = 1
                colnumber = DataOOS.loc[0, 'Date']
                col = list(analyticsOOS.values())[p] + ' ' + str(colnumber)
                ResultsAll.loc[j, col] = np.nan if np.isnan(Analytic) else Analytic[0]
                # col = list(analyticsOOS.values())[p] + ' Mean ' + str(colnumber)
                # ResultsAll.loc[j, col] = np.nan if np.isnan(Analytic) else Analytic[0]
        # Add the fund
        ResultsAll.loc[j, 'FundOOS' + ' ' + str(colnumber)] = DataOOS.loc[0, DataOOS.columns[1]]

    for p in range(len(list(analytics3.values()))):
        for kk in range(1, n + 1):
            analytic = list(analytics3.keys())[p]
            if KPI == True:
                Analytic = Analyticsdata[analytic][:, kk - 1]
                if Regime == True:
                    # Expand potentially reduced analytical data due to regimes
                    temp = pd.concat([Analyticsdata['Dates'], pd.DataFrame(Analytic)], axis=1)
                    Analytic = pd.merge(Data.loc[start:end, 'Date'], temp, left_on='Date', right_on='Date',
                                        how='outer').iloc[:, 1]
                # Average values across all time points of a given window.
                col = list(analytics3.values())[p] + ' ' + Factors.columns[kk + xshift] + ' Mean' + suffix
                ResultsAll.loc[j, col] = Analytic.mean()
                for s in range(1, T + 1):
                    colnumber = Data.loc[start + s + w - 1, 'Date']
                    col = list(analytics3.values())[p] + ' ' + Factors.columns[kk + xshift] + ' ' + str(colnumber)
                    if s >= T - Analytic.shape[0] + 1:
                        # Temporary because predicted betas come out as array with dimension x,1
                        if p == 0:
                            ResultsAll.loc[j, col] = Analytic[s - 1 - (T - Analytic.shape[0])]
                        else:
                            ResultsAll.loc[j, col] = Analytic[s - 1 - (T - Analytic.shape[0]), 0]
                    else:
                        ResultsAll.loc[j, col] = np.nan

    # PR2 Smooth per horizon

    if doKPI == True and KPI == True:
        for s in range(Parameters.get('rollhorstart', 1), Parameters.get('rollhorend', 8) + 1):
            col = 'PR2 Smooth ' + str(s)
            if np.isnan(Analyticsdata['PR2 Smooth'][s - 1]):
                ResultsAll.loc[j, col] = np.nan
            elif isinstance(Analyticsdata['PR2 Smooth'][s-1], np.ndarray):
                ResultsAll.loc[j, col] = Analyticsdata['PR2 Smooth'][s - 1][0]
            else:
                ResultsAll.loc[j, col] = Analyticsdata['PR2 Smooth'][s - 1]

    return ResultsAll
def missing(Data, value):
    cols = Data.columns
    # Check for null, 0 etc
    if value == 'nan':
        data = Data.isnull()
    else:
        data = (Data == float(value))
    # https://stackoverflow.com/questions/47661565/find-consecutive-nans-in-pandas-dataframe
    temp = data.ne(data.shift())
    y1 = data.ne(data.shift()).cumsum()
    if isinstance(data, pd.DataFrame):
        y1 = y1.apply(lambda x: x.map(x.value_counts()))
    else:
        y1 = y1.map(y1.value_counts())
    ymiss = y1.where(data).reset_index(drop=True)
    ymiss.columns = cols

    return ymiss
def dorollold(dfinput, Parameters, horizon):
    # If any column with 'Date' in it exists, temporarily remove and put back at the end
    df = dfinput.copy()
    dfcols = df.columns
    cols = df.columns.tolist()
    cols = [x.lower() for x in df.columns]
    df.columns = cols
    check = df.columns.str.contains('date')
    if sum(check) != 0:
        temp = df[df.columns[check].tolist()]
        df = df.drop(df.columns[check].tolist(), axis=1)

    X = df.to_numpy()
    X = np.asmatrix(np.array(list(X), dtype=float)) / 100
    data = {'X': X, 'Y': X[:, 0]}
    dataroll = RollXY(data, Parameters, 1, horizon)
    dataroll = dataroll['Xroll']
    Dataroll = pd.DataFrame(data=dataroll,
                            index=df.index.values[horizon - 1:],
                            columns=df.columns)
    Dataroll = Dataroll * 100
    if sum(check) != 0:
        Dataroll = pd.merge(temp, Dataroll, left_index=True, right_index=True).reset_index(drop=True)

    # Put back original columns
    cols = dfcols.tolist()
    Dataroll.columns = cols

    return Dataroll
def doroll(dfinput, Parameters, lag, horizon):
    # If no column with 'Date' in it exists, temporarily generate random dates
    df = dfinput.copy()
    cols = [x.lower() for x in df.columns]
    df.columns = cols
    check = df.columns.str.contains('date')
    if sum(check) == 0:
        base = datetime.datetime.today()
        date_list = [base - datetime.timedelta(days=x) for x in range(df.shape[0])]
        temp = pd.DataFrame(date_list)
        temp.rename(columns={list(temp)[0]: 'Date'}, inplace=True)
        df = pd.concat([temp, df], axis=1)
    else:
        df.rename(columns={'date': 'Date'}, inplace=True)
    dataroll = RollXY(df, Parameters, lag, horizon)
    Dataroll = dataroll['Dataroll']
    if sum(check) != 0:
        Dataroll.iloc[:, 1:] = Dataroll.iloc[:, 1:]
    else:
        Dataroll.drop(['Date'], axis=1, inplace=True)
        Dataroll = Dataroll * 100

    Dataroll.columns = dfinput.columns

    return Dataroll
def savefile(results, filepath, filename, universe):
    # Assumes filename is saved as "parameters_results_univese" and csv
    filename = filename + '_' + universe + '_' + results + '.csv'
    file = filepath + '\\' + filename
    globals()[results].to_csv(file)
def findOccurrences(string, a):
    return [i for i, letter in enumerate(string) if letter == a]
def openfile(filepath, filename):
    # Assumes filename is in the form "parameters_results_univese" and csv
    file = filepath + '\\' + filename + '.csv'
    start = findOccurrences(filename, '_')[0]
    end = findOccurrences(filename, '_')[1]
    variable = filename[start + 1:end]
    # https://www.daniweb.com/programming/software-development/threads/111526/setting-a-string-as-a-variable-name#post548936
    globals()[variable] = pd.read_csv(file, header=0)

    return globals()[variable]
def openfile2(results, filepath, filename, universe):
    # Assumes filename is in the form "parameters_results_univese" and csv
    file = filepath + '\\' + filename + '_' + universe + '_' + results + '.csv'
    globals()[results] = pd.read_csv(file, header=0)

    return globals()[results]
def getDuplicateColumns(df):
    '''
    Get a list of duplicate columns.
    It will iterate over all the columns in dataframe and find the columns whose contents are duplicate.
    :param df: Dataframe object
    :return: List of columns whose contents are duplicates.
    '''
    duplicateColumnNames = set()
    # Iterate over all the columns in dataframe
    for x in range(df.shape[1]):
        # Select column at xth index.
        col = df.iloc[:, x]
        # Iterate over all the columns in DataFrame from (x+1)th index till end
        for y in range(x + 1, df.shape[1]):
            # Select column at yth index.
            otherCol = df.iloc[:, y]
            # Check if two columns at x 7 y index are equal
            if sum(df.iloc[:, x] - df.iloc[:, y]) == 0:
                duplicateColumnNames.add(df.columns.values[y])

    return list(duplicateColumnNames)
def Desmoothing(df):
    from statsmodels.regression.linear_model import OLS
    from statsmodels.stats.stattools import durbin_watson
    import statsmodels.api as sm

    Analytics = pd.DataFrame()
    Dates = df.iloc[:, [0]].reset_index(drop=True)
    dfwithlags = pd.merge(df, df.shift(1), left_index=True, right_index=True).iloc[1:, :]
    Dates = dfwithlags.iloc[:, [0]].reset_index(drop=True)
    Dates = df.iloc[:, [0]].reset_index(drop=True)
    Dates.rename(columns={list(Dates)[0]: 'Date'}, inplace=True)
    Desmoothed1 = Dates
    dfwithtwolags = pd.merge(dfwithlags, df.shift(2), left_index=True, right_index=True).iloc[1:, :]
    Dates2 = dfwithtwolags.iloc[:, [0]].reset_index(drop=True)
    Dates2 = df.iloc[:, [0]].reset_index(drop=True)
    Dates2.rename(columns={list(Dates2)[0]: 'Date'}, inplace=True)
    Desmoothed2 = Dates2
    for i in range(df.shape[1] - 1):

        data = df.iloc[:, i + 1].dropna().to_numpy()
        Analytics.loc[0, df.columns[i + 1]] = durbin_watson(data - np.mean(data), axis=0)

        # De-smoothing 1
        # =============================================================================
        #     if Analytics.loc[1,df.columns[i+1]]<1.05:
        # =============================================================================
        if Analytics.loc[0, df.columns[i + 1]] < 999:
            data = dfwithlags.iloc[:, [0, i + 1, df.shape[1] + i + 1]].dropna().reset_index(drop=True)
            data.rename(columns={list(data)[0]: 'Date'}, inplace=True)
            const = pd.DataFrame(1, index=np.arange(data.shape[0]), columns=['Constant'])
            x = pd.merge(const, data.iloc[:, 2], left_index=True, right_index=True)
            # model = OLS(data.iloc[:, 1].to_numpy(), x.to_numpy()).fit()
            model = OLS(np.asarray(data.iloc[:, 1]), np.asarray(x)).fit()
            beta = model.params[1]
            Analytics.loc[1, df.columns[i + 1]] = beta * 100
            Analytics.loc[2, df.columns[i + 1]] = model.rsquared * 100
            # Yo=(1-b)Yt+bYo-1 => Yt=(Yo - bYo-1)/(1-b)
            Desmoothed = ((data.iloc[:, 1] - beta * data.iloc[:, 2]) / (1 - beta)).to_frame().reset_index(drop=True)
            Desmoothed = pd.merge(data.iloc[:, [0]], Desmoothed, left_index=True, right_index=True)
        else:
            Analytics.loc[1, df.columns[i + 1]] = np.nan
            Analytics.loc[2, df.columns[i + 1]] = np.nan
            Desmoothed = df.iloc[:, [0, i + 1]]
        Desmoothed1 = pd.merge(Desmoothed1, Desmoothed, on='Date', how='left')
        Desmoothed1.rename(columns={list(Desmoothed1)[i + 1]: df.columns[i + 1]}, inplace=True)
        data = Desmoothed1.iloc[:, i + 1].dropna().to_numpy()
        Analytics.loc[3, df.columns[i + 1]] = durbin_watson(data - np.mean(data), axis=0)

        # De-smoothing 2
        # =============================================================================
        #     if Analytics.loc[5,df.columns[i+1]]<1.05:
        # =============================================================================
        if Analytics.loc[3, df.columns[i + 1]] < 999:
            data = dfwithtwolags.iloc[:, [0, i + 1, df.shape[1] + i + 1, 2 * df.shape[1] + i + 1]].dropna().reset_index(
                drop=True)
            data.rename(columns={list(data)[0]: 'Date'}, inplace=True)
            x = pd.merge(const, data.iloc[:, [2, 3]], left_index=True, right_index=True)
            model = OLS(np.asarray(data.iloc[:, 1]), np.asarray(x)).fit()
            beta1 = model.params[1]
            beta2 = model.params[2]
            Analytics.loc[4, df.columns[i + 1]] = beta1 * 100
            Analytics.loc[5, df.columns[i + 1]] = beta2 * 100
            Analytics.loc[6, df.columns[i + 1]] = model.rsquared * 100
            # Yo=(1-b1-b2)Yt+b1Yo-1 +b2Yo-2=> Yt=(Yo - b1Yo-1 - b2Yo-2)/(1-b1-b2)
            Desmoothed = ((data.iloc[:, 1] - beta1 * data.iloc[:, 2] - beta2 * data.iloc[:, 3]) / (
                    1 - beta1 - beta2)).to_frame().reset_index(drop=True)
            Desmoothed = pd.merge(data.iloc[:, [0]], Desmoothed, left_index=True, right_index=True)
        else:
            Analytics.loc[4, df.columns[i + 1]] = np.nan
            Analytics.loc[5, df.columns[i + 1]] = np.nan
            Analytics.loc[6, df.columns[i + 1]] = np.nan
            Desmoothed = df.iloc[:, [0, i + 1]]
        Desmoothed2 = pd.merge(Desmoothed2, Desmoothed, on='Date', how='left')
        Desmoothed2.rename(columns={list(Desmoothed2)[i + 1]: df.columns[i + 1]}, inplace=True)
        data = Desmoothed2.iloc[:, i + 1].dropna().to_numpy()
        Analytics.loc[7, df.columns[i + 1]] = durbin_watson(data, axis=0)

    Analyticst = Analytics.T
    cols = Analyticst.columns.tolist()
    temp = Analytics.index.to_frame()
    k = 0
    m = 0
    cols = ['Raw DW' if x == 0 else x for x in cols]
    cols = ['Lag Beta' if x == 1 else x for x in cols]
    cols = ['Desmooth1 R2' if x == 2 else x for x in cols]
    cols = ['Desmooth1 DW' if x == 3 else x for x in cols]
    cols = ['Lag1 Beta' if x == 4 else x for x in cols]
    cols = ['Lag2 Beta' if x == 5 else x for x in cols]
    cols = ['Desmooth2 R2' if x == 6 else x for x in cols]
    cols = ['Desmooth2 DW' if x == 7 else x for x in cols]
    Analyticst.columns = cols

    return [Desmoothed1, Desmoothed2, Analyticst]
def df_autocorr(df, lag=1, axis=0):
    """Compute full-sample column-wise autocorrelation for a DataFrame."""
    return df.apply(lambda col: col.autocorr(lag), axis=axis)
def portfolio_Annualized_performance(weights, mean_returns, cov_matrix, periods):
    returns = np.sum(mean_returns * weights) * periods
    std = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights))) * np.sqrt(periods)
    return std, returns
def random_portfolios(num_portfolios, mean_returns, cov_matrix, risk_free_rate, periods):
    results = np.zeros((3, num_portfolios))
    weights_record = []
    num_assets = len(mean_returns)
    for i in range(num_portfolios):
        weights = np.random.random(num_assets)
        weights /= np.sum(weights)
        weights_record.append(weights)
        portfolio_std_dev, portfolio_return = portfolio_Annualized_performance(weights, mean_returns, cov_matrix,
                                                                               periods)
        results[0, i] = portfolio_std_dev
        results[1, i] = portfolio_return
        results[2, i] = (portfolio_return - risk_free_rate) / portfolio_std_dev
    return results, weights_record
def neg_sharpe_ratio(weights, mean_returns, cov_matrix, risk_free_rate, periods):
    p_var, p_ret = portfolio_Annualized_performance(weights, mean_returns, cov_matrix, periods)
    return -(p_ret - risk_free_rate) / p_var
def max_sharpe_ratio(mean_returns, cov_matrix, risk_free_rate, periods):
    import scipy.optimize as sco
    num_assets = len(mean_returns)
    args = (mean_returns, cov_matrix, risk_free_rate, periods)
    constraints = ({'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bound = (0.0, 1.0)
    bounds = tuple(bound for asset in range(num_assets))
    result = sco.minimize(neg_sharpe_ratio, num_assets * [1. / num_assets, ], args=args,
                          method='SLSQP', bounds=bounds, constraints=constraints)
    return result
def portfolio_volatility(weights, mean_returns, cov_matrix, periods):
    return portfolio_Annualized_performance(weights, mean_returns, cov_matrix, periods)[0]
def min_variance(mean_returns, cov_matrix, periods, methodology='SLSQP'):
    import scipy.optimize as sco
    num_assets = len(mean_returns)
    args = (mean_returns, cov_matrix, periods)
    constraints = ({'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bound = (0.0, 1.0)
    bounds = tuple(bound for asset in range(num_assets))

    result = sco.minimize(portfolio_volatility, num_assets * [1. / num_assets, ], args=args,
                          method=methodology, bounds=bounds, constraints=constraints)
    return result
def efficient_return(mean_returns, cov_matrix, target, periods, methodology='SLSQP'):
    import scipy.optimize as sco
    num_assets = len(mean_returns)
    args = (mean_returns, cov_matrix, periods)

    def portfolio_return(weights, periods):
        return portfolio_Annualized_performance(weights, mean_returns, cov_matrix, periods)[1]

    constraints = ({'type': 'eq', 'fun': lambda x: portfolio_return(x, periods) - target},
                   {'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bounds = tuple((0, 1) for asset in range(num_assets))
    result = sco.minimize(portfolio_volatility, num_assets * [1. / num_assets, ], args=args, method=methodology,
                          bounds=bounds, constraints=constraints)
    return result
def efficient_frontier(mean_returns, cov_matrix, returns_range, periods):
    efficients = []
    for ret in returns_range:
        efficients.append(efficient_return(mean_returns, cov_matrix, ret, periods))
    return efficients
def Analytical(Parameters, mean_returns, Datasetroll, periods, l, h, hh, HorizonsMax, prefix, suffix):
    # hh = '' if HorizonsMax == 2 else ' h' + str(h)

    mean_return = pd.DataFrame(np.round([mean_returns], 4), columns=mean_returns.index.tolist(),
                               index=['Avg Return' + prefix + suffix + hh]).T
    skewness = pd.DataFrame(np.round([Datasetroll.skew()], 4), columns=mean_returns.index.tolist(),
                            index=['Skewness' + prefix + suffix + hh]).T
    kurtosis = pd.DataFrame(np.round([Datasetroll.kurtosis()], 4), columns=mean_returns.index.tolist(),
                            index=['Kurtosis' + prefix + suffix + hh]).T
    cov_matrix = Datasetroll.cov() / covmultiplier(h, 1)
    # New
    cov_matrix = Datasetroll.cov() / covmultiplier(h, l)
    covyx = pd.DataFrame(cov_matrix.iloc[0, -1], columns=[Datasetroll.columns.tolist()[1]], \
                         index=['covyx' + prefix + suffix + hh]).T
    temp = np.diagonal(cov_matrix)
    var = pd.DataFrame(np.round([temp], 4), columns=Datasetroll.columns.tolist(),
                       index=['Var' + prefix + suffix + hh]).T
    temp = np.sqrt(np.diagonal(cov_matrix))
    stdev = pd.DataFrame(np.round([temp], 4), columns=Datasetroll.columns.tolist(),
                         index=['Stdev' + prefix + suffix + hh]).T
    serialcorr = pd.DataFrame(np.round([df_autocorr(Datasetroll, lag=1, axis=0)], 4),
                              columns=Datasetroll.columns.tolist(), index=['Serial Corr' + prefix + suffix + hh]).T
    temp = cov_matrix.copy()
    np.fill_diagonal(temp.values, np.nan)
    median_cov = pd.DataFrame(np.round([temp.median()], 4), columns=Datasetroll.columns.tolist(),
                              index=['Median Cov' + prefix + suffix + hh]).T
    avg_cov = pd.DataFrame(np.round([temp.mean()], 4), columns=Datasetroll.columns.tolist(),
                           index=['Avg Cov' + prefix + suffix + hh]).T
    stdev_cov = pd.DataFrame(np.round([temp.std()], 4), columns=Datasetroll.columns.tolist(),
                             index=['Stdev Cov' + prefix + suffix + hh]).T
    corr_matrix = Datasetroll.corr()
    np.fill_diagonal(corr_matrix.values, np.nan)
    mean_corr = pd.DataFrame(np.round([corr_matrix.mean()], 4), columns=Datasetroll.columns.tolist(),
                             index=['Avg Corr' + prefix + suffix + hh]).T
    stdev_corr = pd.DataFrame(np.round([corr_matrix.std()], 4), columns=Datasetroll.columns.tolist(),
                              index=['Stdev Corr' + prefix + suffix + hh]).T

    data_frames = [mean_return, var, stdev, serialcorr, avg_cov, median_cov, stdev_cov, mean_corr, stdev_corr, skewness,
                   kurtosis, covyx]
    Analytics = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'),
                       data_frames)

    return Analytics
def efficientfrontier(Parameters, file, fileshort, filenameshort, Dataset, mean_returns, minreturn, maxreturn, meancalc,
                      horizonlist, num_portfolios, risk_free_rate, periods, prefix, dochart, covariance):
    Analyticsall = pd.DataFrame()
    Analyticsalllong = pd.DataFrame()
    Allocations = []
    Allocationslong = []
    HorizonsMax = horizonlist[-1]

    for h in horizonlist:
        divider = 1
        if Parameters.get('WeightAvg', 1) == 2:
            divider = h
        if Parameters.get('WeightAvg', 1) == 3:
            divider = np.sqrt(h)

        hh = ''
        if (prefix not in (' d1', ' d2')) or (prefix in (' d1', ' d2') and h > 1):
            hh = ' h' + str(h)
        Datasetroll = doroll(Dataset, Parameters, 1, h)
        # New. Make sure Y is first column below.
        Datasetroll = doroll(Dataset, Parameters, 1, h)
        Datasetroll['Date'] = pd.to_datetime(Datasetroll['Date'], errors='coerce').dt.date
        Datasetroll = Datasetroll.set_index('Date')
        # New
        # Datasetroll = pd.merge(Dataset.iloc[:,[0,1]],Datasetroll.iloc[:,[0]+list(range(2,Datasetroll.shape[1]))], \
        #                        left_on='Date',right_on='Date', how='inner')
        # Datasetroll.iloc[:,1] = Datasetroll.iloc[:,1]*100
        # End new
        Datasetroll.columns = [col for col in Datasetroll.columns]
        if meancalc == True: mean_returns = Datasetroll.mean() * divider / h
        Analytics = Analytical(Parameters, mean_returns, Datasetroll, periods, 1, h, hh, HorizonsMax, prefix, ' OV')
        Analyticsall = pd.concat([Analyticsall, Analytics], axis=1)

        Datasetlong = Dataset.groupby(Dataset.index // h).sum() / divider
        Dateslong = Dataset.loc[h - 1::h, 'Date'].reset_index(drop=True)
        Datasetlong = pd.merge(Dateslong, Datasetlong, left_index=True, right_index=True)
        Datasetlong['Date'] = pd.to_datetime(Datasetlong['Date'], errors='coerce').dt.date
        Datasetlong = Datasetlong.set_index('Date')
        Datasetlong.columns = [col for col in Datasetlong.columns]
        if meancalc == True:
            mean_returns_long = Datasetlong.mean() * divider / h
        else:
            mean_returns_long = mean_returns
        Analyticslong = Analytical(Parameters, mean_returns_long, Datasetlong, periods, 1, h, hh, HorizonsMax, prefix, '')
        Analyticsalllong = pd.concat([Analyticsalllong, Analyticslong], axis=1)

        if (prefix not in (' d1', ' d2')) or (prefix in (' d1', ' d2') and h > 1):
            file = fileshort + ' OV'
            filename = filenameshort + ' OV'
            if Datasetroll.shape[1] > 1 or covariance.shape[0] > 0:
                covarianceused = covariance if covariance.shape[0] > 0 else Datasetroll.cov() * divider / h
                if dochart == True:
                    if num_portfolios > 1:
                        display_calculated_ef_with_random(file, filename, mean_returns, covarianceused,
                                                          num_portfolios, risk_free_rate, periods, minreturn, maxreturn,
                                                          prefix, h)
                    display_ef_with_selected(Parameters, file, filename, mean_returns, covarianceused, risk_free_rate, periods,
                                             minreturn, maxreturn, prefix, h)
                Allocation = display_ef_allocations(Parameters, file, filename, mean_returns, covarianceused, risk_free_rate,
                                                    periods, minreturn, maxreturn, prefix, h, dochart)
                Allocations.append(Allocation)

        file = fileshort
        filename = filenameshort
        if Datasetlong.shape[1] > 1:
            if dochart == True:
                if num_portfolios > 1:
                    display_calculated_ef_with_random(Parameters, file, filename, mean_returns_long, Datasetlong.cov(),
                                                      num_portfolios, risk_free_rate, periods, minreturn, maxreturn,
                                                      prefix, h)
                display_ef_with_selected(Parameters, file, filename, mean_returns_long, Datasetlong.cov(), risk_free_rate, periods,
                                         minreturn, maxreturn, prefix, h)
            Allocation = display_ef_allocations(Parameters, file, filename, mean_returns_long, Datasetlong.cov(), risk_free_rate,
                                                periods, minreturn, maxreturn, prefix, h, dochart)
            Allocationslong.append(Allocation)

    # =============================================================================
    #     Analyticsall = Analyticsall.reindex(sorted(Analyticsall.columns), axis=1)
    #     Analyticsalllong = Analyticsalllong.reindex(sorted(Analyticsalllong.columns), axis=1)
    # =============================================================================
    if prefix in (' d1', ' d2') and h == 1: Allocations = Allocationslong
    if dochart == False: return [Analyticsall, Analyticsalllong, Allocations, Allocationslong]
def randomgenerator(nreturns, nassets, nstale, kstdev, indexrange, startdate,
                    staleness, correlation, MA, spikes):
    from scipy.stats import norm
    from scipy.linalg import cholesky
    # Generates dates and random numbers between 0 and 1
    columnnames = []
    for i in range(nassets):
        columnnames = columnnames + ['Random' + str(i)]
    randomdata = np.random.rand(nreturns, nassets)
    datelist = pd.bdate_range(pd.to_datetime(startdate, errors='coerce'), periods=nreturns).to_pydatetime().tolist()
    Randomdata = pd.DataFrame(norm.ppf(randomdata), columns=columnnames, index=indexrange) * kstdev

    r = np.ones((nassets, nassets)) * correlation
    np.fill_diagonal(r, 1)
    c = cholesky(r, lower=True)
    Randomdata = pd.DataFrame(np.dot(c, Randomdata.T).T / 100, columns=columnnames, index=indexrange)

    Randomdata_stale = pd.DataFrame().reindex_like(Randomdata.iloc[:, :nstale])
    Randomdata_stale.iloc[0, :] = Randomdata.iloc[0, :nstale]
    # Randomdata_MA = pd.DataFrame().reindex_like(Randomdata.iloc[:, :nstale])
    # Randomdata_MA.iloc[0, :] = Randomdata.iloc[0, :nstale]
    WeightAvg = 2
    Randomdata_MA = np.zeros([nreturns, nstale])
    Y = Randomdata.iloc[:, 0:nstale].values
    import random
    stalenumber = staleness
    for i in range(Randomdata.shape[0]):
        if i > 0:
            if staleness == 'random':
                stalenumber = random.uniform(0, 1)
            Randomdata_stale.iloc[i, :] = (1 - stalenumber) * Randomdata.iloc[i,
                                                              :nstale] + stalenumber * Randomdata_stale.iloc[i - 1, :]
        if i >= MA - 1:
            Randomdata_MA[i, :] = np.matmul(Y[i - MA + 1:i + 1, :].T, Lagweights(MA, 0, WeightAvg))
        else:
            Randomdata_MA[i, :] = np.nan

    # Add spikes
    if spikes == True:
        for i in range(Randomdata.shape[0]):
            if np.round(i / 5, 0) == i / 5:
                shock = random.uniform(0, 1) * 0.015
                Randomdata_stale.iloc[i, :] = Randomdata_stale.iloc[i, :] - shock
                Randomdata_stale.iloc[i - 1, :] = Randomdata_stale.iloc[i - 1, :] + shock

    Randomdata_stale = pd.concat([Randomdata_stale.reset_index(drop=True), Randomdata.iloc[:, nstale:]], axis=1)
    Randomdata_MA = pd.concat([pd.DataFrame(Randomdata_MA, columns=Randomdata.columns[:nstale]), Randomdata.iloc[:, nstale:]], axis=1)

    Randomdata = pd.concat([pd.DataFrame(datelist), Randomdata], axis=1)
    Randomdata_stale = pd.concat([pd.DataFrame(datelist), Randomdata_stale], axis=1)
    Randomdata_MA = pd.concat([pd.DataFrame(datelist), Randomdata_MA], axis=1)
    Randomdata.rename(columns={list(Randomdata)[0]: 'Date'}, inplace=True)
    Randomdata_stale.rename(columns={list(Randomdata_stale)[0]: 'Date'}, inplace=True)
    Randomdata_MA.rename(columns={list(Randomdata_MA)[0]: 'Date'}, inplace=True)
    Randomdata['Date'] = pd.to_datetime(Randomdata['Date'], errors='coerce').dt.date
    Randomdata_stale['Date'] = pd.to_datetime(Randomdata_stale['Date'], errors='coerce').dt.date
    Randomdata_MA['Date'] = pd.to_datetime(Randomdata_MA['Date'], errors='coerce').dt.date
    Randomdata_MA = Randomdata_MA.dropna()

    return [Randomdata, Randomdata_stale, Randomdata_MA]
def covmultiplierold(horizon, lag):
    multiplier = 0
    if lag > 1:
        for i in range(horizon):
            for j in range(horizon):
                multiplier = multiplier + 1 - abs(i - j) / lag
    else:
        # for i in range(horizon):
        #     for j in range(horizon):
        #         multiplier = multiplier + 1 - abs(i - j) / horizon
        # multiplier=multiplier/horizon
        multiplier = horizon

    return multiplier
def covmultiplier(horizon, lag):
    multiplier = 0
    lag = abs(lag)
    for i in range(horizon):
        for j in range(lag):
            for k in range(horizon):
                if i == j + k: multiplier = multiplier + 1

    return multiplier
def rolling_window(a, window):
    '''
    Use Numpy's stride tricks to create a rolling window over array a
    Args:
        a (ndarray): Numpy array of values to calculate rolling window over
        window (int): Width of window
    Returns:
        ndarray: Array of rolling values
    '''
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
def solve_for_asset_value(corp_data, header_map, time_horizon, horizon, min_hist_vals, periods):
    '''
    https://github.com/bradfordlynch/p-def-aws-lambda/blob/master/probability_of_default_tools.py
    Solves for a firm's asset value based on a time history of the firm's equity
    value, debt level, and risk-free rate.
    Args:
        corp_data (ndarray): Numpy array of company data (Return, Equity value, face value of debt, and risk-free rate)
        header_map (dict): Map of column name to the data column index in corp_data
        time_horizon (list): List of time horizons (In years) to calculate asset value for
        min_hist_vals (int): Minimum number of days to use for calculating historical data
    Returns:
        ndarray: Numpy array of time-series of asset values
    '''

    # Validation data
    # corp_data = np.arange(4 * 10).reshape(10, 4)
    # header_map = {'RET': 0, 'mkt_val': 1, 'face_value_debt': 2, 'DGS1': 3}
    # time_horizon = [1]
    # min_hist_vals = 5

    # days_in_year = 252
    days_in_year = periods

    # Difference between actual and theoretical equity price given asset value
    # Solves for asset value v_a such that v_e = (v_a * norm.cdf(d1) - np.exp(-r_f * T) * face_val_debt * norm.cdf(d2))
    # given y1 is 0
    def equations(v_a, debug=False):
        d1 = (np.log(v_a / face_val_debt) + (r_f + 0.5 * sigma_a ** 2) * T) / (sigma_a * np.sqrt(T))
        d2 = d1 - sigma_a * np.sqrt(T)
        # Equity is a call option on the firm's assets
        y1 = v_e - (v_a * norm.cdf(d1) - np.exp(-r_f * T) * face_val_debt * norm.cdf(d2))

        if debug:
            print("d1 = {:.6f}".format(d1))
            print("d2 = {:.6f}".format(d2))
            print("Error = {:.6f}".format(y1))

        return y1

    # Set window width for calculating historical data
    win = min_hist_vals
    # win = 5

    # Set start point of historical data. Asset values are calculated after that.
    start_time = min_hist_vals
    # range start end
    timesteps = range(min_hist_vals, len(corp_data))

    # Calculate historical volatility
    # find out which column (number) in corp_data contains returns
    ret_col = header_map['RET']
    sigma_e = np.zeros((corp_data.shape[0]))
    sigma_e[:win - 1] = np.nan
    # Rolling window of stdev based on win (window) size
    sigma_e[win - 1:] = np.std(rolling_window(np.log(corp_data[:, ret_col] + 1), win), axis=-1)
    # Validation
    # temp = np.arange(10)
    # np.std(rolling_window(np.log(temp + 1), 5), axis=-1)
    # np.std(np.log(temp + 1)[:5])

    assert type(time_horizon) in [list, tuple], "time_horizon must be a list"

    # Create array for storing results
    results = np.zeros((corp_data.shape[0], len(time_horizon)))
    for i, years in enumerate(time_horizon):
        T = days_in_year * years
        # Set initial guess for firm value equal to the equity value
        results[:, i] = corp_data[:, header_map['mkt_val']] + corp_data[:, header_map['face_value_debt']]

        # Run through all days
        for i_t, t in enumerate(timesteps):
            # Check if the company is levered
            if corp_data[t, header_map['face_value_debt']] > 1e-10:
                # Company is levered, calculate probability of default
                # Calculate initial guess at sigma_a
                v_a_per = results[t - win:t, i]
                # np.roll shifts array by 1 and places last element as first, which is then set to nan
                # if v_a_per.shape[0]>0:
                # print(str(i_t) + ' i_t, ' + str(t) + ' t, min ' + str(np.min(v_a_per / np.roll(v_a_per, 1))) + \
                #       ', max ' + str(np.max(v_a_per / np.roll(v_a_per, 1))))
                v_a_ret = np.log(v_a_per / np.roll(v_a_per, 1))
                v_a_ret[0] = np.nan
                # nanstd ignores nans
                sigma_a = np.nanstd(v_a_ret)

                if i_t == 0:
                    subset_timesteps = range(t - win, t + 1)
                else:
                    # subset_timesteps = corp_data.loc[t-pd.Timedelta(20,'D'):t].index
                    subset_timesteps = [t]

                # Iterate on previous values of V_a
                n_its = 0
                while n_its < 10:
                    n_its += 1
                    # Loop over timesteps, calculating Va using initial guess for sigma_a
                    for t_sub in subset_timesteps:
                        # r_f = (1 + corp_data[t_sub, header_map['DGS1']]) ** (1.0 / 365) - 1
                        r_f = (1 + corp_data[t_sub, header_map['DGS1']]) ** (1.0 / 12) - 1
                        v_e = corp_data[t_sub, header_map['mkt_val']]
                        face_val_debt = corp_data[t_sub, header_map['face_value_debt']]
                        sol = sco.root(equations, results[t_sub, i])
                        results[t_sub, i] = sol['x'][0]

                    # Update sigma_a based on new values of Va
                    last_sigma_a = sigma_a
                    v_a_per = results[t - win:t, i]
                    # print(str(i_t) + ' i_t, ' + str(t) + ' t, min ' + str(np.min(v_a_per / np.roll(v_a_per, 1))) + \
                    #         ', max ' + str(np.max(v_a_per / np.roll(v_a_per, 1))))
                    # print(v_a_per / np.roll(v_a_per, 1))
                    v_a_ret = np.log(v_a_per / np.roll(v_a_per, 1))
                    v_a_ret[0] = np.nan
                    sigma_a = np.nanstd(v_a_ret)

                    if abs(last_sigma_a - sigma_a) < 1e-3:
                        # corp_data.loc[t_sub, 'sigma_a'] = sigma_a
                        break
            else:
                # Company is unlevered, Va = Ve
                pass

        return results
def QuantileBMcalc(QuantileR, i, j, AnalyticQ, FundamentalsR, Factorgroupvalue, GroupNovalue, FundamentalsR_Eq_unique, Date,TotalReturn,ExcessReturn):
    QuantileR.loc[i + j, 'AnalyticQ'] = AnalyticQ[j]
    # Create index out of all bonds
    QuantileR.loc[i + j, 'Date'] = Date
    if FundamentalsR.shape[0] > 0:
        QuantileR.loc[i + j, 'All bonds mean' + Factorgroupvalue + GroupNovalue] = FundamentalsR[TotalReturn].mean()
        QuantileR.loc[i + j, 'All bonds weighted' + Factorgroupvalue + GroupNovalue] = \
            (FundamentalsR['Weight'] * FundamentalsR[TotalReturn]).sum() / FundamentalsR['Weight'].sum()
        QuantileR.loc[i + j, 'All bonds mean excess' + Factorgroupvalue + GroupNovalue] = FundamentalsR[
            ExcessReturn].mean()
        QuantileR.loc[i + j, 'All bonds weighted excess' + Factorgroupvalue + GroupNovalue] = \
            (FundamentalsR['Weight'] * FundamentalsR[ExcessReturn]).sum() / FundamentalsR['Weight'].sum()
    else:
        QuantileR.loc[i + j, 'All bonds mean' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'All bonds weighted' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'All bonds mean excess' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'All bonds weighted excess' + Factorgroupvalue + GroupNovalue] = np.nan

        # Create index out of all unique issuer bonds
    if FundamentalsR_Eq_unique.shape[0]>0:
        QuantileR.loc[i + j, 'Issuer all unique bonds mean' + Factorgroupvalue + GroupNovalue] = FundamentalsR_Eq_unique[
            TotalReturn].mean()
        QuantileR.loc[i + j, 'Issuer all unique bonds weighted' + Factorgroupvalue + GroupNovalue] = \
            (FundamentalsR_Eq_unique['Weight'] * FundamentalsR_Eq_unique[TotalReturn]).sum() / FundamentalsR_Eq_unique[
                'Weight'].sum()
        QuantileR.loc[i + j, 'Issuer all unique bonds mean excess' + Factorgroupvalue + GroupNovalue] = \
            FundamentalsR_Eq_unique[ExcessReturn].mean()
        QuantileR.loc[i + j, 'Issuer all unique bonds weighted excess' + Factorgroupvalue + GroupNovalue] = \
            (FundamentalsR_Eq_unique['Weight'] * FundamentalsR_Eq_unique[ExcessReturn]).sum() / FundamentalsR_Eq_unique[
                'Weight'].sum()
    else:
        QuantileR.loc[i + j, 'Issuer all unique bonds mean' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'Issuer all unique bonds weighted' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'Issuer all unique bonds mean excess' + Factorgroupvalue + GroupNovalue] = np.nan
        QuantileR.loc[i + j, 'Issuer all unique bonds weighted excess' + Factorgroupvalue + GroupNovalue] = np.nan

    return QuantileR
def Quantilecalc(i, j, FundamentalsR, AnalyticQ, AnalyticQreverse, Nquantile, QuantileR, Quantiles,
                 Factorgroupvalue, GroupNovalue, AnalyticRM, AnalyticRS, suffix, Date,TotalReturn,ExcessReturn):
    QuantileR.loc[i + j, 'AnalyticQ'] = AnalyticQ[j]
    QuantileR.loc[i + j, 'Date'] = Date
    # Filter out issuers with NA AnalyticQ
    QuantileR.loc[i + j, 'Total positions' + Factorgroupvalue + GroupNovalue + suffix] = FundamentalsR.shape[0]
    FundamentalsR_full = FundamentalsR[FundamentalsR[AnalyticQ[j]].notnull()].reset_index(drop=True)
    QuantileR.loc[i + j, 'NA AnalyticQ' + Factorgroupvalue + GroupNovalue + suffix] = FundamentalsR.shape[0] - FundamentalsR_full.shape[0]
    # Filter out issuers with negative AnalyticQ except for Issuer CRV
    start = FundamentalsR_full.shape[0]
    if 'CRV' not in AnalyticQ[j]:
        # print('i=' + str(i))
        # print('Date=' + Date.__str__())
        # print('Analytic=' + AnalyticQ[j])
        FundamentalsR_full = FundamentalsR_full[FundamentalsR_full[AnalyticQ[j]].astype('float') > 0].reset_index(drop=True)
    QuantileR.loc[i + j, 'Negative AnalyticQ' + Factorgroupvalue + GroupNovalue + suffix] = start - FundamentalsR_full.shape[0]
    Quantileflag = 0
    # We may not be left with enough unique values to calculate N quantiles. We need at least 20 bonds.
    if FundamentalsR_full.shape[0] > 20 and len(FundamentalsR_full[AnalyticQ[j]].unique()) >= Nquantile:
        Quantileflag = 1
        FundamentalsR_full[AnalyticQ[j]] = pd.to_numeric(FundamentalsR_full[AnalyticQ[j]])
        # Populate analytic quantiles. Check if same as AlphaTesting. Small quantile means high analytic.
        # First, reverse ranks
        if AnalyticQ[j] in AnalyticQreverse:
            FundamentalsR_full[AnalyticQ[j]] = FundamentalsR_full[AnalyticQ[j]].max() - FundamentalsR_full[AnalyticQ[j]]
        # FundamentalsR_full[AnalyticQ[j] + ' quantile'] = Nquantile - 1 - \
        #                     pd.qcut(FundamentalsR_full[AnalyticQ[j]],Nquantile, labels=False, duplicates='drop')
        # pd.cut(FundamentalsR_full[AnalyticQ[j]], [-np.inf, 5, np.inf], labels=tuple(range(Nquantile)))
        # Sort by small to large
        FundamentalsR_full = FundamentalsR_full.sort_values(by=[AnalyticQ[j]]).reset_index(drop=True)
        idx = np.round(np.linspace(0, FundamentalsR_full[AnalyticQ[j]].shape[0] - 1, Nquantile + 1)).astype(int)
        idxAll = np.round(np.linspace(0, FundamentalsR_full[AnalyticQ[j]].shape[0] - 1, FundamentalsR_full[AnalyticQ[j]].shape[0] + 1)).astype(int)
        for q in range(len(idx) - 1):
            FundamentalsR_full.loc[idx[q]:idx[q + 1], AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] = int(q)
        for q in range(len(idxAll) - 1):
            FundamentalsR_full.loc[idxAll[q]:idxAll[q + 1], AnalyticQ[j] + ' quantile_all' + Factorgroupvalue + GroupNovalue + suffix] = int(q) + 1
            FundamentalsR_full.loc[idxAll[q]:idxAll[q + 1], AnalyticQ[j] + ' quantile_all_norm' + Factorgroupvalue + GroupNovalue + suffix] = int(q) / (len(idxAll) - 1)
        # The below assumes that the combination of ISIN and Symbol is unique.
        # If not, FundamentalsR will eventually explode in terms of rows.
        FundamentalsR = pd.merge(FundamentalsR,
                        FundamentalsR_full[['ISIN', 'Symbol', AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix,
                        AnalyticQ[j] + ' quantile_all' + Factorgroupvalue + GroupNovalue + suffix,
                        AnalyticQ[j] + ' quantile_all_norm' + Factorgroupvalue + GroupNovalue + suffix]],
                        left_on = ['ISIN','Symbol'], right_on = ['ISIN','Symbol'], how = 'left')
        # Create index out of remaining bonds
        QuantileR.loc[i + j, 'Remaining bonds mean' + Factorgroupvalue + GroupNovalue + suffix] = \
            FundamentalsR_full[TotalReturn].mean()
        QuantileR.loc[i + j, 'Remaining bonds weighted' + Factorgroupvalue + GroupNovalue + suffix] = \
            (FundamentalsR_full['Weight'] * FundamentalsR_full[TotalReturn]).sum() / FundamentalsR_full['Weight'].sum()
        QuantileR.loc[i + j, 'Remaining bonds mean excess' + Factorgroupvalue + GroupNovalue + suffix] = \
            FundamentalsR_full[ExcessReturn].mean()
        QuantileR.loc[i + j, 'Remaining bonds weighted excess' + Factorgroupvalue + GroupNovalue + suffix] = \
            (FundamentalsR_full['Weight'] * FundamentalsR_full[ExcessReturn]).sum() / FundamentalsR_full['Weight'].sum()
        # Form quantile portfolios
        for k in range(Nquantile):
            DataQ = FundamentalsR_full[FundamentalsR_full[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k]
            # Report various analytics
            for m in range(len(AnalyticRM)):
                QuantileR.loc[i + j, 'Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + '-' + str(
                    Quantiles[k + 1]) + ' ' + AnalyticRM[m] + ' mean'] = DataQ[AnalyticRM[m]].mean()
                QuantileR.loc[i + j, 'Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + '-' + str(
                        Quantiles[k + 1]) + ' ' + AnalyticRM[m] + ' weighted'] = \
                    (DataQ['Weight'] * DataQ[AnalyticRM[m]]).sum() / DataQ['Weight'].sum()
            for m in range(len(AnalyticRS)):
                QuantileR.loc[i + j, 'Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + '-' + str(
                    Quantiles[k + 1]) + ' ' + AnalyticRS[m] + ' sum'] = DataQ[AnalyticRS[m]].sum()
            # for m in range(len(DataQ['ML Indstry Level 4'].unique())):
            #     QuantileR.loc[i+j, 'Quantile ' + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + ' ' + \
            #               DataQ['ML Indstry Level 4'].unique()[m] + ' count'] \
            #             = DataQ['ML Indstry Level 4'].count()
            #     QuantileR.loc[i+j, 'Quantile ' + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + ' ' + \
            #               DataQ['ML Indstry Level 4'].unique()[m] + ' reweight sum'] \
            #     = DataQ.loc[DataQ['ML Indstry Level 4'] == DataQ['ML Indstry Level 4'].unique()[m], 'Weight'].sum() \
            #       / DataQ['Weight'].sum()
    else:
        FundamentalsR[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
        FundamentalsR[AnalyticQ[j] + ' quantile_all' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
        FundamentalsR[AnalyticQ[j] + ' quantile_all_norm' + Factorgroupvalue + GroupNovalue + suffix] = np.nan

    return [QuantileR, Quantileflag, FundamentalsR]
def OAStcalc(Yfits, col):
    d2 = np.array([Yfits[col].to_numpy()]).T
    T = np.array([(Yfits['T'] * 252).to_numpy()]).T
    R = np.array([(Yfits['R']).to_numpy()]).T
    lamda = np.array([(Yfits['lamda']).to_numpy()]).T
    corr = np.array([(Yfits['corr']).to_numpy()]).T
    PD = norm.cdf(-d2)
    CPD = 1 - (1 - PD) ** (T / 252)
    CQDF = norm.cdf(norm.ppf(CPD) + lamda * np.sqrt((corr ** 2) * T / 252))
    OASt = (-1 / (T / 252)) * np.log(1 - (1 - R) * CQDF) * 10000

    return d2, PD, CPD, CQDF, OASt
def turnoverCalc(Fnow, Fprior):
    Fnow['Weight'] = Fnow['Weight'] / Fnow['Weight'].sum()
    Fnow['Equal'] = 1 / Fnow.dropna().shape[0]
    Fprior['Weight'] = Fprior['Weight'] / Fprior['Weight'].sum()
    Fprior['Equal'] = 1 / Fprior.dropna().shape[0]
    Fall = pd.merge(Fnow, Fprior, left_on='ISIN', right_on='ISIN', how='left')
    Fall['Weight_y'] = Fall['Weight_y'].replace(np.nan, 0)
    Fall['Weight_diff'] = Fall['Weight_x'] - Fall['Weight_y']
    Fall.loc[Fall['Weight_diff'] < 0, 'Weight_diff'] = 0
    Fall['Equal_y'] = Fall['Equal_y'].replace(np.nan, 0)
    Fall['Equal_diff'] = Fall['Equal_x'] - Fall['Equal_y']
    Fall.loc[Fall['Equal_diff'] < 0, 'Equal_diff'] = 0

    turnover_weighted = Fall['Weight_diff'].sum()
    turnover_equal = Fall['Equal_diff'].sum()

    return turnover_weighted, turnover_equal
def translator(Fundamentals_table):

    Maturity = 'Maturty_Date' if Fundamentals_table[:9] == 'Corporate' else 'Maturity Date'
    Duration = 'Dur_Eff' if Fundamentals_table[:9] == 'Corporate' else 'DUR EFF'
    FaceValue = 'Face_Value' if Fundamentals_table[:9] == 'Corporate' else 'Face Value'
    Price = 'Price' if Fundamentals_table[:9] == 'Corporate' else 'PRICE'
    PriceDivider = 100 if Fundamentals_table[:9] == 'Corporate' else 1
    Parent = 'SEDOL' if Fundamentals_table[:9] == 'Corporate' else 'Ticker Parent'
    SedolParent = 'Issuer_ID' if Fundamentals_table[:9] == 'Corporate' else 'SEDOL Parent'
    MarketValue = 'Mkt_Val_USD' if Fundamentals_table[:9] == 'Corporate' else 'MarketValue'
    Issuer = 'Issuer_ID' if Fundamentals_table[:9] == 'Corporate' else 'Issue Name'
    Identifier = 'ISIN' if Fundamentals_table[:9] == 'Corporate' else 'SEDOL'
    Name = 'Security_Name' if Fundamentals_table[:9] == 'Corporate' else 'Issue Name'
    equityIdentifier = 'SEDOL' if Fundamentals_table == 'Corporate' else 'SEDOL Parent'
    Shares = 'EQTY_ORD_SHARES' if Fundamentals_table == 'Corporate' else 'Equity Shares Outstanding'

    return Maturity, Duration, FaceValue, Price, PriceDivider, Parent, SedolParent, MarketValue, \
           Issuer, Identifier, Name, equityIdentifier, Shares
def fundamentalsuniquecreator(Fundamentals, Parent, Name):
    bondlist = []
    Fundamentals_Eq_unique = pd.DataFrame()
    Fundamentals_full = Fundamentals[Fundamentals[Parent].notnull()]
    for _yearmonth in Fundamentals_full['YearMonth'].unique():
        for _Parent in Fundamentals_full.loc[Fundamentals_full['YearMonth'] == _yearmonth, Parent].unique():
            temp = Fundamentals_full.loc[(Fundamentals_full['YearMonth'] == _yearmonth) & (Fundamentals_full[Parent] == _Parent), :]
            bondlist = bondlist + [Fundamentals_full.loc[int(temp['Distance from duration pick'].idxmin()), Name]]
        temp = Fundamentals_full[(Fundamentals_full['YearMonth'] == _yearmonth) & (Fundamentals_full[Name].isin(bondlist))].reset_index(drop=True)
        Fundamentals_Eq_unique = pd.concat([Fundamentals_Eq_unique, temp], axis=0, ignore_index=True)

    return Fundamentals_Eq_unique
def fundamentalsCreator(YearMonth,Window,Shift,Fundamentals_table,Index_table,IndexValue,Equity_table,Analysis,Bondparent,Time_to_maturity_pick,\
                        Duration_pick, Issuerdata,Durationdata,Fundamentals_table_Eq, Fundamentals_table_Eq_unique,cpu,override):
    # Fundamentals_table contains bond attributes and returns across the entire universe
    # Gets filtered on a given universe by merging with the Index table
    # Gets equity subsidiary data added to it
    # Gets rating mapping data, total debt, DTS, market value, time to maturity, distance from maturity and duration pick,
    # issuer weight, issuer bond count, issuer market value, issuer duration added to it
    # Fundamentals_Eq_unique gets created by picking bond closest to duration pick, unique ultimate parent and largest face value
    # Populate tables Fundamentals_table_Eq, Fundamentals_table_Eq_unique

    # Load constituents and fundamental data

    Datacheck0 = sqlgetbyyearmonth('', '', '', Fundamentals_table_Eq, YearMonth, '', '', [], 'YearMonth', '', '')
    Datacheck = sqlgetbyyearmonth('', '', '', Fundamentals_table_Eq_unique, YearMonth, '', '', [], 'YearMonth', '', '')
    if Datacheck0.shape[0] == 0 or Datacheck.shape[0] == 0 or \
        Datacheck0.shape[0] > 999999 or Datacheck.shape[0] > 999999 or override == True:
        Fundamentals = sqlgetbyyearmonth('', '', '', Fundamentals_table, YearMonth, '', '',[], 'YearMonth', '', '')
        # Filter based on all IndexValues
        Fundamentals = IndexFilter(IndexValue,Index_table,YearMonth,Fundamentals)
        Fundamentals_Eq_unique = pd.DataFrame()
        if Fundamentals.shape[0]>0:
            Equity = sqlgetbyyearmonth('', '', '', Equity_table, YearMonth, '', '', [], 'YearMonth', '', '')
            Equity.drop(['ISIN.Number', 'Proper.Name'], axis=1, inplace=True)
            Equitycolumns = Equity.columns.difference(Fundamentals.columns)
            Equitycolumns = Equitycolumns + ['Symbol.1'] if 'Symbol.1' not in Equitycolumns else Equitycolumns
            Fundamentals = pd.merge(Fundamentals, Equity[Equitycolumns], left_on='EqSymbol', right_on='Symbol.1', how='left')
            # Fundamentals['MarketValue'] = Fundamentals['Face Value'] * Fundamentals['PRICE']
            if Fundamentals.shape[0] > 0:
                Fundamentals['Date'] = pd.to_datetime(Fundamentals['Date'], errors='coerce').dt.date
                # Debug:
                # temp0 = Fundamentals[Fundamentals['Issue Name'].str.contains('|'.join(['Chesapeake']))].iloc[0]
                # temp1 = Bondparent[Bondparent['ISIN']==temp0['ISIN']].iloc[0]
                # Remove by properly populating HYupdate
                rating = pd.DataFrame(
                    np.array([['BB1', 1], ['BB2', 2], ['BB3', 3], ['B1', 4], ['B2', 5], ['B3', 6], ['CCC1', 7],
                              ['CCC2', 8],
                              ['CCC3', 9], ['CC1', 10], ['CC2', 11], ['C1', 12], ['C2', 13]]),
                    columns=['Rating', 'RatingNo'])
                if Fundamentals_table[:9] != 'Corporate':
                    Fundamentals = pd.merge(Fundamentals, rating, left_on='Rating', right_on='Rating', how='left')
                else:
                    Fundamentals = pd.merge(Fundamentals, rating, left_on='Comp_Rating', right_on='Rating', how='left')
                    # Fundamentals['RatingNo'] = 1
                    # Fundamentals['Rating'] = 'BB1'
                # Not needed:
                Fundamentals.columns = [s.replace('_x', '') for s in Fundamentals.columns]
                Fundamentals['RatingNo'] = pd.to_numeric(Fundamentals['RatingNo'])
                # Fundamentals['RatingNo'] = Fundamentals['Comp_Rating_Num']

                Maturity, Duration, FaceValue, Price, PriceDivider, Parent, SedolParent, MarketValue, Issuer, Identifier, \
                Name, equityIdentifier, Shares = translator(Fundamentals_table)

                if Analysis == False:
                    if Fundamentals.loc[:, Fundamentals.columns.str.contains('|'.join(['YearMonth']))].shape[1]==0:
                        yearmonth(Fundamentals, 'Date')
                    Fundamentals['Total debt'] = Fundamentals['ST.DEBT.MON'] + 0.5 * Fundamentals['LT.DEBT.MON']

                    Fundamentals['DTS'] = Fundamentals[Duration] * Fundamentals['OAS']
                    Fundamentals['MarketValue'] = Fundamentals[FaceValue] * Fundamentals[Price] / PriceDivider
                    if Fundamentals_table[:9] != 'Corporate':
                        Fundamentals = pd.merge(Fundamentals, Bondparent, left_on='ISIN', right_on='ISIN', how='left')

                # Review the below later to look into problematic calculations
                Fundamentals = Fundamentals.replace(999999999, np.nan)
                # Fundamentals = Fundamentals_all[Fundamentals_all['YearMonth'] == YearMonth]
                if Analysis == True or (Analysis == False and Fundamentals.shape[0] > 1):
                    Fundamentals['Maturity Date'] = pd.to_datetime(Fundamentals[Maturity], errors='coerce').dt.date
                    # Fundamentals['Maturity Date'] = Fundamentals['Maturity Date'].replace(np.nan, '1900-01-01')
                    # Fundamentals['Maturity Date'] = pd.to_datetime(Fundamentals[Maturity], errors='coerce').dt.date
                    Fundamentals['Time_to_maturity'] = (Fundamentals['Maturity Date'] - Fundamentals['Date']).dt.days / 365
                    Fundamentals['Distance from maturity pick'] = abs(Fundamentals['Time_to_maturity'] - Time_to_maturity_pick)
                    Fundamentals['Distance from duration pick'] = abs(Fundamentals[Duration] - Duration_pick)
                    # Remove distance from duration nan - NO!! as reduced data on all bonds
                    Fundamentals = Fundamentals[np.isfinite(Fundamentals['Distance from duration pick'])]
                    # Find issuer weight in the index by summing the weight of all issuer bonds
                    if len([c for c in Fundamentals.columns if c == 'Weight']) == 0:
                        Fundamentals['Weight'] = np.nan
                        for _yearmonth in list(Fundamentals['YearMonth'].unique()):
                            temp = Fundamentals[Fundamentals['YearMonth']==_yearmonth]
                            temp['Weight'] = temp[MarketValue] / temp[MarketValue].sum()
                            Fundamentals.loc[Fundamentals['YearMonth']==_yearmonth,'Weight'] = temp[['Weight']]
                    for t in range(len(Fundamentals[Parent].unique())):
                        Issuerdata.loc[t, Parent] = Fundamentals[Parent].unique()[t]
                        Issuerdata.loc[t, 'Issuer weight'] = Fundamentals.loc[Fundamentals[Parent] == \
                            Fundamentals[Parent].unique()[t], 'Weight'].sum()
                        Issuerdata.loc[t, 'Issuer bond count'] = \
                            Fundamentals.loc[Fundamentals[Parent] == Fundamentals[Parent].unique()[t]].shape[0]
                        Issuerdata.loc[t, 'Issuer MarketValue'] = Fundamentals.loc[
                            Fundamentals[Parent] == Fundamentals[Parent].unique()[t], MarketValue].sum()
                    Issuerdata = Issuerdata.sort_values(by=['Issuer weight']).reset_index(drop=True)
                    Fundamentals = pd.merge(Fundamentals, Issuerdata, left_on=Parent, right_on=Parent, how='left')
                    # Find issuer average duration
                    for t in range(len(Fundamentals[Parent].unique())):
                        Durationdata.loc[t, Parent] = Fundamentals[Parent].unique()[t]
                        Durationdata.loc[t, 'Issuer duration'] = Fundamentals.loc[
                            Fundamentals[Parent] == Fundamentals[Parent].unique()[t], Duration].mean()
                    Fundamentals = pd.merge(Fundamentals, Durationdata, left_on=Parent,right_on=Parent,how='left')
                    Fundamentals = Fundamentals.sort_values(by=[Parent]).reset_index(drop=True)
                    # Save into SQL
                    # action = 'replace' if i==0 else 'append'
                    # savetosql(Fundamentals, 'FundamentalsHY', action)
                    # i = i + 1

                    # Remove duplicate bond names by picking largest face value.
                    Fundamentals = Fundamentals.sort_values(by=[Name, FaceValue])
                    Fundamentals = Fundamentals.drop_duplicates(['YearMonth',Name], keep='last').reset_index(drop=True)
                    # Pick issuer bond that is closest to pick
                    Fundamentals_Eq_unique = fundamentalsuniquecreator(Fundamentals,Parent,Name)
                if Datacheck0.shape[0]>0:
                    deletefromsql(YearMonth, Fundamentals_table_Eq)
                if Datacheck.shape[0]>0:
                    deletefromsql(YearMonth, Fundamentals_table_Eq_unique)
                savetosql(Fundamentals, Fundamentals_table_Eq, 'replace', cpu)
                savetosql(Fundamentals_Eq_unique, Fundamentals_table_Eq_unique, 'replace', cpu)
def turnover(FundamentalsQ, YearMonth, Factorgrouplist, AnalyticQ, suffix, Nquantile):
    # Turnover
    # https://breakingdownfinance.com/trading-strategies/passive-investing/portfolio-turnover/
    Fundamentals = sqlgetbyyearmonth('', '', '', FundamentalsQ, yearmonthshift(YearMonth, 0), '', '', [], 'YearMonth', '', '')
    Fundamentals_prior = sqlgetbyyearmonth('', '', '', FundamentalsQ, yearmonthshift(YearMonth, -1), '', '', [], 'YearMonth', '', '')
    if not Fundamentals.empty and not Fundamentals_prior.empty:
        # For each factor group
        for Factorgroupvalue in Factorgrouplist:
            GroupNovaluelist = list(Factorgroup.loc[Factorgroup['Factorgroup'] == Factorgroupvalue, 'GroupNo'].unique()) \
                if Factorgroupvalue != '' else ['']
            # For each GroupNo within each group
            for GroupNovalue in GroupNovaluelist:
                if Factorgroupvalue != '':
                    Factorgrouptemp = Factorgroup[(Factorgroup['Factorgroup'] == Factorgroupvalue) &
                                                  (Factorgroup['GroupNo'] == GroupNovalue)]
                    # Filter universe based on group
                    if Factorgrouptemp['Labeltype'].iloc[0] == 'value':
                        Fundamentals = Fundamentals[Fundamentals[Factorgroupvalue].
                            isin(Factorgrouptemp['Label'].tolist())].reset_index(drop=True)
                        Fundamentals_prior = Fundamentals_prior[Fundamentals_prior[Factorgroupvalue].
                            isin(Factorgrouptemp['Label'].tolist())].reset_index(drop=True)
                    if Factorgrouptemp['Labeltype'].iloc[0] == 'threshold':
                        Fundamentals = Fundamentals[(Fundamentals[Factorgroupvalue] <
                                                     float(Factorgrouptemp['Label'].iloc[0]))].reset_index(drop=True)
                        Fundamentals_prior = Fundamentals_prior[(Fundamentals_prior[Factorgroupvalue] <
                                                                 float(Factorgrouptemp['Label'].iloc[0]))].reset_index(drop=True)

                # For each construction analytic
                for j in range(len(AnalyticQ)):
                    Fundamentals.loc[:, AnalyticQ[j] + ' turnover Remaining bonds weighted' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    Fundamentals.loc[:, AnalyticQ[j] + ' turnover Remaining bonds equal' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    Fundamentals.loc[:, AnalyticQ[j] + ' turnover All bonds weighted' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    Fundamentals.loc[:, AnalyticQ[j] + ' turnover All bonds equal' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    for k in range(Nquantile):
                        Fundamentals.loc[:, AnalyticQ[j] + ' Q' + str(k + 1) + ' turnover weighted' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                        Fundamentals.loc[:, AnalyticQ[j] + ' Q' + str(k + 1) + ' turnover equal' + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    # Benchmark
                    # Remaining
                    Fnow = Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix].notnull()] \
                        [['ISIN', 'Weight']].reset_index(drop=True)
                    Fprior = Fundamentals_prior.loc[Fundamentals_prior[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix].notnull()] \
                        [['ISIN', 'Weight']].reset_index(drop=True)
                    if not Fnow.empty and not Fprior.empty:
                        Fundamentals.loc[:, AnalyticQ[j] + ' turnover Remaining bonds weighted' + Factorgroupvalue + GroupNovalue + suffix], \
                        Fundamentals.loc[:, AnalyticQ[j] + ' turnover Remaining bonds equal' + Factorgroupvalue + GroupNovalue + suffix] = \
                            turnoverCalc(Fnow, Fprior)
                    # All
                    Fnow = Fundamentals.loc[:, ['ISIN', 'Weight']].reset_index(drop=True)
                    Fprior = Fundamentals_prior.loc[:, ['ISIN', 'Weight']].reset_index(drop=True)
                    if not Fnow.empty and not Fprior.empty:
                        Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                         AnalyticQ[j] + ' turnover All bonds weighted' + Factorgroupvalue + GroupNovalue + suffix], \
                        Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                         AnalyticQ[j] + ' turnover All bonds equal' + Factorgroupvalue + GroupNovalue + suffix] = \
                            turnoverCalc(Fnow, Fprior)
                    # Quantile portfolios
                    for k in range(Nquantile):
                        Fnow = Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                                ['ISIN', 'Weight']].reset_index(drop=True)
                        Fprior = Fundamentals_prior.loc[Fundamentals_prior[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                                        ['ISIN', 'Weight']].reset_index(drop=True)
                        if not Fnow.empty and not Fprior.empty:
                            Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                             AnalyticQ[j] + ' Q' + str(k + 1) + ' turnover weighted' + Factorgroupvalue + GroupNovalue + suffix], \
                            Fundamentals.loc[Fundamentals[AnalyticQ[j] + ' quantile' + Factorgroupvalue + GroupNovalue + suffix] == k,
                                             AnalyticQ[j] + ' Q' + str(k + 1) + ' turnover equal' + Factorgroupvalue + GroupNovalue + suffix] = \
                                turnoverCalc(Fnow, Fprior)
def ColumnPriority(df, Columns1ListOut, Columns2ListOut, Columns3ListOut, Columns4ListOut, PriorityList):

    # 0. All columns
    Columns0 = list(df.columns)
    # 1. Remove Dates and tunings
    Columns1 = list(set(Columns0) - set([x for x in Columns0 if any(sub in x for sub in Columns1ListOut)]))
    # 2. Remove predicted
    Columns2 = list(set(Columns1) - set([x for x in Columns1 if any(sub in x for sub in Columns2ListOut)]))
    # 3. Remove in sample
    Columns3 = list(set(Columns2) - set([x for x in Columns2 if any(sub in x for sub in Columns3ListOut)]))
    # 4. Remove Fund, Style OOS
    Columns4 = list(set(Columns3) - set([x for x in Columns3 if any(sub in x for sub in Columns4ListOut)]))
    # Priority list items on top
    Columns4 = PriorityList + sorted(list(set(Columns4) - set(PriorityList)), key=str.lower)

    # Sorted Results1 by summary, Fund and Style OOS, in sample, predicted, Dates and tunings, rest
    AllColumns = Columns4 + sorted(list(set(Columns3) - set(Columns4)), key=str.lower) + \
           sorted(list(set(Columns2) - set(Columns3)), key=str.lower) + \
           sorted(list(set(Columns1) - set(Columns2)), key=str.lower) + \
           sorted(list(set(Columns0) - set(Columns1)), key=str.lower)

    return (Columns0, Columns1, Columns2, Columns3, Columns4, AllColumns)
def DataFilterColumn(df1, SeriesListIn, SeriesListOut):
    checklist = df1.iloc[:, 0].str.contains('|'.join(SeriesListIn))
    nochecklist = [False for x in range(df1.shape[0])] if not SeriesListOut else \
        df1.iloc[:, 0].str.contains('|'.join(SeriesListOut))
    nochecklist = [not x for x in nochecklist]
    df2 = df1[(pd.Series(x for x in checklist)) & (pd.Series(x for x in nochecklist))] \
        .sort_values(by=df1.columns[0]).reset_index(drop=True)

    return df2
def DataSeriesSimple(df1, SeriesListIn, SeriesListOut):

    df2 = DataFilterColumn(df1, SeriesListIn, SeriesListOut)
    df2.rename(columns={list(df2)[0]: 'Entity'}, inplace=True)
    df2.rename(columns={list(df2)[1]: 'Value'}, inplace=True)
    df2['Date'] = df2['Entity'].str.split('\ ').str[-1]
    df2['Entity'] = df2['Entity'].str.split('\ ').str[0:-1].str.join(' ')
    df2 = df2[['Date', 'Entity', 'Value']].set_index(['Date', 'Entity']).unstack('Entity') \
        .reset_index(col_level=-1)
    df2.columns = df2.columns.droplevel(0).rename(None)

    return df2
def DataSeriesCreator(df1, Data, SeriesListIn, SeriesListOut):

    """ Retrieve entries from dataframe that contain (or not) strings from a given list
        and create a dataframe with Dates and multiple columns. The Dates are taken (split)
        from the original dataframe column which is in the format of 'Style 01/01/2020' """

    # df2 = DataFilterColumn(df1, SeriesListIn, SeriesListOut)
    # df2.rename(columns={list(df2)[0]: 'Entity'}, inplace=True)
    # df2.rename(columns={list(df2)[1]: 'Value'}, inplace=True)
    # df2['Date'] = df2['Entity'].str.split('\ ').str[-1]
    # df2['Entity'] = df2['Entity'].str.split('\ ').str[0:-1].str.join(' ')
    # df2 = df2[['Date', 'Entity', 'Value']].set_index(['Date', 'Entity']).unstack('Entity') \
    #     .reset_index(col_level=-1)
    # df2.columns = df2.columns.droplevel(0).rename(None)
    df2 = DataSeriesSimple(df1, SeriesListIn, SeriesListOut)
    # Add fund. Currently fund column hardcoded to be right after Date in the Data dataframe.
    df2 = pd.merge(df2, Data.iloc[:, 0:2], left_on='Date', right_on='Date', how='inner')
    df2 = df2.apply(pd.to_numeric, errors='ignore')

    return df2
def ResidualCreator(df, SeriesListIn, NameList):

    """ Create residuals by subtracting fund from style.
        Different versions of style may be supplied, like in sample, predicted, OOS.
        Fund column is hardcoded """

    for i, entry in enumerate(SeriesListIn):
        df[NameList[i]] = df.iloc[:,-1] - df[entry]

    return df
def ResidualsAdd(df, Data, SeriesListIn, SeriesListOut, ResidualListOut, NameList):

    # Retrieve time series to add residuals on and add the fund too
    # Below adds to last row
    dfT = df.iloc[[-1], :].T.sort_index().reset_index()
    dfT.rename(columns={list(dfT)[0]: 'Entity'}, inplace=True)
    DataSeries = DataSeriesCreator(dfT, Data, SeriesListIn, SeriesListOut)
    # Add residuals
    DataSeries = ResidualCreator(DataSeries, SeriesListIn, NameList)
    # Merge into current fund/factor iteration results dataframe
    # Drop SeriesListIn and fund to avoid inserting later existing entries
    temp = DataSeries.drop(SeriesListIn + [Data.columns[1]], axis=1).set_index('Date').stack().reset_index()
    temp.columns = ['Date', 'Entity', dfT.columns[1]]
    temp['Entity'] = temp['Entity'] + ' ' + temp['Date']
    temp.drop('Date', axis=1, inplace=True)
    temp = temp.sort_values(by='Entity').reset_index(drop=True)
    # Drop existing residual rows.
    yeschecklist = list(dfT.loc[dfT.iloc[:,0].str.contains('|'.join(ResidualListOut))].iloc[:,0].reset_index(drop=True))
    nochecklist = list(dfT.loc[dfT.iloc[:,0].str.contains('|'.join(SeriesListOut))].iloc[:,0].reset_index(drop=True))
    checklist = list(set(yeschecklist)-set(nochecklist))
    checklist = list(set(list(dfT.iloc[:,0]))-set(checklist))
    dfT = dfT[dfT['Entity'].str.contains('|'.join(checklist))].reset_index(drop=True)
    # Add calculated residuals
    dfT = dfT.append(temp)
    # Merge into overall results dataframe
    dfw = dfT.T
    dfw.columns = dfw.iloc[0]
    dfw.drop('Entity', axis=0, inplace=True)
    dfw = dfw.reset_index(drop=True)
    dfw = dfw.apply(pd.to_numeric, errors='ignore')
    # Check the below after a factor or fund iteration!!!
    df = pd.concat([df.iloc[:-1, :], dfw], axis=0, ignore_index=True)

    return df
def SummaryStats(df0, df2, row, col, suffix):

    if ~np.isnan(sum(df2[col])):
        colout = 'Fund' if col == df2.columns[-1] else col
        df0.loc[row, colout + ' Mean' + suffix] = df2[col].mean()
        df0.loc[row, colout + ' Stdev' + suffix] = df2[col].std()
        df0.loc[row, colout + ' Variance' + suffix] = df2[col].var()
        df0.loc[row, colout + ' Mean/Stdev' + suffix] = df2[col].mean() / df2[col].std()
        df0.loc[row, colout + ' Min' + suffix] = df2[col].min()
        df0.loc[row, colout + ' Max' + suffix] = df2[col].max()
        df0.loc[row, colout + ' Skewness' + suffix] = df2[col].skew()
        df0.loc[row, colout + ' Kurtosis' + suffix] = df2[col].kurt()
        df0.loc[row, colout + ' Durbin-Watson' + suffix] = \
            durbin_watson(df2[col].dropna() - np.mean(df2[col]), axis=0)

    return df0
def DiagnosticsCalc(iter1, df0, iter2, df2, Data, SeriesListIn1, SeriesListIn2, suffix):
    for i, entry in enumerate(SeriesListIn1):
        # R2
        df0.loc[iter1, 'R2 ' + entry + suffix] = 1 - sum((df2.iloc[:, -1] - df2[entry]) ** 2) / sum(df2.iloc[:, -1]**2)
        # Correlation
        df0.loc[iter1, 'Correlation ' + entry + suffix] = df2[[entry, Data.columns[1]]].corr().iloc[0, 1]
        # Fund (dependent) vs Series (independent) Beta
        df0.loc[iter1, 'Beta ' + entry + suffix] = \
            df2[[entry, Data.columns[1]]].cov().iloc[0, 1] / df2[entry].var()
    for i, entry in enumerate(SeriesListIn1 + SeriesListIn2 + [Data.columns[1]]):
        # Summary stats
        df0 = SummaryStats(df0, df2, iter1, entry, suffix)

    return df0
def DiagnosticResults(iter1, df0, iter2, df, Data, SeriesListIn1, SeriesListIn2, SeriesListOut, suffix):
    # Retrieve time series to calculate diagnostics on and add the fund too.
    # Fund is already there, no need to add again!!!
    dfT = df.iloc[[iter2], :].T.sort_index().reset_index()
    dfT.rename(columns={list(dfT)[0]: 'Entity'}, inplace=True)
    DataSeries = DataSeriesCreator(dfT, Data, SeriesListIn1 + SeriesListIn2, SeriesListOut)
    if DataSeries.shape[0]>1:
        # Add diagnostics
        """ Fund vs OOS: R2, alpha, beta
        fund, predicted, oos, in sample/predicted/oos residual: mean, vol, min, max, skew, kurt, DW
        Note that residual mean = alpha """
        df = DiagnosticsCalc(iter1, df0, iter2, DataSeries, Data, SeriesListIn1, SeriesListIn2, suffix)

    return df
def ReturnCreator(df, frequency):
    if frequency=='Weekly':
        # Do the below to get weekly returns based on Wednesdays
        df['DayOfWeek'] = df['Date'].dt.dayofweek
        df = df[df['DayOfWeek'] == 2]
        df.loc[:, 'index'] = df.index.values
        df = df.reset_index(drop=True)
        # Check if Wednesdays are consecutive
        # df.index.values[-1] * 7 == df.iloc[-1, df.columns.get_loc('index')]
        df.drop(['DayOfWeek', 'index'], axis=1, inplace=True)
    dfReturns = (df.iloc[:, 1:] - df.iloc[:, 1:].shift(1)) / df.iloc[:, 1:].shift(1)
    df = pd.concat([df['Date'], dfReturns], axis=1).dropna().reset_index(drop=True)

    return df
def FilterDfCreator(df, SeriesListIn, SeriesListOut, iter2):
    # Retrieve time series to calculate diagnostics on and add the fund too
    dfT = df.iloc[[iter2], :].T.sort_index().reset_index()
    dfT.rename(columns={list(dfT)[0]: 'Entity'}, inplace=True)
    dfOut = DataSeriesSimple(dfT, SeriesListIn, SeriesListOut)

    return dfOut
def InverseStdevCreator(StdevWindowList, filepath, filename):
    # Create rolling inverse stdev factors across multiple windows. Only daily for now.
    df = pd.read_excel(filepath + filename, engine='openpyxl')
    df['Date'] = pd.to_datetime(df['Date'])
    for StdevWindow in StdevWindowList:
        # Create inverse volatility factors
        dfStdev = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True),
                         [df[['Date']],
                          df[[x for x in list(df.columns) if x not in ('Cash')]].rolling(StdevWindow,
                                                                                         min_periods=2).std(),
                          df[['Cash']]])
        dfInverseStdev = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True),
                                [df[['Date']],
                                 df[[x for x in list(df.columns) if x not in ('Cash')]].iloc[:, 1:] /
                                 dfStdev[[x for x in list(df.columns) if x not in ('Cash')]].iloc[:,
                                 1:] * .1 / np.sqrt(252),
                                 df[['Cash']]]).dropna().reset_index(drop=True)
        # Check
        dfnewStdev = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True),
                            [dfInverseStdev[['Date']],
                             dfInverseStdev[[x for x in list(dfInverseStdev.columns)
                                             if x not in ('Cash')]].rolling(StdevWindow, min_periods=2).std(),
                             dfInverseStdev[['Cash']]])

        filename = 'Factors_' + FactorSet + '_Stdev' + str(StdevWindow) + '.csv'
        file = filepath + filename
        dfInverseStdev.to_csv(file, index=False)
def FactorGrouper(StdevWindowList, filepath, filenameIn, filename):

    # Group (average) factors based on excel tabs
    dfDict = pd.read_excel(filepath + filenameIn, engine='openpyxl', sheet_name=None)
    for StdevWindow in StdevWindowList:
        df = pd.read_csv(filepath + filename + str(StdevWindow) + '.csv')
        GroupDf = pd.DataFrame()
        for key in dfDict:
            TempDf = pd.DataFrame()
            for i in range(dfDict[key].shape[0]):
                if i == 0:
                    TempDf = df[['Date', dfDict[key].iloc[i, 1]]]
                    TempDf.rename(columns={list(TempDf)[1]: key}, inplace=True)
                else:
                    TempDf[key] = TempDf[key] + df[dfDict[key].iloc[i, 1]]
            GroupDf = TempDf.copy() if GroupDf.empty else pd.concat([GroupDf, TempDf[key]], axis=1)
            GroupDf[key] = GroupDf[key] / (i + 1)
        GroupDf.to_csv(filepath + filename + str(StdevWindow) + '_Grouped.csv', index=False)
def DfFromDict(dfDict, Data, filepath, filename):
    # Create dictionary from excel tabs
    dfDict = pd.read_excel(filepath + filename, engine='openpyxl', sheet_name=None) if not dfDict else dfDict
    df = pd.DataFrame()
    for key in dfDict:
        for i in range(dfDict[key].shape[0]):
            if i == 0:
                df = Data[['Date', dfDict[key].iloc[i, 1]]]
                df.rename(columns={list(df)[1]: key}, inplace=True)
            else:
                df[key] = df[key] + Data[dfDict[key].iloc[i, 1]]
    return df

###############
### Results1 ###
###############
def AnalyticQProxy(QuantileR):
    # Proxy any AnalyticQ that may not have been populated to the average of all analytics for a given YearMonth
    # FIX later

    # Add top model results (should have been produced from analysis=true)
    # Only used for TopModel. Review if more than one dependent variable.
    # grouplist = ['Yearmonth', 'Model']
    # orderlist = ['Yearmonth','CVR2']
    # Dependent = sqldistinct('', '', '', MLtableAll, 'Dependent').dropna()
    # columns = colslike('', '', '', MLtableAll, ['%CVR2%'], [''], ['%stdev%'])
    # for dependent in Dependent.iloc[:,0]:
    # df = sqlgrouporder('', '', '', MLtableAll, grouplist, orderlist, columns, dependent)
    # # table = df[df['Model'].str.contains('|'.join(['stack','M0']))==False].groupby('Yearmonth').head(1)[['Yearmonth','Model']]
    # table = df.groupby('Yearmonth').head(1)[['Yearmonth','Model']]

    QuantileRnew = pd.DataFrame()
    for yearmonth in list(QuantileR['YearMonth'].unique()):
        df = QuantileR[QuantileR['YearMonth'] == yearmonth].reset_index(drop=True)
        # First, check if AnalyticQ is missing and insert
        missing = [item for item in list(QuantileR['AnalyticQ'].unique()) if item not in list(df['AnalyticQ'].unique())]
        # Exclude model M0
        # missing = [x for x in missing if 'M0' not in x ]
        for i in missing:
            add = pd.DataFrame(data=np.zeros((1, df.shape[1])) * np.nan, columns=df.columns)
            df = df.append(add, ignore_index=True)
            df.loc[df.shape[0] - 1, ['Year', 'Month', 'YearMonth', 'Date']] = df.loc[0, ['Year', 'Month', 'YearMonth', 'Date']]
            df.loc[df.shape[0] - 1, 'AnalyticQ'] = i
            # All NAs. Useful to find later which row got proxied.
            df.loc[df.shape[0] - 1, 'NA AnalyticQ'] = df.loc[df.shape[0] - 1, 'Total positions']
        # Now fill all missing values with averages. Right now we don't keep track of what got filled.
        df = df.fillna(df.mean())
        # TopModel
        # topmodel = table.loc[table['Yearmonth'] == df['YearMonth'].iloc[0], 'Model'].values[0]
        # df = df.append(df[df['AnalyticQ']=='Issuer CRV_'+topmodel], ignore_index=True)
        # df.loc[df.shape[0]-1,'AnalyticQ'] = 'Issuer CRV_Y0M1E8'
        QuantileRnew = QuantileRnew.append(df).reset_index(drop=True)

    return QuantileRnew
def FactorBucket(QuantileR, Factorgroup, AnalyticList, Nquantile, Quantiles, suffix):
    # Bucket factors
    Factorgrouplist = list(Factorgroup['Factorgroup'].unique())
    for m in range(len(AnalyticList)):
        for k in range(Nquantile):
            for Factorgroupvalue in Factorgrouplist:
                GroupNovaluelist = list(Factorgroup.loc[Factorgroup['Factorgroup'] == Factorgroupvalue, 'GroupNo'].unique())
                cols = ['Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + \
                        ' ' + AnalyticList[m] + ' mean' for GroupNovalue in GroupNovaluelist]
                if all(item in list(QuantileR.columns) for item in cols):
                    QuantileR['Quantile ' + Factorgroupvalue + suffix + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + \
                          ' ' + AnalyticList[m] + ' mean'] = QuantileR[cols].mean(axis=1)
                else:
                    QuantileR['Quantile ' + Factorgroupvalue + suffix + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + \
                              ' ' + AnalyticList[m] + ' mean'] = np.nan

    return QuantileR
def BenchmarkBucket(QuantileR, Factorgroup, AnalyticList, benchmarklist, suffix):
    # Bucket benchmarks
    Factorgrouplist = list(Factorgroup['Factorgroup'].unique())
    for m in range(len(AnalyticList)):
        for Factorgroupvalue in Factorgrouplist:
            GroupNovaluelist = list(Factorgroup.loc[Factorgroup['Factorgroup'] == Factorgroupvalue, 'GroupNo'].unique())
            for benchmark in benchmarklist:
                cols = [benchmark + Factorgroupvalue + GroupNovalue for GroupNovalue in GroupNovaluelist]
                if all(item in list(QuantileR.columns) for item in cols):
                    QuantileR[benchmark + Factorgroupvalue + suffix] = QuantileR[cols].mean(axis=1)
                else:
                    QuantileR[benchmark + Factorgroupvalue + suffix] = np.nan

    return QuantileR
def QuantileToTxt(QuantileR,Factorgroup, AnalyticQ, Analytic, Weighting, UniverseName, BMsuffix, suffix, Quantiles, Nquantile, filepath, AnalyticQfile):
    Factorgrouplist = [''] + list(Factorgroup['Factorgroup'].unique())
    for j in range(len(AnalyticQ)):
        print('j=' + str(j))
        Returnsall = pd.DataFrame()
        Returnsallflag = False
        for Factorgroupvalue in Factorgrouplist:
            GroupNovaluelist = list(Factorgroup.loc[Factorgroup['Factorgroup'] == Factorgroupvalue, 'GroupNo'].unique()) \
                if Factorgroupvalue != '' else ['']
            for GroupNovalue in GroupNovaluelist:
                for w in Weighting:
                    returnsuffix = 'EW' if w == 'mean' else ''
                    Returns = pd.DataFrame()
                    Universeloop = UniverseName if Factorgroupvalue == '' else Factorgroupvalue + GroupNovalue
                    columns = ['Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + \
                               '-' + str(Quantiles[k + 1]) + ' ' + Analytic + ' ' + w for k in range(Nquantile)]
                    if all(item in list(QuantileR.columns) for item in columns):
                        Returns = QuantileR.loc[QuantileR['AnalyticQ'] == AnalyticQ[j], :][['Date'] + columns]
                    else:
                        Returns = QuantileR.loc[QuantileR['AnalyticQ'] == AnalyticQ[j], ['Date']]
                        Returns[columns] = pd.DataFrame([[np.nan for x in range(len(columns))]], index=Returns.index)
                    # The spreadsheet doesn't  accommodate granular benchmarks for each factor bucket
                    # even though those are available and populated below. Same for EW benchmarks.
                    if all(item in list(QuantileR.columns) for item in ['Remaining bonds ' + w + BMsuffix + Factorgroupvalue + GroupNovalue + suffix]):
                        Returns['Return' + returnsuffix + 'AllCorp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix] = \
                            QuantileR.loc[QuantileR['AnalyticQ'] == AnalyticQ[j], 'Remaining bonds ' + w + BMsuffix + Factorgroupvalue + GroupNovalue + suffix]
                    else:
                        Returns['Return' + returnsuffix + 'AllCorp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix] = np.nan
                    for k in range(Nquantile):
                        Returns.columns = \
                            [s.replace('Quantile ' + Factorgroupvalue + GroupNovalue + suffix + str(Quantiles[k]) + '-' + str(Quantiles[k + 1]) + ' ',
                                       'Return' + returnsuffix + str(Nquantile - k) + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix)
                             for s in Returns.columns]
                    Returns.columns = [s.replace(Analytic + ' ' + w, '') for s in Returns.columns]
                    Returns['Return' + returnsuffix + 'LongOnlyCorp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix] = \
                        Returns['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix]
                    Returns['Return' + returnsuffix + 'LongShortCorp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix] = \
                        pd.to_numeric(Returns['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix]) - \
                        pd.to_numeric(Returns['Return' + returnsuffix + '5' + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix])
                    Returns['Return' + returnsuffix + 'FLongShortCorp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix] = \
                        pd.to_numeric(Returns['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix]) - \
                        pd.to_numeric(Returns['Return' + returnsuffix + '4' + 'Corp' + UniverseName + Factorgroupvalue + GroupNovalue + suffix])
                    Returnsall = Returns if Returnsall.shape[0] == 0 else pd.merge(Returnsall, Returns, left_on='Date', right_on='Date', how='left')
                    Returnsallflag = True
            # Average factor group returns
            if Factorgroupvalue != '':
                # GroupNovaluelist = list(Factorgroup.loc[Factorgroup['Factorgroup'] == Factorgroupvalue, 'GroupNo'].unique())
                for w in Weighting:
                    returnsuffix = 'EW' if w == 'mean' else ''
                    columns = ['Return' + returnsuffix + 'AllCorp' + UniverseName + Factorgroupvalue + GroupNovaluelist[s] + suffix
                               for s in range(len(GroupNovaluelist))]
                    # allcolumnscheck =  all(elem in Returnsall.columns.to_list()  for elem in columns)
                    # if allcolumnscheck:
                    Returnsall['Return' + returnsuffix + 'AllCorp' + UniverseName + Factorgroupvalue + suffix] \
                        = Returnsall[columns].apply(pd.to_numeric, errors='coerce').mean(axis=1)
                    for k in range(Nquantile):
                        columns = ['Return' + returnsuffix + str(Nquantile - k) + 'Corp' + UniverseName + Factorgroupvalue + GroupNovaluelist[s] + suffix
                                   for s in range(len(GroupNovaluelist))]
                        Returnsall['Return' + returnsuffix + str(Nquantile - k) + 'Corp' + UniverseName + Factorgroupvalue + suffix] \
                            = Returnsall[columns].apply(pd.to_numeric, errors='coerce').mean(axis=1)
                    Returns['Return' + returnsuffix + 'LongOnlyCorp' + UniverseName + Factorgroupvalue + suffix] = \
                        Returnsall['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + suffix]
                    Returns['Return' + returnsuffix + 'LongShortCorp' + UniverseName + Factorgroupvalue + suffix] = \
                        pd.to_numeric(Returnsall['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + suffix]) - \
                        pd.to_numeric(Returnsall['Return' + returnsuffix + '5' + 'Corp' + UniverseName + Factorgroupvalue + suffix])
                    Returns['Return' + returnsuffix + 'FLongShortCorp' + UniverseName + Factorgroupvalue + suffix] = \
                        pd.to_numeric(Returnsall['Return' + returnsuffix + '1' + 'Corp' + UniverseName + Factorgroupvalue + suffix]) - \
                        pd.to_numeric(Returnsall['Return' + returnsuffix + '4' + 'Corp' + UniverseName + Factorgroupvalue + suffix])
        if Returnsallflag == True:
            print('Returnsallflagnew=' + str(Returnsallflag))
            Returnsall = Returnsall.replace(np.nan, 0)
            Returnsall['Date'] = pd.to_datetime(Returnsall['Date'], errors='coerce').dt.strftime('%m/%d/%Y')
            Returnsall = Datenozero(Returnsall).reset_index(drop=True)
            # Returnsall['Date'] = pd.to_datetime(Returnsall['Date']).dt.date
            # Returnsall.to_excel(writer, sheet_name=AnalyticQ[j], index=False)
            Returnsall.to_csv(filepath + AnalyticQfile[j] + '.txt', sep='\t', header=True, index=False)

##########################
### Time series models ###
##########################

def MLregression(TrainingData, PeriodLabel, Period, CurrentData, FactorListM, FactorListO, Ylist, \
                 Ylistsave, estimatorlist, modelestimator, stackestimator, alphas, n_iters, max_iters, \
                 Identifier, minfactors, DDcolumns, tableAll, table,Datacheck,override,log,Horizon, Window, min_x_obs,
                 xscaling):
    # FactorListM: Mandatory factor list like ['AssetDebt']
    # FactorListO: Optional factor list, depending on data availability

    TrainingData_current = TrainingData[TrainingData[PeriodLabel].isin([Period])].reset_index(drop=True)
    if CurrentData == False:
        TrainingData = TrainingData[TrainingData[PeriodLabel] != Period].reset_index(drop=True)

    # Models = [['AssetDebt TST']] + [[m+ext] for m in FactorListO] + \
    #          [['AssetDebt TST', m+ext] for m in FactorListO] + \
    #          [['AssetDebt TST'] + [x+ext for x in FactorListO if x not in (
    #          'QUALITY', 'QUALITY_ALPHA', 'QUALITY_DEFENSIVE')]]
    # Models = [['AssetDebt TST']] + [['AssetDebt TST'] + [x+ext for x in FactorListO if x not in (
    #          'QUALITY', 'QUALITY_ALPHA', 'QUALITY_DEFENSIVE')]]
    # Models = [['AssetDebt TST'] + [x + ext for x in FactorListO if x not in (
    #     'QUALITY', 'QUALITY_ALPHA', 'QUALITY_DEFENSIVE')]]
    Models = [FactorListM + FactorListO]
    Allfactors = list(set(sorted([item for sublist in Models for item in sublist]))) + estimatorlist
    # estimators = [modelestimator, stackestimator]
    estimators = [modelestimator]

    # Check original data
    # m = [x.replace(ext, '') for x in Models[0]]
    # correlogram(DDdataHY[m])
    # x.describe()
    Save = False
    for E in range(len(estimators)):
        for Y in Ylist:
            for m in Models:
                # y = np.array([0,1,0,1,2,3,-2,-1,0,1,2,3])
                # x = np.array([[-2,-1,0,2,1,4,-2,-1,0,2,1,4],[-1,1,3,1,-1,4,-2,-2,1,0,0,1],[2,-4,-1,-2,2,-4,2,1,0,-2,1,0]]).reshape(-1, 3)
                # data=x
                # y0 = np.array([1, 2, 3])
                # x0 = np.array([2, 1, 4]).reshape(-1,1)

                # alpha=0.0000000001 is equivalent to OLS
                if E == 0:
                    suffix = ''
                    # Prediction data:
                    # Identifier: descriptive variable that we want to keep in addition to the factors and that is present in the dataset
                    DataCurrent = TrainingData_current[[PeriodLabel, Identifier] + [Y] + m].sort_values(by=[Identifier, PeriodLabel]).reset_index(drop=True)
                    DataCurrent = DataCurrent.replace([np.inf, -np.inf], np.nan)
                    DataCurrent = DataCurrent[pd.notnull(DataCurrent[Y])].reset_index(drop=True)
                    # AssetDebt: Mandatory factor (i.e. no nan)
                    DataCurrent = DataCurrent.dropna(subset=FactorListM).reset_index(drop=True)

                    # Regression data:
                    # Identifiers may not be the same as in Current data, creating beta discrepancies
                    Data = TrainingData[[PeriodLabel, Identifier] + [Y] + m].sort_values(by=[Identifier, PeriodLabel]).reset_index(drop=True)
                    Data = Data.replace([np.inf, -np.inf], np.nan)
                    Data = Data[pd.notnull(Data[Y])].reset_index(drop=True)
                    Data = Data.dropna(subset=FactorListM).reset_index(drop=True)

                    # Find columns between two dataframes that maximizes data availability across both dataframes and number of columns selected
                    # 4: column after which we find optional (non-mandatory) factors
                    if len(FactorListO)>0:
                        FactorListOnew, CommonCols = max_common_cols(DataCurrent[FactorListO], Data, minfactors, '')
                        DataCurrent = DataCurrent[[PeriodLabel, Identifier, Y] + FactorListM + FactorListOnew].dropna().reset_index(drop=True)
                    else:
                        FactorListOnew = FactorListO
                    xCurrent = DataCurrent[FactorListM + FactorListOnew]
                    yCurrent = np.asarray(DataCurrent.loc[:, Y])
                    Data = Data[[PeriodLabel, Identifier, Y] + FactorListM + FactorListOnew].dropna().reset_index(drop=True)
                    x = Data[FactorListM + FactorListOnew]
                    y = np.asarray(Data.loc[:, Y])
                else:
                    # We select all models to be stacked except the top one
                    modelestimatornotop = [x for x in modelestimator if x != 'Top']
                    suffix = 'stack' + ''.join(['E' + str(modelestimatornotop.index(k)) for k in modelestimatornotop])
                    ylist = Y if Y == 'd2' else ['Issuer OAS']
                    columns = [Identifier] + ylist + [Ylistsave[Ylist.index(Y)] + '_' + \
                                                      'Y' + str(Ylist.index(Y)) + 'M' + str(Models.index(m)) + 'E' + \
                                                      str(modelestimatornotop.index(k)) for k in modelestimatornotop]
                    # data = sqlgetbyyearmonth('', '', '', table, Period, columns, '')
                    data = DDdataHYplus[columns]
                    data = data.sort_values(by=Identifier)
                    # Ignore OAS less than 1bp
                    num = data._get_numeric_data()
                    num[num < 1] = np.nan
                    # Inf cannot happen:
                    data = data.replace([np.inf, -np.inf], np.nan).dropna().reset_index(drop=True)
                    columnstemp = [x for x in data if x != Identifier]
                    y = np.log(np.asarray(data[columnstemp].iloc[:, 0]))
                    # We can include the below only if we want to see what % of each model was used, as the weights would almost add to 1
                    # y = StandardScaler().fit_transform(y.reshape(-1, 1)).flatten()
                    x = np.log(data[columnstemp].iloc[:, 1:])
                    # x.descibe()
                    # Find which models are not populated for the beta columns later
                    betacolumns = []
                    for i in range(len(estimatorlist)):
                        if sum(data.columns.str.contains('E' + str(i))) > 0:
                            betacolumns = betacolumns + [estimatorlist[i]]
                    # Not needed as we are not doing top model for stacking
                    # CVR2data = sqlgetbyyearmonth('', '', '', tableAll, Period, ['CVR2', 'Model'], '')
                    CVR2data = DataNew[['CVR2', 'Model']]
                    CVR2data = CVR2data.replace([np.inf, -np.inf], np.nan).dropna().drop_duplicates(['CVR2', 'Model'], keep='last').reset_index(drop=True)
                    CVR2data = CVR2data[CVR2data['Model'].str.contains('M' + str(Models.index(m)))]
                    topmodel = CVR2data.loc[int(CVR2data['CVR2'].idxmax()), 'Model'][-1]
                x = np.asarray(x)
                xCurrent = np.asarray(xCurrent)
                x_unscaled = x
                # We need at least 50 observations to proceed
                if x.shape[0] <= min_x_obs or xCurrent.shape[0]==0:
                    print(Period)
                else:
                    Save = True
                    # Enhance to happen during cross-validation
                    if xscaling==True:
                        x = StandardScaler().fit_transform(x)
                        xCurrent = x[-1,:]
                    # correlogram(pd.DataFrame(x,columns=TrainingData.columns[3:]))
                    N = y.shape[0]
                    K = x.shape[1]
                    # n_folds = N
                    n_folds = 22
                    for e in range(len(estimators[E])):
                        # print('Y' + str(Ylist.index(Y)) + 'M' + str(Models.index(m)) + 'E' + str(e) + suffix)
                        # if e==0 or (e!=0 and K==len(AnalyticQ)-2):
                        # We are stacking models with greater than one factor
                        if e == 0 or (e != 0 and K > 1):
                            if estimators[E][e] != 'Top':
                                ee = e
                            else:
                                DataNew = sqlgetbyyearmonth('', '', '', tableAll, Period, '', '',[],PeriodLabel,'','')
                                temp = DataNew.replace([np.inf, -np.inf], np.nan).drop_duplicates(['CVR2', 'Model'], keep='last').reset_index(drop=True)
                                temp = temp[temp['Model'].str.contains('M' + str(Models.index(m)))]
                                topmodel = temp.loc[int(temp['CVR2'].idxmax()), 'Model'][-1]
                                ee = int(topmodel)
                            args = [x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters]
                            results = estimators[E][ee](*args)
                            # Proceed if no failure
                            if bool(results):

                                R2 = np.ones((N, 1)) * results.get('R2', np.nan)
                                CVR2 = np.ones((N, 1)) * results.get('CVR2', np.nan)
                                CVR2stdev = np.ones((N, 1)) * results.get('CVR2stdev', np.nan)
                                Modelalpha = np.ones((N, 1)) * results.get('modelalpha', np.nan)
                                Yfit = pd.DataFrame(results.get('yfit', np.empty((N,)) * np.nan), index=Data[Identifier], columns=['Yfit']).reset_index()
                                Ytemp = Yfit.sort_values(by=Identifier)
                                # len(Yfit) - Yfit.nunique()
                                YfitCV = pd.DataFrame(results.get('yfitCV', np.empty((N,)) * np.nan), index=Data[Identifier], columns=['YfitCV']).reset_index()
                                # Yfit = pd.merge(Yfit, TrainingData[[Identifier, 'T', 'R', 'lamda', 'corr']],left_on=Identifier, right_on=Identifier, how='left')
                                # Yfits = reduce(lambda left, right: pd.merge(left, right, on=[Identifier], how='inner'), [Yfit, YfitCV, DDdataHY[[Identifier, 'T', 'R', 'lamda', 'corr']]])
                                # Yfits = pd.concat([Yfit, YfitCV, TrainingData[[Identifier, 'T', 'R', 'lamda', 'corr']]], join='inner', axis=1)
                                Yfits = pd.concat([Yfit, YfitCV], join='inner', axis=1)
                                # CHANGED THE BELOW TO USE THE REDUCED FACTOR SET
                                # factors = betacolumns if E==1 else m
                                factors = betacolumns if E == 1 else FactorListM + FactorListOnew
                                Intercept = np.ones((N, 1)) * results.get('Intercept', np.nan)
                                beta = results.get('beta', np.empty((K,)) * np.nan)
                                Betasall = mergearray(beta, factors, Allfactors, N, True, Intercept, 'Beta', '').get('arrayall')
                                Betasallcols = mergearray(beta, factors, Allfactors, N, True, Intercept, 'Beta', '').get('arrayallcols')
                                # Recover unscaled alpha, beta. Only applicable to OLS for now, to extend to Ridge and Lasso.
                                Intercept_unscaled = results.get('Intercept', np.nan)
                                beta_unscaled = results.get('beta', np.empty((K,)) * np.nan)
                                if xscaling==True:
                                    for f in range(len(factors)):
                                        Intercept_unscaled = Intercept_unscaled - beta[f] * x_unscaled[:,f].mean() / x_unscaled[:,f].std()
                                        beta_unscaled[f] = beta[f] / x_unscaled[:,f].std()
                                Intercept_unscaled = np.ones((N, 1)) * Intercept_unscaled
                                Betasall_unscaled = mergearray(beta_unscaled, factors, Allfactors, N, True, Intercept_unscaled, 'Beta', '_unscaled').get('arrayall')
                                Betasallcols_unscaled = mergearray(beta_unscaled, factors, Allfactors, N, True, Intercept_unscaled, 'Beta', '_unscaled').get('arrayallcols')
                                pdpBetas = results.get('pdpbeta', np.empty((K,)) * np.nan)
                                pdpBetasall = mergearray(pdpBetas, factors, Allfactors, N, False, [], 'pdpBeta', '').get('arrayall')
                                pdpBetascols = mergearray(pdpBetas, factors, Allfactors, N, False, [], 'pdpBeta', '').get('arrayallcols')
                                FeatImp = results.get('FeatImp', np.empty((K,)) * np.nan)
                                FeatImpall = mergearray(FeatImp, factors, Allfactors, N, False, [], 'FeatImp', '').get('arrayall')
                                FeatImpcols = mergearray(FeatImp, factors, Allfactors, N, False, [], 'FeatImp', '').get('arrayallcols')
                                FeatImpCV = results.get('FeatImpCV', np.empty((K,)) * np.nan)
                                FeatImpCVall = mergearray(FeatImpCV, factors, Allfactors, N, False, [], 'FeatImpCV', '').get('arrayall')
                                FeatImpcolsCV = mergearray(FeatImp, factors, Allfactors, N, False, [], 'FeatImpCV', '').get('arrayallcols')
                                FeatImpstd = results.get('FeatImpstd', np.empty((K,)) * np.nan)
                                FeatImpstdall = mergearray(FeatImpstd, factors, Allfactors, N, False, [], 'FeatImpstd', '').get('arrayall')
                                FeatImpcolsstd = mergearray(FeatImp, factors, Allfactors, N, False, [], 'FeatImpstd', '').get('arrayallcols')
                                FeatImpstdCV = results.get('FeatImpstdCV', np.empty((K,)) * np.nan)
                                FeatImpstdCVall = mergearray(FeatImpstdCV, factors, Allfactors, N, False, [], 'FeatImpstdCV', '').get('arrayall')
                                FeatImpcolsstdCV = mergearray(FeatImp, factors, Allfactors, N, False, [], 'FeatImpstdCV', '').get('arrayallcols')
                                yfit = np.array([Yfits['Yfit'].to_numpy()]).T
                                yfitCV = np.array([Yfits['YfitCV'].to_numpy()]).T
                                residual = np.array([y]).T - yfit
                                residualCV = np.array([y]).T - yfitCV
                                # Low p-value = no unit root
                                # Case 3: KPSS indicates stationarity and ADF indicates non-stationarity - The series is trend stationary.
                                # Trend needs to be removed to make series strict stationary. The detrended series is checked for stationarity.
                                # Case 4: KPSS indicates non-stationarity and ADF indicates stationarity - The series is difference stationary.
                                # Differencing is to be used to make series stationary. The differenced series is checked for stationarity.
                                ADF = np.zeros((N, K+9)) * np.nan
                                KPSS = np.zeros((N, K+2)) * np.nan
                                ADF[:, [0]] = adfuller(y)[1] * np.ones((N, 1))
                                KPSS[:, [0]] = kpss(y, regression='c', nlags="auto")[1] * np.ones((N, 1))
                                for i in range(x.shape[1]):
                                    ADF[:, [i+1]] = adfuller(x[:,i])[1] * np.ones((N, 1))
                                    KPSS[:, [i+1]] = kpss(x[:,i], regression='c', nlags="auto")[1] * np.ones((N, 1))
                                ADF[:, [i + 2]] = adfuller(residual, regression='c')[1] * np.ones((N, 1))
                                ADF[:, [i + 3]] = adfuller(residual, regression='ct')[1] * np.ones((N, 1))
                                ADF[:, [i + 4]] = adfuller(residual, regression='ctt')[1] * np.ones((N, 1))
                                ADF[:, [i + 5]] = adfuller(residual, regression='nc')[1] * np.ones((N, 1))
                                residual_syst = residual + Intercept
                                ADF[:, [i + 6]] = adfuller(residual_syst, regression='c')[1] * np.ones((N, 1))
                                ADF[:, [i + 7]] = adfuller(residual_syst, regression='ct')[1] * np.ones((N, 1))
                                ADF[:, [i + 8]] = adfuller(residual_syst, regression='ctt')[1] * np.ones((N, 1))
                                ADF[:, [i + 9]] = adfuller(residual_syst, regression='nc')[1] * np.ones((N, 1))
                                KPSS[:, [i+2]] = kpss(residual)[1] * np.ones((N, 1))
                                ADFcolumns = ['Dependent_ADFpvalue'] + [x + '_ADFpvalue' for x in factors] + ['Residual_ADFcpvalue'] \
                                            + ['Residual_ADFctpvalue']+ ['Residual_ADFcttpvalue']+ ['Residual_ADFncpvalue'] \
                                            + ['Residual_syst_ADFcpvalue'] \
                                            + ['Residual_syst_ADFctpvalue'] + ['Residual_syst_ADFcttpvalue'] + ['Residual_syst_ADFncpvalue']
                                KPSScolumns = ['Dependent_KPSSpvalue'] + [x + '_KPSSpvalue' for x in factors] + ['Residual_KPSSpvalue']

                                # tstatsallcols = ['tstat Constant'] + \
                                #                 [sub.replace('Beta', 'tstat') for sub in Betasallcols[1:]]
                                # if x.shape[1] <= 3:
                                #     tstats = np.ones((N, len(beta))) * beta/se
                                # else:
                                #     tstats = np.ones((N, len(beta)))
                                #     tstats[:,:] = np.nan
                                # Tstatsall = pd.DataFrame(np.ones((N, len(columnlist))) * np.nan, columns=columnlist)
                                # Tstats = pd.DataFrame(tstats,columns=['Constant'] + ['Beta ' + z for z in Data.columns[2:].to_list()])
                                # Tstatsall[Tstats.columns.to_list()] = Tstats[Tstats.columns.to_list()]
                                # Tstatsall = Tstatsall.to_numpy()
                                if len(DDcolumns) > 0:
                                    d2, PD, CPD, CQDF, d2CV, PDCV, CPDCV, CQDFCV, OASt, OAStCV = MLDDresults(N, Y, Yfits)
                                    DDlist = [d2, PD, CPD, CQDF, OASt, d2CV, PDCV, CPDCV, CQDFCV, OAStCV]

                                Model = pd.DataFrame(np.ones((N, 1)))
                                Model.iloc[:, 0] = m[0] + '_' + '_'.join(map(str, m[1:])) if K > 1 else m[0]
                                Model = Model.to_numpy()
                                Dependent = pd.DataFrame(np.ones((N, 1)))
                                Dependent.iloc[:, 0] = Y
                                Dependent = Dependent.to_numpy()
                                Estimator = pd.DataFrame(np.ones((N, 1)))
                                Estimator.iloc[:, 0] = estimatorlist[e]
                                Estimator = Estimator.to_numpy()
                                ListAll = [Betasall_unscaled, Betasall, pdpBetasall, FeatImpall, FeatImpCVall, FeatImpstdall, FeatImpstdCVall, \
                                    R2, CVR2, CVR2stdev, Modelalpha, yfit, yfitCV, yfit, yfitCV, ADF, KPSS, Dependent, Model, Estimator]
                                if len(DDcolumns)>0:
                                    ListAll = DDlist + ListAll
                                ArrayAll = np.concatenate(ListAll, axis=1)
                                Resultsplus = pd.DataFrame(ArrayAll, index=Yfit[Identifier], columns=DDcolumns + \
                                    Betasallcols_unscaled + Betasallcols + pdpBetascols + FeatImpcols + FeatImpcolsCV + FeatImpcolsstd + FeatImpcolsstdCV + \
                                    ['R2', 'CVR2', 'CVR2stdev', 'Modelalpha', 'Yfit', 'YfitCV', 'Residual', 'ResidualCV'] + ADFcolumns + KPSScolumns + \
                                     ['Dependent', 'Model', 'Estimator'])
                                Resultsplus = Resultsplus.reset_index()
                                Resultsplus = Resultsplus.apply(pd.to_numeric, errors='ignore')

                                # Create table with existing data and factor columns. Rows are different models.
                                DataNew = Resultsplus.loc[[0],:].copy()
                                DataNew['Dependent'] = Y
                                DataNew['Model'] = 'Y' + str(Ylist.index(Y)) + 'M' + str(Models.index(m)) + 'E' + str(e) + suffix
                                DataNew['Estimator'] = estimatorlist[e]
                                DataNew[PeriodLabel] = Period
                                DataNew['Horizon'] = Horizon
                                DataNew['Window'] = Window
                                # Save into SQL
                                # CRVplusflag = CRVplusflag + 1
                                DataNew = DataNew.reindex(sorted(DataNew.columns), axis=1)
                                action = 'replace'
                                if override == True or Datacheck.shape[0] > 999999:
                                    deletefromsql(Period, tableAll)
                                # ColsFloat = list(DataNew.columns[pd.Index([x.upper() for x in DataNew.columns]).str.contains('|'.join(['YR', 'ASSET MON', 'ACCRUED', 'DEBT', 'BETA', 'SPREAD', 'VALUE', 'CF',
                                #  'COM EQ MON', 'CPD', 'CQD', 'RATIO', 'R2', 'CASH', 'SHARES', 'NUM', 'TOT RET', 'CONVEX', 'PRICE', 'DEBT', 'DTS',
                                #  'DTW', 'DISTANCE', 'YIELD', 'DUR', 'EARN SH', 'EARN_REVS', 'EBIT', 'TST', 'EPS', 'EQTY', 'EARNS', 'FEATIMP', 'OAS', 'MARKETVALUE', 'COUNT',
                                #  'DURATION', 'WEIGHT', 'DEBT', 'MKT_VAL', 'MKTVAL', 'CPN', 'SALES', 'NET SALE', 'MARGIN', 'PD', 'CPN', 'DTW', 'PR_RET','QUALITY','REGRESS','ROA','ROE','ROIC','RETURN','GRTH','TR_IDX','TR_PCT',
                                #  'VALUE','YTM','YTW','YLD','YFIT','CORR','D2','LAMDA']))])
                                # ColsObject = [x for x in DataNew.columns if x not in ColsFloat]
                                # DataNewColumns = sqlcol('', ColsFloat, 'float64')
                                # DataNew = DataNew.astype(DataNewColumns)
                                # DataNewColumns = sqlcol('', ColsObject, 'object')
                                # DataNew = DataNew.astype(DataNewColumns)
                                savetosql(DataNew, tableAll, action, 1, log, Period)

                                if len(DDcolumns) > 0:
                                    # Create table with existing data and (fit, not cv) result columns for each model as a new column
                                    # 1. Merge results with existing data or with merged results from previous model
                                    end = '_Y' + str(Ylist.index(Y)) + 'M' + str(Models.index(m)) + 'E' + str(e) + suffix
                                    Resultsplus.columns = [s.replace('fit', end) for s in Resultsplus.columns]
                                    temp0 = TrainingData if E == 0 and e == 0 and Y == Ylist[0] else TrainingDataplus
                                    TrainingDataplus = pd.merge(temp0, Resultsplus[[Identifier, 'd2' + end, 'PD' + end, 'CPD' + end, 'CQDF' + end, \
                                                                                    'Issuer OASt' + end]], left_on=Identifier, right_on=Identifier, how='left').reset_index(drop=True)
                                    # 2. Calculate CRV for each model
                                    temp = (TrainingDataplus['Issuer OAS'] / TrainingDataplus['Issuer OASt' + end]).to_frame().replace(np.inf, np.nan)
                                    temp['Log'] = np.log(temp.iloc[:, 0])
                                    temp.drop(temp.columns[0], axis=1, inplace=True)
                                    TrainingDataplus['Issuer CRV' + end] = temp
                                    # TrainingDataflag = TrainingDataflag + 1
    if len(DDcolumns) > 0:
        # Save into SQL
        if Save == True:
            TrainingDataplus = TrainingDataplus.reindex(sorted(TrainingDataplus.columns), axis=1)
            if override == True or Datacheck.shape[0] > 999999:
                deletefromsql(Period, table)
            savetosql(TrainingDataplus, table, action, 1, log, Period)
def nonstatRegress(data, Params):
    assets_src = np.asmatrix(data.get('X'))
    fund_src = data.get('Y')
    fund_src = np.asmatrix(np.array(list(fund_src), dtype=float)).transpose()
    lam = lambdaConvert(Params['lam'])

    T = np.size(assets_src, 0)

    outPoint = Params.get('outPoint', np.zeros(T))
    dynamicmodel = Params.get('dynamicmodel', 0)
    IndividualScaling = Params.get('IndividualScaling', 0)
    # Lcheck = Params.get('Lcheck',0)
    Leverage = Params.get('Leverage', 3)
    Lcheck = 0 if Leverage == 0 else 1

    if (Lcheck):
        Params['Constraints'] = 'long'

    Constraints = Params.get('Constraints', 'long')

    const = Params.get('Constant', 0)
    lamconst = lambdaConvert(Params.get('constlam', 1))
    budget = Params.get('Budget', 0)

    numInd = np.size(assets_src, 1)
    n = np.size(assets_src, 1) - budget + const

    # budget Constraints: cash is the last one in the list of assets
    if (budget):
        # Create excess fund and factor returns by subtracting cash from them
        # all factors except last
        assets_bud = np.zeros([T, numInd - 1])
        fund = fund_src.copy()
        lastelem = numInd - 1
        # last factor
        Xlast = np.repeat(assets_src[:, lastelem], lastelem, 1)
        assets_bud = assets_src[:, 0:lastelem]
        # subtract last factor (cash) from all other factors
        assets_bud = assets_bud - Xlast
        # subtract last factor (cash) from fund
        fund = fund - assets_src[:, lastelem]
    else:
        assets_bud = assets_src.copy()
        fund = fund_src.copy()

    #    alpha
    if (const):
        assets = np.ones((T, n))
        assets[:, 0:n - 1] = assets_bud
        assets = np.asmatrix(assets)
    else:
        assets = assets_bud

    assets = np.asmatrix(assets)
    fund = np.asmatrix(fund)

    Q = np.zeros([T * n, T * n])
    Y = np.zeros([T * n, 1])
    regul = 0 * np.eye(n)

    VtUVt = np.eye(n)
    VtU = np.eye(n)
    if IndividualScaling:
        U = Scale(assets)
    else:
        U = np.eye(n)

    for i in range(n):
        U[i, i] = lam * U[i, i]  # lam/abs(np.mean(assets[:,i]))
    if (const):
        U[n - 1, n - 1] = lamconst
    # import ipdb; ipdb.set_trace()

    for t in range(T):
        returns_t = assets[t, :]

        for i in range(n):
            if dynamicmodel > 0:
                # import ipdb; ipdb.set_trace()
                vti = (1 + returns_t[0, i] / 100) / (1 + fund[t] / 100)
            else:
                vti = 1

            VtU[i, i] = vti * U[i, i]
            VtUVt[i, i] = vti * U[i, i] * vti

        if outPoint[t] == 0:
            # factor return at time t squared
            temp = np.mat(returns_t).T * np.mat(returns_t)
            xxT = temp + regul
            yx = fund[t] * returns_t
        else:
            xxT = np.zeros([n, n])
            yx = np.zeros([n])
        a, b, c = t * n, (t + 1) * n, (t + 2) * n

        if t == 0:
            Q[a:b, a:b] = xxT + VtUVt
        elif t == T - 1:
            Q[a:b, a:b] = xxT + U
        else:
            Q[a:b, a:b] = xxT + VtUVt + U
        # import ipdb; ipdb.set_trace()
        Y[a:b, 0] = yx

        if t < T - 1: Q[b:c, a:b] = Q[a:b, b:c] = -VtU

    if Constraints == 'no':
        betas_alpha = np.dot(np.linalg.inv(Q), Y)
    elif Constraints == 'long':
        # Q: xxT + VtUVt, Y: yx
        betas_alpha = QPSolve(Q, Y, n, T, Params)

    betas_alpha = np.reshape(betas_alpha, (T, n))
    fund_est = np.zeros([T, 1])
    for t in range(T):
        returns_t = assets[t, :]

        fund_est[t, 0] = np.dot(betas_alpha[t, :], assets[t, :].T)

    alpha = np.zeros([T, 1])
    if (const):
        beta = betas_alpha[:, 0:(n - 1)]
        alpha[:, 0] = betas_alpha[:, n - 1]
    else:
        beta = betas_alpha
        alpha = np.zeros([T, 1])

    if (budget):
        beta_res = np.zeros([T, numInd])
        for t in range(T):
            beta_res[t, 0:(numInd - 1)] = beta[t, :]
            beta_res[t, numInd - 1] = 1.0 - sum(beta[t, :])
    else:
        beta_res = beta

    result = {'beta': beta_res, 'alpha': alpha, 'fund_est': fund_est}
    return result
def OLS(data, Params):
    X = np.asmatrix(data.get('X'))
    # Before:
    # Y = np.asmatrix(data.get('Y'))
    # After:
    Y = data.get('Y')
    Y = np.asmatrix(np.array(list(Y), dtype=float)).transpose()
    Y = np.asmatrix(data.get('Y'))

    vebose = Params.get('vebose', 0)
    budget = Params.get('Budget', 0)
    const = Params.get('Constant', 0)
    levereged = Params.get('Leverage', 0)
    n = np.size(X, 1)
    T = np.size(X, 0)
    outPoint = Params.get('outPoint', np.zeros(T))
    if (budget):
        Xnew = np.zeros([T, n - 1])
        Ynew = Y.copy()
        lastelem = n - 1
        Xlast = np.repeat(X[:, lastelem], lastelem, 1)
        Xnew = X[:, 0:lastelem]
        Xnew = Xnew - Xlast
        Ynew = Ynew - X[:, lastelem]
    else:
        lastelem = n
        Xnew = X.copy()
        Ynew = Y.copy()
    if (const):
        ones = np.ones([T, 1])
        Xnew = np.append(Xnew, ones, 1)
    XTX = np.matmul(np.transpose(Xnew), Xnew)
    XTY = np.matmul(np.transpose(Xnew), Ynew)
    XTXinv = np.linalg.inv(XTX)

    if (levereged):
        beta = hlLeveragedQPSolve(XTX, XTY, Params)
    else:
        beta = np.linalg.solve(XTX, XTY)

    if (const):
        alpha = beta[lastelem, 0]
        beta = beta[0:lastelem, 0]
    else:
        alpha = 0
    if (budget):
        newbeta = np.zeros([n, 1])
        newbeta[0:lastelem, :] = beta[0:lastelem, :]
        newbeta[lastelem, 0] = 1.0 - np.sum(beta[0:lastelem, 0])
        beta = newbeta.copy()

    betas = np.array([x for sublist in beta.tolist() for x in sublist] *
                     outPoint.shape[0]).reshape(outPoint.shape[0], n)
    alphas = np.ones([outPoint.shape[0], 1]) * alpha

    result = {'beta_est': betas, 'alpha_est': alphas, 'betaCov': XTXinv}
    return result
def OLSEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    model = skOLS(fit_intercept=True)
    modelfit = model.fit(x, y)
    yfitCV = cross_val_predict(model, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = modelfit.predict(x)
    R2 = r2_score(y, yfit)
    beta = modelfit.coef_
    Intercept = modelfit.intercept_
    yfitPredict = Intercept + np.dot(x, np.transpose(beta))

    def score(x, y):
        if n_folds == y.shape[0]:
            yfit = cross_val_predict(model, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(model, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = modelfit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(model, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev,
               'beta': beta, 'Intercept': Intercept, 'pdpbeta': pdpbeta,
               'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd, 'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def OLSposEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):

    model = Lasso(alpha=0.00001, positive=True, fit_intercept=True, normalize=False)
    modelfit = model.fit(x, y)
    yfitCV = cross_val_predict(model, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = modelfit.predict(x)
    R2 = r2_score(y, yfit)
    beta = modelfit.coef_
    Intercept = modelfit.intercept_
    a = 0.00001

    def score(x, y):
        if n_folds == y.shape[0]:
            yfit = cross_val_predict(model, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(model, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = modelfit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(model, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev,
               'beta': beta, 'Intercept': Intercept, 'pdpbeta': pdpbeta, 'modelalpha': a,
               'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd, 'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def RidgeEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    model_folds = None if n_folds==y.shape[0] else n_folds
    model = RidgeCV(alphas=alphas, fit_intercept=True, normalize=False, cv=model_folds)
    modelfit = model.fit(x, y)
    yfitCV = cross_val_predict(model, x, y.reshape(-1, 1), cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds==y.shape[0] else np.mean(cross_val_score(model,x,y,cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = modelfit.predict(x)
    R2 = r2_score(y, yfit)
    beta = modelfit.coef_
    Intercept = modelfit.intercept_
    a = modelfit.alpha_
    # yfitPredict = Intercept + np.dot(xCurrent, np.transpose(beta))
    # R2Predict = r2_score(yCurrent, yfitPredict)

    def score(x, y):
        if n_folds==y.shape[0]:
            yfit = cross_val_predict(model, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(model, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = modelfit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    # fig = plt.subplots()
    # plot_partial_dependence(model, x, [0,1])
    # plt.show()
    # pdp, axes = partial_dependence(model, x, [0])

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(model, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev,
               'beta': beta, 'Intercept': Intercept, 'pdpbeta': pdpbeta, 'modelalpha': a,
               'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd, 'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def LassoEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    # First CV to gets smaller set of factors
    # Elements of statistical learning:
    # The idea is to use cross - validation to estimate the initial penalty parameter
    # for the lasso, and then again for a second penalty parameter applied to the selected set of predictors
    # the variables in the second step have less “competition” from noise variables, cross-validation
    # will tend to pick a smaller value for λ, and hence their coefficients will be shrunken less than
    # those in the initial estimate.
    xdf = pd.DataFrame(x)
    CVmodel = LassoCV(alphas=alphas, fit_intercept=True, normalize=False, cv=n_folds)
    CVfit = CVmodel.fit(x, y)
    CVbeta = CVfit.coef_
    Betanonzero = (CVbeta != 0).tolist()
    xsmall = xdf.loc[:, Betanonzero].to_numpy()
    if xsmall.shape[1]>0:
        # Second CV to get better parameter
        modelfit = CVmodel.fit(xsmall, y)
        model = Lasso(alpha=modelfit.alpha_, fit_intercept=True, normalize=False)
        yfitCV = cross_val_predict(model, xsmall, y, cv=n_folds)
        CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, xsmall, y, cv=n_folds))
        CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, xsmall, y, cv=n_folds))
        yfit = modelfit.predict(xsmall)
        R2 = r2_score(y, yfit)
        CVbeta = modelfit.coef_
        # Populate original factor betas
        CVbetadf = pd.DataFrame(CVbeta).T
        beta = pd.DataFrame(np.zeros((1, x.shape[1])))
        CVbetadf.columns = beta.loc[:, Betanonzero].columns
        beta.loc[:, Betanonzero] = CVbetadf
        betadf = beta.copy()
        beta = beta.to_numpy().flatten()
        Intercept = modelfit.intercept_
        a = modelfit.alpha_
        # yfitPredict = Intercept + np.dot(xCurrent, np.transpose(beta))
        # R2Predict = r2_score(yCurrent, yfitPredict)

        def score(x, y):
            if n_folds == y.shape[0]:
                yfit = cross_val_predict(model, x, y, cv=n_folds)
                return r2_score(y, yfit)
            else:
                return np.mean(cross_val_score(model, x, y, cv=n_folds))

        base_score, score_decreases = get_score_importances(score, x, y)
        FeatImpCV = np.mean(score_decreases, axis=0)
        FeatImpstdCV = np.std(score_decreases, axis=0)

        def score(x, y):
            yfit = modelfit.predict(x)
            return r2_score(y, yfit)

        base_score, score_decreases = get_score_importances(score, xsmall, y)
        FeatImp = np.mean(score_decreases, axis=0)
        FeatImpstd = np.std(score_decreases, axis=0)

        FeatImpdf = pd.DataFrame(FeatImp).T
        FeatImp = pd.DataFrame(np.zeros((1, x.shape[1])))
        FeatImpdf.columns = betadf.loc[:, Betanonzero].columns
        FeatImp.loc[:, Betanonzero] = FeatImpdf
        FeatImp = FeatImp.to_numpy().flatten()

        FeatImpstddf = pd.DataFrame(FeatImpstd).T
        FeatImpstd = pd.DataFrame(np.zeros((1, x.shape[1])))
        FeatImpstddf.columns = betadf.loc[:, Betanonzero].columns
        FeatImpstd.loc[:, Betanonzero] = FeatImpstddf
        FeatImpstd = FeatImpstd.to_numpy().flatten()

        K = xsmall.shape[1]
        pdpbeta = np.empty((K,)) * np.nan
        for i in range(K):
            pdp, axes = partial_dependence(modelfit, xsmall, [i])
            N = pdp.shape[1]
            pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])
        pdpbetadf = pd.DataFrame(pdpbeta).T
        pdpbeta = pd.DataFrame(np.zeros((1, x.shape[1])))
        pdpbetadf.columns = betadf.loc[:, Betanonzero].columns
        pdpbeta.loc[:, Betanonzero] = pdpbetadf
        pdpbeta = pdpbeta.to_numpy().flatten()

        results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev, 'pdpbeta': pdpbeta,
                   'beta': beta, 'Intercept': Intercept, 'modelalpha': a,
                   'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd, 'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}
    else:
        results = {}

    return results
def SVREst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    grid = {
        'C': 10.0 ** np.arange(0, 4),
        'gamma': np.logspace(-2, 2, 5),
        'kernel': ['rbf']}
    # Light
    # grid = {
    #     'C': 10.0 ** np.arange(1),
    #     'gamma': np.logspace(-2, 2, 1),
    #     'kernel': ['rbf']}

    model = SVR()
    # determine the optimal values to be used for the hyperparameters of our model from a specified range of values
    modelrandomCV = RandomizedSearchCV(estimator=model, param_distributions=grid,
                                       n_iter=n_iters, cv=n_folds, verbose=2, random_state=False)
    randomCVfit = modelrandomCV.fit(x, y)
    best_params = randomCVfit.best_params_
    modelrandom = SVR(C=best_params["C"],
                      gamma=best_params["gamma"],
                      kernel=best_params["kernel"],
                      verbose=False)
    fit = modelrandom.fit(x, y)
    yfitCV = cross_val_predict(modelrandom, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = fit.predict(x)
    R2 = r2_score(y, yfit)

    def score(x, y):
        if n_folds==y.shape[0]:
            yfit = cross_val_predict(modelrandom, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(modelrandom, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = fit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    # fig = plt.subplots()
    # plot_partial_dependence(modelrandom, x, [0,1])
    # plt.show()
    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(modelrandom, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev, 'pdpbeta': pdpbeta,
               'best_params': best_params, 'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd,
               'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def GBEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    # https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingRegressor.html#sklearn.ensemble.GradientBoostingRegressor
    grid = {
        'learning_rate': [0.05, 0.1],
        'n_estimators': [int(x) for x in np.linspace(start=100, stop=400, num=4)],
        'subsample': [0.25, 0.5, 0.75, 1],
        'max_depth': [int(x) for x in np.linspace(start=2, stop=int(np.sqrt(x.shape[1])), num=1)],
        'min_samples_split': [int(x) for x in np.linspace(start=max(int(np.sqrt(x.shape[1])), 3), stop=2 * x.shape[1], num=3)],
        'min_samples_leaf': [int(x) for x in np.linspace(start=max(int(0.5 * np.sqrt(x.shape[1])), 3), stop=x.shape[1], num=3)],
        'min_impurity_decrease': [0, 0.1],
        'max_features': ['auto', 'sqrt']}
    # Light
    # grid = {
    #     'learning_rate': [0.1],
    #     'n_estimators': [int(x) for x in np.linspace(start=10, stop=10, num=1)],
    #     'subsample': [1],
    #     'max_depth': [int(x) for x in np.linspace(start=2, stop=int(np.sqrt(x.shape[1])), num=1)],
    #     'min_samples_split': [int(x) for x in np.linspace(start=max(int(np.sqrt(x.shape[1])), 3), stop=2 * x.shape[1], num=1)],
    #     'min_samples_leaf': [int(x) for x in np.linspace(start=max(int(0.5 * np.sqrt(x.shape[1])), 3), stop=x.shape[1], num=1)],
    #     'min_impurity_decrease': [0.1],
    #     'max_features': ['auto']}

    # Gradient boosting
    model = GradientBoostingRegressor()
    # determine the optimal values to be used for the hyperparameters of our model from a specified range of values
    modelrandomCV = RandomizedSearchCV(estimator=model, param_distributions=grid,
                                       n_iter=n_iters, cv=n_folds, verbose=0, random_state=42, refit=True)
    # https://medium.com/datadriveninvestor/random-forest-regression-9871bc9a25eb
    randomCVfit = modelrandomCV.fit(x, y)
    best_params = randomCVfit.best_params_
    modelrandom = GradientBoostingRegressor(learning_rate=best_params["learning_rate"],
                                            n_estimators=best_params["n_estimators"],
                                            subsample=best_params["subsample"],
                                            max_depth=best_params["max_depth"],
                                            min_samples_split=best_params["min_samples_split"],
                                            min_samples_leaf=best_params["min_samples_leaf"],
                                            min_impurity_decrease=best_params["min_impurity_decrease"],
                                            max_features=best_params["max_features"],
                                            random_state=False,
                                            verbose=False)
    fit = modelrandom.fit(x, y)
    yfitCV = cross_val_predict(modelrandom, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = fit.predict(x)
    R2 = r2_score(y, yfit)

    def score(x, y):
        if n_folds==y.shape[0]:
            yfit = cross_val_predict(modelrandom, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(modelrandom, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = fit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(modelrandom, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev, 'pdpbeta': pdpbeta,
               'best_params': best_params, 'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd,
               'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def RandomForestEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    # Random forest parameter space
    # https://towardsdatascience.com/hyperparameter-tuning-the-random-forest-in-python-using-scikit-learn-28d2aa77dd74
    # https: // towardsdatascience.com / how - to - tune - a - decision - tree - f03721801680
    grid = {
        'n_estimators': [int(x) for x in np.linspace(start=100, stop=400, num=4)],
        'max_depth': [int(x) for x in np.linspace(start=2, stop=int(np.sqrt(x.shape[1])), num=1)],
        'min_samples_split': [int(x) for x in
                              np.linspace(start=max(int(np.sqrt(x.shape[1])), 5), stop=min(max(int(np.sqrt(x.shape[1])), 3), 2 * x.shape[1]), num=3)],
        'min_samples_leaf': [int(x) for x in
                             np.linspace(start=max(int(0.5 * np.sqrt(x.shape[1])), 5), stop=min(max(int(0.5 * np.sqrt(x.shape[1])), 5), x.shape[1]), num=3)],
        'min_impurity_decrease': [0, 0.1],
        'max_features': ['auto', 'sqrt'],
        'bootstrap': [False]}
    # light
    # grid = {
    #     'n_estimators': [int(x) for x in np.linspace(start=10, stop=10, num=1)],
    #     'max_depth': [int(x) for x in np.linspace(start=2, stop=2, num=1)],
    #     'min_samples_split': [int(x) for x in
    #           np.linspace(start=5, stop=5, num=1)],
    #     'min_samples_leaf': [int(x) for x in
    #                          np.linspace(start=5, stop=5, num=1)],
    #     'min_impurity_decrease': [0.1],
    #     'max_features': ['auto'],
    #     'bootstrap': [False]}

    model = RandomForestRegressor()
    # determine the optimal values to be used for the hyperparameters of our model from a specified range of values
    modelrandomCV = RandomizedSearchCV(estimator=model, param_distributions=grid,
                                       n_iter=n_iters, cv=n_folds, verbose=2, random_state=False, refit=True)
    # https://medium.com/datadriveninvestor/random-forest-regression-9871bc9a25eb
    randomCVfit = modelrandomCV.fit(x, y)
    best_params = randomCVfit.best_params_
    modelrandom = RandomForestRegressor(n_estimators=best_params["n_estimators"],
                                        max_features=best_params["max_features"],
                                        max_depth=best_params["max_depth"],
                                        min_samples_split=best_params["min_samples_split"],
                                        min_samples_leaf=best_params["min_samples_leaf"],
                                        min_impurity_decrease=best_params["min_impurity_decrease"],
                                        bootstrap=best_params["bootstrap"],
                                        random_state=False,
                                        verbose=False)
    fit = modelrandom.fit(x, y)
    yfitCV = cross_val_predict(modelrandom, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = fit.predict(x)
    R2 = r2_score(y, yfit)

    # https: // github.com / TeamHG - Memex / eli5 / blob / master / eli5 / permutation_importance.py  # L55
    def score(x, y):
        if n_folds==y.shape[0]:
            yfit = cross_val_predict(modelrandom, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(modelrandom, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = fit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(modelrandom, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    # https: // christophm.github.io / interpretable - ml - book / feature - importance.html
    # how much the model relies on each feature for making predictions(-> training data)
    # or how much the feature contributes to the performance of the model on unseen data (-> test data)
    # https: // github.com / TeamHG - Memex / eli5 / issues / 316
    # PI = permutation_importance(RFmodelrandom, x, y, n_repeats=10,random_state=42, n_jobs=-1)
    # PImean = PI.importances_mean
    # perm = PermutationImportance(RFfit).fit(x, y, cv=n_folds)
    # perm.FeatImp_
    # perm.FeatImp_std_

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev, 'pdpbeta': pdpbeta,
               'best_params': best_params, 'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd,
               'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
def MLPEst(x, xCurrent, y, yCurrent, alphas, n_folds, n_iters, max_iters):
    # MLP parameter space
    # https://datascience.stackexchange.com/questions/36049/how-to-adjust-the-hyperparameters-of-mlp-classifier-to-get-more-perfect-performa
    # For 3 hidden layers of 100,50,25 units we have: hidden_layer_sizes=(100,50,25)
    grid = {
        'hidden_layer_sizes': [(10,), (10, 5,), (5, 5)],
        'activation': ['tanh', 'relu'],
        'solver': ['lbfgs', 'adam'],
        'alpha': alphas.tolist(),
        'learning_rate': ['constant'],
        'max_iter': [500]}
    # Light
    # grid = {
    #     'hidden_layer_sizes': [(10,)],
    #     'activation': ['tanh'],
    #     'solver': ['lbfgs'],
    #     'alpha': alphas.tolist(),
    #     'learning_rate': ['constant'],
    #     'max_iter': [10]}

    # Multilayer perceptron
    model = MLPRegressor()
    modelrandomCV = RandomizedSearchCV(estimator=model, param_distributions=grid,
                                       n_iter=n_iters, cv=n_folds, verbose=2, random_state=42, refit=True)
    randomCVfit = modelrandomCV.fit(x, y)
    best_params = randomCVfit.best_params_
    modelrandom = MLPRegressor(hidden_layer_sizes=best_params["hidden_layer_sizes"],
                               activation=best_params["activation"],
                               solver=best_params["solver"],
                               alpha=best_params["alpha"],
                               learning_rate=best_params["learning_rate"],
                               max_iter=best_params["max_iter"],
                               random_state=False,
                               verbose=False)

    fit = modelrandom.fit(x, y)
    yfitCV = cross_val_predict(modelrandom, x, y, cv=n_folds)
    CVR2 = r2_score(y, yfitCV) if n_folds == y.shape[0] else np.mean(cross_val_score(model, x, y, cv=n_folds))
    CVR2stdev = np.nan if n_folds == y.shape[0] else np.std(cross_val_score(model, x, y, cv=n_folds))
    yfit = fit.predict(x)
    R2 = r2_score(y, yfit)

    def score(x, y):
        if n_folds==y.shape[0]:
            yfit = cross_val_predict(modelrandom, x, y, cv=n_folds)
            return r2_score(y, yfit)
        else:
            return np.mean(cross_val_score(model, x, y, cv=n_folds))

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImpCV = np.mean(score_decreases, axis=0)
    FeatImpstdCV = np.std(score_decreases, axis=0)

    def score(x, y):
        yfit = fit.predict(x)
        return r2_score(y, yfit)

    base_score, score_decreases = get_score_importances(score, x, y)
    FeatImp = np.mean(score_decreases, axis=0)
    FeatImpstd = np.std(score_decreases, axis=0)

    K = x.shape[1]
    pdpbeta = np.empty((K,)) * np.nan
    for i in range(K):
        pdp, axes = partial_dependence(modelrandom, x, [i])
        N = pdp.shape[1]
        pdpbeta[i] = (pdp[0, N - 1] - pdp[0, 0]) / (axes[0][N - 1] - axes[0][0])

    results = {'yfit': yfit, 'yfitCV': yfitCV, 'R2': R2, 'CVR2': CVR2, 'CVR2stdev': CVR2stdev, 'pdpbeta': pdpbeta,
               'best_params': best_params, 'FeatImp': FeatImp, 'FeatImpstd': FeatImpstd,
               'FeatImpCV': FeatImpCV, 'FeatImpstdCV': FeatImpstdCV}

    return results
# def KNNEst(x, xpredict, y, ypredict, alphas, n_folds, n_iters, max_iters):
#
#     # Random forest parameter space
#     # https://towardsdatascience.com/hyperparameter-tuning-the-random-forest-in-python-using-scikit-learn-28d2aa77dd74
#     # https: // towardsdatascience.com / how - to - tune - a - decision - tree - f03721801680
#     KNNgrid = {
#         'n_neighbors': list(range(5, 20, 4)),
#         'weights': ['uniform','distance'],
#         'leaf_size': list(range(1, 50, 5)),
#         'p':[1,2]}
#
#     KNNmodel = KNN()
#     # determine the optimal values to be used for the hyperparameters of our model from a specified range of values
#     KNNmodelrandomCV = RandomizedSearchCV(estimator=KNNmodel, param_distributions=KNNgrid,
#                      scoring="accuracy", n_iter=n_iters, cv=n_folds, verbose=2, random_state=False, n_jobs=-1)
#     # https://medium.com/datadriveninvestor/random-forest-regression-9871bc9a25eb
#     KNNrandomCVfit = KNNmodelrandomCV.fit(x, y)
#     best_params = KNNrandomCVfit.best_params_
#     KNNmodelrandom = KNN(C=best_params["C"],
#                       gamma=best_params["gamma"],
#                       kernel=best_params["kernel"],
#                       verbose=False)
#     KNNfit = KNNmodelrandom.fit(x, y)
#     KNNy_predCV = cross_val_predict(KNNmodelrandom, x, y, cv=n_folds)
#     KNNCVR2 = r2_score(y, KNNy_predCV)
#     KNNy_pred = KNNfit.predict(x)
#     KNNR2 = r2_score(y, KNNy_pred)
#     PI = permutation_importance(KNNmodelrandom, x, y, n_repeats=10,random_state=42, n_jobs=-1)
#     PImean = PI.importances_mean
#
#     # https: // github.com / TeamHG - Memex / eli5 / blob / master / eli5 / permutation_importance.py  # L55
#     def score(x, y):
#         y_pred = cross_val_predict(RFmodelrandom, x, y, cv=n_folds)
#         return r2_score(y, y_pred)
#     base_score, score_decreases = get_score_importances(score, x, y)
#     FeatImp = np.mean(score_decreases, axis=0)
#     FeatImpstd = np.std(score_decreases, axis=0)
#     def score(x, y):
#         y_pred = RFfit.predict(x)
#         return r2_score(y, y_pred)
#     base_score, score_decreases = get_score_importances(score, x, y)
#     FeatImp1 = np.mean(score_decreases, axis=0)
#     FeatImpstd1 = np.std(score_decreases, axis=0)
#
#     # https: // christophm.github.io / interpretable - ml - book / feature - importance.html
#     # how much the model relies on each feature for making predictions(-> training data)
#     # or how much the feature contributes to the performance of the model on unseen data (-> test data)
#     # https: // github.com / TeamHG - Memex / eli5 / issues / 316
#     perm = PermutationImportance(RFfit).fit(x, y, cv=n_folds)
#     eli5.show_weights(perm)
#     perm.FeatImp_std_
#     perm.
#
#     return RFy_pred, RFR2, RFCVR2, best_params
