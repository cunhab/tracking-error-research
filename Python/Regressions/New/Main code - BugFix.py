
'''
ResultsAll stores results for each run (parameter, fund, window, factorset, lamda, horizon, lag),
shown in each column

ResultsAllC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag), shown in each column

ResultsRC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag).
The difference with ResultsAllC is that results across windows are saved on the same column,
each window being represented by only one value, corresponding typically to the last point
of the window (what is noted as quantile below).

The code doesn't currently calculate stats across windows per FactorSet.
That would have to be another loop before the window loop while disabling
FactorSet calibration.

The below results get populated for the following analytics:
Alpha (regression intercept), Beta, Fund, Horizons, Lags, Lamda, PR2,
PR2 Smooth, Predicted Beta, Predicted Residual, Predicted Style, Predicted Style Smooth,
R2 Style, R2 Predicted Style, R2 Smooth, Residual Unsmooth, Residual, Style

Analytic Date:                  Analytic at each point in time
Analytic Mean Date:             Mean Analytic across all dates of a given window.
                                Date is the window quantile Date.
Analytic OV Windows Mean Date:  Mean Analytic across all windows of a given point in time
Analytic Rolling Windows Mean:  Mean Analytic across all quantile points and windows
                                (one quantile point per window)
Analytic Window Date:           Analytic of a given window and quantile point in time
                                (the Analytic that is used in rolling window calculations)

The below summary statistics get populated for the following analytics:
Fund, Predicted Residual, Predicted Style, Residual, Style, FundOOS, ResidualOOS, StyleOOS
Note that residual mean corresponds to alpha (intercept) if no intercept is selected.
Fund, Fund OOS, ResidualOOS, StyleOOS also report entire date range analytics (like Fund Mean).

Mean, Stdev, Variance, Mean/Stdev, Min, Max, Skewness, Kurtosis, Durbin-Watson

The below summary statistics get populated for the following analytics:
Predicted Style, Style

R2, Correlation, Beta + Date:   Date is the window quantile Date

'''

##########################################################################################################################################
#######################################################             ######################################################################
#######################################################  S T A R T  ######################################################################
#######################################################             ######################################################################
##########################################################################################################################################
import pandas as pd
from Functions import *
pd.options.mode.chained_assignment = None  # default='warn'
from functools import reduce
import time
from progressbar import ProgressBar
from tqdm import tqdm

####################################################### User inputs #####################################################################

##########################
### Files and Controls ###
##########################

TestCase = True
DoReshuffle = True

filepath = 'C:/Users/apoll/Dropbox/Python/Data/'
filepathOut = 'C:/Users/apoll/Dropbox/Python/Results1/'
ParametersFilename = 'ParametersPE.xlsx'
AssetSet = 'AQR_Daily_Proxied' if TestCase == False else 'PE'
FactorFilename = 'FactorGroupDict.xlsx' if TestCase == False else 'FactorGroupDictPE.xlsx'

LamdaList = [0.6, 0.65, 0.7, 0.85, 1] if TestCase == False else [1]
ParameterCaseList = [18]

###############
### Generic ###
###############

NReshuffle = 100
MC = 10 if DoReshuffle == True else 1
# Retrieve parameters from excel
excel = 1
if excel == 1:
    file = filepath + '//' + ParametersFilename
    ParametersOrig = pd.read_excel(open(file, 'rb'), engine='openpyxl',sheet_name='Parameters', header=1)
    # Drop notes
    ParametersOrig.drop([ParametersOrig.index[0]], inplace=True)
    ParametersOrig = ParametersOrig.reset_index(drop=True)
else:
    filepath = ''
    filename = ''
    ParametersOrig = pd.DataFrame()
# Data frequency, Daily or Monthly
frequency = 'Weekly'
# Risk parity
if TestCase==False:
    RiskParity = True
    RiskParityGroups = True
else:
    RiskParity = False
    RiskParityGroups = False
StdevWindowList = [66, 126, 252]
# Latest start date
Datestartmax = datetime.strptime('2019-05-01', '%Y-%m-%d')
# Earliest start date for data availability
Datestartmin = datetime.strptime('2000-01-01', '%Y-%m-%d')
# Earliest start date for windows
DateStart = np.nan
# Latest end date for windows
DateEnd = np.nan
NoOfWindows = np.nan
Frequencyvalue = frequency
for a, b in {'Daily': 'B', 'Weekly': 'W', 'Monthly': 'M'}.items():
    Frequencyvalue = Frequencyvalue.replace(a, b)
# OOS shift. 0 means we are using the next point OOS to forecast, -1 the last in sample point
OOSShift = 0
OOSShiftMin = 0
OOSShiftMax = 0
# Consecutive missing points up to which processing takes place
ConsecutiveMissing = 10
# max % missing points up to which processing takes place
MaxMissingPercent = 0.5
Horizons = 0
# Min horizons. Starts from 0.
HorizonsMin = 0
# Max horizons. Starts from 0.
HorizonsMax = 0
# Horizon calibration step
HorizonStep = 2
# Rolling window trigger: 1 do, 0 do not.
DoRolling = 1
# Rolling window size
window = 10
# Rolling window quantile: 0 for first point, 1 for last point, 0.5 for center window
Point = 1
# Number of returns has to be at least MinReturnsDesired
# Assumming just one factor, long horizons reduce data by Ky-1 (regression) + Ky-1 (PR2 Smooth) = 2(Ky - 1)
MinReturnsDesired = 10
# Min return catch - making sure MinReturnsDesired and window size are consistent
windowreturns = window if DoRolling == 1 else MinReturnsDesired
#Minreturns = max(MinReturnsDesired, 1 + 1 + 2 * (HorizonsMax - 1) + 10, windowreturns)
Minreturns = min(MinReturnsDesired, max(MinReturnsDesired, windowreturns))
# Regime. Conditional calculations by using Absolute or raw factor data (returns) that are greater or less than a threshold
# In that case we do not calculate KPIs, for now, as the data may be disjoint
Regime = False
if Regime == True: doKPI = False
# Regime threshold
RegimeThreshold = 0
Direction = 'down'
Absolute = False

###########################
### Regression specific ###
###########################

# Predicted results: 'Predicted Beta','R2','PR2','PR2 Smooth','Style','Predicted Style','PR2 Smooth Avg','Predicted Style Smooth'
doKPI = True
# Model. Sharpe for constant betas, DSA for dynamic betas
Modelvalue = 'DSA'
# mdAPT (no budget constraint) or mdCAPM (budget constraint)
Modeltypevalue = 'mdCAPM'
Intercept = 0
# Constraints: hlNoConstraints (Unconstrained), hlLeveraged (leveraged), hlLongOnly (long only)
Holdingsvalue = 'hlLongOnly'
# lamda
Lamda = 0.65
MarketDynamics = 0
IndividualScaling = 1
UseHalfLife = 0
HalfLife = 0
Decay = 0
Calibrate = 0
Leverage = 0
Threshold = 0.05
# Redundant ?
LongOnly = 0
Weighting = 0
Lags = 0
# Min lags. Min is less (more negative than) max. -3 will turn into -2 which means current and one lag and will show as -1
LagsMin = 0
# Max lags. Starts from -2 which is turned into -1 which in turn means no adjustment.
LagsMax = 0
# Lag calibration step
LagStep = 2
# Regress up to MaxRows of data due to memory issues
MaxRows = 10000
# Rolling style in first (last) point: Extends style based on earliest (latest) available betas
# For example, if rolling window is 30 and last point is checked, the first 29 points will not have
# betas. If RollingStyleInFirstPoint=1, we then use the betas of the last point to populate earlier
# betas and style. If we do not do rolling windows then betas and style are populated over the
# entire window.
RollingStyleInFirstPoint = 1
RollingStyleInLastPoint = 1
if DoRolling == 0:
    RollingStyleInFirstPoint = 1
    Point = 1
# Long horizon style in first point. Not applicable to predicted style - we do not (need to) proxy for predicted purposes.
# So, if horizon>0 and StyleInFirstPoint=1, betas will have more points than predicted betas.
StyleInFirstPoint = 0
# Forward bias - (lagged) factors shifted forward by xshift periods
xshift = 0
# Report time average outside center point, in which case time average will be based on progressively less number of
# overlapping runs. For the first period for example, time average results will equal those from just the first run.
FullTimeAvg = True
# 1:Sum, 2:Avg, 3:Sqrt
WeightAvg = 2
# SummaryStats
SummaryStatsList = ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson']

###############################################################################################################################
#######################################################  Data  ################################################################
###############################################################################################################################

# All daily returns have to be in decimal, not % form. Like 0.05 instead of 5 for a 5% return.
# All monthly returns come in as log returns.
# For long horizons, the code transforms to log.
# This means that monthly data on long horizons are log transformed to log - Fix

# Dependent variables: date - single column with multiple names
# Description, Date, Total Return
AssetreturnsOrig = pd.read_excel(filepath + 'Assets_' + AssetSet + '.xlsx', engine='openpyxl')
AssetreturnsOrig['Date']=pd.to_datetime(AssetreturnsOrig['Date'])
# Non Daily contains proxied daily prices out of which we can construct returns
if TestCase == False and frequency=='Weekly':
    AssetreturnsOrig = ReturnCreator(AssetreturnsOrig, frequency)
    # Transform to Date - Single Column
    AssetreturnsOrig = AssetreturnsOrig.set_index('Date').stack().reset_index()
    AssetreturnsOrig.columns = ['Date', 'Fund', 'Return']
AssetreturnsOrig['Date'] = pd.to_datetime(AssetreturnsOrig['Date']).dt.strftime('%Y-%m-%d')
AssetreturnsOrig.rename(columns = {'Return': 'Total_Return'}, inplace = True)
# Dataset = AssetreturnsOrig
Description = 'Fund'
# Union of all dates across all funds
AllDatesDf = pd.DataFrame(AssetreturnsOrig.loc[:, 'Date'].unique(), columns=['Date'])

# Create dictionary with all factors
FactorGroupDict = pd.read_excel(filepath + FactorFilename, engine='openpyxl', sheet_name=None, header=None)
# List of factor sets, or dictionary keys, to process
FactorGroupList=['RiskParity_Daily_Grouped'] if TestCase == False else ['Test']
FactorSetList = []

if RiskParity==True:

    # Create rolling inverse stdev factors across multiple windows. Only daily for now.
    # Independent variables: date - multiple column names
    FactorSet = 'RiskParity_Daily_More'
    filename = 'Factors_' + FactorSet + '.xlsx'
    InverseStdevCreator(StdevWindowList, filepath, filename)

    # Group (average) rolling inverse stdev factors
    filenameIn = 'FactorsDict_' + FactorSet + '.xlsx'
    filename = 'Factors_' + FactorSet + '_Stdev'
    FactorGrouper(Factorsorig, StdevWindowList, filepath, filenameIn, filename)

    # Factorsorig = pd.concat([Factorsorig[[x for x in list(Factorsorig.columns) if x not in ('Cash')]], Factorsorig[['Cash']]], axis=1)

###############################################################################################################################
#######################################################  Algorithm  #######################################################################
###############################################################################################################################

# Monte Carlo
for MCi in range(MC):

    Date = 'Date'
    ResultsAll = pd.DataFrame()
    ResultsAllC = pd.DataFrame()
    ResultsRC = pd.DataFrame()
    ResultsRCAllPoints = pd.DataFrame()
    ResultsAllReshuffle = pd.DataFrame()
    ResultsAllCReshuffle = pd.DataFrame()
    ResultsRCReshuffle = pd.DataFrame()
    ResultsRCAllPointsReshuffle = pd.DataFrame()
    ParametersDf = pd.DataFrame()
    j = 0
    jlist = [0]
    m = 0 # ResultsAll
    c = 0 # ResultsAllC
    k = 0 # ResultsRC
    r = 0 # Reshuffle
    rName = ''
    mReshuffle = 0 # ResultsAllReshuffle
    cReshuffle = 0 # ResultsAllCReshuffle
    kReshuffle = 0 # ResultsRCReshuffle
    jReshuffle = 0
    jlistReshuffle = [0]
    rReshuffle = 0 # Reshuffle
    end = False

    # For each parameter set
    # for p in range(ParametersOrig.shape[0]):
    # for p in tqdm(range(0,2)):
    for p in ParameterCaseList:
    # p=2
        print('Parameter ' + str(p))

        Analyticsdata = {}
        ParametersLoop = ParametersOrig.iloc[[p], :].reset_index(drop=True)
        # All horizons are augmented by 1 after the below
        Parameters = Parameterset(ParametersLoop, excel, filepath, ParametersFilename, HorizonsMin, HorizonsMax, LagsMin, LagsMax,
                                   StyleInFirstPoint, RollingStyleInFirstPoint, RollingStyleInLastPoint, Modelvalue,
                                   Modeltypevalue, Holdingsvalue, HorizonStep, LagStep, WeightAvg, Horizons,
                                   Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling, UseHalfLife,
                                   HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                                  DateStart, DateEnd, NoOfWindows)
        # Create to save at the end
        ParametersDfTemp = pd.DataFrame(Parameters.items(), columns = ['Parameter', 'Case_' + str(p)])
        ParametersDf = ParametersDfTemp if ParametersDf.empty else pd.merge(ParametersDf, ParametersDfTemp, \
                        left_on = 'Parameter', right_on = 'Parameter', how='inner')

        if excel == 1:
            for d, key in enumerate(list(Parameters.keys())):
                globals()[key] = list(Parameters.values())[d]
            # Min return catch - making sure MinReturnsDesired and window size are consistent
            if TestCase==True:
                MinReturnsDesired = 3
            windowreturns = window if DoRolling == 1 else MinReturnsDesired
            Minreturns = min(MinReturnsDesired, windowreturns)

        # Window start
        WStart = AssetreturnsOrig[AssetreturnsOrig['Date'] == DateStart].index.values[0] if not np.isnan(DateStart) else 0
        # Window end
        if not np.isnan(NoOfWindows):
            WEnd = WStart + NoOfWindows
        elif not np.isnan(DateEnd):
            WEnd = AssetreturnsOrig[AssetreturnsOrig['Date'] == DateEnd].index.values[0]
        else:
            WEnd = np.inf

        # For each Fund
        for i in range(AssetreturnsOrig[Description].unique().shape[0]):
        # i=0 if TestCase == False else 0
            print('Fund ' + str(i))

            # Dataset=readfromsql(table,identifier,idlist)
            # Gather data for bonds
            y = AssetreturnsOrig[AssetreturnsOrig.loc[:, Description].isin([AssetreturnsOrig.loc[:, Description].unique()[i]])].loc[:,
                [Date, 'Total_Return']].reset_index(drop=True)
            cols = y.columns.tolist()
            cols[1] = AssetreturnsOrig.loc[:, Description].unique()[i]
            y.columns = cols
            # Remove any dates that have NaT
            y = y[y[Date].notnull()]
            # Create date range so that we expand the missing values
            # Enhance to align month end dates between given data and generated dates
            # =============================================================================
            #     if frequency=='Daily':
            #         Dates=pd.bdate_range(start=y.iloc[0,0], end=y.iloc[-1,0],freq=Frequencyvalue)
            #     else:
            #         Dates=pd.date_range(start=y.iloc[0,0], end=y.iloc[-1,0],freq='M')
            #     Dates = pd.DataFrame({'Date': Dates})
            #     Dates['Date'] = pd.to_datetime(Dates['Date']).dt.date
            #     y=pd.merge(Dates, y, left_on='Date', right_on=Date, how='left').sort_values(by=['Date']).reset_index(drop=True)
            #     y=y.loc[:,['Date']+[y.columns.tolist()[2]]]
            # =============================================================================
            # Only for monthly data, where the above is commented out!
            y.rename(columns={list(y)[0]: 'Date'}, inplace=True)
            Dependent = y.columns[1]
            # Define missing values
            values = ['nan', 0]
            # Create dataframe with structure of another
            ymissing = pd.DataFrame().reindex_like(y.iloc[:, [1]])
            # Iterate all definitions of missing data. The below adds transparency as we can investigate each type of missing data.
            for s in range(0, len(values)):
                ymiss = missing(y.iloc[:, [1]], values[s])
                ymissing = ymissing.fillna(ymiss)
                y = y.replace(values[s], np.nan)
            ymissing = ymissing.replace(np.nan, 0)
            # First valid data row after OOSing missing data. Why not first, last valid index ?
            start = (ymissing.iloc[:, 0].isnull()).idxmax()
            # Last valid data row before trailing missing data
            end = (ymissing.sort_index(ascending=False).iloc[:, 0].isnull()).idxmax()
            # Proceed only if rest of consecutive missing data is under threshold
            # and number of missing data points after OOSing data and prior to trailing data is under threshold.
            datacheck = float(ymissing.iloc[start:end, :].fillna(0).max()) <= ConsecutiveMissing and float(
                y.iloc[start:end, [1]].isna().sum()) <= MaxMissingPercent * y.iloc[start:end, [1]].shape[0]
            if datacheck == True:

                DatesDf = y.loc[start:end+1, ['Date']].reset_index(drop=True)
                YallOrig = y.iloc[start:end+1, [1]].reset_index(drop=True)
                #######################
                # Rolling window loop #
                #######################

                if DoRolling == 0: window = min(window, YallOrig.shape[0])

                for w in range(max(0, WStart), max(min(YallOrig.shape[0] - window + 1, WEnd), 1 + max(0, WStart))):
                # w=0
                    print('Window ' + str(w))

                    j, jlist, analytics2, analytics3, optimalrow, ResultsAll, ResultsAllC, ResultsRC, ResultsRCAllPoints, Dataall = \
                    Regressor(w, m, c, k, i, p, j, jlist, r, rName, MaxRows, y, Description, window, RollingStyleInFirstPoint,
                      RollingStyleInLastPoint, xshift, start, end, Minreturns, Datestartmax, LamdaList, Parameters, Dependent, FactorGroupList,
                      FactorGroupDict, FactorSetList, AssetreturnsOrig, ResultsAll, ResultsAllC, ResultsRC, ResultsRCAllPoints, TestCase, OOSShift, Point,
                      frequency, filepath, RegimeThreshold, Direction, Absolute, doKPI, Regime)

                    #############
                    # Reshuffle #
                    #############

                    if DoReshuffle == True:

                        # Reshuffling takes place on the optimal parameters and works like a window
                        ParametersReshuffle = Parameters
                        FactorGroupListReshuffle = [ResultsAll.loc[optimalrow, 'FactorGroup']]
                        FactorSetListReshuffle = [ResultsAll.loc[optimalrow, 'FactorSet']]
                        LamdaListReshuffle = [ResultsAll.loc[optimalrow, 'Lamda']]
                        ParametersReshuffle['lam'] = ResultsAll.loc[optimalrow, 'Lamda']
                        ParametersReshuffle['Horizons'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                        ParametersReshuffle['HorizonsMin'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                        ParametersReshuffle['HorizonsMax'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                        ParametersReshuffle['Lags'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                        ParametersReshuffle['LagsMin'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                        ParametersReshuffle['LagsMax'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                        doKPIReshuffle = False

                        BetasDf = FilterDfCreator(ResultsAllC, ['Beta'], ['Predicted', 'Mean', 'Style'], c). \
                            dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                        BetaFactorColumns = list(BetasDf.columns.str.replace('Beta ', ''))
                        AlphaDf = FilterDfCreator(ResultsAllC, ['Alpha'], ['Predicted', 'Mean', 'Style'], c). \
                            dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                        ResidualUnsmoothDf = FilterDfCreator(ResultsAllC, ['Residual Unsmooth'], ['Mean', 'Window'], c). \
                            dropna().reset_index(drop=True)
                        ResidualUnsmoothArr = ResidualUnsmoothDf.iloc[:, 1].to_numpy()

                        Factorsorig = pd.read_excel(filepath + ResultsAll.loc[optimalrow, 'FactorSet'] + '.xlsx', engine='openpyxl')
                        if TestCase == False and frequency == 'Weekly':
                            Factorsorig = ReturnCreator(Factorsorig, frequency)
                        elif TestCase == True:
                            # Transform to Date - multiple factor columns if the above day of the week didn't take place
                            Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
                                ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
                            Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
                        Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
                        # For now, keep only common dates below
                        Factorsorig = Factorsorig[
                            Factorsorig['Date'].isin(list(AssetreturnsOrig['Date']))].reset_index(drop=True)
                        Factors = Factorsorig
                        # Combine factors based on lags. Enhanced to gather factors outside the fund data availability.
                        # Dataall below needs to correspond to optimal factor set. If the factor have less data than the fund
                        # then this is an issue.
                        Data = Dataall.loc[w:w + window - 1, :].reset_index(drop=True)
                        Kx = int(ResultsAll.loc[optimalrow, 'Lags'])
                        Kxx = 1 if (Kx == -1 or Kx == 0) else Kx
                        rolldata = RollXY(Data, Parameters, Kxx, 1)
                        Datarolllag = rolldata.get('Dataroll')
                        FactorsCombined = Datarolllag[Factors.columns]

                        for rReshuffle in range(1 + NReshuffle):

                            print('rReshuffle=' + str(rReshuffle))
                            # (Lag) Residual reshuffle
                            rng = np.random.default_rng()
                            ResidualUnsmoothReshuffleDf = pd.concat([ResidualUnsmoothDf[['Date']],
                             pd.DataFrame(rng.permutation(ResidualUnsmoothArr), columns=['Residual Unsmooth Reshuffled'])], axis = 1)
                            AllDf = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='inner'),
                                           [AlphaDf, BetasDf, FactorsCombined, ResidualUnsmoothReshuffleDf, ResidualUnsmoothDf, y])

                            # Set beta of each combined factor to 0 and create bootstrapped fund. Same for alpha.
                            for f in range(1, 1 + BetasDf.shape[1]):
                                AllLoopDf = AllDf.copy()
                                AllLoopDf.iloc[:, f] = 0
                                for b in range(1, BetasDf.shape[1]):
                                    AllLoopDf['BetaCombinedFactor ' + BetaFactorColumns[b]] = \
                                        AllLoopDf['Beta ' + BetaFactorColumns[b]] * AllLoopDf[BetaFactorColumns[b]]
                                AllLoopSumDf = AllLoopDf.loc[:,AllLoopDf.columns.str.contains
                                                   ('|'.join(['Alpha','BetaCombinedFactor', 'Residual Unsmooth Reshuffled']))]
                                yReshuffle = pd.concat([AllLoopDf[['Date']], AllLoopSumDf.sum(axis = 1)], axis = 1)
                                yReshuffle.rename(columns={list(yReshuffle)[1]: y.columns[1]}, inplace=True)

                                # Estimate alpha and betas based on optimal horizons, lags from original regression
                                # Most of the below is not used
                                jReshuffle, jlistReshuffle, analytics2Reshuffle, analytics3Reshuffle, optimalrowReshuffle, \
                                ResultsAllReshuffle, ResultsAllCReshuffle, ResultsRCReshuffle, ResultsRCAllPointsReshuffle, DataallReshuffle = \
                                Regressor(w, mReshuffle, cReshuffle, kReshuffle, i, p, jReshuffle, jlistReshuffle, rReshuffle, AllLoopDf.columns[f],
                                  MaxRows, yReshuffle, Description, window, RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                  xshift, start, end, Minreturns, Datestartmax, LamdaListReshuffle, ParametersReshuffle,
                                  Dependent, FactorGroupListReshuffle, FactorGroupDict, FactorSetListReshuffle, AssetreturnsOrig,
                                  ResultsAllReshuffle, ResultsAllCReshuffle, ResultsRCReshuffle, ResultsRCAllPointsReshuffle,
                                  TestCase, OOSShift, Point, frequency, filepath, RegimeThreshold, Direction, Absolute, doKPIReshuffle,
                                  Regime)

                                cReshuffle = cReshuffle + 1
                                jlistReshuffle.append(jReshuffle)
                                kReshuffle = kReshuffle + 1

                        filename = 'Reshuffle MC ' + str(MCi) + ' R ' + str(NReshuffle) + 'Assets_' + AssetSet + \
                                   FactorFilename + '.csv'
                        file = filepathOut + '\\' + filename
                        ResultsAllReshuffleT = ResultsAllReshuffle.T.sort_index()
                        ResultsAllReshuffleT.to_csv(file, header=False)

                    m = m + 1
                    c = c + 1
                    jlist.append(j)

                ##############################
                # End of Rolling window loop #
                ##############################

                # One more pass for average results across rolling windows - redundant for single window
                if w>0:
                    # We do not populate average of average
                    SummaryStatistics = ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max',
                                         'Skewness', 'Kurtosis', 'Durbin-Watson']
                    SummaryAnalytics = ['Fund ']
                    if doKPI == True:
                        SummaryAnalytics = SummaryAnalytics + ['Style ', 'Predicted Style ', 'Residual ',
                                                               'Predicted Residual ']
                    SummaryStatisticsAnalytics = [x+y for x in SummaryAnalytics for y in SummaryStatistics]
                    analytics2 = analytics2 + SummaryStatisticsAnalytics
                    if doKPI == True:
                        analytics2 = analytics2 + \
                                ['Beta Style', 'Beta Predicted Style'] + \
                                ['R2 Style', 'R2 Predicted Style'] + \
                                ['Correlation Style', 'Correlation Predicted Style']
                    analytics2 = list(set(analytics2) - set(['FundOOS', 'StyleOOS', 'ResidualOOS']))
                    for t in range(len(list(analytics2))):
                        analytic = analytics2[t]
                        analyticcols = [col for col in ResultsRC.columns if col[:len(analytic)] == analytic \
                                        and len(col) > len(analytic) and col[
                                        len(analytic) + 1:len(analytic) + 2].isdigit() == True]
                        Analytic = ResultsRC.loc[ResultsRC.index[k], analyticcols]
                        col = analytic + ' Rolling Windows Mean'
                        ResultsRC.loc[k, col] = Analytic.mean()
                        ResultsAllC.loc[c, col] = Analytic.mean()
                    # Loop through all factors below
                    for t in range(len(list(analytics3))):
                        for FactorGroup in FactorGroupList:
                            for FactorSet in list(FactorGroupDict[FactorGroup][0]):
                                Factorsorig = pd.read_excel(filepath + ResultsAll.loc[optimalrow, 'FactorSet'] + '.xlsx',
                                                            engine='openpyxl')
                                if TestCase == False and frequency == 'Weekly':
                                    Factorsorig = ReturnCreator(Factorsorig, frequency)
                                elif TestCase == True:
                                    # Transform to Date - multiple factor columns if the above day of the week didn't take place
                                    Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
                                        ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
                                    Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
                                Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
                                # For now, keep only common dates below
                                Factorsorig = Factorsorig[
                                    Factorsorig['Date'].isin(list(AssetreturnsOrig['Date']))].reset_index(drop=True)
                                Factors = Factorsorig
                                n = Factors.shape[1] - 1
                                for kk in range(1, n + 1):
                                    analytic = analytics3[t] + ' ' + Factors.columns[kk + xshift] + ' Window'
                                    analyticcols = [col for col in ResultsRC.columns if col[:len(analytic)] == analytic \
                                                    and len(col) > len(analytic) and col[len(analytic) + 1:len(
                                        analytic) + 2].isdigit() == True]
                                    Analytic = ResultsRC.loc[ResultsRC.index[k], analyticcols]
                                    col = analytics3[t] + ' ' + Factors.columns[kk + xshift] + ' Rolling Windows Mean'
                                    ResultsRC.loc[k, col] = Analytic.mean()
                                    ResultsAllC.loc[c, col] = Analytic.mean()
                    # Average at each time point.
                    analytics2 = list(set(analytics2) - set(SummaryStatisticsAnalytics + \
                                 ['Beta Style', 'Beta Predicted Style'])) + ['Fund']
                    for s in range(1, DatesDf.shape[0] + 1):
                        colnumber = DatesDf.loc[start + s - 1, 'Date']
                        for t in range(len(list(analytics2))):
                            analytic = analytics2[t]
                            if analytic[-3:] != 'OOS':
                                col = analytic + ' ' + str(colnumber)
                                col2 = analytic + ' OV Windows Mean ' + str(colnumber)
                                # Only report time average if reported in ResultsRC, i.e. center point etc
                                if col in ResultsRC:
                                    if FullTimeAvg == True and np.isnan(ResultsRC.loc[k, col]) == False:
                                        ResultsRC.loc[k, col2] = \
                                        ResultsAllC[ResultsAllC[Description] ==
                                                             AssetreturnsOrig[Description].unique()[i]][col].mean()
                                        ResultsAllC.loc[c, col2] = \
                                        ResultsAllC[ResultsAllC[Description] ==
                                                             AssetreturnsOrig[Description].unique()[i]][col].mean()
                        for t in range(len(list(analytics3))):
                            for FactorGroup in FactorGroupList:
                                for FactorSet in list(FactorGroupDict[FactorGroup][0]):
                                    Factorsorig = pd.read_excel(
                                        filepath + ResultsAll.loc[optimalrow, 'FactorSet'] + '.xlsx',
                                        engine='openpyxl')
                                    if TestCase == False and frequency == 'Weekly':
                                        Factorsorig = ReturnCreator(Factorsorig, frequency)
                                    elif TestCase == True:
                                        # Transform to Date - multiple factor columns if the above day of the week didn't take place
                                        Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
                                            ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
                                        Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
                                    Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
                                    # For now, keep only common dates below
                                    Factorsorig = Factorsorig[
                                        Factorsorig['Date'].isin(list(AssetreturnsOrig['Date']))].reset_index(drop=True)
                                    Factors = Factorsorig
                                    n = Factors.shape[1] - 1
                                    for kk in range(1, n + 1):
                                        analytic = analytics3[t] + ' ' + Factors.columns[kk + xshift]
                                        col = analytic + ' ' + str(colnumber)
                                        col4 = analytics3[t] + ' ' + Factors.columns[kk + xshift] + \
                                               ' OV Windows Mean ' + str(colnumber)
                                        # Only report time average if reported in ResultsRC, i.e. center point etc
                                        if col in ResultsRC:
                                            if FullTimeAvg == True and np.isnan(ResultsRC.loc[k, col]) == False:
                                                ResultsRC.loc[k, col4] = ResultsAllC[ResultsAllC[Description] ==
                                                                       AssetreturnsOrig[Description].unique()[i]][col].mean()
                                                ResultsAllC.loc[c, col4] = ResultsAllC[ResultsAllC[Description] ==
                                                                       AssetreturnsOrig[Description].unique()[i]][col].mean()

                    # # PR2 Smooth per horizon - we can only report for single window
                    # if doKPI == True:
                    #     for s in range(Parameters.get('rollhorstart', 1), Parameters.get('rollhorend', 8) + 1):
                    #         col = 'PR2 Smooth ' + str(s)
                    #         ResultsRC.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                    #         ResultsAllC.loc[c, col] = ResultsAll.copy().loc[optimalrow, col]

                    ###################
                    # OOS Diagnostics #
                    ###################

                    # R2, Correlation, Beta against Fund
                    SeriesListIn1 = ['StyleOOS']
                    # Summary stats for SeriesListIn1 + SeriesListIn2
                    SeriesListIn2 = ['FundOOS', 'ResidualOOS']
                    SeriesListOut = \
                        ['R2', 'Correlation', 'Beta', 'Smooth', 'Mean', 'Stdev', 'Variance', 'Mean/Stdev',
                         'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson']
                    # Instead of Dataall below change to use all dates across fund and ALL factors.
                    # This will only be an issue if the last FactorSet tried does not have as many dates as the Fund (rare)
                    ResultsRC = DiagnosticResults(k, ResultsRC, k, ResultsRC, Dataall, SeriesListIn1, SeriesListIn2,
                                               SeriesListOut, '')
                    ResultsAllC = DiagnosticResults(c, ResultsAllC, k, ResultsRC,
                                                             Dataall, SeriesListIn1, SeriesListIn2, SeriesListOut, '')
                    # Add Fund, FundID, CaseID
                    ResultsAllC.loc[c, 'Fund'] = ResultsAllC.loc[c-1, 'Fund']
                    ResultsAllC.loc[c, 'FundID'] = ResultsAllC.loc[c-1, 'FundID']
                    ResultsAllC.loc[c, 'CaseID'] = ResultsAllC.loc[c-1, 'CaseID']
                    ResultsAllC.loc[c, 'Window'] = np.nan
                    ResultsAllC.loc[c, 'WindowDateFrom'] = np.nan
                    ResultsAllC.loc[c, 'WindowDateTo'] = np.nan
                    ResultsAllC.loc[c, 'FactorGroup'] = np.nan
                    ResultsAllC.loc[c, 'FactorSet'] = np.nan
                    ResultsAllC.loc[c, 'Lamda'] = np.nan
                    ResultsAllC.loc[c, 'Horizons'] = np.nan
                    ResultsAllC.loc[c, 'Lags'] = np.nan
                    ResultsAllC.loc[c, 'Reshuffle'] = np.nan
                    c = c + 1

                k = k + 1

    end = True

    if TestCase == True:
        ResultsAllT = ResultsAll.T.sort_index()
        ResultsAllCT = ResultsAllC.T.sort_index()
        ResultsRCT = ResultsRC.T.sort_index()
        ResultsAllReshuffleT = ResultsAllReshuffle.T.sort_index()
        ResultsAllCReshuffleT = ResultsAllCReshuffle.T.sort_index()
        ResultsRCReshuffleT = ResultsRCReshuffle.T.sort_index()

##############################
#  Edit, Save, Open results  #
##############################

DateList = Dataall.loc[:, 'Date'].astype('str').tolist()
Columns1ListOut = DateList + ['Smooth', 'Residual Unsmooth']
Columns2ListOut = ['Predicted']
Columns3ListOut = ['Fund ', 'Residual ', 'Style ']
Columns4ListOut = ['FundOOS', 'StyleOOS ']
PriorityList = ['CaseID', 'Fund', 'FundID', 'Window', 'WindowDateFrom', 'WindowDateTo', 'FactorGroup', 'FactorSet',
                'Lamda', 'Horizons', 'Lags']
Columns0, Columns1, Columns2, Columns3, Columns4, AllColumns = \
    ColumnPriority(ResultsAllC, Columns1ListOut, Columns2ListOut, Columns3ListOut, Columns4ListOut, PriorityList)
# Summary Results1
subset = list(set(Columns4) - set(['CaseID', 'Fund', 'FundID', 'Window', 'WindowDateFrom', 'WindowDateTo',
                                   'FactorGroup', 'FactorSet', 'Lamda', 'Horizons', 'Lags']))
ResultsAllCSummary = ResultsAllC[Columns4].dropna(axis=0, how='all',subset=subset).T
# Sorted and Transposed Results1
ResultsAllSortedT=ResultsAll[ColumnPriority(ResultsAll, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                            Columns4ListOut, PriorityList)[5]].T
ResultsAllCSortedT=ResultsAllC[AllColumns].T
ResultsRCSortedT=ResultsRC[ColumnPriority(ResultsRC, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                          Columns4ListOut, PriorityList)[5]].T

universe = 'SampleFunds'
# Save all files
files = ['ResultsAllCSummary', 'ResultsAllSortedT', 'ResultsAllCSortedT', 'ResultsRCSortedT']
for f in range(0, len(files)):
    filename = 'ParameterCaseList_' + str(ParameterCaseList)[1:-1].replace(', ', '_') + '-' + Modelvalue + \
               '-' + frequency + '-Roll_' + str(round(DoRolling)) + '-W_' + str(window)
    filename = 'Results1-' + filename + '_' + universe + '_' + files[f] + '.csv'
    file = filepathOut + '//' + filename
    globals()[files[f]].to_csv(file, header=False)
filename = 'Parameters-' + filename + '_' + universe + '_' + files[f] + '.csv'
file = filepathOut + '\\' + filename
ParametersDf.to_csv(file, header=False)
# filename = 'Reshuffle ' + str(MC) + ' ' + str(NReshuffle) + '.csv'
# file = filepathOut + '\\' + filename
# ResultsAllReshuffleT.to_csv(file, header=False)
# =============================================================================
#         savefile(files[f],filepath,filename,universe)
# =============================================================================

# ResultsAlltemp = ResultsAll.replace(np.nan,-999)
# savetosql(ResultsAlltemp, 'ResultsAll', 'replace')
