
'''
ResultsAll stores results for each run (parameter, fund, window, factorset, lamda, horizon, lag),
shown in each column

ResultsAllC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag), shown in each column

ResultsRC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag).
The difference with ResultsAllC is that results across windows are saved on the same column,
each window being represented by only one value, corresponding typically to the last point
of the window (what is noted as quantile below).

The code doesn't currently calculate stats across windows per FactorSet.
That would have to be another loop before the window loop while disabling
FactorSet calibration.

The below results get populated for the following analytics:
Alpha (regression intercept), Beta, Fund, Horizons, Lags, Lamda, PR2,
PR2 Smooth, Predicted Beta, Predicted Residual, Predicted Style, Predicted Style Smooth,
R2 Style, R2 Predicted Style, R2 Smooth, Residual Unsmooth, Residual, Style

Analytic Date:                  Analytic at each point in time
Analytic Mean Date:             Mean Analytic across all dates of a given window.
                                Date is the window quantile Date.
Analytic OV Windows Mean Date:  Mean Analytic across all windows of a given point in time
Analytic Rolling Windows Mean:  Mean Analytic across all quantile points and windows
                                (one quantile point per window)
Analytic Window Date:           Analytic of a given window and quantile point in time
                                (the Analytic that is used in rolling window calculations)

The below summary statistics get populated for the following analytics:
Fund, Predicted Residual, Predicted Style, Residual, Style, FundOOS, ResidualOOS, StyleOOS
Note that residual mean corresponds to alpha (intercept) if no intercept is selected.
Fund, Fund OOS, ResidualOOS, StyleOOS also report entire date range analytics (like Fund Mean).

Mean, Stdev, Variance, Mean/Stdev, Min, Max, Skewness, Kurtosis, Durbin-Watson

The below summary statistics get populated for the following analytics:
Predicted Style, Style

R2, Correlation, Beta + Date:   Date is the window quantile Date

'''

##########################################################################################################################################
#######################################################             ######################################################################
#######################################################  S T A R T  ######################################################################
#######################################################             ######################################################################
##########################################################################################################################################

from Functions import *
pd.options.mode.chained_assignment = None  # default='warn'
# from functools import reduce
# import time
from progressbar import ProgressBar
from tqdm import tqdm

####################################################### User inputs #####################################################################

##########################
### Files and Controls ###
##########################

TestCase = False
DoReshuffle = False
RiskParity = False
DoMissing = False

# Locations
# fileroot = 'C:/'
fileroot = 'C:/Users/afragkiskos/OneDrive - Brighthouse Financial/Python/'
filepath = fileroot + 'Data/'
filepathOut = fileroot + 'Results1/'

##############
# Parameters #
##############
ParametersFilename = 'Parameters.xlsx'
ParameterCaseList = [1] if not TestCase else [2]
SubParameterCaseList = []
# SubParameterCaseList = []
# Enhance to retrieve from excel Parameters
# LamdaList = [0.65, 0.75, 0.85, 1] if not TestCase else [1]
LamdaList = [1] if not TestCase else [1]
RegressionFrequency = 'Weekly' if not TestCase else 'Daily'

########
# Data #
########
filenameRaw = 'AllFactors' # Raw data
AssetSet = 'AQR_Daily' if not TestCase else 'TestSmall'
Entity = 'AQR' if not TestCase else 'Test'
# Specific list of Factor Groups, Sets or Subsets to process. Leave empty to process all.
FactorGroupList = ['Daily'] if not TestCase else []
FactorSetList = []
FactorSubsetList = []

#########
# Other #
#########
AssetFrequency = 'Daily'
FactorFrequency = 'Daily'
# Enhance to obtain from excel
AssetDataType = 'Return'
FactorDataType = 'Return'
AssetMultiplier = 1
TargetVol = 15

##########################
### Default Parameters ###
##########################

Parameters = ParametersDefault()
for d, key in enumerate(list(Parameters.keys())):
    globals()[key] = list(Parameters.values())[d]
# Retrieve parameters and subparameters from excel
if excel == 1:
    file = filepath + '//' + ParametersFilename
    ParametersOrig = pd.read_excel(open(file, 'rb'), engine='openpyxl',sheet_name='Parameters', header=1)
    # Drop notes
    ParametersOrig.drop([ParametersOrig.index[0]], inplace=True)
    ParametersOrig = ParametersOrig.reset_index(drop=True)
else:
    filepath = ''
    filename = ''
    ParametersOrig = pd.DataFrame()
if len(SubParameterCaseList)==0: SubParameterCaseList = ['NA']
Frequencyvalue = RegressionFrequency
for a, b in {'Daily': 'B', 'Weekly': 'W', 'Monthly': 'M'}.items():
    Frequencyvalue = Frequencyvalue.replace(a, b)
if not DoReshuffle: MC = 1
if TestCase==True: RiskParity = False
# Latest start date
Datestartmax = datetime.strptime('2019-05-01', '%Y-%m-%d')
# Earliest start date for data availability
Datestartmin = datetime.strptime('2000-01-01', '%Y-%m-%d')
# Min return catch - making sure MinReturnsDesired and window size are consistent
windowreturns = window if DoRolling == 1 else MinReturnsDesired
Minreturns = min(MinReturnsDesired, max(MinReturnsDesired, windowreturns))
if Regime == True: doKPI = False
if DoRolling == 0:
    RollingStyleInFirstPoint = 1
    Point = 1
# Enhance to retrieve from excel
spike = 0.3
MissingPercentRow = 0.3
MissingPercentColumn = 0.2
ConsecutiveMissingColumns = 2
ConsecutiveMissingThreshold = 10

###############################################################################################################################
#######################################################  Data  ################################################################
###############################################################################################################################

''' All daily returns have to be in % form. Like 5 for a 5% return.
    All monthly returns come in as log returns.
    For long horizons, the code transforms to log.
    This means that monthly data on long horizons are log transformed to log - Fix '''

# Dependent variables: date - single column with multiple names
AssetReturnsOrig = pd.read_excel(filepath + 'Assets_' + AssetSet + '.xlsx', engine='openpyxl')
AssetReturnsOrig['Date'] = pd.to_datetime(AssetReturnsOrig['Date']).dt.strftime('%Y-%m-%d')
AssetReturnsOrig.rename(columns={list(AssetReturnsOrig)[2]: AssetDataType}, inplace=True)
DependentType = AssetReturnsOrig.columns[1]

if DoMissing:
    ''' Go through dictionary of dataframes, replace consecutive duplicates, missing,
    infinite and spike values with nan and save into excel as filename + 'Missing'.
    The source and output returns are assumed to be %, like 1 instead of 0.01'''
    # (filepath, filename, spike, MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
    DfDictMissing(filepath, filenameRaw, 100, 30, 0.3, '', 8)

if RiskParity:

    ''' Go through dictionary of dataframes, create stdev, inverse stdev return (based on 10% target vol), 
    stdev of inverse stdev return dataframes and save into excel as filename + 'MissingStdevAnnual' + ... 
    The source returns are assumed to be %, like 1 instead of 0.01, whereas the output returns or stdev 
    are whole numbers, like 0.01 instead of 1
    stdev is annualized, InverseStdevReturn = (Return / Return Stdev Annualized) * TargetVol
    Annualized stdev of InverseStdevReturn = TargetVol, InverseStdevReturn is comparable to Return.'''
    # (filepath, filename, StdevWindowList, TargetVol, Horizon, ReturnMultiplier)
    DfDictInverseStdevReturn(filepath, filenameRaw + 'Missing', StdevWindowList, TargetVol/100, 252, 0.01)

    ''' Group (calculate average) rolling inverse stdev factors based on file that maps factors to groups and
    save into excel as filenameSource_FactorsGroupMap_Entity. We check if each row has nan greater than 
    MissingPercent of all columns in which case we set all values to nan prior to grouping.
    Both source and output data are whole numbers. '''
    # (StdevWindowList, filepath, filenameSource, filenameGroupMap, MissingPercent)
    FactorGrouper(StdevWindowList, filepath, filenameRaw + 'MissingInverseStdevReturn', 'FactorsGroupMap_' +
                  str(TargetVol) + '_' + Entity, 0.3)

''' Load dictionary of factors. Each tab shows a different FactorGroup, each row contains 
a different FactorSet and next to each FactorSet, each column shows a different FactorSubset. '''
FactorGroupDictFilename = 'FactorGroupDict' + '_' + Entity + '.xlsx'
FactorGroupDict = pd.read_excel(filepath + FactorGroupDictFilename, engine='openpyxl', sheet_name=None, header=None)
# List of factor groups to process
FactorGroupList = list(FactorGroupDict.keys()) if len(FactorGroupList) == 0 else FactorGroupList

###############################################################################################################################
#######################################################  Algorithm  #######################################################################
###############################################################################################################################

# Monte Carlo
for MCi in range(MC):

    Date = 'Date'
    ResultsAll = pd.DataFrame()
    ResultsAllC = pd.DataFrame()
    ResultsRC = pd.DataFrame()
    ResultsRCAllPoints = pd.DataFrame()
    ResultsAllReshuffle = pd.DataFrame()
    ResultsAllCReshuffle = pd.DataFrame()
    ResultsRCReshuffle = pd.DataFrame()
    ResultsRCAllPointsReshuffle = pd.DataFrame()
    ParametersDf = pd.DataFrame()
    j = 0
    jlist = [0]
    m = 0 # ResultsAll
    c = 0 # ResultsAllC
    k = 0 # ResultsRC
    r = 0 # Reshuffle
    rName = ''
    mReshuffle = 0 # ResultsAllReshuffle
    cReshuffle = 0 # ResultsAllCReshuffle
    kReshuffle = 0 # ResultsRCReshuffle
    jReshuffle = 0
    jlistReshuffle = [0]
    rReshuffle = 0 # Reshuffle
    end = False
    FundMissingLogDf = pd.DataFrame()
    FactorMissingLogDf = pd.DataFrame()
    filename = AssetSet
    writerMissingFundLog = pd.ExcelWriter(filepath + filename + 'MissingFundLog.xlsx', engine='xlsxwriter')
    writerMissingFactorLog = pd.ExcelWriter(filepath + filename + 'MissingFactorLog.xlsx', engine='xlsxwriter')

    # For each parameter set
    # for p in tqdm(range(0,2)):
    for p in ParameterCaseList:
        print('Parameter ' + str(p))

        ParametersLoop = ParametersOrig.loc[[p], :].reset_index(drop=True)
        # All horizons are augmented by 1 after the below
        Parameters = Parameterset(ParametersLoop, excel, filepath, ParametersFilename, HorizonsMin, HorizonsMax, LagsMin, LagsMax,
                                   StyleInFirstPoint, RollingStyleInFirstPoint, RollingStyleInLastPoint, Modelvalue,
                                   Modeltypevalue, Holdingsvalue, HorizonStep, LagStep, WeightAvg, Horizons,
                                   Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling, UseHalfLife,
                                   HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                                  DateStart, DateEnd, NoOfWindows)
        # Create to save at the end
        ParametersDfTemp = pd.DataFrame(Parameters.items(), columns = ['Parameter', 'Case_' + str(p)])
        ParametersDf = ParametersDfTemp if ParametersDf.empty else pd.merge(ParametersDf, ParametersDfTemp, \
                        left_on = 'Parameter', right_on = 'Parameter', how='inner')
        if excel == 1:
            for d, key in enumerate(list(Parameters.keys())):
                globals()[key] = Parameters[key]
            # Min return catch - making sure MinReturnsDesired and window size are consistent
            if TestCase==True:
                MinReturnsDesired = 3
            windowreturns = window if DoRolling == 1 else MinReturnsDesired
            Minreturns = min(MinReturnsDesired, windowreturns)

        # Window start
        WStart = AssetReturnsOrig[AssetReturnsOrig['Date'] == DateStart].index.values[0] if not np.isnan(DateStart) else 0
        # Window end
        if not np.isnan(NoOfWindows):
            WEnd = WStart + NoOfWindows
        elif not np.isnan(DateEnd):
            WEnd = AssetReturnsOrig[AssetReturnsOrig['Date'] == DateEnd].index.values[0]
        else:
            WEnd = np.inf

        # For each Fund
        # for i in range(AssetReturnsOrig[DependentType].unique().shape[0]):
            i=0
            print('Fund ' + str(i))

            Dependent = AssetReturnsOrig[AssetReturnsOrig.loc[:, DependentType].
                isin([AssetReturnsOrig.loc[:, DependentType].unique()[i]])].loc[:, ['Date', AssetDataType]].\
                reset_index(drop=True)
            cols = Dependent.columns.tolist()
            cols[1] = AssetReturnsOrig.loc[:, DependentType].unique()[i]
            Dependent.columns = cols
            Dependent.rename(columns={list(Dependent)[0]: 'Date'}, inplace=True)
            DependentName = Dependent.columns[1]

            ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities 
            if (consecutive) missing data is above threshold, remove start or end of consecutive missing data 
            across all columns and create Log including for missing values. '''
            # df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
            # ConsecutiveMissingColumns, ConsecutiveMissingThreshold
            Dependent, FundLogDf = MissingDataFix(Dependent, ['Parameter', DependentType], [str(p), DependentName],
                                                  100*spike, 1, 0.3, '', 8)
            FundMissingLogDf = FundMissingLogDf.append(FundLogDf)
            Dependent = Dependent.dropna(axis='rows',thresh=1).reset_index(drop=True)
            # Calculate weekly returns if instructed. By default, all nan values are filled forward.
            # Enhance to check if a week contains at least x% non nan returns
            if RegressionFrequency == 'Weekly':
                Dependent = ReturnCreator(Dependent, RegressionFrequency, AssetDataType, AssetMultiplier, 1)
            # We only process funds with min return history and making sure to start prior to Datestartmax
            if Dependent.shape[1] > 1 and Dependent.shape[0] > Minreturns and \
                    pd.to_datetime(Dependent.iloc[0, 0]) < Datestartmax:

                #######################
                # Rolling window loop #
                #######################

                if DoRolling == 0: window = Dependent.shape[0]

                for w in range(max(0, WStart), max(min(Dependent.shape[0] - window + 1, WEnd), 1 + max(0, WStart))):
                # w=0
                    print('Window ' + str(w))

                    #####################
                    # SubParameter loop #
                    #####################

                    ''' Defines all parameters except: 'DoRolling', 'Window', 'NoOfWindows', 'DateStart', 'DateEnd', 
                    'RegressionFrequency', 'MinReturnsDesired', 'Datestartmax' '''
                    for s in SubParameterCaseList:
                        # s=0
                        print('SubParameter ' + str(s))

                        if s != 'NA':
                            SubParametersLoop = ParametersOrig.loc[[s], :].reset_index(drop=True)
                            # All horizons are augmented by 1 after the below
                            SubParameters = Parameterset(SubParametersLoop, excel, filepath, ParametersFilename, HorizonsMin,
                                HorizonsMax, LagsMin, LagsMax, StyleInFirstPoint, RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                Modelvalue, Modeltypevalue, Holdingsvalue, HorizonStep, LagStep, WeightAvg, Horizons,
                                Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling, UseHalfLife,
                                HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                                DateStart, DateEnd, NoOfWindows)
                            if excel == 1:
                                for d, key in enumerate(list(set(list(SubParameters.keys())) - set(['DoRolling', 'window',
                                     'NoOfWindows', 'DateStart', 'DateEnd', 'RegressionFrequency', 'MinReturnsDesired',
                                     'Datestartmax']))):
                                    globals()[key] = SubParameters[key]
                        else:
                            SubParameters = Parameters

                        j, jlist, analytics2, analytics3, optimalrow, ResultsAll, ResultsAllC, ResultsRC, \
                        ResultsRCAllPoints, Dataall, FactorMissingLogDf = \
                        Regressor(w, m, c, k, i, p, s, j, jlist, r, rName, MaxRows, Dependent, DependentType, window,
                                  RollingStyleInFirstPoint, RollingStyleInLastPoint, xshift, Minreturns,
                                  Datestartmax, LamdaList, SubParameters, DependentName, FactorGroupList, FactorGroupDict,
                                  FactorSetList, FactorSubsetList, ResultsAll, ResultsAllC, ResultsRC, ResultsRCAllPoints,
                                  TestCase, OOSShift, Point, RegressionFrequency, filepath, filenameRaw, RegimeThreshold, Direction, Absolute,
                                  doKPI, Regime, 100*spike, MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,
                                  ConsecutiveMissingThreshold, FactorDataType,
                                  ['Parameter', 'SubParameter', DependentType, 'Window'],
                                  [str(p), str(s), DependentName, str(w)], FactorMissingLogDf)

                        #############
                        # Reshuffle #
                        #############

                        if DoReshuffle:

                            # Reshuffling takes place on the optimal parameters and works like a window
                            ParametersReshuffle = Parameters
                            FactorGroupListReshuffle = [ResultsAll.loc[optimalrow, 'FactorGroup']]
                            FactorSetListReshuffle = [ResultsAll.loc[optimalrow, 'FactorSet']]
                            LamdaListReshuffle = [ResultsAll.loc[optimalrow, 'Lamda']]
                            ParametersReshuffle['lam'] = ResultsAll.loc[optimalrow, 'Lamda']
                            ParametersReshuffle['Horizons'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                            ParametersReshuffle['HorizonsMin'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                            ParametersReshuffle['HorizonsMax'] = int(ResultsAll.loc[optimalrow, 'Horizons'])
                            ParametersReshuffle['Lags'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                            ParametersReshuffle['LagsMin'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                            ParametersReshuffle['LagsMax'] = int(ResultsAll.loc[optimalrow, 'Lags'])
                            doKPIReshuffle = False

                            BetasDf = FilterDfCreator(ResultsAllC, ['Beta'], ['Predicted', 'Mean', 'Style'], c). \
                                dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                            BetaFactorColumns = list(BetasDf.columns.str.replace('Beta ', ''))
                            AlphaDf = FilterDfCreator(ResultsAllC, ['Alpha'], ['Predicted', 'Mean', 'Style'], c). \
                                dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                            ResidualUnsmoothDf = FilterDfCreator(ResultsAllC, ['Residual Unsmooth'], ['Mean', 'Window'], c). \
                                dropna().reset_index(drop=True)
                            ResidualUnsmoothArr = ResidualUnsmoothDf.iloc[:, 1].to_numpy()

                            Factorsorig = pd.read_excel(filepath + ResultsAll.loc[optimalrow, 'FactorSet'] + '.xlsx', engine='openpyxl')
                            if Parameters['Leverage'] != 1:
                                # Check if cash is there, following the notation "Cash"
                                if 'Cash' not in Factorsorig.columns:
                                    CashDf = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx', engine='openpyxl',
                                                           sheet_name='Cash')
                                    CashDf['Date'] = pd.to_datetime(CashDf['Date'])
                                    Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date'])
                                    Factorsorig = pd.merge(Factorsorig, CashDf, left_on='Date', right_on='Date',
                                                           how='left')
                                elif Factorsorig.columns[-1] != 'Cash':
                                    # Place cash in last column
                                    Factorsorig = pd.concat([Factorsorig.drop('Cash', axis=1), Factorsorig['Cash']],
                                                            axis=1)
                            if TestCase == False and frequency == 'Weekly':
                                Factorsorig = ReturnCreator(Factorsorig, frequency, 1)
                            elif TestCase == True:
                                # Transform to Date - multiple factor columns if the above day of the week didn't take place
                                Factorsorig = Factorsorig[['Date', 'Factor', 'Return']].set_index(
                                    ['Date', 'Factor']).unstack('Factor').reset_index(col_level=-1)
                                Factorsorig.columns = Factorsorig.columns.droplevel(0).rename(None)
                            Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
                            # For now, keep only common dates below
                            Factorsorig = Factorsorig[
                                Factorsorig['Date'].isin(list(AssetReturnsOrig['Date']))].reset_index(drop=True)
                            Factors = Factorsorig
                            # Combine factors based on lags. Enhanced to gather factors outside the fund data availability.
                            # Dataall below needs to correspond to optimal factor set.
                            # If the factor have less data than the fund then this is an issue.
                            Data = Dataall.loc[w:w + window - 1, :].reset_index(drop=True)
                            Kx = int(ResultsAll.loc[optimalrow, 'Lags'])
                            Kxx = 1 if (Kx == -1 or Kx == 0) else Kx
                            rolldata = RollXY(Data, Parameters, Kxx, 1)
                            Datarolllag = rolldata.get('Dataroll')
                            FactorsCombined = Datarolllag[Factors.columns]

                            for rReshuffle in range(1 + NReshuffle):

                                print('rReshuffle=' + str(rReshuffle))
                                # (Lag) Residual reshuffle
                                rng = np.random.default_rng()
                                ResidualUnsmoothReshuffleDf = pd.concat([ResidualUnsmoothDf[['Date']],
                                 pd.DataFrame(rng.permutation(ResidualUnsmoothArr), columns=['Residual Unsmooth Reshuffled'])], axis = 1)
                                AllDf = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='inner'),
                                               [AlphaDf, BetasDf, FactorsCombined, ResidualUnsmoothReshuffleDf, ResidualUnsmoothDf, Dependent])

                                # Set beta of each combined factor to 0 and create bootstrapped fund. Same for alpha.
                                for f in range(1, 1 + BetasDf.shape[1]):
                                    AllLoopDf = AllDf.copy()
                                    AllLoopDf.iloc[:, f] = 0
                                    for b in range(1, BetasDf.shape[1]):
                                        AllLoopDf['BetaCombinedFactor ' + BetaFactorColumns[b]] = \
                                            AllLoopDf['Beta ' + BetaFactorColumns[b]] * AllLoopDf[BetaFactorColumns[b]]
                                    AllLoopSumDf = AllLoopDf.loc[:,AllLoopDf.columns.str.contains
                                                       ('|'.join(['Alpha','BetaCombinedFactor', 'Residual Unsmooth Reshuffled']))]
                                    yReshuffle = pd.concat([AllLoopDf[['Date']], AllLoopSumDf.sum(axis = 1)], axis = 1)
                                    yReshuffle.rename(columns={list(yReshuffle)[1]: y.columns[1]}, inplace=True)

                                    # Estimate alpha and betas based on optimal horizons, lags from original regression
                                    # Most of the below is not used
                                    jReshuffle, jlistReshuffle, analytics2Reshuffle, analytics3Reshuffle, optimalrowReshuffle, \
                                    ResultsAllReshuffle, ResultsAllCReshuffle, ResultsRCReshuffle, ResultsRCAllPointsReshuffle, DataallReshuffle = \
                                    Regressor(w, mReshuffle, cReshuffle, kReshuffle, i, p, jReshuffle, jlistReshuffle, rReshuffle, AllLoopDf.columns[f],
                                      MaxRows, yReshuffle, DependentType, window, RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                      xshift, start, end, Minreturns, Datestartmax, LamdaListReshuffle, ParametersReshuffle,
                                      DependentName, FactorGroupListReshuffle, FactorGroupDict, FactorSetListReshuffle, AssetReturnsOrig,
                                      ResultsAllReshuffle, ResultsAllCReshuffle, ResultsRCReshuffle, ResultsRCAllPointsReshuffle,
                                      TestCase, OOSShift, Point, frequency, filepath, RegimeThreshold, Direction, Absolute, doKPIReshuffle,
                                      Regime)

                                    cReshuffle = cReshuffle + 1
                                    jlistReshuffle.append(jReshuffle)
                                    kReshuffle = kReshuffle + 1

                            filename = 'Reshuffle MC ' + str(MCi) + ' R ' + str(NReshuffle) + 'Assets_' + AssetSet + \
                                       FactorFilename + '.csv'
                            file = filepathOut + '\\' + filename
                            ResultsAllReshuffleT = ResultsAllReshuffle.T.sort_index()
                            ResultsAllReshuffleT.to_csv(file, header=False)

                        m = m + 1
                        c = c + 1
                        jlist.append(j)

                ##############################
                # End of Rolling window loop #
                ##############################

                # One more pass for average results across rolling windows - redundant for single window
                if w > 0:
                    # We do not populate average of average
                    SummaryStatistics = ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max',
                                         'Skewness', 'Kurtosis', 'Durbin-Watson']
                    SummaryAnalytics = ['Fund ']
                    if doKPI:
                        SummaryAnalytics = SummaryAnalytics + ['Style ', 'Predicted Style ', 'Residual ',
                                                               'Predicted Residual ']
                    SummaryStatisticsAnalytics = [x+y for x in SummaryAnalytics for y in SummaryStatistics]
                    analytics2 = analytics2 + SummaryStatisticsAnalytics
                    if doKPI:
                        analytics2 = analytics2 + \
                                ['Beta Style', 'Beta Predicted Style'] + \
                                ['R2 Style', 'R2 Predicted Style'] + \
                                ['Correlation Style', 'Correlation Predicted Style']
                    analytics2 = list(set(analytics2) - set(['FundOOS', 'StyleOOS', 'ResidualOOS']))
                    for t in range(len(list(analytics2))):
                        analytic = analytics2[t]
                        analyticcols = [col for col in ResultsRC.columns if col[:len(analytic)] == analytic \
                                        and len(col) > len(analytic) and col[
                                        len(analytic) + 1:len(analytic) + 2].isdigit() == True]
                        Analytic = ResultsRC.loc[ResultsRC.index[k], analyticcols]
                        col = analytic + ' Rolling Windows Mean'
                        ResultsRC.loc[k, col] = Analytic.mean()
                        ResultsAllC.loc[c, col] = Analytic.mean()
                    # Loop through all factors below
                    for t in range(len(list(analytics3))):
                        for FactorGroup in FactorGroupList:
                            FactorSetLoopList = FactorSetList if len(FactorSetList) > 0 else \
                                list(FactorGroupDict[FactorGroup][0])
                            for f, FactorSet in enumerate(FactorSetLoopList):
                                FactorSubsetLoopList = FactorSubsetList if len(FactorSubsetList) > 0 else \
                                    [str(x) for x in list(FactorGroupDict[FactorGroup].iloc[f, 1:])]
                                FactorSetDict = pd.read_excel(filepath + FactorSet + '.xlsx', engine='openpyxl',
                                                              sheet_name=None)
                                # Possible that FactorSubsetLoopList is still empty if nothing is defined in the rows
                                FactorSubSetLoopList = FactorSubsetLoopList if len(FactorSubsetLoopList) > 0 else list(
                                    FactorSetDict.keys())
                                FactorSet_Setup = pd.read_excel(filepath + 'FactorSet_Setup.xlsx', engine='openpyxl')
                                for FactorSubset in FactorSubsetLoopList:
                                    Factorsorig = FactorSetDict[FactorSubset]
                                    if Parameters['Leverage'] != 1:
                                        # Check if cash is there, following the notation "Cash"
                                        if 'Cash' not in Factorsorig.columns:
                                            CashDf = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx', engine='openpyxl', sheet_name='Cash')
                                            CashDf['Date'] = pd.to_datetime(CashDf['Date'])
                                            Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date'])
                                            Factorsorig = pd.merge(Factorsorig, CashDf, left_on='Date', right_on='Date', how='left')
                                        elif Factorsorig.columns[-1] != 'Cash':
                                            # Place cash in last column
                                            Factorsorig = pd.concat([Factorsorig.drop('Cash', axis=1), Factorsorig['Cash']], axis=1)
                                    if RegressionFrequency == 'Weekly':
                                        Factorsorig = ReturnCreator(Factorsorig, RegressionFrequency,
                                            list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) == FactorSet) &
                                                (FactorSet_Setup['FactorSubset'].astype(str) == FactorSubset)]['Type'])[0],
                                                list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) == FactorSet) &
                                                 (FactorSet_Setup['FactorSubset'].astype(str) == FactorSubset)]['Multiplier'])[0]
                                                                    , 1)
                                    ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities
                                    if (consecutive) missing data is above threshold, remove start or end of consecutive missing data
                                    across all columns and create Log including for missing values '''
                                    # (df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
                                    # ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
                                    Factorsorig, FactorLogDf = MissingDataFix(Factorsorig,
                                      ['Parameter', 'SubParameter', 'Fund', 'Window', 'FactorGroup', 'FactorSet', 'FactorSubset'],
                                      [str(p), str(s), Dependent.columns[1], str(w), FactorGroup, FactorSet, FactorSubset],
                                      spike, MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,
                                      ConsecutiveMissingThreshold)
                                    if not Factorsorig.empty:
                                        n = Factorsorig.shape[1] - 1
                                        # Drop rows where all factors are nan
                                        Factorsorig = Factorsorig.dropna(axis='rows',
                                                     thresh=Factorsorig.shape[1] - 1).reset_index(drop=True)
                                        # Merge with factors based on Date. For now, keep only common dates.
                                        Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date'])
                                        Dependent['Date'] = pd.to_datetime(Dependent['Date'])
                                        Dataall = pd.merge(Dependent, Factorsorig, left_on='Date', right_on='Date',
                                           how='inner').sort_values(by=['Date']).reset_index(drop=True)
                                        Dataall['Date'] = pd.to_datetime(Dataall['Date']).dt.strftime('%Y-%m-%d')
                                        # Drop rows with at least one nan, for now.
                                        Dataall = Dataall.dropna().reset_index(drop=True)
                                        # Select up to MaxRows
                                        Dataall = Dataall.iloc[-MaxRows:].reset_index(drop=True)
                                        Factorsorig = Dataall.drop(DependentName, axis=1)
                                        Factors = Factorsorig
                                        n = Factors.shape[1] - 1
                                        for kk in range(1, n + 1):
                                            analytic = analytics3[t] + ' ' + Factors.columns[kk + xshift] + ' Window'
                                            analyticcols = [col for col in ResultsRC.columns if col[:len(analytic)] == analytic \
                                                            and len(col) > len(analytic) and col[len(analytic) + 1:len(
                                                analytic) + 2].isdigit() == True]
                                            Analytic = ResultsRC.loc[ResultsRC.index[k], analyticcols]
                                            col = analytics3[t] + ' ' + Factors.columns[kk + xshift] + ' Rolling Windows Mean'
                                            ResultsRC.loc[k, col] = Analytic.mean()
                                            ResultsAllC.loc[c, col] = Analytic.mean()
                    # Average at each time point.
                    analytics2 = list(set(analytics2) - set(SummaryStatisticsAnalytics + \
                                 ['Beta Style', 'Beta Predicted Style'])) + [DependentType]
                    for s in range(1, Dependent.shape[0] + 1):
                        # colnumber = Dependent.loc[start + s - 1, 'Date']
                        colnumber = Dependent.loc[s - 1, 'Date']
                        for t in range(len(list(analytics2))):
                            analytic = analytics2[t]
                            if analytic[-3:] != 'OOS':
                                col = analytic + ' ' + str(colnumber)
                                col2 = analytic + ' OV Windows Mean ' + str(colnumber)
                                # Only report time average if reported in ResultsRC, i.e. center point etc
                                if col in ResultsRC:
                                    if FullTimeAvg == True and np.isnan(ResultsRC.loc[k, col]) == False:
                                        ResultsRC.loc[k, col2] = \
                                        ResultsAllC[ResultsAllC[DependentType] ==
                                                             AssetReturnsOrig[DependentType].unique()[i]][col].mean()
                                        ResultsAllC.loc[c, col2] = \
                                        ResultsAllC[ResultsAllC[DependentType] ==
                                                             AssetReturnsOrig[DependentType].unique()[i]][col].mean()
                        for t in range(len(list(analytics3))):
                            for FactorGroup in FactorGroupList:
                                FactorSetLoopList = FactorSetList if len(FactorSetList) > 0 else \
                                    list(FactorGroupDict[FactorGroup][0])
                                for f, FactorSet in enumerate(FactorSetLoopList):
                                    FactorSubsetLoopList = FactorSubsetList if len(FactorSubsetList) > 0 else \
                                        [str(x) for x in list(FactorGroupDict[FactorGroup].iloc[f, 1:])]
                                    FactorSetDict = pd.read_excel(filepath + FactorSet + '.xlsx', engine='openpyxl',
                                                                  sheet_name=None)
                                    # Possible that FactorSubsetLoopList is still empty if nothing is defined in the rows
                                    FactorSubSetLoopList = FactorSubsetLoopList if len(
                                        FactorSubsetLoopList) > 0 else list(
                                        FactorSetDict.keys())
                                    for FactorSubset in FactorSubsetLoopList:

                                        Factorsorig = FactorSetDict[FactorSubset]
                                        if Parameters['Leverage'] != 1:
                                            # Check if cash is there, following the notation "Cash"
                                            if 'Cash' not in Factorsorig.columns:
                                                CashDf = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx',engine='openpyxl', sheet_name='Cash')
                                                CashDf['Date'] = pd.to_datetime(CashDf['Date'])
                                                Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date'])
                                                Factorsorig = pd.merge(Factorsorig, CashDf, left_on='Date',right_on='Date', how='left')
                                            elif Factorsorig.columns[-1] != 'Cash':
                                                # Place cash in last column
                                                Factorsorig = pd.concat([Factorsorig.drop('Cash', axis=1), Factorsorig['Cash']], axis=1)
                                        if RegressionFrequency == 'Weekly':
                                            Factorsorig = ReturnCreator(Factorsorig, RegressionFrequency,
                                                list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) == FactorSet) &
                                                    (FactorSet_Setup['FactorSubset'].astype(str) == FactorSubset)]['Type'])[0],
                                                    list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) == FactorSet) &
                                                    (FactorSet_Setup['FactorSubset'].astype(str) == FactorSubset)]['Multiplier'])[0]
                                                                        , 1)
                                        ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities
                                        if (consecutive) missing data is above threshold, remove start or end of consecutive missing data
                                        across all columns and create Log including for missing values '''
                                        # (df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
                                        # ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
                                        Factorsorig, FactorLogDf = MissingDataFix(Factorsorig,
                                          ['Parameter', 'SubParameter', 'Fund', 'Window', 'FactorGroup', 'FactorSet',
                                           'FactorSubset'], [str(p), str(s), Dependent.columns[1], str(w),
                                           FactorGroup, FactorSet, FactorSubset], spike, MissingPercentRow,
                                          MissingPercentColumn, ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
                                        if not Factorsorig.empty:
                                            n = Factorsorig.shape[1] - 1
                                            # Drop rows where all factors are nan
                                            Factorsorig = Factorsorig.dropna(axis='rows',
                                                                             thresh=Factorsorig.shape[
                                                                                        1] - 1).reset_index(drop=True)
                                            # Merge with factors based on Date. For now, keep only common dates.
                                            Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date'])
                                            Dependent['Date'] = pd.to_datetime(Dependent['Date'])
                                            Dataall = pd.merge(Dependent, Factorsorig, left_on='Date', right_on='Date',
                                               how='inner').sort_values(by=['Date']).reset_index(drop=True)
                                            Dataall['Date'] = pd.to_datetime(Dataall['Date']).dt.strftime('%Y-%m-%d')
                                            # Drop rows with at least one nan, for now.
                                            Dataall = Dataall.dropna().reset_index(drop=True)
                                            # Select up to MaxRows
                                            Dataall = Dataall.iloc[-MaxRows:].reset_index(drop=True)
                                            Factorsorig = Dataall.drop(DependentName, axis=1)
                                            Factors = Factorsorig
                                            n = Factors.shape[1] - 1
                                            for kk in range(1, n + 1):
                                                analytic = analytics3[t] + ' ' + Factors.columns[kk + xshift]
                                                col = analytic + ' ' + str(colnumber)
                                                col4 = analytics3[t] + ' ' + Factors.columns[kk + xshift] + \
                                                       ' OV Windows Mean ' + str(colnumber)
                                                # Only report time average if reported in ResultsRC, i.e. center point etc
                                                if col in ResultsRC:
                                                    if FullTimeAvg == True and np.isnan(ResultsRC.loc[k, col]) == False:
                                                        ResultsRC.loc[k, col4] = ResultsAllC[ResultsAllC[DependentType] ==
                                                                               AssetReturnsOrig[DependentType].unique()[i]][col].mean()
                                                        ResultsAllC.loc[c, col4] = ResultsAllC[ResultsAllC[DependentType] ==
                                                                               AssetReturnsOrig[DependentType].unique()[i]][col].mean()

                    # # PR2 Smooth per horizon - we can only report for single window
                    # if doKPI == True:
                    #     for s in range(Parameters.get('rollhorstart', 1), Parameters.get('rollhorend', 8) + 1):
                    #         col = 'PR2 Smooth ' + str(s)
                    #         ResultsRC.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                    #         ResultsAllC.loc[c, col] = ResultsAll.copy().loc[optimalrow, col]

                    ###################
                    # OOS Diagnostics #
                    ###################

                    # R2, Correlation, Beta against Fund
                    SeriesListIn1 = ['StyleOOS']
                    # Summary stats for SeriesListIn1 + SeriesListIn2
                    SeriesListIn2 = ['FundOOS', 'ResidualOOS']
                    SeriesListOut = \
                        ['R2', 'Correlation', 'Beta', 'Smooth', 'Mean', 'Stdev', 'Variance', 'Mean/Stdev',
                         'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson']
                    # Instead of Dataall below change to use all dates across fund and ALL factors.
                    # This will only be an issue if the last FactorSet tried does not have as many dates as the Fund (rare)
                    ResultsRC = DiagnosticResults(k, ResultsRC, k, ResultsRC, Dataall, SeriesListIn1, SeriesListIn2,
                                               SeriesListOut, '')
                    ResultsAllC = DiagnosticResults(c, ResultsAllC, k, ResultsRC,
                                                     Dataall, SeriesListIn1, SeriesListIn2, SeriesListOut, '')
                    # Add Fund, FundID, ParameterCaseID
                    ResultsAllC.loc[c, DependentType] = ResultsAllC.loc[c-1, DependentType]
                    ResultsAllC.loc[c, 'FundID'] = ResultsAllC.loc[c-1, 'FundID']
                    ResultsAllC.loc[c, 'ParameterCaseID'] = ResultsAllC.loc[c-1, 'ParameterCaseID']
                    ResultsAllC.loc[c, 'SubParameterCaseID'] = ResultsAllC.loc[c - 1, 'SubParameterCaseID']
                    ResultsAllC.loc[c, 'Window'] = np.nan
                    ResultsAllC.loc[c, 'WindowDateFrom'] = np.nan
                    ResultsAllC.loc[c, 'WindowDateTo'] = np.nan
                    ResultsAllC.loc[c, 'FactorGroup'] = np.nan
                    ResultsAllC.loc[c, 'FactorSet'] = np.nan
                    ResultsAllC.loc[c, 'Lamda'] = np.nan
                    ResultsAllC.loc[c, 'Horizons'] = np.nan
                    ResultsAllC.loc[c, 'Lags'] = np.nan
                    ResultsAllC.loc[c, 'Reshuffle'] = np.nan
                    c = c + 1

                k = k + 1

    FundMissingLogDf.to_excel(writerMissingFundLog, sheet_name=filename, index=False)
    FactorMissingLogDf.to_excel(writerMissingFactorLog, sheet_name=filename, index=False)
    writerMissingFundLog.save()
    writerMissingFactorLog.save()

    if TestCase:
        ResultsAllT = ResultsAll.T.sort_index()
        ResultsAllCT = ResultsAllC.T.sort_index()
        ResultsRCT = ResultsRC.T.sort_index()
        ResultsAllReshuffleT = ResultsAllReshuffle.T.sort_index()
        ResultsAllCReshuffleT = ResultsAllCReshuffle.T.sort_index()
        ResultsRCReshuffleT = ResultsRCReshuffle.T.sort_index()

##############################
#  Edit, Save, Open results  #
##############################

DateList = Dataall.loc[:, 'Date'].astype('str').tolist()
Columns1ListOut = DateList + ['Smooth', 'Residual Unsmooth']
Columns2ListOut = ['Predicted']
Columns3ListOut = ['Fund ', 'Residual ', 'Style ']
Columns4ListOut = ['FundOOS', 'StyleOOS ']
PriorityList = ['ParameterCaseID', 'SubParameterCaseID'] + [DependentType] + ['FundID', 'Window', 'WindowDateFrom', 'WindowDateTo', 'FactorGroup', 'FactorSet',
                'Lamda', 'Horizons', 'Lags']
Columns0, Columns1, Columns2, Columns3, Columns4, AllColumns = \
    ColumnPriority(ResultsAllC, Columns1ListOut, Columns2ListOut, Columns3ListOut, Columns4ListOut, PriorityList)
# Summary Results1
subset = list(set(Columns4) - set(['ParameterCaseID', 'SubParameterCaseID'] + [DependentType] + ['FundID', 'Window', 'WindowDateFrom', 'WindowDateTo',
                                   'FactorGroup', 'FactorSet', 'Lamda', 'Horizons', 'Lags']))
ResultsAllCSummary = ResultsAllC[Columns4].dropna(axis=0, how='all',subset=subset).T.iloc[:,[-1]]
# Sorted and Transposed Results1
ResultsAllSortedT=ResultsAll[ColumnPriority(ResultsAll, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                            Columns4ListOut, PriorityList)[5]].T
ResultsAllCSortedT=ResultsAllC[AllColumns].T
ResultsRCSortedT=ResultsRC[ColumnPriority(ResultsRC, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                          Columns4ListOut, PriorityList)[5]].T

universe = ''
# Save all files
files = ['ResultsAllCSummary', 'ResultsAllSortedT', 'ResultsAllCSortedT', 'ResultsRCSortedT']
for f in range(0, len(files)):
    filename = 'ParameterCaseList_' + str(ParameterCaseList)[1:-1].replace(', ', '_') + \
               'SubParameterCaseList_' + str(SubParameterCaseList)[1:-1].replace(', ', '_') + \
               '-' + Modelvalue + '-' + frequency + '-Roll_' + str(round(DoRolling)) + '-W_' + str(window)
    filename = 'Results1-' + filename + '_' + AssetSet + '_' + files[f] + '.csv'
    file = filepathOut + '\\' + filename
    globals()[files[f]].to_csv(file, header=False)
filename = 'Parameters-' + filename + '_' + universe + '_' + files[f] + '.csv'
file = filepathOut + '\\' + filename
ParametersDf.to_csv(file, header=False)
# filename = 'Reshuffle ' + str(MC) + ' ' + str(NReshuffle) + '.csv'
# file = filepathOut + '\\' + filename
# ResultsAllReshuffleT.to_csv(file, header=False)
# =============================================================================
#         savefile(files[f],filepath,filename,universe)
# =============================================================================

# ResultsAlltemp = ResultsAll.replace(np.nan,-999)
# savetosql(ResultsAlltemp, 'ResultsAll', 'replace')
