
'''
ResultsAll stores results for each run (parameter, fund, window, factorset, lamda, horizon, lag),
shown in each column

ResultsAllC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag), shown in each column

ResultsRC stores results across all window points for each parameter, fund, window
and only results that correspond to calibrated values (lamda, horizon, lag).
The difference with ResultsAllC is that results across windows are saved on the same column,
each window being represented by only one value, corresponding typically to the last point
of the window (what is noted as quantile below).

The code doesn't currently calculate stats across windows per FactorSet.
That would have to be another loop before the window loop while disabling
FactorSet calibration.

The below results get populated for the following analytics:
Alpha (regression intercept), Beta, Fund, Horizons, Lags, Lamda, PR2,
PR2 Smooth, Predicted Beta, Predicted Residual, Predicted Style, Predicted Style Smooth,
R2 Style, R2 Predicted Style, R2 Smooth, Residual Unsmooth, Residual, Style

Analytic Date:                  Analytic at each point in time
Analytic Mean Date:             Mean Analytic across all dates of a given window.
                                Date is the window quantile Date.
Analytic OV Windows Mean Date:  Mean Analytic across all windows of a given point in time
Analytic Rolling Windows Mean:  Mean Analytic across all quantile points and windows
                                (one quantile point per window)
Analytic DateWindow:            Analytic of a given window and quantile point in time
                                (the Analytic that is used in rolling window calculations)

The below summary statistics get populated for the following analytics:
Fund, Predicted Residual, Predicted Style, Residual, Style, FundOOS, ResidualOOS, StyleOOS
Note that residual mean corresponds to alpha (intercept) if no intercept is selected.
Fund, Fund OOS, ResidualOOS, StyleOOS also report entire date range analytics (like Fund Mean).

Mean, Stdev, Variance, Mean/Stdev, Min, Max, Skewness, Kurtosis, Durbin-Watson

The below summary statistics get populated for the following analytics:
Predicted Style, Style

R2, Correlation, Beta + Date:   Date is the window quantile Date

$600 per trade Aladdin operational costs and (about) $2000 new trade costs if notional is less than $1M

For risk parity to work, the below tables need to be setup:


'''

##########################################################################################################################################
#######################################################             ######################################################################
#######################################################  S T A R T  ######################################################################
#######################################################             ######################################################################
##########################################################################################################################################

from Functions import *
pd.options.mode.chained_assignment = None  # default='warn'
# from functools import reduce
# import time
from progressbar import ProgressBar
from tqdm import tqdm
import os.path

####################################################### User inputs ####################################################

################################
### Controls and most common ###
################################

TestCase = False
# Entity affects Assets_Entity, Assets_EntityP and FactorGroupDict_Entity.
# It also affects AllFactorsEntity, EntityRiskParity if TestCase is True
Entity = 'TV' if not TestCase else 'TestMid'
FundsList = [1] if not TestCase else [0]
# WindowsList = ['All']
WindowsList = list(range(5))
OutputSuffix = 'AQR' if not TestCase else Entity
# Specific list of Factor Groups, Sets or Subsets to process. Leave empty to process all.
FactorGroupList = ['Daily'] if not TestCase else ['Daily']
# FactorGroupList = ['AllFactors'] if not TestCase else ['FactorGroup_Test']
FactorSetList = [] if not TestCase else []
FactorSubsetList = ['252'] if not TestCase else []
# MonitorStrategyDictOrig = {'MonthEnd': ['MonthEnd'], 'QuarterEnd': ['QuarterEnd'], 'Period': [1, 2, 3, 4, 13]} \
#     if not TestCase else {'Period': [1, 4]}
# MonitorStrategyDictOrig = {'Period': [1, 2, 3, 4, 13]} if not TestCase else {'Period': [1, 4]}
MonitorStrategyDictOrig = {'Period': [1]}
RiskParity = True
DoMissingFactors = False
DoTrading = True
DoTrade = False
Monitor = True
MonitorFrequency = 'RegressionFrequency'
DoFundLevelCreator = False if not DoTrading and not DoTrade else False
DoFactorLevelCreator = False if not DoTrading and not DoTrade else False
DoRebalanceMVOrig = True if DoTrading else True
DoRebalanceBetaOrig = True if DoTrading else True

#############
# Locations #
#############

fileroot = 'C:/Users/bcunha/proj/tracking-error-research/Python/'
# fileroot = 'C:/Users/afragkiskos/OneDrive - Brighthouse Financial/Python/'
# fileroot = 'Z:/Apollon/Python/'
filepath = fileroot + 'Data/'
filepathOut = fileroot + 'Results5/'

##############
# Parameters #
##############

DoAverages = True
ParameterCaseList = [4] if not TestCase else [4]
RegressionFrequency = 'Weekly' if not TestCase else 'Daily'
windowTest = 20 if Entity=='TestMid' else 3
iLoopmax = 15
LeverageMin = - 0.05
LeverageMax = 0.05
frequencyP = 'Monthly'
DoFactorsorig = False
MonitorStrategyDict = MonitorStrategyDictOrig if Monitor else {'Fixed': ['Fixed']}
DoReshuffle = False
Cash = True if DoTrade else False
DoRegression = True
ParametersFilename = 'Parameters.xlsx'
SubParameterCaseList = []
# SubParameterCaseList = []
# Enhance to retrieve from excel Parameters
# LamdaList = [0.65, 0.75, 0.85, 1] if not TestCase else [1]
LamdaList = [1] if not TestCase else [1]
DateList = []

########
# Data #
########

filenameRaw = 'AllFactors' if not TestCase else 'AllFactors' + Entity # Raw data
# EntityRiskParity is used in relation to risk parity if we want to adopt risk parity factors created for a fund
# to other funds
EntityRiskParity = 'AQR' if not TestCase else Entity

#########
# Other #
#########
AssetFrequency = 'Daily'
FactorFrequency = 'Daily'
# Enhance to obtain from excel
FactorDataType = 'Level'
AssetMultiplier = 1
TargetVol = 0.15
# Name convention used to create risk parity factors and deleverage during regression to obtain underlying betas
FactorsGroupMapPrefix = 'FactorsGroupMap'
filenameGroupMap = FactorsGroupMapPrefix + '_' + EntityRiskParity


##########################
### Default Parameters ###
##########################

Parameters = ParametersDefault()
for d, key in enumerate(list(Parameters.keys())):
    globals()[key] = list(Parameters.values())[d]
# Retrieve parameters and subparameters from excel
if excel == 1:
    file = filepath + '//' + ParametersFilename
    ParametersOrig = pd.read_excel(open(file, 'rb'), engine='openpyxl',sheet_name='Parameters', header=1)
    # Drop notes
    ParametersOrig.drop([ParametersOrig.index[0]], inplace=True)
    ParametersOrig = ParametersOrig.reset_index(drop=True)
else:
    filepath = ''
    filename = ''
    ParametersOrig = pd.DataFrame()
if len(SubParameterCaseList)==0: SubParameterCaseList = ['NA']
Frequencyvalue = RegressionFrequency
for a, b in {'Daily': 'B', 'Weekly': 'W', 'Monthly': 'M'}.items():
    Frequencyvalue = Frequencyvalue.replace(a, b)
if not DoReshuffle: MC = 1
# Latest start date
Datestartmax = datetime.strptime('2021-05-01', '%Y-%m-%d')
# Earliest start date for data availability
Datestartmin = datetime.strptime('2000-01-01', '%Y-%m-%d')
# Min return catch - making sure MinReturnsDesired and window size are consistent
windowreturns = window if DoRolling == 1 else MinReturnsDesired
Minreturns = min(MinReturnsDesired, max(MinReturnsDesired, windowreturns))
if Regime == True: doKPI = False
if DoRolling == 0:
    RollingStyleInFirstPoint = 1
    Point = 1
# Enhance to retrieve from excel and by frequency
spike = 1
MissingPercentRow = 0.3
MissingPercentColumn = 0.2
ConsecutiveMissingColumns = 2
ConsecutiveMissingThreshold = 10
AllSubFactorList = []
SuffixOrig = ''
if TestCase==True:
    Datestartmax = datetime.strptime('2999-01-01', '%Y-%m-%d')
    Datestartmin = datetime.strptime('1900-01-01', '%Y-%m-%d')
    StdevWindowList = [3]
    spike = 999
    MissingPercentRow = 1
    MissingPercentColumn = 1
    ConsecutiveMissingColumns = 999
    ConsecutiveMissingThreshold = 999

########################################################################################################################
#######################################################  Data  #########################################################
########################################################################################################################

''' All daily returns have to be in % form. Like 5 for a 5% return.
    All monthly returns come in as log returns.
    For long horizons, the code transforms to log.
    This means that monthly data on long horizons are log transformed to log - Fix '''

# Dependent variables: date - single column with multiple names
AssetReturnsOrig = pd.read_excel(filepath + 'Assets_' + Entity + '.xlsx', engine='openpyxl')
AssetReturnsOrig['Date'] = pd.to_datetime(AssetReturnsOrig['Date']).dt.strftime('%Y-%m-%d')
DependentType = AssetReturnsOrig.columns[1]
AssetDataType = AssetReturnsOrig.columns[2]
FactorSet_Setup = pd.read_excel(filepath + 'FactorSet_Setup.xlsx', engine='openpyxl')

if DoMissingFactors:
    ''' Go through dictionary of dataframes, replace consecutive duplicates, missing,
    infinite and spike values with nan and save into excel as filename + 'Missing'.
    The source and output returns are assumed to be %, like 1 instead of 0.01.
    It is able to differentiate between returns and levels via the FactorSet_Setup table '''
    # (filepath, filename, spikeReturn, spikeLevel, MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,
    # ConsecutiveMissingThreshold)
    # Remove entities if consecutive missing data is above threshold
    # This doesn't work well if there are multiple entities with different start Dates
    DfDictMissing(filepath, filenameRaw, 100, 999999, 30, 0.4, '', 999999)
    # Proxy returns with 0 and ffill, if returns, else just ffill. Not needed ???
    DfDictProxy(filepath, filenameRaw)

# Fixed Trading Costs.
BetaThresholdsDf = pd.DataFrame()
if DoTrading or DoTrade:
    # Beta thresholds indicating when to trade
    BetaThresholdsDf = pd.read_excel(filepath + filenameRaw + 'BetaThresholds.xlsx', engine='openpyxl')
    # Transform annual costs to the regression frequency
    BetaThresholdsDf['FixedCostBuy'] = BetaThresholdsDf['FixedCostBuy']/52 if RegressionFrequency=='Weekly' \
                                    else BetaThresholdsDf['FixedCostBuy']/252
    BetaThresholdsDf['FixedCostSell'] = BetaThresholdsDf['FixedCostSell']/52 if RegressionFrequency=='Weekly' \
                                    else BetaThresholdsDf['FixedCostSell']/252
    # AllFactorsDict = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx', engine='openpyxl', sheet_name=None)
    # writerTC = pd.ExcelWriter(filepath + filenameRaw + 'Missing.xlsx', engine='xlsxwriter')
    # for key in AllFactorsDict:
    #     for Factor in AllFactorsDict[key].columns[1:]:
    #         AllFactorsDict[key][Factor] = AllFactorsDict[key][Factor] - \
    #                                BetaThresholdsDf[BetaThresholdsDf['Factor'] == Factor]['FixedCost'].values[0]
    #     AllFactorsDict[key].to_excel(writerTC, sheet_name=key, index=False)
    # writerTC.save()

''' Create total return Levels. Only if loading is not possible. '''
AssetReturnsOrigP = pd.DataFrame()
if DoFundLevelCreator:
    ''' Create Levels out of fund returns. '''
    FundLevelCreator(filepath, Entity, AssetReturnsOrig)
    AssetReturnsOrigP = pd.read_excel(filepath + 'Assets_' + Entity + 'P.xlsx', engine='openpyxl', sheet_name='Sheet1')
elif DoTrading or DoTrade:
    if not os.path.isfile(filepath + 'Assets_' + Entity + 'P.xlsx'):
        AssetReturnsOrigP = pd.read_excel(filepath + 'Assets_' + Entity + 'PRaw.xlsx', engine='openpyxl', sheet_name='Sheet1')
        if frequencyP!='Daily':
            ''' Create Levels out of Levels making sure all business days are populated. 
                No missing data treatment for now. '''
            FundLevelCreator(filepath, Entity, AssetReturnsOrigP)
    else:
        AssetReturnsOrigP = pd.read_excel(filepath + 'Assets_' + Entity + 'P.xlsx', engine='openpyxl', sheet_name='Sheet1')

if DoFactorLevelCreator:
    ''' Create total return Levels out of factor returns. Enhance to remove duplicates '''
    writerP = pd.ExcelWriter(filepath + filenameRaw + 'P.xlsx', engine='xlsxwriter')
    writerPMissing = pd.ExcelWriter(filepath + filenameRaw + 'PMissing.xlsx', engine='xlsxwriter')
    AllFactorsDf = GetFactorsFromDict(filepath + filenameRaw + 'Missing.xlsx', [])
    # Retrieve the multiplier below
    AllFactorsPDf = LevelCreator(AllFactorsDf, 'Return', 100, 1).dropna().reset_index(drop=True)
    AllFactorsPDf.to_excel(writerP, index=False)
    # AllFactorsPDf = MissingDataFix(AllFactorsPDf, [], [], 999999, 1, 100, '', 999)[0]
    AllFactorsPDf.to_excel(writerPMissing, index=False)
    writerP.save()
    writerPMissing.save()
else:
    AllFactorsPDf = pd.read_excel(filepath + filenameRaw + 'P' + 'Missing.xlsx', engine='openpyxl')

SourceFactorDf = pd.DataFrame()
if RiskParity:

    ''' Go through dictionary of dataframes, create stdev, inverse stdev return (based on 10% target vol), 
    stdev of inverse stdev return dataframes and save into excel as filename + 'MissingStdevAnnual' + ... 
    The source returns are assumed to be %, like 1 instead of 0.01, whereas the output returns or stdev 
    are whole numbers, like 0.01 instead of 1
    stdev is annualized, TargetVolReturn = (Return / Return Stdev Annualized) * TargetVol
    Annualized stdev of TargetVolReturn = TargetVol, TargetVolReturn is comparable to Return.
    stdev is calculated based on unproxied returns.
    FactorTargetVolReturn is proxied with 0 and ffill and Leverage is proxied with ffill.
    AllFactorsTestMissing 
    -> 
    AllFactorsTestMissingStdevAnnual3Window
    AllFactorsTestMissingTargetVolReturn0.15Vol3Window
    AllFactorsTestMissingTargetVolLeverage0.15Vol3Window
    AllFactorsTestMissingTargetVolReturn0.15Vol_FactorsGroupMap_Test
    AllFactorsTestMissingStdevInverseAnnual0.15Vol3Window
    '''
    # (filepath, filename, StdevWindowList, TargetVol, Horizon)
    DfDictTargetVolReturn(filepath, filenameRaw + 'Missing', StdevWindowList, TargetVol, 252)

    ''' Group (calculate average) rolling inverse stdev factors based on file that maps factors to groups and
    save into excel as filenameSource_FactorsGroupMap_EntityRiskParity. We check if each row has nan greater than 
    MissingPercent of all columns in which case we set all values to nan prior to grouping.
    Both source and output data are whole numbers. Also calculate combined Leverage.
    AllFactorsTestMissingTargetVolReturn0.15Vol3Window and all other stdev windows
    FactorsGroupMap_Test 
    ->
    AllFactorsTestMissingTargetVolReturn0.15Vol_FactorsGroupMap_Test with tab names taken from StdevWindowList
    AllFactorsTestMissingTargetVolReturn0.15Vol_FactorsGroupMap_Test3GroupLeverage
    AllFactorsTestMissingTargetVolReturn0.15Vol_FactorsGroupMap_Test3Leverage
    '''
    # (StdevWindowList, filepath, filenameSource, FactorsGroupMapPrefix, EntityRiskParity, MissingPercent)
    FactorGrouper(StdevWindowList, filepath, filenameRaw + 'MissingTargetVolReturn' + str(TargetVol) + 'Vol',
                  FactorsGroupMapPrefix, EntityRiskParity, 1)

''' Load dictionary of factors. Each tab shows a different FactorGroup, each row contains 
a different FactorSet and next to each FactorSet, each column shows a different FactorSubset. '''
FactorGroupDictFilename = 'FactorGroupDict' + '_' + Entity + '.xlsx'
FactorGroupDict = pd.read_excel(filepath + FactorGroupDictFilename, engine='openpyxl', sheet_name=None, header=None)
# List of factor groups to process
FactorGroupList = list(FactorGroupDict.keys()) if len(FactorGroupList) == 0 else FactorGroupList
# Check if there are subfactors
FactorMapCount = \
    RegressorLoop(FactorGroupList=FactorGroupList, FactorSetList=FactorSetList, FactorGroupDict=FactorGroupDict,
                  FactorSubsetList=FactorSubsetList, filepath=filepath, Cash=Cash, filenameRaw=filenameRaw,
                  RegressionFrequency=RegressionFrequency, filenameGroupMap=filenameGroupMap, Minreturns=Minreturns,
                  Datestartmax=Datestartmax, LamdaList=LamdaList, DoRegression=DoRegression,
                  FactorsGroupMapPrefix=FactorsGroupMapPrefix, EntityRiskParity=EntityRiskParity,
                  AssetReturnsOrigP=AssetReturnsOrigP, DoFactorsorig=DoFactorsorig, FactorSet_Setup=FactorSet_Setup,
                  AllSubFactorList=AllSubFactorList, DoFactorMap=True, FactorSetDict={},
                  FactorTradesDf=pd.DataFrame(), SubParameters = {}, FactorType = '', FactorMultiplier=np.nan,
                  Dependent=pd.DataFrame(), p=np.nan, s=np.nan, wReport=np.nan, spike=np.nan, MissingPercentRow=np.nan,
                  MissingPercentColumn=np.nan, ConsecutiveMissingColumns=np.nan, ConsecutiveMissingThreshold=np.nan,
                  MaxRows=np.nan, w=np.nan, window=np.nan, xshift=np.nan, AllSubFactorMapList=[], DependentName='',
                  OOSShift=np.nan, Point=np.nan, RegimeThreshold=np.nan, DoMonitor=np.nan, Direction='', Absolute=np.nan,
                  doKPI=np.nan, Regime=np.nan, ResultsAllC=pd.DataFrame(), c=np.nan, Suffix='', ResultsAll=pd.DataFrame(),
                  j=np.nan, DependentType='', i=np.nan, r=np.nan, rName='', SourceFactorDf=pd.DataFrame(),
                  ResultsFlag=np.nan, ResultsRC=pd.DataFrame(), k=np.nan, DoRollingWindowsMean=np.nan, FullTimeAvg=np.nan,
                  AllSubFactorListCreator=np.nan, DateList=[], AnalyticName='', MonitorKey='', MonitorValue='')[8]
FactorMap = True if FactorMapCount>0 else False
if FactorMap or Monitor:
    ''' Create association of Source and Factor to be used when reverse finding leverage '''
    FactorSetDict = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx', engine='openpyxl', sheet_name=None)
    for key in FactorSetDict:
        Temp = pd.DataFrame(FactorSetDict[key].columns[1:], columns=['Factor'])
        Temp['Source'] = key
        SourceFactorDf = SourceFactorDf.append(Temp) if not SourceFactorDf.empty else Temp

AllFactorsFrequencyDfOrig = pd.DataFrame()
if FactorMap or Monitor or DoTrading or DoTrade:
    if MonitorFrequency == 'RegressionFrequency' and RegressionFrequency!='Daily':
        ''' Create returns at regression frequency across all subfactors '''
        AllFactorsDict = pd.read_excel(filepath + filenameRaw + 'Missing.xlsx', sheet_name=None)
        for key in AllFactorsDict:
            df = AllFactorsDict[key]
            Temp = FactorSet_Setup[
                (FactorSet_Setup['FactorSet'] == filenameRaw + 'Missing') & (FactorSet_Setup['FactorSubset'] == key)]
            df = ReturnCreator(df, RegressionFrequency, Temp['Type'].iloc[0], Temp['Multiplier'].iloc[0], 1)
            ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities
            if (consecutive) missing data is above threshold, remove start or end of consecutive missing data
            across all columns and create Log including for missing values '''
            # (df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
            # ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
            df, dfLogDf = \
                MissingDataFix(df, ['FactorSet', 'FactorSubset'], [filenameRaw + 'Missing', key], spike,
                               MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,ConsecutiveMissingThreshold)
            dfColumns = list(set(df.columns) - set(AllFactorsFrequencyDfOrig.columns))
            dfColumns = ['Date'] + dfColumns if 'Date' not in dfColumns else dfColumns
            AllFactorsFrequencyDfOrig = pd.merge(AllFactorsFrequencyDfOrig, df[dfColumns], left_on='Date', right_on='Date', how='right') if \
                                    not AllFactorsFrequencyDfOrig.empty else df
        AllFactorsMonitorFrequencyDf = AllFactorsFrequencyDfOrig
    else:
        try:
            AllFactorsMonitorFrequencyDf = AllFactorsDf
            AllFactorsFrequencyDfOrig = AllFactorsDf
        except NameError:
            AllFactorsDf = GetFactorsFromDict(filepath + filenameRaw + 'Missing.xlsx', [])
            AllFactorsMonitorFrequencyDf = AllFactorsDf
            AllFactorsFrequencyDfOrig = AllFactorsDf

###############################################################################################################################
#######################################################  Algorithm  #######################################################################
###############################################################################################################################

fileResultsAll = filepathOut + '\\' + 'ResultsAll.csv'
fileResultsAllC = filepathOut + '\\' + 'ResultsAllC.csv'
fileResultsRC = filepathOut + '\\' + 'ResultsRC.csv'
fileResultsRCAllPoints = filepathOut + '\\' + 'ResultsRCAllPoints.csv'
ResultsAll = pd.DataFrame()
ResultsAllC = pd.DataFrame()
ResultsRC = pd.DataFrame()
ResultsRCAllPoints = pd.DataFrame()
FactorTradesDf = pd.DataFrame()
FactorType = None
FactorMultiplier = None
ResultsCountAll = 0
j = 0
jlist = [0]
m = 0  # ResultsAll
c = 0  # ResultsAllC
r = 0  # Reshuffle
k = 0  # ResultsRC

ResultsAllFixed = pd.DataFrame()
ResultsAllCFixed = pd.DataFrame()
ResultsRCFixed = pd.DataFrame()
ResultsRCAllPointsFixed = pd.DataFrame()
jFixed = 0
jlistFixed = [0]
mFixed = 0  # ResultsAllFixed
cFixed = -1 # ResultsAllCFixed
rFixed = 0  # ReshuffleFixed
kFixed = 0  # ResultsRCFixed

# Monte Carlo
for MCi in range(MC):

    Date = 'Date'
    rName = ''
    rReshuffle = 0 # Reshuffle
    end = False
    ParametersDf = pd.DataFrame()
    FundMissingLogDf = pd.DataFrame()
    FactorMissingLogDf = pd.DataFrame()
    FactorMissingLogDfReshuffle = pd.DataFrame()
    writerMissingFundLog = pd.ExcelWriter(filepath + 'MissingFundLog.xlsx', engine='xlsxwriter')
    writerMissingFactorLog = pd.ExcelWriter(filepath + 'MissingFactorLog.xlsx', engine='xlsxwriter')

    ''' Monitor '''
    for MonitorKey in MonitorStrategyDict.keys():
        for MonitorValue in MonitorStrategyDict[MonitorKey]:

            # For each parameter set
            # for p in tqdm(range(0,2)):
            for p in ParameterCaseList:
                print('Parameter ' + str(p))

                ParametersLoop = ParametersOrig.loc[[p], :].reset_index(drop=True)
                # All horizons are augmented by 1 after the below
                Parameters = Parameterset(ParametersLoop, excel, filepath, ParametersFilename, HorizonsMin, HorizonsMax, LagsMin, LagsMax,
                                           StyleInFirstPoint, RollingStyleInFirstPoint, RollingStyleInLastPoint, Modelvalue,
                                           Modeltypevalue, Holdingsvalue, HorizonStep, LagStep, WeightAvg, Horizons,
                                           Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling, UseHalfLife,
                                           HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                                          DateStart, DateEnd, NoOfWindows)
                # Create to save at the end
                ParametersDfTemp = pd.DataFrame(Parameters.items(), columns = ['Parameter', 'Case_' + str(p)])
                ParametersDf = ParametersDfTemp if ParametersDf.empty else pd.merge(ParametersDf, ParametersDfTemp, \
                                left_on = 'Parameter', right_on = 'Parameter', how='inner')
                if excel == 1:
                    for d, key in enumerate(list(Parameters.keys())):
                        globals()[key] = Parameters[key]
                    # Min return catch - making sure MinReturnsDesired and window size are consistent
                    if TestCase==True:
                        MinReturnsDesired = window
                        spike = 999999
                        window = windowTest
                    windowreturns = window if DoRolling == 1 else MinReturnsDesired
                    Minreturns = min(MinReturnsDesired, windowreturns)

                # Window start
                WStart = AssetReturnsOrig[AssetReturnsOrig['Date'] == DateStart].index.values[0] if not np.isnan(DateStart) else 0
                # Window end
                if not np.isnan(NoOfWindows):
                    WEnd = WStart + NoOfWindows
                elif not np.isnan(DateEnd):
                    WEnd = AssetReturnsOrig[AssetReturnsOrig['Date'] == DateEnd].index.values[0]
                else:
                    WEnd = np.inf

                # For each Fund
                FundList = range(AssetReturnsOrig[DependentType].unique().shape[0]) if FundsList==['All'] else FundsList
                for i in FundList:

                    print('Fund ' + str(i))

                    Dependent = AssetReturnsOrig[AssetReturnsOrig.loc[:, DependentType].
                        isin([AssetReturnsOrig.loc[:, DependentType].unique()[i]])].loc[:, ['Date', AssetDataType]].\
                        reset_index(drop=True)
                    cols = Dependent.columns.tolist()
                    cols[1] = AssetReturnsOrig.loc[:, DependentType].unique()[i]
                    Dependent.columns = cols
                    Dependent.rename(columns={list(Dependent)[0]: 'Date'}, inplace=True)
                    DependentName = Dependent.columns[1]

                    # Calculate weekly returns if instructed. By default, all nan values are filled forward.
                    # Enhance to check if a week contains at least x% non nan returns
                    Dependent = ReturnCreator(Dependent, RegressionFrequency, AssetDataType, AssetMultiplier, 1)
                    ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities 
                    if (consecutive) missing data is above threshold, remove start or end of consecutive missing data 
                    across all columns and create Log including for missing values. '''
                    # df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
                    # ConsecutiveMissingColumns, ConsecutiveMissingThreshold
                    Dependent, FundLogDf = MissingDataFix(Dependent, ['Parameter', DependentType], [str(p), DependentName],
                                                          spike, 1, 0.3, '', 8)
                    FundMissingLogDf = FundMissingLogDf.append(FundLogDf)
                    Dependent = Dependent.dropna(axis='rows',thresh=1).reset_index(drop=True)
                    Dependent['Date'] = (pd.to_datetime(Dependent['Date'])).dt.strftime('%Y-%m-%d')
                    # start = Dependent.iloc[:, 1].first_valid_index()
                    start = 0
                    ResultsCount = 0
                    ResultsFlag = False

                    # We only process funds with min return history and making sure to start prior to Datestartmax
                    if Dependent.shape[1] > 1 and Dependent.shape[0] > Minreturns and \
                            pd.to_datetime(Dependent.iloc[0, 0]) < Datestartmax:

                        #######################
                        # Rolling window loop #
                        #######################

                        if DoRolling == 0: window = Dependent.shape[0]

                        # Change to run on monitor frequency!!!
                        WindowList = \
                            range(max(0, WStart), max(min(Dependent.shape[0] - window + 1, WEnd), 1 + max(0, WStart))) \
                            if WindowsList == ['All'] else WindowsList
                        for w in WindowList:

                            DateList = list(Dependent.loc[start + w:start + window + w - 1, 'Date'])
                            print('Window ' + str(w))
                            # k = 0  # ResultsRC
                            wReport = w

                            # Determine whether to do regression or monitor
                            # Do regression at the first available window point
                            # Do regression only on the last period of each month.
                            # Enhance to check if last point is end of month, right now it does regression.
                            ''' Set alpha and betas to zero, calculate BH-1 '''
                            ResultsCount += 1 if ResultsFlag else ResultsCount
                            ResultsCountAll += 1 if ResultsFlag else ResultsCountAll
                            wStart = w if ResultsCount == 0 else wStart
                            if Monitor:
                                DoMonitorStart = True if ResultsCount==0 else False
                                if MonitorKey == 'MonthEnd':
                                    DoMonitor = False if DoMonitorStart or (w > 0 and \
                                        (w == max(min(Dependent.shape[0] - window + 1, WEnd), 1 + max(0, WStart)) - 1 or \
                                        (datetime.strptime(Dependent.loc[w + window - 1, 'Date'], '%Y-%m-%d').month != \
                                        datetime.strptime(Dependent.loc[w + window, 'Date'], '%Y-%m-%d').month))) \
                                        else True
                                if MonitorKey == 'QuarterEnd':
                                    DoMonitor = False if DoMonitorStart or (w > 0 and \
                                        (w == max(min(Dependent.shape[0] - window + 1, WEnd), 1 + max(0, WStart)) - 1 or \
                                        ((datetime.strptime(Dependent.loc[w + window - 1, 'Date'], '%Y-%m-%d').month - 1)//3 != \
                                         (datetime.strptime(Dependent.loc[w + window, 'Date'], '%Y-%m-%d').month - 1)//3))) \
                                        else True
                                elif MonitorKey == 'Period':
                                    DoMonitor = False if DoMonitorStart or (1 + ResultsCount) % MonitorValue == 0 else True
                            else:
                                DoMonitor = False

                            #####################
                            # SubParameter loop #
                            #####################

                            ''' Defines all parameters except: 'DoRolling', 'Window', 'NoOfWindows', 'DateStart', 'DateEnd', 
                            'RegressionFrequency', 'MinReturnsDesired', 'Datestartmax' '''
                            for s in SubParameterCaseList:
                                # s=0
                                print('SubParameter ' + str(s))

                                if s != 'NA':
                                    SubParametersLoop = ParametersOrig.loc[[s], :].reset_index(drop=True)
                                    # All horizons are augmented by 1 after the below
                                    SubParameters = Parameterset(SubParametersLoop, excel, filepath, ParametersFilename, HorizonsMin,
                                        HorizonsMax, LagsMin, LagsMax, StyleInFirstPoint, RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                        Modelvalue, Modeltypevalue, Holdingsvalue, HorizonStep, LagStep, WeightAvg, Horizons,
                                        Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling, UseHalfLife,
                                        HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting, Lags, Point,
                                        DateStart, DateEnd, NoOfWindows)
                                    if excel == 1:
                                        for d, key in enumerate(list(set(list(SubParameters.keys())) - set(['DoRolling', 'window',
                                             'NoOfWindows', 'DateStart', 'DateEnd', 'RegressionFrequency', 'MinReturnsDesired',
                                             'Datestartmax']))):
                                            globals()[key] = SubParameters[key]
                                else:
                                    SubParameters = Parameters

                                # ResultsAll = pd.DataFrame()
                                # ResultsAllC = pd.DataFrame()
                                # ResultsRC = pd.DataFrame()
                                # ResultsRCAllPoints = pd.DataFrame()
                                # j = 0
                                # jlist = [0]
                                # m = 0  # ResultsAll
                                # c = 0  # ResultsAllC
                                # r = 0  # Reshuffle

                                AllFactorsFrequencyDf = AllFactorsMonitorFrequencyDf if DoMonitor else AllFactorsFrequencyDfOrig
                                doKPI = False if DoMonitor else doKPI
                                DoRebalanceMV = False if DoMonitor else DoRebalanceMVOrig
                                DoRebalanceBeta = False if DoMonitor else DoRebalanceBetaOrig
                                if FactorMap:
                                    Suffix = 'Sub' if DoMonitor else SuffixOrig
                                else:
                                    Suffix = ''

                                j, jlist, optimalrow, ResultsAll, ResultsAllC, ResultsRC, \
                                ResultsRCAllPoints, Dataall, FactorMissingLogDf, ResultsFlag, DateWindow, \
                                DataOOS, AllSubFactorMapList, FactorGroup, FactorSet, FactorSubset, Data = \
                                Regressor(w, m, c, k, i, p, s, j, jlist, r, rName, MaxRows, Dependent.copy(), DependentType, window,
                                          RollingStyleInFirstPoint, RollingStyleInLastPoint, xshift, Minreturns,
                                          Datestartmax, LamdaList, SubParameters, DependentName, FactorGroupList, FactorGroupDict,
                                          FactorSetList, FactorSubsetList, ResultsAll.copy(), ResultsAllC.copy(),
                                          ResultsRC.copy(), ResultsRCAllPoints.copy(),
                                          TestCase, OOSShift, Point, RegressionFrequency, filepath, filenameRaw, RegimeThreshold,
                                          Direction, Absolute, doKPI, Regime, 100*spike, MissingPercentRow, MissingPercentColumn,
                                          ConsecutiveMissingColumns, ConsecutiveMissingThreshold, FactorDataType,
                                          ['Parameter', 'SubParameter', DependentType, 'Window'],
                                          [str(p), str(s), DependentName, str(w)], FactorMissingLogDf, FactorsGroupMapPrefix,
                                          EntityRiskParity, SourceFactorDf, AssetReturnsOrigP, FactorTradesDf.copy(),
                                          FactorType, FactorMultiplier, DoTrading, DoRebalanceMV, wReport, DoRegression,
                                          AllSubFactorList, DoRebalanceBeta, Cash, BetaThresholdsDf, AllFactorsPDf,
                                          AllFactorsFrequencyDf, DoMonitor, Suffix, DoTrade, FactorSet_Setup, DateList,
                                          MonitorKey, MonitorValue, wStart)
                                # else:
                                #     ''' 1. Copy all results from previous window, calculate BH-1
                                #         2. Set betas equal to BH-1
                                #         3. Set 0 trades for all
                                #         4. Pass alpha and betas to get proper results '''
                                #
                                #     ''' 1. Copy all results from previous window, calculate BH-1 '''
                                #     ResultsAllC.loc[c, :] = ResultsAllC.loc[c - 1, :]
                                #     DoRebalanceMVStatic = False
                                #     DoRebalanceBetaStatic = False
                                #     ResultsAllC = TradingStats(DoRebalanceMVStatic, DoRebalanceBetaStatic, ResultsAllC, c,
                                #                                DateWindow, AllFactorsDict, AllSubFactorMapList,
                                #                                FactorSet_Setup, filenameRaw, AllFactorsPDf,
                                #                                AllFactorsMonitorFrequencyDf, AssetReturnsOrigP,
                                #                                DependentName, DataOOS, doKPI, BetaThresholdsDf)

                            ''' Debug
                                DateFilterList = \
                                [(datetime.strptime(DateWindow, '%Y-%M-%d').date() - BDay(x)).date().__str__() \
                                for x in range(3, window*3)]
                                DateFilterList = []
                                copy(ResultsAllC, ['Beta', 'Level', 'Return', 'MarketValue', 'Quantity', 'Window', 'Trade', 'R2'],
                                     ['Horizons','Lags','Lamda', 'Smooth', 'min', 'max', 'Predicted Beta', 'Alpha', 'Style', 'Mean', 'From', 'To'] \
                                     + DateFilterList)
                                copy(ResultsAllCFixed, ['Beta', 'Level', 'Return', 'MarketValue', 'Quantity', 'Window', 'Trade', 'R2'],
                                     ['Horizons','Lags','Lamda', 'Smooth', 'min', 'max', 'Predicted Beta', 'Alpha', 'Style', 'Mean', 'From', 'To'] \
                                     + DateFilterList)
                                copy(ResultsAllC, ['AEX ' + DateWindow, 'AEX ' + ResultsAllC.loc[c-2, 'DateWindow'], 'BetaSum', 'Level Fund', 'LevelBH Fund', 
                                'Return Fund', 'ReturnBH Fund'], ['Predicted'])
                                copy(ResultsAllC, ['AEX', 'BetaSum', 'Level Fund', 'LevelBH Fund', 
                                'Return Fund', 'ReturnBH Fund'], ['Predicted'])
                                copy(ResultsAllC, ['AEX ' + DateWindow, 'AEX ' + ResultsAllC.loc[max(0, c-2), 'DateWindow'], 'BetaSum', 'Level Fund', 'LevelBH Fund', 
                                'Return Fund', 'ReturnBH Fund', 'R2', 'Style', 'Residual'], ['Predicted', 'Smooth'])
                                copy(ResultsRC, ['FundOOS'], [])
                                copy(ResultsAllC, ['FundOOS'], [])
                                copy(ResultsRC, [], [])
                                copy(ResultsAll, ['FundOOS'], [])
                            '''

                            if ResultsFlag and ((w - wStart > 0 and (DoTrade or DoMonitor)) or FactorMap):
                                ''' Compare betas and keep the same if not enough difference
                                The comparison takes place against buy and hold, not continuous rebalancing, betas.
                                The reason of doing this is that, after the first iteration the TradeList betas may
                                end up being not different enough.
                                Do not rebalance if previous rebalance happened too recently 
                                Rebalance if Fund Level BH has deviated too much from Fund Level '''

                                c = ResultsAllC.shape[0] - 1
                                FactorGroup = str(ResultsAllC.loc[c, 'FactorGroup'])
                                FactorSet = str(ResultsAllC.loc[c, 'FactorSet'])
                                FactorSubset = str(ResultsAllC.loc[c, 'FactorSubset'])
                                FactorMap = \
                                    RegressorLoop(FactorGroupList=[FactorGroup], FactorSetList=[FactorSet],
                                                  FactorGroupDict=FactorGroupDict,
                                                  FactorSubsetList=[FactorSubset], filepath=filepath, Cash=Cash,
                                                  filenameRaw=filenameRaw,
                                                  RegressionFrequency=RegressionFrequency,
                                                  filenameGroupMap=filenameGroupMap,
                                                  Minreturns=Minreturns, Datestartmax=Datestartmax, LamdaList=LamdaList,
                                                  DoRegression=DoRegression,
                                                  FactorsGroupMapPrefix=FactorsGroupMapPrefix,
                                                  EntityRiskParity=EntityRiskParity,
                                                  AssetReturnsOrigP=AssetReturnsOrigP, DoFactorsorig=DoFactorsorig,
                                                  FactorSet_Setup=FactorSet_Setup, AllSubFactorList=AllSubFactorList,
                                                  DoFactorMap=True,
                                                  FactorSetDict={}, FactorTradesDf=pd.DataFrame(), SubParameters={},
                                                  FactorType='',
                                                  FactorMultiplier=np.nan, Dependent=pd.DataFrame(), p=np.nan, s=np.nan,
                                                  wReport=np.nan,
                                                  spike=np.nan, MissingPercentRow=np.nan, MissingPercentColumn=np.nan,
                                                  ConsecutiveMissingColumns=np.nan, ConsecutiveMissingThreshold=np.nan,
                                                  MaxRows=np.nan, w=np.nan,
                                                  window=np.nan, xshift=np.nan, AllSubFactorMapList=[],
                                                  DependentName='', OOSShift=np.nan,
                                                  Point=np.nan, RegimeThreshold=np.nan, DoMonitor=np.nan, Direction='',
                                                  Absolute=np.nan,
                                                  doKPI=np.nan, Regime=np.nan, ResultsAllC=pd.DataFrame(), c=np.nan,
                                                  Suffix='',
                                                  ResultsAll=pd.DataFrame(), j=np.nan, DependentType='', i=np.nan,
                                                  r=np.nan, rName='',
                                                  SourceFactorDf=pd.DataFrame(), ResultsFlag=np.nan,
                                                  ResultsRC=pd.DataFrame(), k=np.nan,
                                                  DoRollingWindowsMean=np.nan, FullTimeAvg=np.nan,
                                                  AllSubFactorListCreator=np.nan, DateList=[],
                                                  AnalyticName='', MonitorKey='', MonitorValue='')[7]
                                # start = Dataall.iloc[:, 1].first_valid_index()
                                # DateList = list(Dataall.loc[start + w:start + window + w - 1, 'Date'])
                                DateList = list(Data['Date'])
                                DateWindow = ResultsAllC.loc[c, 'DateWindow']
                                Temp = DataFilterColumn(ResultsAllC.loc[c, :].T.reset_index(), ['Quantity'], ['BH', 'NewTrade']).T
                                Temp = DataFilterColumn(Temp.T, [' ' + DateWindow], []).T
                                Temp.columns = Temp.iloc[0]
                                Temp = Temp.drop(Temp.index[0])
                                SuffixFixed = 'Sub' if (FactorMap or DoMonitor) and not DoTrade else ''
                                AllFactorsPDict = pd.read_excel(filepath + filenameRaw + 'P' + 'Missing.xlsx',
                                                               engine='openpyxl', sheet_name=None)
                                AllSubFactorListFixed = [x.replace('Quantity ', '') for x in Temp.columns]
                                AllSubFactorListFixed = [x.replace(' ' + DateWindow, '') for x in AllSubFactorListFixed]
                                FactorType = list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) ==
                                                                   FactorSet) &
                                                                  (FactorSet_Setup['FactorSubset'].astype(str) ==
                                                                   FactorSubset)]['Type'])[0]
                                FactorMultiplier = list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) ==
                                                                         FactorSet) &
                                                                        (FactorSet_Setup['FactorSubset'].astype(str) ==
                                                                         FactorSubset)]
                                                        ['Multiplier'])[0]

                                ''' 1. Compute all betas
                                    2. Check which betas are not very different from BH betas and update last betas to the BH ones
                                    3. Check which betas are very different from BH betas and calculate them against Fund - BH betafactors
                                    4. Check if TradeList betas from #3 are still very different from BH betas
                                        4a1. If they are, calculate betas for the NoTradeList factors against Fund - TradeList betafactors
                                        4a2. Check if #4a1 betas are different enough from #2. 
                                            4a2a. If they are, update them and re-compute the #3 betas.
                                            4a2b. Check if #4a2a betas are different enough from #3.
                                            4a2c. If they are, update them and re-compute the #4a1 betas.
                                        Repeat
                                        4b1. If they are not, update last beta to the BH one
                                    Repeat '''

                                for iLoop in range(iLoopmax):
                                    if iLoop==0:
                                        ResultsAllCFixed = ResultsAllC.loc[[c], :].reset_index(drop=True) \
                                            if ResultsAllCFixed.empty else \
                                            pd.concat([ResultsAllCFixed, ResultsAllC.loc[[c], :]], axis=0, ignore_index=True).\
                                                reset_index(drop=True)
                                        cFixed = cFixed + 1
                                        cStart = cFixed
                                    TradeListLoop = AllSubFactorListFixed if iLoop == 0 else TradeList

                                    if DoTrade and not DoMonitor:

                                        jFixed, jlistFixed, TradeList, ResultsAllFixed, ResultsAllCFixed, ResultsRCFixed, \
                                        ResultsRCAllPointsFixed, mFixed, cFixed, ResultsFlagFixed, BreakFlag, BetaThresholdsDf = \
                                            Trader(filepath, filenameRaw, ResultsAllC, TradeListLoop, c,
                                                   DependentName, DateWindow, AllSubFactorListFixed,
                                                   Data, start, w, window, DateList, SubParameters,
                                                   FactorType, FactorMultiplier, i, p, s, MaxRows,
                                                   DependentType, xshift, Minreturns, Datestartmax,
                                                   FactorGroupDict, RegressionFrequency, spike,
                                                   MissingPercentRow, MissingPercentColumn,
                                                   ConsecutiveMissingColumns, ConsecutiveMissingThreshold,
                                                   FactorDataType, FactorsGroupMapPrefix,
                                                   EntityRiskParity, SourceFactorDf, AssetReturnsOrigP,
                                                   BetaThresholdsDf, iLoop, ResultsAllFixed, ResultsAllCFixed, ResultsRCFixed,
                                                   ResultsRCAllPointsFixed, jFixed, jlistFixed, mFixed, cFixed, rFixed, kFixed,
                                                   cStart, Cash, LeverageMin, LeverageMax)

                                        # copy(ResultsAllCFixed,['Window', 'Beta', 'Trade'],['Mean'])
                                        if ResultsFlagFixed:
                                            ResultsAllCFixed.loc[cFixed, 'TradingLoop'] = iLoop
                                            # mFixed and jlistFixed are set to prepare adding results in the next loop whereas
                                            # cFixed corresponds to current results and will be incremented later inside Trader
                                            mFixed = mFixed + 1
                                            jlistFixed.append(jFixed)

                                        # Break loop if no more TradeList due to convergence or if everything needs to be traded
                                        if BreakFlag:
                                            break
                                    else:
                                        BreakFlag = True
                                        break

                                if DoMonitor or (not FactorMap and not DoTrade):
                                    ResultsAllCFixed.loc[cFixed, ['Trade ' + x for x in AllSubFactorListFixed]] = \
                                        np.array([0 for x in AllSubFactorListFixed])
                                if not DoTrade: TradeList = []
                                ResultsAllC.loc[c, ['Trade ' + x for x in AllSubFactorListFixed]] = \
                                    ResultsAllCFixed.loc[cFixed, ['Trade ' + x for x in AllSubFactorListFixed]].values \
                                    if iLoop!=iLoopmax - 1 and BreakFlag else np.array([1 for x in AllSubFactorListFixed])
                                # Only update alpha and betas if not everything is going to be traded
                                # if len(TradeList) == len(AllSubFactorListFixed) then the below first term can be replaced by TradeList
                                if ResultsAllCFixed.loc[cFixed, ['Trade ' + x for x in AllSubFactorListFixed]].sum() < \
                                    len(AllSubFactorListFixed) or (FactorMap and not DoTrade):

                                        # AllTradesDf = ResultsAllCFixed.loc[[cFixed], ['Trade ' + x for x in AllSubFactorListFixed]]
                                        # TradeList = list(AllTradesDf.loc[:,(AllTradesDf==1).any(axis=0)])
                                        # TradeList = [x.replace('Trade ', '') for x in TradeList]

                                        # If at least one factor gets traded, all BH analytics need to be updated
                                        # Enhance to check MV rebalancing
                                        if ResultsAllCFixed.loc[cFixed, ['Trade ' + x for x in AllSubFactorListFixed]].sum() > 0:
                                            DoRebalanceMVFixed = False
                                            DoRebalanceBetaFixed = True
                                        else:
                                            # If nothing gets traded do not change BH analytics
                                            DoRebalanceMVFixed = False
                                            DoRebalanceBetaFixed = False
                                        DoRegressionFixed = False
                                        doKPIFixed = False
                                        CashFixed = Cash
                                        DoMonitorFixed = False
                                        DoTradingFixed = DoTrading if DoTrade or DoMonitor else False

                                        # Need to allow alpha to be calculated further below!!! For now we don't support alpha.
                                        AlphaColumns = ['Alpha ' + Date for Date in DateList]
                                        AlphaColumnsSuffix = ['Alpha' + SuffixFixed + ' ' + Date for Date in DateList]
                                        ResultsAllC.loc[c, AlphaColumns] = 0
                                        ResultsAllC.loc[c, AlphaColumnsSuffix] = 0
                                        if DoTrade and not DoMonitor:
                                            BetaColumns = \
                                                ['Beta ' + x + ' ' + Date for x in AllSubFactorListFixed for Date in DateList]
                                            ResultsAllC.loc[c, BetaColumns] = ResultsAllCFixed.loc[cFixed, BetaColumns].values
                                        elif w - wStart > 0 and DoMonitor:
                                            # Past betas are copied forward (doesn't affect with TE calculations and works with FLS)
                                            for Date in DateList[:-1]:
                                                BetaColumns = ['Beta ' + x + ' ' + Date for x in AllSubFactorListFixed]
                                                ResultsAllC.loc[c, BetaColumns] = ResultsAllC.loc[c - 1, BetaColumns].values
                                            # Only last betas are carried forward as buy and hold
                                            BetaColumnsSource = ['BetaBH-1 ' + x + ' ' + DateWindow for x in AllSubFactorListFixed]
                                            BetaColumns = ['Beta ' + x + ' ' + DateWindow for x in AllSubFactorListFixed]
                                            ResultsAllC.loc[c, BetaColumns] = ResultsAllCFixed.loc[cFixed, BetaColumnsSource].values

                                        # Obtain results based on above alpha, betas
                                        FactorGroupListFixed = FactorGroup
                                        FactorSetListFixed = FactorSet
                                        FactorSubsetListFixed = FactorSubset
                                        # The below requires one more run for DoTrade and FactorMap
                                        if DoMonitor or (FactorMap and not DoTrade):
                                            # Work against the subfactors
                                            FactorGroupListFixed = ['Fixed']
                                            FactorSetListFixed = ['Fixed']
                                            FactorSubsetListFixed = ['Fixed']
                                            FactorTradesDfFixed = GetFactorsFromDict(filepath + filenameRaw + 'Missing.xlsx',
                                                                                     AllSubFactorListFixed)
                                            # Only one type and multiplier can apply across all sheets. Need to multiple inside
                                            # GetFactorsFromDict and return all rates with one multiplier and same type.
                                            FactorTypeFixed = 'Return'
                                            FactorMultiplierFixed = 100
                                        LamdaList = [ResultsAllC.loc[c, 'Lamda']]
                                        SubParameters['lam'] = ResultsAllC.loc[c, 'Lamda']
                                        SubParameters['Horizons'] = 1 + int(ResultsAllC.loc[c, 'Horizons'])
                                        SubParameters['HorizonsMin'] = 1 + int(ResultsAllC.loc[c, 'Horizons'])
                                        SubParameters['HorizonsMax'] = 1 + int(ResultsAllC.loc[c, 'Horizons'])
                                        SubParameters['Lags'] = 1 + int(ResultsAllC.loc[c, 'Lags'])
                                        SubParameters['LagsMin'] = 1 + int(ResultsAllC.loc[c, 'Lags'])
                                        SubParameters['LagsMax'] = 1 + int(ResultsAllC.loc[c, 'Lags'])

                                        ResultsAllC, ResultsRC, ResultsRCAllPoints = \
                                            [Regressor(w, m, c, k, i, p, s, j, jlist, r, rName, MaxRows, Dependent.copy(),
                                                      DependentType, window, RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                                      xshift, Minreturns, Datestartmax, LamdaList, SubParameters, DependentName,
                                                      FactorGroupListFixed, FactorGroupDict, FactorSetListFixed,
                                                      FactorSubsetListFixed, ResultsAll.copy(), ResultsAllC, ResultsRC,
                                                      ResultsRCAllPoints, TestCase, OOSShift, Point, RegressionFrequency,
                                                      filepath, filenameRaw, RegimeThreshold, Direction, Absolute, doKPIFixed,
                                                      Regime, 100 * spike, MissingPercentRow, MissingPercentColumn,
                                                      ConsecutiveMissingColumns, ConsecutiveMissingThreshold, FactorDataType,
                                                      ['Parameter', 'SubParameter', DependentType, 'Window'],
                                                      [str(p), str(s), DependentName, str(w)], FactorMissingLogDf,
                                                      FactorsGroupMapPrefix, EntityRiskParity, SourceFactorDf, AssetReturnsOrigP,
                                                      FactorTradesDfFixed, FactorTypeFixed, FactorMultiplierFixed, DoTradingFixed,
                                                      DoRebalanceMVFixed, wReport, DoRegressionFixed, AllSubFactorListFixed,
                                                      DoRebalanceBetaFixed, CashFixed, BetaThresholdsDf.copy(), AllFactorsPDf.copy(),
                                                      AllFactorsFrequencyDf, DoMonitorFixed, SuffixFixed, DoTrade,
                                                       FactorSet_Setup.copy(), DateList, MonitorKey, MonitorValue, wStart)[l]
                                             for l in [4,5,6]]
                                        if DoTrading:
                                            ResultsRC = \
                                                TotalTradeFeeReturnUpdater(doKPI, ResultsRC, k, SuffixFixed, DateWindow, DataOOS)
                                            ResultsAllC = \
                                                TotalTradeFeeReturnUpdater(doKPI, ResultsAllC, c, SuffixFixed, DateWindow, DataOOS)

                                        ''' Debug
                                        FactorGroupList = FactorGroupListFixed
                                        FactorSetList = FactorSetListFixed
                                        FactorSubsetList = FactorSubsetListFixed
                                        doKPI = doKPIFixed
                                        DoRebalanceMV=DoRebalanceMVFixed
                                        DoRebalanceBeta=DoRebalanceBetaFixed
                                        DoRegression=DoRegressionFixed
                                        AllSubFactorList = AllSubFactorListFixed
                                        FactorTradesDf = FactorTradesDfFixed
                                        FactorType = FactorTypeFixed
                                        FactorMultiplier = FactorMultiplierFixed
                                        Cash = CashFixed
                                        DoMonitor = DoMonitorFixed
                                        Suffix = SuffixFixed
                                        DoTrading = DoTradingFixed
                                        copy(ResultsAllC, [], [])
                                        '''

                                        AllSubFactorListFixed = list(set(AllSubFactorListFixed) - set(['Cash']))
                                        ResultsAllC.loc[c, ['Trade ' + x for x in AllSubFactorListFixed]] = \
                                            ResultsAllCFixed.loc[cFixed, ['Trade ' + x for x in AllSubFactorListFixed]].values
                                else:
                                    # Update BH analytics as everything gets traded
                                    DoRebalanceMVFixed = False
                                    DoRebalanceBetaFixed = True
                                    ResultsAllC = TradingStats(DoRebalanceMVFixed, DoRebalanceBetaFixed, ResultsAllC, c,
                                                               DateWindow, AllFactorsPDict, AllSubFactorList, FactorSet_Setup,
                                                               filenameRaw, AllFactorsPDf, AllFactorsFrequencyDf,
                                                               AssetReturnsOrigP, DependentName, DataOOS, doKPI, BetaThresholdsDf,
                                                               SuffixOrig, w, wStart)

                            # if os.path.isfile(fileResultsAll):
                            #     ResultsAll.to_csv(fileResultsAll, mode='a', index=False, header=False)
                            #     ResultsAllC.to_csv(fileResultsAllC, mode='a', index=False, header=False)
                            #     ResultsRC.to_csv(fileResultsRC, mode='a', index=False, header=False)
                            #     ResultsRCAllPoints.to_csv(fileResultsRCAllPoints, mode='a', index=False, header=False)
                            # else:
                            #     ResultsAll.to_csv(fileResultsAll, mode='a', index=False)
                            #     ResultsAllC.to_csv(fileResultsAllC, mode='a', index=False)
                            #     ResultsRC.to_csv(fileResultsRC, mode='a', index=False)
                            #     ResultsRCAllPoints.to_csv(fileResultsRCAllPoints, mode='a', index=False)

                            if ResultsFlag:
                                m = m + 1
                                c = c + 1
                                jlist.append(j)

                            #############
                            # Reshuffle #
                            #############

                            if DoReshuffle:

                                DoTradingReshuffle = False
                                DoRebalanceMVReshuffle = False
                                DoRebalanceBetaReshuffle = False
                                DoRegressionReshuffle = True
                                CashReshuffle = False
                                AllSubFactorListReshuffle = []
                                AllSubFactorListCreatorReshuffle = False
                                AnalyticNameReshuffle = ''
                                FactorMapReshuffle = False
                                # Reshuffling takes place on the optimal parameters and works like a window
                                s = ResultsAll.loc[optimalrow, 'SubParameterCaseID']
                                if s != 'NA':
                                    SubParametersLoop = ParametersOrig.loc[[s], :].reset_index(drop=True)
                                    # All horizons are augmented by 1 after the below
                                    SubParametersReshuffle = Parameterset(SubParametersLoop, excel, filepath, ParametersFilename,
                                                                 HorizonsMin,
                                                                 HorizonsMax, LagsMin, LagsMax, StyleInFirstPoint,
                                                                 RollingStyleInFirstPoint, RollingStyleInLastPoint,
                                                                 Modelvalue, Modeltypevalue, Holdingsvalue, HorizonStep,
                                                                 LagStep, WeightAvg, Horizons,
                                                                 Threshold, Intercept, Lamda, MarketDynamics, IndividualScaling,
                                                                 UseHalfLife,
                                                                 HalfLife, Decay, Calibrate, Leverage, LongOnly, Weighting,
                                                                 Lags, Point,
                                                                 DateStart, DateEnd, NoOfWindows)
                                    if excel == 1:
                                        for d, key in enumerate(
                                                list(set(list(SubParametersReshuffle.keys())) - set(['DoRolling', 'window',
                                                                                            'NoOfWindows', 'DateStart',
                                                                                            'DateEnd', 'RegressionFrequency',
                                                                                            'MinReturnsDesired',
                                                                                            'Datestartmax']))):
                                            globals()[key] = SubParametersReshuffle[key]
                                else:
                                    SubParametersReshuffle = Parameters

                                FactorGroupListReshuffle = FactorGroup
                                FactorSetListReshuffle = FactorSet
                                FactorSubsetListReshuffle = FactorSubset
                                LamdaListReshuffle = [ResultsAll.loc[optimalrow, 'Lamda']]
                                SubParametersReshuffle['lam'] = ResultsAll.loc[optimalrow, 'Lamda']
                                SubParametersReshuffle['Horizons'] = 1 + int(ResultsAll.loc[optimalrow, 'Horizons'])
                                SubParametersReshuffle['HorizonsMin'] = 1 + int(ResultsAll.loc[optimalrow, 'Horizons'])
                                SubParametersReshuffle['HorizonsMax'] = 1 + int(ResultsAll.loc[optimalrow, 'Horizons'])
                                SubParametersReshuffle['Lags'] = 1 + int(ResultsAll.loc[optimalrow, 'Lags'])
                                SubParametersReshuffle['LagsMin'] = 1 + int(ResultsAll.loc[optimalrow, 'Lags'])
                                SubParametersReshuffle['LagsMax'] = 1 + int(ResultsAll.loc[optimalrow, 'Lags'])
                                doKPIReshuffle = False
                                DoMonitorFixedReshuffle = False
                                SuffixReshuffle = False
                                DoTradeReshuffle = False
                                DateListReshuffle = []

                                BetasDf = FilterDfCreator(ResultsAllC, ['Beta'], ['Predicted', 'Mean', 'Style'], c-1). \
                                    dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                                BetaFactorColumns = list(BetasDf.columns.str.replace('Beta ', ''))
                                AlphaDf = FilterDfCreator(ResultsAllC, ['Alpha'], ['Predicted', 'Mean', 'Style'], c-1). \
                                    dropna(axis='columns', how='all').dropna().reset_index(drop=True)
                                ResidualUnsmoothDf = FilterDfCreator(ResultsAllC, ['Residual Unsmooth'], ['Mean', 'Window'], c-1). \
                                    dropna().reset_index(drop=True)
                                ResidualUnsmoothArr = ResidualUnsmoothDf.iloc[:, 1].to_numpy()

                                # Reload optimal FactorSubset
                                FactorSetDict = pd.read_excel(filepath + FactorSet + '.xlsx', engine='openpyxl', sheet_name=None)
                                Factorsorig, Data = \
                                [RegressorCalc(FactorGroup, FactorSet, FactorSubset, FactorSet_Setup, FactorTradesDf, filepath,
                                              SubParameters, Cash,
                                              filenameRaw, FactorType, FactorMultiplier, RegressionFrequency, Dependent, p, s,
                                              wReport, spike,
                                              MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,
                                              ConsecutiveMissingThreshold,
                                              MaxRows, w, window, filenameGroupMap, xshift, AllSubFactorMapList,
                                              AllSubFactorList, Minreturns,
                                              Datestartmax, DependentName, OOSShift, Point, LamdaList, Minreturns, w, window,
                                              RegimeThreshold,
                                              Direction, Absolute, doKPI, Regime, ResultsAllC, DoRegression, AllSubFactorList,
                                              c, DoMonitor, SuffixReshuffle,
                                              ResultsAll, j, DependentType, Dependent, i, p, s, wReport, r, rName, xshift,
                                              Point, filepath,
                                              FactorsGroupMapPrefix, EntityRiskParity, SourceFactorDf, AssetReturnsOrigP,
                                              filenameRaw, ResultsFlag,
                                              ResultsRC, k, DoRollingWindowsMean, FullTimeAvg, DoFactorsorig, FactorSetDict,
                                               AllSubFactorListCreatorReshuffle, DateList, AnalyticNameReshuffle,
                                               MonitorKey, MonitorValue, FactorMapReshuffle)[l]
                                 for l in [5,7]]
                                Factors = Factorsorig

                                # Combine factors based on lags. Enhanced to gather factors outside the fund data availability.
                                Kx = int(ResultsAll.loc[optimalrow, 'Lags'])
                                Kxx = 1 if (Kx == -1 or Kx == 0) else Kx
                                rolldata = RollXY(Data, Parameters, Kxx, 1)
                                Datarolllag = rolldata.get('Dataroll')
                                FactorsCombined = Datarolllag[Factors.columns]

                                for rReshuffle in range(1 + NReshuffle):

                                    print('rReshuffle=' + str(rReshuffle))
                                    # (Lag) Residual reshuffle
                                    rng = np.random.default_rng()
                                    ResidualUnsmoothReshuffleDf = pd.concat([ResidualUnsmoothDf[['Date']],
                                                                        pd.DataFrame(rng.permutation(ResidualUnsmoothArr),
                                                                          columns=['Residual Unsmooth Reshuffled'])], axis=1)
                                    AllDf = reduce(lambda left, right: pd.merge(left, right, on=['Date'], how='inner'),
                                                   [AlphaDf, BetasDf, FactorsCombined, ResidualUnsmoothReshuffleDf,
                                                    ResidualUnsmoothDf, Dependent])

                                    # Set beta of each combined factor to 0 and create bootstrapped fund. Same for alpha.
                                    # filename = 'Reshuffle MC ' + str(MCi) + ' R ' + str(NReshuffle) + 'Assets_' + Entity + \
                                    #            FactorSubset + ' ' + datetime.now().strftime("%d_%m_%Y %H_%M_%S") + '.csv'
                                    filename = 'Reshuffle ' + 'Assets_' + Entity + FactorSubset + '.csv'
                                    file = filepathOut + '\\' + filename

                                    for f in range(1, 1 + BetasDf.shape[1]):
                                        ResultsAllReshuffle = pd.DataFrame()
                                        ResultsAllCReshuffle = pd.DataFrame()
                                        ResultsRCReshuffle = pd.DataFrame()
                                        ResultsRCAllPointsReshuffle = pd.DataFrame()
                                        mReshuffle = 0  # ResultsAllReshuffle
                                        cReshuffle = 0  # ResultsAllCReshuffle
                                        kReshuffle = 0  # ResultsRCReshuffle
                                        jReshuffle = 0
                                        jlistReshuffle = [0]

                                        AllLoopDf = AllDf.copy()
                                        AllLoopDf.iloc[:, f] = 0
                                        for b in range(1, BetasDf.shape[1]):
                                            AllLoopDf['BetaCombinedFactor ' + BetaFactorColumns[b]] = \
                                                AllLoopDf['Beta ' + BetaFactorColumns[b]] * AllLoopDf[BetaFactorColumns[b]]
                                        AllLoopSumDf = AllLoopDf.loc[:, AllLoopDf.columns.str.contains
                                                                        ('|'.join(['Alpha', 'BetaCombinedFactor',
                                                                                   'Residual Unsmooth Reshuffled']))]
                                        yReshuffle = pd.concat([AllLoopDf[['Date']], AllLoopSumDf.sum(axis=1)], axis=1)
                                        yReshuffle.rename(columns={list(yReshuffle)[1]: Dependent.columns[1]}, inplace=True)

                                        # Estimate alpha and betas based on optimal horizons, lags from original regression
                                        # Most of the below is not used
                                        jReshuffle, jlistReshuffle, optimalrowReshuffle, \
                                        ResultsAllReshuffle, ResultsAllCReshuffle, ResultsRCReshuffle, ResultsRCAllPointsReshuffle, \
                                        DataallReshuffle, FactorMissingLogDfReshuffle, ResultsFlag, DateWindow, \
                                        DataOOSReshuffle, AllSubFactorMapListReshuffle, FactorGroupReshuffle, FactorSetReshuffle, \
                                        FactorSubsetReshuffle, DataReshuffle = \
                                            Regressor(w, mReshuffle, cReshuffle, kReshuffle, i, p, s, jReshuffle, jlistReshuffle,
                                                      rReshuffle, AllLoopDf.columns[f], MaxRows, yReshuffle,
                                                      DependentType, window,
                                                      RollingStyleInFirstPoint, RollingStyleInLastPoint, xshift, Minreturns,
                                                      Datestartmax, LamdaListReshuffle, SubParametersReshuffle, DependentName,
                                                      FactorGroupListReshuffle, FactorGroupDict,
                                                      FactorSetListReshuffle, FactorSubsetListReshuffle, ResultsAllReshuffle,
                                                      ResultsAllCReshuffle, ResultsRCReshuffle,
                                                      ResultsRCAllPointsReshuffle,
                                                      TestCase, OOSShift, Point, RegressionFrequency, filepath, filenameRaw,
                                                      RegimeThreshold, Direction, Absolute,
                                                      doKPIReshuffle, Regime, 100 * spike, MissingPercentRow, MissingPercentColumn,
                                                      ConsecutiveMissingColumns,
                                                      ConsecutiveMissingThreshold, FactorDataType,
                                                      ['Parameter', 'SubParameter', DependentType, 'Window'],
                                                      [str(p), str(s), DependentName, str(w)], FactorMissingLogDf,
                                                      FactorsGroupMapPrefix, EntityRiskParity, SourceFactorDf,
                                                      AssetReturnsOrigP, DoTradingReshuffle, DoRebalanceMVReshuffle, w,
                                                      DoRegressionReshuffle, AllSubFactorListReshuffle, DoRebalanceBetaReshuffle,
                                                      CashReshuffle, BetaThresholdsDf, AllFactorsPDf, AllFactorsFrequencyDfOrig,
                                                      DoMonitorFixedReshuffle, SuffixReshuffle, DoTradeReshuffle, FactorSet_Setup,
                                                      DateListReshuffle, MonitorKey, MonitorValue, wStart)

                                        if os.path.isfile(file):
                                            ResultsAllReshuffle.to_csv(file, mode='a', index=False, header=False)
                                        else:
                                            ResultsAllReshuffle.to_csv(file, index=False, mode='a')

                        ##############################
                        # End of Rolling window loop #
                        ##############################

                        if w - wStart > 0 and ResultsCountAll > 0:

                            if DoAverages or not ResultsFlag:
                                # Even if some factors tried do not have a map, we want to gather results collectively
                                Suffix = 'Sub' if FactorMap and Monitor else ''
                                FactorGroup = str(ResultsAllC.loc[c -1, 'FactorGroupReport'])
                                FactorSet = str(ResultsAllC.loc[c -1, 'FactorSetReport'])
                                FactorSubset = str(ResultsAllC.loc[c -1, 'FactorSubsetReport'])
                                if FactorTradesDf.empty and FactorSet != 'Fixed':
                                    FactorSetDict = pd.read_excel(filepath + FactorSet + '.xlsx', engine='openpyxl',
                                                                  sheet_name=None)
                                FactorMap = \
                                    RegressorLoop(FactorGroupList=[FactorGroup], FactorSetList=[FactorSet],
                                                  FactorGroupDict=FactorGroupDict,
                                                  FactorSubsetList=[FactorSubset], filepath=filepath, Cash=Cash,
                                                  filenameRaw=filenameRaw,
                                                  RegressionFrequency=RegressionFrequency,
                                                  filenameGroupMap=filenameGroupMap,
                                                  Minreturns=Minreturns, Datestartmax=Datestartmax, LamdaList=LamdaList,
                                                  DoRegression=DoRegression,
                                                  FactorsGroupMapPrefix=FactorsGroupMapPrefix,
                                                  EntityRiskParity=EntityRiskParity,
                                                  AssetReturnsOrigP=AssetReturnsOrigP, DoFactorsorig=DoFactorsorig,
                                                  FactorSet_Setup=FactorSet_Setup, AllSubFactorList=AllSubFactorList,
                                                  DoFactorMap=True,
                                                  FactorSetDict={}, FactorTradesDf=pd.DataFrame(), SubParameters={},
                                                  FactorType='',
                                                  FactorMultiplier=np.nan, Dependent=pd.DataFrame(), p=np.nan, s=np.nan,
                                                  wReport=np.nan,
                                                  spike=np.nan, MissingPercentRow=np.nan, MissingPercentColumn=np.nan,
                                                  ConsecutiveMissingColumns=np.nan, ConsecutiveMissingThreshold=np.nan,
                                                  MaxRows=np.nan, w=np.nan,
                                                  window=np.nan, xshift=np.nan, AllSubFactorMapList=[],
                                                  DependentName='', OOSShift=np.nan,
                                                  Point=np.nan, RegimeThreshold=np.nan, DoMonitor=np.nan, Direction='',
                                                  Absolute=np.nan,
                                                  doKPI=np.nan, Regime=np.nan, ResultsAllC=pd.DataFrame(), c=np.nan,
                                                  Suffix='',
                                                  ResultsAll=pd.DataFrame(), j=np.nan, DependentType='', i=np.nan,
                                                  r=np.nan, rName='',
                                                  SourceFactorDf=pd.DataFrame(), ResultsFlag=np.nan,
                                                  ResultsRC=pd.DataFrame(), k=np.nan,
                                                  DoRollingWindowsMean=np.nan, FullTimeAvg=np.nan,
                                                  AllSubFactorListCreator=np.nan, DateList=[],
                                                  AnalyticName='', MonitorKey='', MonitorValue='')[7]
                                if not ResultsFlag:
                                    Dataall = \
                                        RegressorCalc(FactorGroup, FactorSet, FactorSubset, FactorSet_Setup,
                                                       FactorTradesDf.copy(), filepath,
                                                       SubParameters, Cash, filenameRaw, FactorType, FactorMultiplier,
                                                       RegressionFrequency, Dependent.copy(),
                                                       p, s, wReport, spike, MissingPercentRow, MissingPercentColumn,
                                                       ConsecutiveMissingColumns,
                                                       ConsecutiveMissingThreshold, MaxRows, w, window,
                                                       filenameGroupMap, xshift, AllSubFactorMapList,
                                                       AllSubFactorList, Minreturns, Datestartmax, DependentName,
                                                       OOSShift, Point, LamdaList,
                                                       RegimeThreshold, Direction, Absolute, doKPI, Regime,
                                                       ResultsAllC.copy(), DoRegression, c, DoMonitor,
                                                       Suffix, ResultsAll.copy(), j, DependentType, i, r, rName,
                                                       FactorsGroupMapPrefix, EntityRiskParity,
                                                       SourceFactorDf.copy(), AssetReturnsOrigP.copy(), ResultsFlag,
                                                       ResultsRC.copy(), k, FullTimeAvg=FullTimeAvg, DoFactorsorig=DoFactorsorig, FactorSetDict=FactorSetDict,
                                                      DoRollingWindowsMean=np.nan,
                                                       AllSubFactorListCreator=np.nan, DateList=DateList, AnalyticName='',
                                                       MonitorKey='', MonitorValue='', FactorMap=FactorMap)[6]

                                SummaryStatistics = ['Mean', 'Stdev', 'Variance', 'Mean/Stdev', 'Min', 'Max', 'Skewness', 'Kurtosis',
                                                     'Durbin-Watson']
                                SummaryEntities = ['Fund ']
                                if doKPI:
                                    SummaryEntities = \
                                        SummaryEntities + ['Style' + Suffix + ' ' , 'Predicted Style' + Suffix + ' ',
                                                           'Residual' + Suffix + ' ', 'Predicted Residual' + Suffix + ' ']
                                SummaryStatisticsEntities = [x + y for x in SummaryEntities for y in SummaryStatistics]
                                analytics2 = list(set(['Fund', 'Alpha', 'Alpha' + Suffix, 'Lamda', 'Horizons', 'Lags', 'FundOOS',
                                                       'StyleOOS', 'Style' + Suffix + 'OOS', 'ResidualOOS', 'Residual' + Suffix + 'OOS']))
                                analytics2 = list(set(['Fund', 'Alpha', 'Alpha' + Suffix, 'Lamda', 'Horizons', 'Lags']))
                                analytics3 = ['Beta']
                                if doKPI:
                                    analytics3 = analytics3 + ['Predicted Beta']
                                if DoTrading:
                                    analytics2 = \
                                        analytics2 + ['FixedTradeFeeReturn', 'NewTradeFee', 'NewTradeFeeReturn', 'TotalTradeFeeReturn']
                                    analytics3 = analytics3 + ['FixedTradeFeeReturn', 'NewTradeFee', 'MarketValue', 'MarketValueBH',
                                                               'MarketValueNewTrade', 'Trade']


                                ''' Rolling Windows Mean '''
                                # ResultsAllC = pd.read_csv(fileResultsAllC)
                                # ResultsRC = pd.read_csv(fileResultsRC)
                                # We do not populate average of average
                                DoRollingWindowsMeanLoop = True
                                FullTimeAvgLoop = False
                                AllSubFactorListCreator = False
                                DoFactorMap = False
                                if doKPI:
                                    analytics2 = analytics2 + \
                                            ['Beta Style' + Suffix, 'Beta Predicted Style' + Suffix] + \
                                            ['R2 Style' + Suffix, 'R2 Predicted Style' + Suffix] + \
                                            ['Correlation Style' + Suffix, 'Correlation Predicted Style' + Suffix]
                                # analytics2 = \
                                #     list(set(analytics2) - set(['FundOOS', 'StyleOOS', 'Style'  + Suffix + 'OOS',
                                #                                 'ResidualOOS', 'Residual' + Suffix + 'OOS']))
                                for t in range(len(list(analytics2))):
                                    analytic = analytics2[t]
                                    analyticcols = [col for col in ResultsRC.columns if col[:len(analytic)] == analytic \
                                                    and len(col) > len(analytic) and col[
                                                    len(analytic) + 1:len(analytic) + 2].isdigit() == True]
                                    Analytic = ResultsRC.loc[ResultsRC.index[k], analyticcols]
                                    col = analytic + ' Rolling Windows Mean'
                                    ResultsRC.loc[k, col] = Analytic.mean()
                                    ResultsAllC.loc[c, col] = Analytic.mean()
                                # Loop through all factors below
                                for t in range(len(list(analytics3))):
                                    AnalyticName = analytics3[t]  # Beta
                                    # ResultsRC, ResultsAllC = \
                                    # [RegressorLoop(FactorGroupList, FactorSetList, FactorGroupDict, FactorSubsetList, FactorTradesDf,
                                    #               filepath, SubParameters, Cash, filenameRaw, FactorType, FactorMultiplier,
                                    #               RegressionFrequency, Dependent, p, s, wReport, spike, MissingPercentRow,
                                    #               MissingPercentColumn, ConsecutiveMissingColumns, ConsecutiveMissingThreshold,
                                    #               MaxRows, w, window, filenameGroupMap, xshift, AllSubFactorMapList,
                                    #               AllSubFactorList, Minreturns, Datestartmax, DependentName, OOSShift, Point,
                                    #               LamdaList, RegimeThreshold, Direction, Absolute, doKPI, Regime, ResultsAllC,
                                    #               DoRegression, c, DoMonitor, Suffix, ResultsAll, j, DependentType, i, r, rName,
                                    #               FactorsGroupMapPrefix, EntityRiskParity, SourceFactorDf, AssetReturnsOrigP,
                                    #               ResultsFlag, ResultsRC, k, DoRollingWindowsMeanLoop, FullTimeAvgLoop,
                                    #               DoFactorsorig, AllSubFactorListCreator, FactorSet_Setup, FactorSetDict, DateList,
                                    #               AnalyticName, MonitorKey, MonitorValue, DoFactorMap)[l] for l in [5, 6]]
                                    ResultsRC, ResultsAllC = \
                                        [RegressorCalc(FactorGroup, FactorSet, FactorSubset, FactorSet_Setup, FactorTradesDf,
                                                  filepath, SubParameters, Cash, filenameRaw, FactorType, FactorMultiplier,
                                                  RegressionFrequency, Dependent, p, s, wReport, spike, MissingPercentRow,
                                                  MissingPercentColumn, ConsecutiveMissingColumns, ConsecutiveMissingThreshold,
                                                  MaxRows, w, window, filenameGroupMap, xshift, AllSubFactorMapList,
                                                  AllSubFactorList, Minreturns, Datestartmax, DependentName, OOSShift, Point,
                                                  LamdaList, RegimeThreshold, Direction, Absolute, doKPI, Regime, ResultsAllC,
                                                  DoRegression, c, DoMonitor, Suffix, ResultsAll, j, DependentType, i, r,
                                                  rName, FactorsGroupMapPrefix, EntityRiskParity, SourceFactorDf,
                                                  AssetReturnsOrigP, ResultsFlag, ResultsRC, k, DoRollingWindowsMeanLoop,
                                                  FullTimeAvgLoop, DoFactorsorig, FactorSetDict, AllSubFactorListCreator, DateList,
                                                  AnalyticName, MonitorKey, MonitorValue, FactorMap)[l] for l in [3, 4]]

                                ''' Average at each time point '''
                                if FullTimeAvg == True:
                                    DoRollingWindowsMeanLoop = False
                                    FullTimeAvgLoop = True
                                    analytics2 = []
                                    analytics3 = ['Beta']
                                    for s in range(1, Dependent.shape[0] + 1):
                                        DateWindow = Dependent.loc[s - 1, 'Date']
                                        for t in range(len(list(analytics2))):
                                            analytic = analytics2[t]
                                            if analytic[-3:] != 'OOS':
                                                col = analytic + ' ' + str(DateWindow)
                                                col2 = analytic + ' OV Windows Mean ' + str(DateWindow)
                                                # Only report time average if reported in ResultsRC, i.e. center point etc
                                                if col in ResultsRC:
                                                    if FullTimeAvg == True and np.isnan(ResultsRC.loc[k, col]) == False:
                                                        ResultsRC.loc[k, col2] = \
                                                        ResultsAllC[ResultsAllC[DependentType] == DependentName][col].mean()
                                                        ResultsAllC.loc[c, col2] = \
                                                        ResultsAllC[ResultsAllC[DependentType] == DependentName][col].mean()
                                        for t in range(len(list(analytics3))):
                                            AnalyticName = analytics3[t]  # Beta
                                            ResultsRC, ResultsAllC = \
                                                [RegressorLoop(FactorGroupList, FactorSetList, FactorGroupDict, FactorSubsetList,
                                                              FactorTradesDf, filepath, SubParameters, Cash, filenameRaw, FactorType,
                                                              FactorMultiplier, RegressionFrequency, Dependent, p, s, wReport, spike,
                                                              MissingPercentRow, MissingPercentColumn, ConsecutiveMissingColumns,
                                                              ConsecutiveMissingThreshold, MaxRows, w, window, filenameGroupMap,
                                                              xshift, AllSubFactorMapList, AllSubFactorList, Minreturns, Datestartmax,
                                                              DependentName, OOSShift, Point, LamdaList, RegimeThreshold, Direction,
                                                              Absolute, doKPI, Regime, ResultsAllC, DoRegression, c, DoMonitor, Suffix,
                                                              ResultsAll, j, DependentType, i, r, rName, FactorsGroupMapPrefix,
                                                              EntityRiskParity, SourceFactorDf, AssetReturnsOrigP,
                                                              ResultsFlag, ResultsRC, k, DoRollingWindowsMean, FullTimeAvg,
                                                              DoFactorsorig, AllSubFactorListCreator, DateList, AnalyticName,
                                                               MonitorKey, MonitorValue, DoFactorMap)[l]
                                                 for l in [5, 6]]
                                # # PR2 Smooth per horizon - we can only report for single window
                                # if doKPI == True:
                                #     for s in range(Parameters.get('rollhorstart', 1), Parameters.get('rollhorend', 8) + 1):
                                #         col = 'PR2 Smooth ' + str(s)
                                #         ResultsRC.loc[k, col] = ResultsAll.copy().loc[optimalrow, col]
                                #         ResultsAllC.loc[c, col] = ResultsAll.copy().loc[optimalrow, col]

                            ''' OOS Diagnostics '''

                            # R2, Correlation, Beta against Fund
                            SeriesListIn1 = list(set(['Style' + 'OOS'] + ['Style' + Suffix + 'OOS']))
                            # Summary stats for SeriesListIn1 + SeriesListIn2
                            SeriesListIn2 = list(set(['FundOOS', 'ResidualOOS', 'Residual' + Suffix + 'OOS']))
                            SeriesListOut = \
                                ['R2', 'Correlation', 'Beta', 'Smooth', 'Mean', 'Stdev', 'Variance', 'Mean/Stdev',
                                 'Min', 'Max', 'Skewness', 'Kurtosis', 'Durbin-Watson']

                            # Instead of Data below change to use all dates across fund and ALL factors.
                            # This will only be an issue if the last FactorSet tried does not have as many dates as the Fund (rare)
                            ResultsRC = DiagnosticResults(k, ResultsRC, k, ResultsRC, Dataall, SeriesListIn1, SeriesListIn2,
                                                          SeriesListOut, '')
                            ResultsAllC = DiagnosticResults(c, ResultsAllC, k, ResultsRC, Dataall, SeriesListIn1, SeriesListIn2,
                                                            SeriesListOut, '')
                            ''' Debug
                            iter1 = k
                            df0 = ResultsRC
                            iter2 = k
                            df = ResultsRC
                            Data = Dataall
                            DateWindow = ''
                            '''

                            # Add Fund, FundID, ParameterCaseID
                            ResultsAllC.loc[c, DependentType] = Dependent.columns[1]
                            ResultsAllC.loc[c, 'FundID'] = i + 1
                            ResultsAllC.loc[c, 'ParameterCaseID'] = np.nan
                            ResultsAllC.loc[c, 'SubParameterCaseID'] = np.nan
                            ResultsAllC.loc[c, 'Window'] = np.nan
                            ResultsAll.loc[j, 'DateWindow'] = np.nan
                            ResultsAllC.loc[c, 'DateWindowFrom'] = np.nan
                            ResultsAllC.loc[c, 'DateWindowTo'] = np.nan
                            ResultsAllC.loc[c, 'FactorGroup'] = np.nan
                            ResultsAllC.loc[c, 'FactorSet'] = np.nan
                            ResultsAllC.loc[c, 'Lamda'] = np.nan
                            ResultsAllC.loc[c, 'Horizons'] = np.nan
                            ResultsAllC.loc[c, 'Lags'] = np.nan
                            ResultsAllC.loc[c, 'Reshuffle'] = np.nan
                            c = c + 1

                        if ResultsCount > 0 :
                            k = k + 1

                FundMissingLogDf.to_excel(writerMissingFundLog, sheet_name=DependentName[:30], index=False)
                FactorMissingLogDf.to_excel(writerMissingFactorLog, sheet_name=DependentName[:30], index=False)

    writerMissingFundLog.save()
    writerMissingFactorLog.save()

    # ResultsAll = pd.read_csv(fileResultsAll)

##############################
#  Edit, Save, Open results  #
##############################

if ResultsCountAll > 0:

    DateList = Dataall['Date'].astype('str').tolist()
    PersistList = ['R2 ', 'Correlation', 'Beta Style']
    Columns1ListOut = DateList + ['Smooth', 'Residual Unsmooth', 'Residual' + Suffix + ' Unsmooth']
    Columns2ListOut = ['Predicted']
    Columns3ListOut = ['Fund ', 'Residual ', 'Residual' + Suffix + ' ', 'Style ', 'Style' + Suffix + ' ']
    Columns4ListOut = ['FundOOS', 'StyleOOS ', 'StyleOOS' + Suffix + ' ', 'MarketValueBH']
    PriorityList = ['ParameterCaseID', 'SubParameterCaseID'] + [DependentType] + \
                   ['FundID', 'Window', 'DateWindow', 'DateWindowFrom', 'DateWindowTo', 'FactorGroup', 'FactorSet',
                    'FactorSubset', 'Lamda', 'Horizons', 'Lags']
    Columns0, Columns1, Columns2, Columns3, Columns4, AllColumns = \
        ColumnPriority(ResultsAllC, Columns1ListOut, Columns2ListOut, Columns3ListOut, Columns4ListOut, PriorityList,
                       PersistList)
    subset = list(set(Columns4) - set(['ParameterCaseID', 'SubParameterCaseID'] + [DependentType] +
                                      ['FundID', 'Lamda', 'Horizons', 'Lags']))

    # Summary Results
    ResultsAllCSummary = pd.DataFrame()
    for kk in range(k):
        Temp = ResultsAllC.loc[[int((ResultsAllC.index[-1] + 1) /(kk + 1) - 1)], Columns4].dropna(axis=1)
        if ResultsAllCSummary.empty:
            ResultsAllCSummary = Temp.reset_index(drop=True)
        else:
            ResultsAllCSummary.loc[kk, Temp.columns] = Temp.values[0]
    # Sorted and Transposed Results
    ResultsAllCSummary = ResultsAllCSummary.T.reset_index()
    ResultsAllSortedT=ResultsAll[ColumnPriority(ResultsAll, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                                Columns4ListOut, PriorityList, PersistList)[5]].T
    ResultsAllCSortedT=ResultsAllC[AllColumns].T
    ResultsRCSortedT=ResultsRC[ColumnPriority(ResultsRC, Columns1ListOut, Columns2ListOut, Columns3ListOut,
                                              Columns4ListOut, PriorityList, PersistList)[5]].T

    universe = ''
    # Save all files
    files = ['ResultsAllCSummary', 'ResultsAllSortedT', 'ResultsAllCSortedT', 'ResultsRCSortedT']
    for f in range(0, len(files)):
        filename = 'Fund_' + str(i) + '-ParameterCaseList_' + str(ParameterCaseList)[1:-1].replace(', ', '_').replace("'", '') + \
                   '-FactorSubsetList_' + str(FactorSubsetList)[1:-1].replace(', ', '_').replace("'", '') + \
                   '-' + Modelvalue + '-' + frequency + '-Roll_' + str(round(DoRolling)) + '-W_' + str(window)
        filename = filename + '_' + '_' + files[f] + OutputSuffix + '.csv'
        file = filepathOut + '\\' + filename
        globals()[files[f]].to_csv(file, header=False)
    filename = 'Parameters-' + filename + '_' + universe + '_' + files[f] + '.csv'
    file = filepathOut + '\\' + filename
    ParametersDf.to_csv(file, header=False)
    # filename = 'Reshuffle ' + str(MC) + ' ' + str(NReshuffle) + '.csv'
    # file = filepathOut + '\\' + filename
    # ResultsAllReshuffleT.to_csv(file, header=False)
    # =============================================================================
    #         savefile(files[f],filepath,filename,universe)
    # =============================================================================

    # ResultsAlltemp = ResultsAll.replace(np.nan,-999)
    # savetosql(ResultsAlltemp, 'ResultsAll', 'replace')

DoDisplay = False

if DoDisplay:
    '' 'Display results '''

    files = ['ResultsAllCSortedT']
    f=0
    filename = 'Fund_' + str(i) + '-ParameterCaseList_' + str(ParameterCaseList)[1:-1].replace(', ', '_').replace("'", '') + \
               '-SubParameterCaseList_' + str(SubParameterCaseList)[1:-1].replace(', ', '_').replace("'", '') + \
               '-FactorSubsetList_' + str(FactorSubsetList)[1:-1].replace(', ', '_').replace("'", '') + \
               '-' + Modelvalue + '-' + frequency + '-Roll_' + str(round(DoRolling)) + '-W_' + str(window)
    filename = filename + '_' + '_' + files[f] + '.csv'
    file = filepathOut + '\\' + filename
    ResultsAllCSortedT = pd.read_csv(file, header=None).T
    ResultsAllCSortedT.columns = ResultsAllCSortedT.loc[0, :]
    ResultsAllCSortedT = ResultsAllCSortedT.loc[1:, :].reset_index(drop=True)
    Fund = 'AQR Global Risk Balanced Portfolio'
    AnalyticList2 = ['NewTradeFee', 'NewTradeFeeReturn', 'FixedTradeFeeReturn', 'TotalTradeFeeReturn']
    AnalyticList2Multiplier = [1, 10000, 10000, 10000]
    AnalyticList3 = ['Beta', 'MarketValue']
    AllFactorsList = \
        [x for x in ResultsAllCSortedT.columns if all(sub in x for sub in ['Beta','Mean']) and 'Rolling' not in x]
    for date in ResultsAllCSortedT['DateWindow'].dropna():
        AllFactorsList=[x.replace(date, '') for x in AllFactorsList]
    AllFactorsList = [x.replace('Beta', '') for x in AllFactorsList]
    AllFactorsList = [x.replace('Mean', '') for x in AllFactorsList]
    AllFactorsList = [x.replace(' ', '') for x in AllFactorsList]
    AllFactorsList = list(set(AllFactorsList))

    # for Fund in ResultsAllCSortedT['Fund'].unique
    Results = ResultsAllCSortedT[ResultsAllCSortedT['Fund']==Fund][['DateWindow']]
    for j, Analytic in enumerate(AnalyticList2):
        for i in range(ResultsAllCSortedT.shape[0] - 1):
            DateWindow = ResultsAllCSortedT.loc[i, 'DateWindow']
            ResultsAllCSortedT.loc[i, Analytic] = AnalyticList2Multiplier[j] * \
                                                  ResultsAllCSortedT.loc[i, Analytic + ' ' + DateWindow]
    for Analytic in AnalyticList3:
        for Factor in AllFactorsList:
            for i in range(ResultsAllCSortedT.shape[0] - 1):
                # Aggregate factors don't have marketvalues
                if Analytic + ' ' + Factor + ' ' + DateWindow in ResultsAllCSortedT.columns:
                    DateWindow = ResultsAllCSortedT.loc[i, 'DateWindow']
                    ResultsAllCSortedT.loc[i, Analytic + ' ' + Factor] = \
                        ResultsAllCSortedT.loc[i, Analytic + ' ' + Factor + ' ' + DateWindow]

    # ResultsAllCSortedT = \
    #     pd.concat([ResultsAllCSortedT['DateWindow'],
    #               ResultsAllCSortedT.drop('DateWindow', axis=1).
    #               reindex(sorted(ResultsAllCSortedT.drop('DateWindow', axis=1).columns), axis=1)], \
    #     axis=1)

    ResultsAllCSortedT[ResultsAllCSortedT['Fund']==Fund] \
    [['DateWindow'] + sorted(AnalyticList2, key=str.lower) + \
    sorted(list(set([x + ' ' + Factor for x in AnalyticList3 for Factor in AllFactorsList])
                & set(ResultsAllCSortedT.columns)), key=str.lower)] \
        .to_clipboard(index=False)
