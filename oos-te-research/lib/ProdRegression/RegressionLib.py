import pandas as pd
import numpy as np
import pyodbc
from datetime import datetime
from dateutil.relativedelta import relativedelta
import calendar
from sklearn.linear_model import LinearRegression
import os
from scipy.optimize import minimize, SR1
import sys



# Define Constants
Cs = {
    'PRICE_DATE': 'PRICE_DATE',
    'EFFECTIVE_DATE': 'EFFECTIVE_DATETIME',
    'EFA_CLASS_ID': 'EFA_CLASS_ID',
    'EFA_FUND_ID' : 'EFA_FUND_ID',
    'UID': 'PORTFOLIO_UID',
    'FUND_CATEGORY' : 'FUND_MANAGEMENT_CLASSIFICATION',
    'OTHER_FUND_INFO' : ['PORTFOLIO_NAME', 'PRIMARY_PROSPECTUS_BENCHMARK'],
    'OTHER_FUND_INFO_TYPE': ['STRING', 'STRING'],
    'STAT' : ['N_OBS', 'N_VAR', 'RSQ', 'TRACKING_ERROR_VOL', 'TRACKING_ERROR_DOLLARS', 'AUM'],
    'BETA1_CASH' : 'MONEY',
    'DY'  : 'NAV_TR',
    'DX_GAAP' : ['SANDP500_PCT','RUSSELL2000_PCT','NASDAQ_PCT','SBBIG_PCT','EAFE_PCT'],
    'R_GAAP'  : 'MM_PCT',
    'DX_STAT' : ['US', 'SMALL', 'LTCORP', 'INT', 'INTGOV'],
    'R_STAT'  : 'MONEY',
    'DX_OTHER': ['IBOXHY', 'RU10GRTR', 'RU10VATR', 'XNDX'],
    'CLASS_BALANCE' : 'BALANCE_END',
    'FUND_BALANCE'  : 'FUND_BALANCE_END',
    'FUND_GROUP'    : 'FUND_GROUP',
    'IS_PRIMARY_EFA_CLASS_ID': 'IS_PRIMARY_EFA_CLASS_ID',
    'IS_RMGMP' : 'IS_RMGMP',
    'NAV_DATE_END' : 'NAV_DATE_END',
    'LOWERIQR' : 0.25,
    'UPPERIQR' : 0.75,
    'IQRTHRESHOLD' : 15,
    'PORTFOLIO_NAME' : 'PORTFOLIO_NAME',
    'CONFIG_ID'      :  'CALIBRATION_CODE',
    'PROD_CALIBRATION_CODE' : 'LEGACY_CLSQ',
    'OLSQ_OPTIMAL' : 'OLSQ_OPTIMAL'
}

RegressParams = {
    'IQR_THRESHOLD' :  15,
    'HALF_LIFE' : 2,  # years
    'MINIMUM_OBSERVATIONS_REQUIRED' : 85,  #250
    'LAMBDA': np.log(2.0)/2, # 5 year %/1.0; % 1 year half-life
    'PROD_CALIBRATION_CODE': 'LEGACY_CLSQ',
    'CALIBRATION_CODE': [ 'OLSQ', 'LEGACY_CLSQ' ],
    'CONFIG_ID_COL' :   'calibration_code',
    'ROUNDINGDIGITS': 6
}

class ALMRODDB:
   __instance = None
   @staticmethod
   def Conn():
      """ Static access method. """
      if ALMRODDB.__instance == None:
         ALMRODDB()
      return ALMRODDB.__instance
   def __init__(self):
      """ Virtually private constructor. """
      if ALMRODDB.__instance != None:
         raise Exception("This class is a singleton!")
      else:
         ALMRODDB.__instance = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                                                 'Server=use01azvasqlbhf003.database.windows.net;'
                                                 'PORT=1433;'
                                                 'Database=alm;'
                                                 'Trusted_Connection=yes;')

def calc_daily_total_return(inputData):
    '''
    daily total return: daily return + dividend
    daily total return: daily return + dividend + fee


    '''
    inputData.columns = inputData.columns.str.upper()
    inputData['PRICE_DATE'] = pd.to_datetime(inputData['PRICE_DATE'], format='%Y-%m-%d')
    # Remove the empty lines
    df1 = inputData[inputData['NAV'].notna()].copy()
    df1 = df1.fillna(0)
    abc = inputData.dtypes
    idList = inputData['EFA_CLASS_ID'].unique()
    df1['EFA_CLASS_ID'] = df1['EFA_CLASS_ID'].astype(int)


    # Calculate daily net total return
    df1['NAV_M1'] = df1['NAV'].shift(1)
    df1['N_LN_NET_TR_PERIOD'] = np.log((df1['NAV'] + df1['DIV']) / df1['NAV_M1'])
    df1['N_LN_NET_TR_PERIOD'].iloc[0] = 0

    # Calculate daily gross total return, add the expense back to net return
    # df1['TER_M1'] = df1['TER'].shift(1)
    df1['TER_M1'] = df1['TER']   # JD question: fee should come from T-1?
    df1['PRICE_DATE_M1'] = df1['PRICE_DATE'].shift(1).copy()
    df1['N_LN_GROSS_TR_PERIOD'] = df1['N_LN_NET_TR_PERIOD'] - np.log(1 - df1['TER']/100 * (df1['PRICE_DATE'] - df1['PRICE_DATE_M1']).dt.days/365)
    df1['N_LN_GROSS_TR_PERIOD'].iloc[0] = 0

    df_test = df1.loc[df1['EFA_CLASS_ID'] == 2023]
    df_test.to_csv(r"C:\Users\jxu\OneDrive - Brighthouse Financial\VA ALM\Projects\Fund Mapping\Data\Output\df_test.csv")
    # JD question: daily return is enough, why do we need to recalculate the level?

    return None

def yearFrac(date1, date2, basis, targetSystem = 'MATLAB'):
    # basis:
    #   0: actual/actual
    #      MatLab:
    #        For the actual / actual basis, the fraction of a year is calculated as:
    #        (Actual Days between Start Date and End Date) / (Actual Days between Start Date and exactly one year after Start Date)

    year1 = date1.year
    year2 = date2.year
    yFrac = 0

    if basis == 0:
        if year1 > year2:
           # Assume year1 <= year2
           return np.NAN
        else:
            if targetSystem == 'MATLAB':
                 days = (date2 - date1).days
                 dateOneYear = date1 + relativedelta(years = 1)
                 # add extra day if the start date is the leap day
                 if calendar.isleap(year1) and date1.month == 2 and date1.day == 29:
                     dateOneYear = dateOneYear + relativedelta(days = 1)

                 daysInOneYear = (dateOneYear - date1).days
                 yFrac = days / daysInOneYear
            else:
               if year1 == year2:
                  days = (date2 - date1).days
                  daysInOneYear = (datetime(year1 + 1, 1, 1) - datetime(year1, 1, 1)).days
                  yFrac = days / daysInOneYear
               else:
                  days1 = (datetime(year1, 12, 31) - date1).days
                  days2 = (date2 - datetime(year2 - 1, 12, 31)).days
                  yFrac1 = days1 / (datetime(year1 + 1, 1, 1) - datetime(year1, 1, 1)).days
                  yFrac2 = days2 / (datetime(year2 + 1, 1, 1) - datetime(year2, 1, 1)).days
                  yFrac = yFrac1 + yFrac2 + (year2 - year1 - 1)

    return yFrac


def xlnda(df, date_col, r_col, dx_col):
    dfx = df.copy()

    # Sort by descending from recent date
    dfx[date_col] = pd.to_datetime(dfx[date_col], format='%Y-%m-%d')
    dfx.sort_values( date_col, ascending=False, inplace=True)
    dfx_prior = dfx.shift(-1)
    # Calculate risk free return
    dfx_prior[r_col] = np.log(dfx[r_col]/dfx_prior[r_col])
    # Calculate excess return
    dfx_prior[dx_col] = np.log(dfx[dx_col] / dfx_prior[dx_col])
    ## Add in new BHF60 returns here
    dfx_prior[dx_col] = dfx_prior[dx_col].sub(dfx_prior[r_col], axis = 0)

    # Rename column name
    dfx_prior = dfx_prior.rename(columns = {date_col: date_col + '1'})
    dfx_prior[date_col + '2'] = dfx[date_col]

    #Drop the last row
    dfx_prior = dfx_prior[:-1]

    # Current date
    curDate = dfx[date_col].iloc[0]
    dfx_prior['T'] = dfx_prior.apply(lambda x : yearFrac(x[date_col + '2'], curDate, 0), axis  = 1)
    dfx_prior['DT'] = dfx_prior.apply(lambda x : yearFrac(x[date_col + '1'], x[date_col + '2'], 0), axis  = 1)

    return dfx_prior

def getDBData(sqlQuery, fileName = ""):
    if fileName:
       rel = pd.read_csv(fileName)
    else:
       rel = pd.read_sql_query(sqlQuery, ALMRODDB.Conn())

    rel.columns = rel.columns.str.upper()

    return rel


def getClassAttr(startDate, endDate,inputFiles, auditSample):
    classAttrFile = inputFiles['CLASSATTR'] if 'CLASSATTR' in inputFiles else ''

    # format sql query for index prices
    sqlQuery = "SELECT m.* FROM dbo.[reg_get_class_summary_DEV]('{0}', '{1}') m".format(startDate, endDate)

    classAttrData = getDBData(sqlQuery, classAttrFile)
    classAttrData[Cs['UID']] = classAttrData[Cs['UID']].astype(str)
    # Filter by audit sample
    if auditSample:
       classAttrData = classAttrData[classAttrData[Cs['UID']].isin(auditSample)]

    # Put one penny in classes without an ending balance to avoid DIV0 errors
    classAttrData[Cs['CLASS_BALANCE']].fillna(0.01, inplace=True)

    # Recompute total fund balance for each fund group
    classAttrData[Cs['FUND_BALANCE']] = classAttrData.groupby(Cs['UID'])[Cs['CLASS_BALANCE']].transform('sum')

    # Fill the remaining NaN with 0
    classAttrData.fillna(0, inplace=True)

    # Filter out only the rows for primary class
    logic_cols = [col for col in classAttrData.columns if 'IS_' in col]
    classAttrData[logic_cols] = classAttrData[logic_cols].astype('bool')
    id_cols = [Cs['EFA_CLASS_ID'], Cs['EFA_FUND_ID']]
    classAttrData[id_cols] = classAttrData[id_cols].astype('int32')

    return classAttrData

def getIndexPrice(startDate, endDate, list1, list2, inputFiles):
    indexFile = inputFiles['INDEXPRICE'] if 'INDEXPRICE' in inputFiles else ''

    # format sql query for index prices
    part1 = 'i.' + Cs['PRICE_DATE'] + ','
    for l in list1:
        part1 += 'i.' + l
        if l != list1[-1]:
           part1 += ','
        elif list2:
           part1 += ','

    for l in list2:
        part1 += 'ii.' + l
        if l != list2[-1]:
           part1 += ','

    sqlQuery = "select {0} from [dbo].[reg_AAA_index_hist_view] i LEFT JOIN reg_index_pvt_view ii " \
               "on i.{1} = ii.{1} where i.{1} >= '{2}' and i.{1} <= '{3}'".format(part1, Cs['PRICE_DATE'], startDate, endDate)
    indexPrice = getDBData(sqlQuery, indexFile)
    # Need to check whether all the requested index data are available
    indexPrice[Cs['PRICE_DATE']] = pd.to_datetime(indexPrice[Cs['PRICE_DATE']], format='%Y-%m-%d')

    # Drop all the rows with NaN
    indexPrice.dropna(inplace=True)

    return indexPrice

def getClassPrice(startDate, endDate, classList, inputFiles):
    classFile = inputFiles['CLASSPRICE'] if 'CLASSPRICE' in inputFiles else ''

    part1 = ''
    for l in classList:
        part1 += "'" + str(l) +"'"
        if l != classList[-1]:
           part1 += ","

    sqlQuery = "select i.{0}, i.{1}, i.NAV_GROSS_TR as NAV_TR " \
               "from [dbo].[efa_class_total_return_view] i " \
               "where {1} in ({2}) and " \
               "i.{0} >= '{3}' and i.{0} <= '{4}'".format(Cs['PRICE_DATE'], Cs['EFA_CLASS_ID'], part1, startDate, endDate)

    classPrice = getDBData(sqlQuery, classFile)

    return classPrice

def sumf(x, DYY, DYY_P):

    if len(DYY) != len(DYY_P):
       return 'Error!'
    else:
       sum1 = 0
       sum2 = 0
       for i in range(0, len(DYY)):
           sum1 += x[DYY[i]] * x[DYY_P[i]]
           sum2 += x[DYY_P[i]]


       return sum1 / sum2

def runIQRTest(df, cols, lowerQ = 0.25, upperQ = 0.75, distance = 1):
    # Native Python percentile function does not work the same as that in Matlab
    # lq = df[cols].quantile(lowerQ)
    # uq = df[cols].quantile(upperQ)

    lq = df[cols].apply(lambda x : matlab_percentile(x, lowerQ * 100), axis = 0)
    uq = df[cols].apply(lambda x : matlab_percentile(x, upperQ * 100), axis = 0)

    LB = lq - distance * (uq - lq)
    UB = uq + distance * (uq - lq)

    LB = LB.to_frame().T
    UB = UB.to_frame().T

    LB = pd.DataFrame(np.repeat(LB.values,df.shape[0],axis=0), columns=cols)
    UB = pd.DataFrame(np.repeat(UB.values, df.shape[0], axis=0), columns=cols)

    filterIQR = (df[cols] >= LB) & (df[cols] <= UB) | ( df[cols].isna() )
    filterIQR_ALL = filterIQR.all( axis='columns' )

    return filterIQR_ALL

def getClassReturn(classPrice, classAttrData, indexPrice, dict_info):

    classPrice = classPrice[classPrice[Cs['EFA_CLASS_ID']].notna()]
    classPrice[Cs['PRICE_DATE']] = pd.to_datetime(classPrice[Cs['PRICE_DATE']])#, format='%Y-%m-%d')

    classAttrData[Cs['EFA_CLASS_ID']] = classAttrData[Cs['EFA_CLASS_ID']].astype('int32')
    classAttrData[Cs['NAV_DATE_END']] = pd.to_datetime(classAttrData[Cs['NAV_DATE_END']], format='%Y-%m-%d')

    # Add more attributes
    ca_cols = [Cs['EFA_CLASS_ID'], Cs['CLASS_BALANCE'], Cs['FUND_BALANCE'], Cs['NAV_DATE_END']]

    priceTable = classPrice.copy()
    priceTable = priceTable.merge(classAttrData[ca_cols], on=Cs['EFA_CLASS_ID'], how='left')

    testDtype = priceTable.dtypes
    lastValue = priceTable[ priceTable[Cs['NAV_DATE_END']] == priceTable['PRICE_DATE'] ]
    lastValue = lastValue[[Cs['EFA_CLASS_ID'], Cs['DY']]]
    lastValue = lastValue.rename(columns={Cs['DY']: 'ENDING_Y'})

    priceTable = priceTable.merge(lastValue, on=Cs['EFA_CLASS_ID'], how='left')
    priceTable[Cs['DY']] = 100 * priceTable[Cs['DY']] / priceTable['ENDING_Y'] * priceTable[Cs['CLASS_BALANCE']]/priceTable[Cs['FUND_BALANCE']]  #JD why normalized by fund balance, nothing left

    priceTable = pd.pivot_table(priceTable, values = Cs['DY'], index = Cs['PRICE_DATE'],
                         columns =['EFA_CLASS_ID'], aggfunc = np.sum)
    #priceTable.dropna(inplace=True)
    priceTable.columns = priceTable.columns.astype(str)
    # Combined with index price
    DYY = list(priceTable)
    priceTable = pd.DataFrame(priceTable.to_records())
    indexPriceNoNA = indexPrice.dropna()
    priceTable = priceTable.merge(indexPriceNoNA, on = Cs['PRICE_DATE'], how='inner')

    priceTable.sort_values( Cs['PRICE_DATE'], ascending=False, inplace=True)


    priceTable.reset_index(inplace=True, drop=True)

    if priceTable.shape[0] < RegressParams['MINIMUM_OBSERVATIONS_REQUIRED']:
       errorMsg = "Insufficient data - n_Obs={0},   {1}!!".format(priceTable.shape[0], "FundInfo") #Need to add more in here.
       raise ValueError(errorMsg)

    classReturn = xlnda(priceTable, Cs['PRICE_DATE'], dict_info['DR'], dict_info['DX']  + DYY )

    filter1 = runIQRTest(classReturn, DYY, Cs['LOWERIQR'], Cs['UPPERIQR'], Cs['IQRTHRESHOLD'])

    # Convert class return to fund return

    priceTableSub = priceTable[[Cs['PRICE_DATE']] + DYY]
    DYY_P = [x + '_N' for x in DYY]
    priceTableSub.columns = [Cs['PRICE_DATE']] + DYY_P

    classReturn = classReturn.merge(priceTableSub, left_on = Cs['PRICE_DATE'] + '1', right_on = Cs['PRICE_DATE'] , how = 'left')

    classReturn[DYY + DYY_P] = classReturn[DYY + DYY_P].fillna(value=0)
    classReturn[Cs['DY']] = classReturn.apply(lambda x: sumf(x, DYY, DYY_P), axis = 1)
    classReturn.drop([Cs['PRICE_DATE']] + DYY_P + DYY, axis = 1, inplace=True)
    filter2 = runIQRTest(classReturn, dict_info['DX'] + [Cs['DY']], Cs['LOWERIQR'], Cs['UPPERIQR'], Cs['IQRTHRESHOLD'])

    classReturn = classReturn[filter1 & filter2]

    # Calculate weights
    classReturn['WEIGHT'] = np.power(0.5, classReturn['T'] / RegressParams['HALF_LIFE'])
    classReturn['WEIGHT'] = classReturn['WEIGHT']  / classReturn['WEIGHT'].sum() * classReturn.shape[0]

    classReturn.reset_index(inplace=True, drop=True)

    # Multiply by weight?
    dx_col1 = ['DT', dict_info['DR']] + dict_info['DX']
    dy_col1 = [Cs['DY']]
    classReturn_w = classReturn[ dx_col1 +  dy_col1 ].mul(np.sqrt(classReturn['WEIGHT']), axis=0)


    dict_classReturn = {
        'DATA': classReturn_w,
        'DX':  ['DT'] + dict_info['DX'],
        'DY': dy_col1,
        'RAWDATA' : classReturn
    }

    return dict_classReturn

def h(k, c, n):
    return  np.power(n, c) * (np.power(np.log(n), k/(k+1)) - 1)

def getLRStats(xdata, ydata, coef, weight, linear=True):
    lrStats = {}

    #coefs = pd.DataFrame(np.repeat(coef, ydata.shape[0],axis=0), columns=xdata.columns)

    y_calc = xdata.mul(coef)
    y_predict = pd.DataFrame(data=y_calc.sum(axis = 1), columns = ydata.columns)
    y_mean = ydata.div(np.sqrt(weight), axis=0)
    y_mean = np.average(y_mean, weights=weight, axis=0)

    y_mean = np.sqrt(weight) * y_mean[0]
    y_mean = pd.DataFrame( y_mean )
    y_mean.columns = [Cs['DY']]

    sse = ((ydata - y_predict).pow(2)).to_numpy().sum()  # Rounding errors cause difference with Excel
    ssr = ((y_predict- y_mean).pow(2)).to_numpy().sum()

    sst = sse + ssr
    rsqLinear = ssr / sst

    # rsqNonLinear
    # sst_nonlinear = ((ydata - y_mean).pow(2)).to_numpy().sum()
    sst_nonlinear = ((ydata -0).pow(2)).to_numpy().sum()
    rsqNonLinear = 1 - sse / sst_nonlinear

    rsq = rsqLinear if linear else rsqNonLinear

    dof = xdata.shape[1]
    n = ydata.shape[0]

    mse = sse/(n - dof)
    rmse = np.sqrt(mse)

    se_coef = np.sqrt(mse * np.diagonal(np.linalg.inv((np.dot(xdata.T,xdata)))))
    tStat = (coef / se_coef).tolist()
    #tStat = pd.DataFrame(data = tStat)
    tStat = dict(zip(list(xdata), tStat))

    lrStats['mse'] = mse
    lrStats['rmse'] = np.sqrt(mse)
    lrStats['sse'] = sse
    lrStats['sst'] = sst
    lrStats['tStat'] = tStat
    lrStats['rsquared'] = rsq

    return lrStats

def fundMappingRegression(startDate, endDate, dataSource, isGAAP, isCompareToProd, isOutputResiduals, isOutputSourceData, outputPath, dx_other, auditSample=[], inputFiles = {}):
    dx_primary_col = Cs['DX_GAAP']  if isGAAP else Cs['DX_STAT']
    dx_other_col = dx_other
    dx_col = dx_primary_col + dx_other_col
    dr_col = Cs['R_GAAP'] if isGAAP else Cs['R_STAT']

    # "10" is the GAAP result. "9" is STAT
    prod_fund_regression_set_id = 10 if isGAAP else 9

    dict_info = {
        'DX': dx_col,
        'DR': dr_col,
    }

    start = datetime.now()
    # index data to be regressed on
    indexPrice = getIndexPrice(startDate, endDate, [dr_col] + dx_primary_col, dx_other_col, inputFiles)
    # Convert price levels to return and excess returns
    # JD no need for this
    # indexReturn = xlnda(indexPrice,Cs['PRICE_DATE'], dr_col, dx_col)
    if not inputFiles:
       indexPrice.to_csv(outputPath + r'\\indexprice.csv', index=False )

    # fund class attributes including loan balance
    classAttrData = getClassAttr(startDate, endDate,inputFiles, auditSample)
    classAttrData.sort_values([Cs['IS_RMGMP'],Cs['FUND_BALANCE']], ascending=False, inplace=True)
    if not inputFiles:
       classAttrData.to_csv(outputPath + r'\\classAttrData.csv', index=False)
    # Only keep the primary class as fund reference
    fundAttrData = classAttrData[classAttrData[Cs['IS_PRIMARY_EFA_CLASS_ID'] ]]
    fund_uids = fundAttrData[Cs['UID']].unique().tolist()
    classList = classAttrData[Cs['EFA_CLASS_ID']].unique().tolist()

    classPriceData_all = getClassPrice(startDate, endDate, classList, inputFiles)
    classPriceData_all = classPriceData_all[classPriceData_all[Cs['EFA_CLASS_ID']].notna()]
    if not inputFiles:
       classPriceData_all.to_csv(outputPath + r'\\classPrice.csv', index=False)
    classPriceData_all[Cs['EFA_CLASS_ID']] = classPriceData_all[Cs['EFA_CLASS_ID']].astype('int')

    end = datetime.now() - start
    print("It takes {0} to query all the data.".format(end))
    # Save all the calibration results
    resultsByFund = []
    # Save OLSQ_OPTIMAL in a separate dicitonary as the datatype is complex
    dict_olsq_optimal = {}
    # Save all the residual calculation
    residTable = []

    effective_date = endDate
    compute_date_time = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    dtstr = effective_date + "_" + compute_date_time
    outFullPath = outputPath + r'\\' + dtstr + r'\\'
    os.makedirs(outFullPath, exist_ok=True)

    # Loop through each fund and the corresponding class

    for fund_uid in ['90753E']: #fund_uids:

        uID = fund_uid
        classidx = classAttrData[Cs['UID']] == fund_uid
        efa_fund_id =  fundAttrData.loc[fundAttrData[Cs['UID']] == fund_uid, Cs['EFA_FUND_ID']].tolist()
        efa_fund_id = efa_fund_id[0] if len(efa_fund_id) == 1 else np.NaN

        fundName =  fundAttrData.loc[fundAttrData[Cs['UID']] == fund_uid, Cs['PORTFOLIO_NAME']].tolist()
        fundName = fundName[0] if len(fundName) == 1 else np.NaN
        fundInfo = str(uID) + ' - ' + str(fundName)

        try:
            class_ids = classAttrData.loc[classidx , Cs['EFA_CLASS_ID']].tolist()
            classPriceData = classPriceData_all[classPriceData_all[Cs['EFA_CLASS_ID']].isin(class_ids)]

            if classPriceData.empty:
               errorMsg = "No data available for {0}.".format(fundInfo)  # Need to add more in here.
               raise ValueError(errorMsg)
            elif classPriceData.shape[0] < RegressParams['MINIMUM_OBSERVATIONS_REQUIRED']:
               errorMsg = "Insufficient data - n_Obs={0},   {1}!!".format(classPriceData.shape[0], fundInfo)
               raise ValueError(errorMsg)

            classReturn_w = getClassReturn(classPriceData, classAttrData, indexPrice, dict_info)
            classReturn_raw = classReturn_w['RAWDATA']

            # Unconstrained OLSQ
            # solve unconstrained OLSQ linear regression to establish sort order
            # 'Intercept' is false because I have included non - stochastic 'dt',
            # the constant term for in continuous model, as a predictor variable.

            xdata_all = classReturn_w['DATA'][classReturn_w['DX']]
            xdata = xdata_all.copy()
            ydata = classReturn_w['DATA'][classReturn_w['DY']]
            olsq = LinearRegression(fit_intercept=False).fit(xdata , ydata)
            olsq_coef = olsq.coef_.tolist()[0]

            n = ydata.shape[0]
            m = xdata.shape[1]
            c = np.log(m) / np.log(n)

            stats = getLRStats(xdata, ydata, olsq_coef, classReturn_raw['WEIGHT'], True)
            mse_m = stats['mse']
            sse_k = stats['sse']
            tStat = stats['tStat']


            k = m
            list_zl = []
            dict_zl = {}
            dict1_k = {}

            dict1_k['k'] = k
            dict1_k['phi_s'] = sse_k + h(k, (c + 1)/2, n) * mse_m
            dict1_k['phi']   = sse_k + h(k, c + (1-c)/4, n) * mse_m
            dict1_k['phi_w'] = sse_k + h(k, c, n) * mse_m
            list_zl.append(dict1_k)

            dict2_k = {}
            dict2_k['stats'] = stats
            dict2_k['olsq'] = olsq
            dict2_k['coefs'] = dict(zip(list(xdata), olsq_coef))
            dict2_k['predictor_COL'] = tStat.items()
            dict_zl[k] = dict2_k

            for k in range(m-1, 0, -1):
                wald = sorted(tStat.items(), key = lambda x:abs(x[1]), reverse = True)
                predictor_COL = [x[0] for x in wald]
                predictor_COL = predictor_COL[ 0 : k ]

                xdata = xdata_all[predictor_COL].copy()
                olsq = LinearRegression(fit_intercept=False).fit(xdata, ydata)
                olsq_coef = olsq.coef_.tolist()[0]

                stats = getLRStats(xdata, ydata, olsq_coef, classReturn_raw['WEIGHT'], True)
                sse_k = stats['sse']
                tStat = stats['tStat']

                dict1_k = {}
                dict1_k['k'] = k
                dict1_k['phi_s'] = sse_k + h(k, (c + 1) / 2, n) * mse_m
                dict1_k['phi']   = sse_k + h(k, 1, n) * mse_m
                dict1_k['phi_w'] = sse_k + h(k, c, n) * mse_m
                list_zl.append(dict1_k)

                dict2_k = {}
                dict2_k['predictor_COL'] = predictor_COL
                dict2_k['stats'] = stats.copy()
                dict2_k['olsq'] = olsq
                dict2_k['coefs'] = dict(zip(list(xdata), olsq_coef))
                dict_zl[k] = dict2_k

            df_zl = pd.DataFrame(list_zl)
            df_zl.set_index('k', inplace=True)
            df_zl = (df_zl - df_zl.min()) / (df_zl.max() - df_zl.min())

            khat = df_zl['phi_s'].idxmin()
            olsq_optimal = dict_zl[khat]['olsq']
            olsq_coef_optimal = dict_zl[khat]['coefs']
            predictor_COL = dict_zl[khat]['predictor_COL']

            numObservations = ydata.shape[0]
            numPredictors = olsq_optimal.n_features_in_
            rsquared = dict_zl[khat]['stats']['rsquared']
            aum = classAttrData.loc[classidx , Cs['CLASS_BALANCE']].sum()

            rmse = dict_zl[khat]['stats']['rmse']

            nPerYr = numObservations / (classReturn_raw['T'].iloc[-1] + classReturn_raw['DT'].iloc[-1])
            tracking_error = rmse * np.sqrt(nPerYr)

            stat_out = [numObservations, numPredictors, rsquared, tracking_error,round(tracking_error*aum,2),aum]

            wald = sorted( dict_zl[khat]['stats']['tStat'].items(), key=lambda x: abs(x[1]), reverse=True)

            beta = dict_zl[khat]['coefs'].copy()
            alpha = 0
            if 'DT' in beta:
                alpha = beta['DT']
                # beta = {k:v for k,v in beta.iteritems() if k != 'DT'}
                beta.pop('DT')
                #JZ
                predictor_COL = list(beta.keys())
                if abs(alpha) < 0.0001:
                   xdata = xdata_all[predictor_COL].copy()
                   #JZ question
                   olsq_optimal = LinearRegression(fit_intercept=False).fit(xdata , ydata)
                   olsq_coef = olsq_optimal.coef_.tolist()[0]
                   olsq_coef_optimal = dict(zip(list(xdata), olsq_coef))

                   beta = dict(zip(list(xdata), olsq_optimal.coef_.tolist()[0]))
                   alpha = 0
                else:
                   # Then there is significant constant drift in the excess return
                   # before fees. seldom occurs and if it does, we will ignore.
                   print('Significant alpah {0} for {1}.'.format(alpha, fundInfo))  #JZ add fundInfo

            # populate results
            dict_rel = {}
            dict_rel[Cs['UID']] = uID
            dict_rel[Cs['EFA_FUND_ID']] = efa_fund_id

            for ofi in Cs['OTHER_FUND_INFO']:
                dict_rel[ofi]  = classAttrData.loc[classidx , ofi].tolist()[0]

            dict_rel[Cs['FUND_CATEGORY']] = classAttrData.loc[classidx , Cs['FUND_CATEGORY']].tolist()[0]
            dict_rel[Cs['CONFIG_ID']] = 'OLSQ'

            dict_stat = dict(zip(Cs['STAT'], stat_out))
            dict_rel.update(dict_stat)
            dict_rel['DT'] = alpha

            #JZ rounding to 10 decimals
            money = 1
            if khat > 0 and beta:
                for k, v in beta.items():
                    beta[k] = round(v, RegressParams['ROUNDINGDIGITS'])
                money = round(1- sum(beta.values()),RegressParams['ROUNDINGDIGITS'])

                if abs(money) <= 2 / np.power( 10, RegressParams['ROUNDINGDIGITS'] ):
                   smoney = money / len(beta)
                   money = 0
                   for k, v in beta.items():
                       beta[k] = v + smoney


                dict_rel.update(beta)

            dict_rel[Cs['BETA1_CASH']] = money
            dict_olsq_optimal[uID] = olsq_optimal

            resultsByFund.append(dict_rel.copy())

            est_col = "Y_" + dict_rel[Cs['CONFIG_ID']]
            coefs = [olsq_coef_optimal[x] for x in predictor_COL]
            y_calc = classReturn_raw[predictor_COL].mul(coefs)
            classReturn_raw[est_col] = pd.DataFrame(data=y_calc.sum(axis=1), columns=ydata.columns)

            newResiduals = classReturn_raw[[Cs['PRICE_DATE'] + '1', Cs['PRICE_DATE'] + '2', Cs['DY']]].copy()
            newResiduals[Cs['UID']] = uID
            newResiduals[Cs['CONFIG_ID']] = dict_rel[Cs['CONFIG_ID']]
            newResiduals['DE'] = classReturn_raw[Cs['DY']] - classReturn_raw[est_col]
            residTable.append(newResiduals.copy())

            print("{0} - {1} - nIndices = {2} RSQ = {3}".format(datetime.now(), fundInfo, khat, round(rsquared,3)  ))

            # Legacy constrained case
            # Copy unconstrained results except
            dict_crel = dict_rel
            dict_crel[Cs['CONFIG_ID']] = Cs['PROD_CALIBRATION_CODE']
            dict_crel['DT'] = 0  #JZ cannot just zero out DT

            m = len(predictor_COL) #nVAR
            b = 1  # no leverage
            lb =  0
            ub = 1

            beta_est = coefs
            isOutOfBounds = not( sum(beta_est) <= b
                                 and all(x >= lb for x in beta_est)
                                 and all(x <= ub for x in beta_est)
                               ) and len(beta_est) > 0

            if isOutOfBounds:
               xdata = xdata_all[predictor_COL].copy()
               w0 = [(x) for x in beta_est]
               bnds = tuple((lb, ub)  for x in beta_est)
               cons = ({'type': 'ineq', 'fun': lambda w:  1.0 - np.sum(w)})
               #test = sse_der(w0, xdata, ydata)
               res = minimize(fun=sse, x0=w0, args=(xdata, ydata), constraints = cons, bounds = bnds, method = 'SLSQP',  jac=sse_der,options={'maxiter' : 10000, 'ftol':0.00000001 })
                              #method = 'trust-constr',  jac='2-point', hess=SR1() )
               #method = 'SLSQP', bounds = bnds, constraints=cons,,  method=' SLSQP' , options={'disp': True}

               est_col = "Y_" + dict_crel[Cs['CONFIG_ID']]
               beta = res.x
               y_calc = classReturn_raw[predictor_COL].mul(beta)
               classReturn_raw[est_col] = pd.DataFrame(data=y_calc.sum(axis=1), columns=ydata.columns)

               stats = getLRStats(xdata, ydata, beta, classReturn_raw['WEIGHT'], True)
               wsse = stats['sse']
               wsst = stats['sst']
               n = ydata.shape[0]
               nPerYr = n/yearFrac(classReturn_raw[Cs['PRICE_DATE'] + '1'].iloc[-1], classReturn_raw[Cs['PRICE_DATE'] + '1'].iloc[0], 0)
               tracking_error = np.sqrt(wsse / n * nPerYr)
               rsq = stats['rsquared']

               beta = [round(x, RegressParams['ROUNDINGDIGITS']) for x in beta]
               money = round(1 - sum(beta), RegressParams['ROUNDINGDIGITS'])

               if abs(money) <= 2 / np.power(10, RegressParams['ROUNDINGDIGITS']):
                  smoney = money / len(beta)
                  money = 0
                  beta = [x +  smoney for x in beta]

               dict_crel[Cs['BETA1_CASH']] = money

               dict_crel.update(dict(zip(list(xdata), beta)))

               #JZ may not equal khat
               stat_out = [n, len(predictor_COL), rsq, tracking_error, round(tracking_error * aum, 2), aum]
               dict_stat = dict(zip(Cs['STAT'], stat_out))
               dict_crel.update( dict_stat )

               dict_olsq_optimal[uID] = np.NaN

               print("{0} - {1} - Constrained fit: nIndices = {2} RSQ = {3}".format(datetime.now(), fundInfo, khat, round(rsq, 3)))

               newResiduals = classReturn_raw[[Cs['PRICE_DATE'] + '1', Cs['PRICE_DATE'] + '2', Cs['DY']]].copy()
               newResiduals[Cs['UID']] = uID
               newResiduals[Cs['CONFIG_ID']] = dict_crel[Cs['CONFIG_ID']]
               newResiduals['DE'] = classReturn_raw[Cs['DY']] - classReturn_raw[est_col]

            else:
               print("{0} - {1} - UnConstrained fit.".format(datetime.now(), fundInfo))
               newResiduals[Cs['CONFIG_ID']] = dict_crel[Cs['CONFIG_ID']]

            residTable.append(newResiduals.copy())
            resultsByFund.append(dict_crel.copy())

            if auditSample:
                #JZ: Matlab 710-719, clean up name?
                fileName = uID + '_' + fundName + '_' + dtstr
                fileName = fileName.replace("/", " ")
                classReturn_raw.to_csv(outFullPath + fileName + ".csv", index = False)

        except ValueError:
            print("Raised error:{0}, {1}".format(sys.exc_info()[0], sys.exc_info()[1]))
        except:
            print("{0} cannot be processed!".format(fundInfo))
            print("Unexpected error:", sys.exc_info()[0])
            # res = minimize(fun=sse, x0=np.array(beta_est), args=(xdata, ydata), method='Powell')

    classAttrData['STRATEGY'] = classAttrData[Cs['IS_RMGMP']].apply(lambda x : 'RISK MANAGED' if x else 'CONVENTIONAL')

    df_resultsByFund = pd.DataFrame( resultsByFund ).fillna(0)
    df_resultsByFund = df_resultsByFund.fillna(0)
    df_residTable = pd.concat( residTable).reset_index(drop=True)
    pred_cols = [Cs['BETA1_CASH']] + dx_col

    df_resultsByFund[Cs['EFFECTIVE_DATE']] = effective_date
    df_resultsByFund['COMPUTE_DATETIME'] = compute_date_time
    # Add strategy column from attr table, need to remove duplicated entry
    df_resultsByFund = df_resultsByFund.merge(classAttrData[[Cs['UID'], 'STRATEGY']].drop_duplicates(Cs['UID'], keep='last'), on=Cs['UID'], how='left')

    notExistDx = [ col for col in  dx_col if col not in list(df_resultsByFund) ]
    df_resultsByFund[notExistDx] = pd.DataFrame([[0] * len(notExistDx)], index=df_resultsByFund.index)

    df_resultsByClass = df_resultsByFund[[Cs['UID'], Cs['CONFIG_ID'], Cs['BETA1_CASH']] + dx_col ].merge(classAttrData[[Cs['EFA_CLASS_ID'], Cs['EFA_FUND_ID'], Cs['UID']]], on=Cs['UID'], how='left')
    df_resultsByClass = df_resultsByClass[[Cs['UID'], Cs['CONFIG_ID'],Cs['EFA_CLASS_ID'], Cs['EFA_FUND_ID']] + dx_col]


    # df_resultsByCategory = df_resultsByFund.groupby(['STRATEGY', Cs['CONFIG_ID']]).apply( weighted, pred_cols, 'AUM' )
    # df_resultsByCategory['AUM'] = df_resultsByFund.groupby(['STRATEGY', Cs['CONFIG_ID']]).agg({'AUM': 'sum'})
    # df_resultsByCategory = pd.DataFrame(df_resultsByCategory.to_records())

    # if not isCompareToProd:
    #    fileName = 'Beta_By_'
    #    df_resultsByFund.to_csv(outFullPath + fileName + Cs['EFA_FUND_ID'] + '_' + dtstr + '.csv', index=False )
    #    df_resultsByClass.to_csv(outFullPath + fileName + Cs['EFA_CLASS_ID'] + '_' + dtstr + '.csv', index=False )

    # df_residTable = df_residTable.merge(classAttrData[[Cs['UID'], 'STRATEGY', Cs['FUND_BALANCE']]].drop_duplicates(Cs['UID'], keep='last'), on=Cs['UID'], how='left')

    groupingCols = [Cs['PRICE_DATE'] + '1', Cs['PRICE_DATE'] + '2', Cs['CONFIG_ID']]

    # if isOutputResiduals:
    #    df_residTable.to_csv(outFullPath + 'Backtest_by_' + Cs['UID'] + '_' + dtstr + '.csv', index = False )

    #    dYTable = pd.pivot_table(df_residTable, values=Cs['DY'], index=groupingCols, columns=Cs['UID']).reset_index()
    #    dYTable.sort_values(by=[Cs['CONFIG_ID'], Cs['PRICE_DATE'] + '2'], ascending=False, inplace=True)
    #    dYTable.to_csv(outFullPath + 'dNAV_matrix_' + Cs['UID'] + '_' + dtstr + '.csv', index = False )
    #
    #    deTable = pd.pivot_table(df_residTable, values='DE', index=groupingCols, columns=Cs['UID']).reset_index()
    #    deTable.sort_values(by=[Cs['CONFIG_ID'], Cs['PRICE_DATE'] + '2'], ascending=False, inplace=True)
    #    deTable.to_csv(outFullPath + 'residual_matrix_' + Cs['UID'] + '_' + dtstr + '.csv', index = False )
    #
    # groupingCols += ['STRATEGY']
    #
    # #df_residTable = df_residTable.fillna(0) #JZ
    # balanceMatrix = pd.pivot_table(df_residTable, values=Cs['FUND_BALANCE'], index=groupingCols, columns=Cs['UID']).reset_index()
    # dataCols = np.setdiff1d(list(balanceMatrix), groupingCols).tolist()
    # balanceMatrix = balanceMatrix[dataCols]  #JZ  ?
    #
    # df_residTable['DEDOLLARS'] = df_residTable['DE'] * df_residTable[Cs['FUND_BALANCE']]
    # df_residTable['DYDOLLARS'] = df_residTable[Cs['DY']] * df_residTable[Cs['FUND_BALANCE']]
    #
    # dYDollarMatrix = pd.pivot_table(df_residTable, values='DYDOLLARS', index=groupingCols, columns=Cs['UID']).reset_index()
    # dYDollarMatrix = dYDollarMatrix[dataCols]
    #
    # deDollarMatrix = pd.pivot_table(df_residTable, values='DEDOLLARS', index=groupingCols, columns=Cs['UID']).reset_index()
    # backtest = deDollarMatrix[groupingCols].copy()
    # deDollarMatrix = deDollarMatrix[dataCols]
    #
    # backtest['T'] = backtest.apply(lambda x : yearFrac(x[Cs['PRICE_DATE'] + '1'], backtest[Cs['PRICE_DATE'] + '2'].max(), 0), axis  = 1)
    # backtest['W'] = np.power(0.5, backtest['T'] / RegressParams['HALF_LIFE'])
    # backtest['BALANCETOTAL'] = (~deDollarMatrix.isnull() * balanceMatrix).sum( axis = 1 )
    # backtest['DE'] = deDollarMatrix.sum(axis = 1) / backtest['BALANCETOTAL']
    # backtest[Cs['DY']] = dYDollarMatrix.sum(axis = 1) / backtest['BALANCETOTAL']
    #
    # backtest['W'] = backtest.groupby([Cs['CONFIG_ID'], 'STRATEGY'])['W'].transform(lambda x: x / float(x.sum()))
    # backtest  = backtest.fillna(0)
    #
    # if isOutputResiduals:
    #    backtest.to_csv(outFullPath + 'Backtest_by_' + Cs['CONFIG_ID'] + '_STRAT_' + dtstr + '.csv', index = False )
    #
    # backtestSummary = backtest.groupby([Cs['CONFIG_ID'], 'STRATEGY']).apply( rmse_w, ['DE', Cs['DY']], 'W' )
    # backtestSummary.columns = ['RESIDUALVOL', 'TOTALVOL']
    # backtestSummary['RSQ'] = 1 - np.power(backtestSummary['RESIDUALVOL'], 2) / np.power(backtestSummary['TOTALVOL'], 2)
    # backtestSummary = pd.DataFrame(backtestSummary.to_records())
    #
    # #JZ total vol, minus mean?
    # df_resultsByCategory = df_resultsByCategory.merge(backtestSummary, on=['STRATEGY', Cs['CONFIG_ID']], how='left')
    # df_resultsByCategory['TRACKING_ERROR_DOLLARS'] = df_resultsByCategory['RESIDUALVOL'].mul( df_resultsByCategory['AUM'], axis = 0)
    # df_resultsByCategory['RUNCODE'] = dtstr
    # df_resultsByCategory.to_csv(outFullPath + 'Beta_by_category-' + dtstr + '.csv', index=False)
    #
    #
    # if isOutputSourceData:
    #    indexPrice.to_csv(outFullPath + 'index_price_' + dtstr + '.csv', index = False )
    #    classAttrData.to_csv(outFullPath + 'fund_table_' + dtstr + '.csv', index=False)
    #    classPriceData_all.to_csv(outFullPath + 'class_price_' + dtstr + '.csv', index=False)
    #
    # df_resultsByCategory.to_csv(outFullPath + compute_date_time  + '_resultsByCategory.csv', index = False )
    # df_resultsByFund.to_csv(outFullPath + compute_date_time  + '_resultsByFund.csv', index = False )

    end = datetime.now() - start
    print("It takes {0} to run the process.".format(end))

    return df_resultsByFund, df_residTable


def weighted(x, cols, w):
    return pd.Series(np.average(x[cols], weights=x[w], axis=0), cols)

def rmse_w(x, cols, w):
    t1 = np.power(x[cols],2)
    t1 = t1.mul( x[w], axis = 0) * 252
    t1 = np.sum(t1, axis = 0)
    return pd.Series(np.sqrt(t1),cols)

def sse(coef, xdata, ydata):
    y_calc = xdata.mul(coef)
    y_predict = pd.DataFrame(data=y_calc.sum(axis=1), columns=ydata.columns)
    sse = ((ydata - y_predict).pow(2)).to_numpy().sum()

    return sse


def sse_der(coef, xdata, ydata):
    y_calc = xdata.mul(coef)
    y_predict = pd.DataFrame(data=y_calc.sum(axis=1), columns=ydata.columns)
    df1 = -2*(ydata - y_predict)
    sse_der1 = xdata.mul(df1.iloc[:,0], axis=0)
    sse_der = sse_der1.sum().tolist()
    return sse_der

# Reproduce the behavior of percentile in Matlab
def matlab_percentile(x, p):
    p = np.asarray(p, dtype=float)
    cl = [xx for xx in x if xx == xx]
    n = len(cl)
    p = (p-50)*n/(n-1) + 50
    p = np.clip(p, 0, 100)
    return np.percentile(cl, p)