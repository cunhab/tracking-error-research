"""

"""
import datetime
from datetime import date, timedelta
import pandas as pd
from pandas.tseries.offsets import (BDay, MonthEnd)
from BuyAndHold import *
from RegressionLib import fundMappingRegression, Cs
from utils import *
import math
from dateutil.parser import parse
import glob


##########################################################################
## Loop through dates and query the database; return a pandas dataframe ##
##########################################################################
def get_qry_dates(set_id, effective_date, to_date, tvol=False):
    if tvol is False:
        out_df = pd.DataFrame(columns=['regression_set_id', 'from_date', 'to_date', 'balance_date'])
        from_date = pd.date_range(effective_date, to_date, freq=BDay())
        to_date = from_date + BDay(1)
        balance_date = from_date - MonthEnd(1)
        out_df['from_date'] = from_date.strftime('%Y-%m-%d')
        out_df['to_date'] = to_date.strftime('%Y-%m-%d')
        out_df['balance_date'] = balance_date.strftime('%Y-%m-%d')

        out_df['regression_set_id'] = set_id
    else:
        out_df = pd.DataFrame(columns=['regression_set_id', 'from_date', 'to_date', 'balance_date', 'effective_date'])
        from_date = pd.date_range(effective_date, to_date, freq=BDay())
        to_date = from_date + BDay(1)
        balance_date = from_date - MonthEnd(1)
        out_df['from_date'] = from_date.strftime('%Y-%m-%d')
        out_df['to_date'] = to_date.strftime('%Y-%m-%d')
        out_df['balance_date'] = balance_date.strftime('%Y-%m-%d')

        out_df['regression_set_id'] = set_id
        out_df['effective_date'] = effective_date
        out_df['calibration_code'] = 'tvol'
    return out_df


def reg_set_time_series(set_id, effective_date, to_date, calibration_code, *sql_params):
    query_dates = get_qry_dates(set_id, effective_date, to_date)
    cols = sql_params[0]
    tbl = sql_params[1]
    where = sql_params[2]
    ### Store results
    oos_time_series = pd.DataFrame()
    for index, row in query_dates.iterrows():
        # Set up SQL query dynamically
        set_id = row['regression_set_id']
        from_date = row['from_date']
        to_date = row['to_date']
        balance_date = row['balance_date']
        tmp_where = where.format(set_id, from_date, to_date, balance_date, calibration_code)
        # Pull from ALM DB
        qry_alm = SQL(columns=cols, table=tbl, where=tmp_where)
        query = qry_alm.query
        tmp_df = qry_alm.fetch_data(query)
        oos_time_series = oos_time_series.append(tmp_df)
    return oos_time_series


def get_tvol_time_series(set_id, from_date, to_date, *sql_params):
    tvol_params = pd.DataFrame()
    for month_beg in pd.date_range(from_date, to_date, freq='MS'):
        month_end = (month_beg + MonthEnd(1)).strftime("%Y-%m-%d")
        if month_end == '2020-02-29':
            effective_date = '2020-02-28'
        else:
            effective_date = month_end
        to_date = ((month_beg + MonthEnd(2)) - BDay(1)).strftime("%Y-%m-%d")
        tmp_params = get_qry_dates(set_id, effective_date, to_date, tvol=True)
        tvol_params = tvol_params.append(tmp_params)

    # Query db
    oos_time_series = pd.DataFrame()
    cols = sql_params[0]
    tbl = sql_params[1]
    where = sql_params[2]
    for index, row in tvol_params.iterrows():
        # Set up SQL query dynamically
        set_id = row['regression_set_id']
        from_date = row['from_date']
        to_date = row['to_date']
        balance_date = row['balance_date']
        effective_date = row['effective_date']
        tmp_where = where.format(set_id, from_date, to_date, balance_date, effective_date)
        # Pull from ALM DB
        qry_alm = SQL(columns=cols, table=tbl, where=tmp_where)
        query = qry_alm.query
        tmp_df = qry_alm.fetch_data(query)
        oos_time_series = oos_time_series.append(tmp_df)

    return oos_time_series


def get_tvol_betas(set_id, from_date, to_date):
    sql_qry = """SELECT reg.fund_regression_set_id,
                       reg.efa_class_id,
                       mstr.portfolio_name,
                       reg.effective_date,
                       reg.index_name,
                       reg.allocation_pct
                
                FROM efa_fund_regression_output_view as reg
                JOIN efa_class_master_view as mstr
                on mstr.efa_class_id = reg.efa_class_id
                WHERE reg.fund_regression_set_id = {0} AND 
                      reg.effective_date BETWEEN {1} AND {2}
                ORDER BY mstr.portfolio_name, reg.effective_date, reg.index_name
    
    """

    # Set up SQL query dynamically
    tmp_qry = sql_qry.format(set_id, from_date, to_date)
    # Pull from ALM DB
    beta_df = oh_query(tmp_qry)
    out_tvol_betas = {}
    for portfolio_name in beta_df['portfolio_name'].unique():
        df = beta_df[beta_df['portfolio_name'] == portfolio_name]
        beta_pvt = pd.pivot_table(df,
                                  values='allocation_pct',
                                  index=['effective_date'],
                                  columns='index_name')
        beta_pvt.rename(index={datetime.date(2020,2,28):datetime.date(2020,2,29)}, inplace=True)
        beta_pvt.index = pd.to_datetime(beta_pvt.index)
        out_tvol_betas[portfolio_name] = beta_pvt


    return out_tvol_betas

####################################
## Manipulate dataframe and pivot ##
####################################
class Rearrange:
    def __init__(self, df, calibration_code='OLSQ'):
        # Inputs
        self.df = df
        self.calibration_code = calibration_code
        # Functions upon initialization
        self.filter_calibration_code()
        self.reg_return()
        # Storage
        self.time_series_ = None

    def filter_calibration_code(self):
        self.df = self.df[self.df['calibration_code'].str.contains(self.calibration_code, regex=False)]

    def reg_return(self):
        self.df.loc[:, 'reg_return'] = self.df.loc[:, 'regression_gross_to_balance'] / \
                                       self.df.loc[:, 'valuation_balance'] - 1

    def pivot_dataframe(self, value_cols, index_cols, cols):
        df = self.df.copy()
        # pivot dataframe
        out_pvt = (pd.pivot_table(data=df,
                                  values=value_cols,  # ['nav_gross_return', 'reg_return', 'tracking_error_return',
                                  #  'valuation_balance'],
                                  index=index_cols,  # ['from_date', 'to_date'],
                                  columns=cols)).swaplevel(2, 0, axis=1).sort_index(axis=1)  # ['portfolio_name',\
        #  'efa_fund_id']
        self.time_series_ = out_pvt
        return out_pvt


class RearrangeMultiple:
    def __init__(self, df_list, calibration_code=['OLSQ']):
        self.df_list = df_list
        self.calibration_code = calibration_code

    def concat(self, values, index, cols):
        out_df = pd.DataFrame()
        for df_num in range(len(self.df_list)):
            df = self.df_list[df_num]
            data = Rearrange(df, self.calibration_code[df_num])
            pivot = data.pivot_dataframe(values, index, cols)
            out_df = out_df.append(pivot)

        return out_df


##########################
## Compute linear stats ##
##########################
class Data:
    def __init__(self, df, level_key, freq, additional_data=[], manual_dates=None, tvol_betas=None):
        self.df = df
        self.level_key = level_key
        self.freq = freq
        self.additional_data = additional_data
        self.manual_dates = manual_dates
        self.tvol_betas = tvol_betas
        # Store data
        self.balance_df = None
        self.fund_returns_df = None
        self.fund_nav_returns_df = None
        self.fund_style_returns_df = None
        self.fund_tracking_error_df = None
        self.dates = None
        self.added_funds = None
        self.model_betas = None
        self.balance_month = 5#int(input('Input balance month #: '))
        self.balance_year = 2021#int(input('Input balance year #: '))
        # Call data functions
        self.separate_balances()
        self.shift_frequency()
        self.separate_returns()
        self.names = self.get_names()
        self.switch_data()
        self.set_manual_dates()
        # Store results in a dictionary
        self.results_ = {}

    @property
    def additional_data(self):
        return self._additional_data

    @additional_data.setter
    def additional_data(self, value):
        if not isinstance(value, list):
            raise TypeError('Must input additional data as list.')
        self._additional_data = value

    def separate_balances(self):
        df = self.df.copy()
        # Check if MultiIndex
        if isinstance(df.index, pd.MultiIndex):
            key = df.index.names[1]
            rmv = df.index.names[0]
            new_df = df.reset_index().set_index(key)
            del new_df[rmv]
        else:
            print('input DF not multiindex!')  # Come back

        returns_df = new_df.copy()
        balance_df = pd.DataFrame()
        for col_num in range(3, len(new_df.columns), 4):
            name = new_df.columns[col_num][self.level_key]
            balance_df[name] = new_df.iloc[:, col_num]
            del returns_df[new_df.columns[col_num]]

        self.balance_df = balance_df
        self.fund_returns_df = returns_df
        return None

    def shift_frequency(self):
        returns_df = self.fund_returns_df.copy()
        balance_df = self.balance_df.copy()
        if self.freq.lower() == 'w':
            print('Calculating weekly returns\n')
            self.fund_returns_df = (returns_df + 1).resample('W-Wed').prod() - 1
            self.balance_df = balance_df.resample('W-Wed').first()
        return None

    def separate_returns(self):
        returns_df = self.fund_returns_df.copy()
        fund_nav = pd.DataFrame()
        fund_style = pd.DataFrame()
        fund_te = pd.DataFrame()
        for col_num in range(0, len(returns_df.columns), 3):
            name = returns_df.columns[col_num][self.level_key]
            fund_nav[name] = returns_df.iloc[:, col_num]
            fund_style[name] = returns_df.iloc[:, col_num + 1]
            fund_te[name] = returns_df.iloc[:, col_num + 2]
        self.fund_nav_returns_df = fund_nav
        self.fund_style_returns_df = fund_style
        self.fund_tracking_error_df = fund_te

        return None

    def get_names(self):
        return self.fund_nav_returns_df.columns

    def switch_data(self):
        if len(self.additional_data) > 0:
            if self.freq.lower() == 'w':
                df_list = self.additional_data
                if df_list[0].shape[1] < 2:
                    new_names_list, new_df_list = self.parse_additional_data(df_list=df_list)
                else:
                    new_names_list, new_df_list = self.parse_additional_data(ResultsRC=df_list[0])
                self.added_funds = new_names_list
                for i in range(len(new_names_list)):
                    fund = new_names_list[i]
                    if fund in ['SSGA Emerging Markets Enhanced Index Portfolio', 'AB International Bond Portfolio']:
                        continue
                    else:
                        style = new_df_list[i]['style']
                        tracking_error = new_df_list[i]['tracking_error']
                        self.fund_style_returns_df[fund] = style
                        self.fund_tracking_error_df[fund] = tracking_error
                    self.fund_style_returns_df, self.fund_tracking_error_df = self.fund_style_returns_df.dropna(), \
                                                                              self.fund_tracking_error_df.dropna()
                new_index = self.fund_style_returns_df.index
                self.fund_nav_returns_df = self.fund_nav_returns_df.loc[new_index, :]
                self.balance_df = self.balance_df.loc[new_index, :]
                self.dates = self.fund_nav_returns_df.index

            else:
                pass

    def parse_additional_data(self, df_list=None, ResultsRC=None):

        def date_parser(series, get=1):
            series_idx = series.index
            parametercaseid = series_idx.to_series(index=range(len(series_idx))).str.split(' ').str.get(get)
            out_indices = []
            out_names = []
            for index, value in parametercaseid.iteritems():
                try:
                    parse(value)
                    out_names.append(value)
                    out_indices.append(index)
                except:
                    continue

            out_names = pd.to_datetime(pd.Series(data=out_names)).values
            return out_indices, out_names


        style_key = 'StyleOOS'
        residual_key = 'ResidualOOS'
        beta_key = 'Beta'
        window_key = 'Window'
        out_df_list = []
        out_names_list = []
        out_betas_portfolio = pd.DataFrame()
        if df_list is not None:
            for df in df_list:
                # Get name of fund
                out_names_list.append(df.loc['Fund', '1'])
                # Separate style from residual
                style = df.loc[df.index.str.startswith(style_key)]
                residual = df.loc[df.index.str.startswith(residual_key)]
                # Get dates and row numbers
                style_indices, style_dates = date_parser(style)
                residual_indices, residual_dates = date_parser(residual)
                # Set up new series
                new_style = (style.iloc[style_indices]).astype(np.float)
                new_style.index = style_dates

                new_residual = (residual.iloc[residual_indices]).astype(np.float)
                new_residual.index = residual_dates
                # Concatenate two series to one dataframe
                out_df = pd.concat([new_style, new_residual], axis=1)
                out_df.columns = ['style', 'tracking_error']
                out_df_list.append(out_df)

        elif ResultsRC is not None:
            qry = '''SELECT mstr.efa_class_id, mstr.portfolio_name FROM efa_class_master_view as mstr
                     WHERE mstr.end_date = (SELECT MAX(mstr2.end_date)
                                        FROM efa_class_master_view as mstr2
                                        WHERE mstr2.efa_class_id = mstr.efa_class_id)
            '''
            class_name_grid = oh_query(qry)
            class_name_grid['efa_class_id'] = class_name_grid['efa_class_id'].astype(int)
            class_name_grid = class_name_grid.set_index('efa_class_id')
            port_balance = self.balance_df.loc[(self.balance_df.index.month == self.balance_month) & (
                        self.balance_df.index.year == self.balance_year)].iloc[-1, :].sum()
            # Get list of weights
            class_ids = [int(x) for x in pd.to_numeric(ResultsRC.loc['Fund', :].values) if x not in (1537, 1539)]
            balances = []
            lookup_series = class_name_grid.loc[class_ids].squeeze()
            for index, value in enumerate(lookup_series):
                if lookup_series.index[index] in (1537, 1539):
                    continue
                else:
                    try:
                        balance_series = self.balance_df.copy()[value]
                    except:
                        except_qry = 'SELECT portfolio_name, efa_class_id from efa_class_master_view WHERE efa_class_id = ' + str(lookup_series.index[index])
                        tbl = oh_query(except_qry)
                        for other_name in tbl['portfolio_name']:
                            if other_name in self.balance_df.columns:
                                balance_series = self.balance_df.copy()[other_name]
                    balances.append(balance_series[(balance_series.index.month == self.balance_month) & (
                            balance_series.index.year == self.balance_year)].iloc[-1])

            class_weights_df = pd.DataFrame(lookup_series)
            class_weights_df['balance'] = balances
            class_weights_df['weight'] = class_weights_df['balance'] / class_weights_df['balance'].sum()

            for col_num in range(len(ResultsRC.columns)):
                # Get name of fund
                df = ResultsRC.iloc[:, col_num]
                class_id = int(float(df.loc['Fund']))
                if class_id in (1537, 1539):
                    continue
                name = class_name_grid.loc[class_id][0]
                out_names_list.append(name)
                # Separate style from residual
                style = df.loc[df.index.str.startswith(style_key)]
                residual = df.loc[df.index.str.startswith(residual_key)]
                # Get dates and row numbers
                style_indices, style_dates = date_parser(style)
                residual_indices, residual_dates = date_parser(residual)
                # Set up new series
                new_style = (style.iloc[style_indices]).astype(np.float)
                new_style.index = style_dates

                new_residual = (residual.iloc[residual_indices]).astype(np.float)
                new_residual.index = residual_dates
                # Concatenate two series to one dataframe
                out_df = pd.concat([new_style, new_residual], axis=1)
                out_df.columns = ['style', 'tracking_error']
                out_df_list.append(out_df)

                # Get betas
                betas = df.loc[df.index.str.contains('(?=.*' + beta_key + ')(?=.*' + window_key + ')') & ~df.index.str.contains('Rolling')]
                factors = list(betas.index.to_series(index=range(len(betas.index))).str.split(' ').str.get(1).unique())
                tmp_betas_portfolio = pd.DataFrame(columns=factors)
                for factor in factors:
                    key = beta_key + ' ' + factor + ' ' + window_key
                    betas_series = pd.to_numeric(betas.loc[betas.index.str.contains(key)])
                    beta_indices, beta_dates = date_parser(betas_series, get=3)
                    tmp_series = pd.Series(data=betas_series.values,
                                           index=beta_dates)
                    tmp_betas_portfolio[factor] = tmp_series

                weight = class_weights_df.loc[class_id, 'weight']

                if col_num == 0:
                    out_betas_portfolio = tmp_betas_portfolio * weight
                else:
                    out_betas_portfolio = out_betas_portfolio.add(tmp_betas_portfolio * weight)

            self.model_betas = out_betas_portfolio




        return out_names_list, out_df_list

    def set_manual_dates(self):
        if self.manual_dates is not None:
            print('Setting manual dates')
            new_index = self.manual_dates
            self.fund_style_returns_df = self.fund_style_returns_df.loc[new_index, :]
            self.fund_tracking_error_df = self.fund_tracking_error_df.loc[new_index, :]
            self.fund_nav_returns_df = self.fund_nav_returns_df.loc[new_index, :]
            self.balance_df = self.balance_df.loc[new_index, :]
            self.dates = new_index
        else:
            pass
        return None

    def bottom_up_agg(self, weights, fof_name='Brighthouse Asset Allocation 60 Portfolio'):
        if self.results_ is None:
            TypeError('Must compute top down stats for FoFs first')
        else:
            portfolio_funds = self.fund_nav_returns_df.columns
            fof_constituents = weights.index
            fof_constituents_weights = weights.iloc[:, 2]
            # Get funds we have mappings for
            tracked = list(set(portfolio_funds).intersection(set(fof_constituents)))
            # Filter data
            tracked_nav_returns = self.fund_nav_returns_df.loc[:, tracked]
            tracked_style_returns = self.fund_style_returns_df.loc[:, tracked]
            tracked_tracking_error_returns = self.fund_tracking_error_df.loc[:, tracked]
            # Create new FoF time series
            tracked_weights = fof_constituents_weights.loc[tracked]
            fof_nav_returns = tracked_nav_returns.dot(tracked_weights)
            fof_style_returns = tracked_style_returns.dot(tracked_weights)
            fof_tracking_error_returns = tracked_tracking_error_returns.dot(tracked_weights)
            # Replace existing FoF series with bottom up computed series
            self.fund_nav_returns_df[fof_name] = fof_nav_returns
            self.fund_style_returns_df[fof_name] = fof_style_returns
            self.fund_tracking_error_df[fof_name] = fof_tracking_error_returns

        return None


class LinearStats(Data):

    def linear_stats(self):
        nav_df = self.fund_nav_returns_df.copy()
        style_df = self.fund_style_returns_df.copy()
        te_df = self.fund_tracking_error_df.copy()
        balance_df = self.balance_df.copy()
        # Store results
        out_stats = {}
        for name in self.names:
            nav_series = nav_df[name]
            style_series = style_df[name]
            # Get nearest balance
            balance_series = balance_df[name]
            balance = balance_series[(balance_series.index.month == self.balance_month) & (balance_series.index.year == self.balance_year)].iloc[-1]
            # Check if balance is available
            try:
                if balance.empty:
                    balance = np.nan
            except:
                pass
            # Stats
            temp_stats = stats(nav_series, style_series, balance)
            out_stats[name] = temp_stats

        out_stats = pd.DataFrame(out_stats).T.dropna()  # Drop funds with na for balance
        out_stats['% of All Funds'] = out_stats['Balance'] / out_stats['Balance'].sum()
        self.results_['fund_stats'] = out_stats
        funds = self.results_['fund_stats'].index
        # Portfolio time series
        # Split into two based on NaNs
        nav_df_list = self.split_time_series(nav_df[funds])
        style_df_list = self.split_time_series(style_df[funds])
        rp_known = self.append_time_series(nav_df_list, 'ground_truth')
        rp_style = self.append_time_series(style_df_list, 'style')
        # Save portfolio data
        self.results_['portfolio_time_series'] = pd.concat([rp_known, rp_style, (rp_known - rp_style)], axis=1)
        self.results_['portfolio_time_series'].columns = ['ground_truth', 'style', 'tracking_error']
        # Compute correlations
        self.residual_correlation()
        self.zero_correlation()
        # Portfolio stats
        portfolio_balance = out_stats['Balance'].sum()
        portfolio_stats = stats(rp_known, rp_style, balance=portfolio_balance)
        # Add TE_StdDev_0
        portfolio_stats['TE_StdDev_0'] = self.results_['fund_stats']['Risk_Contribution_0'].sum()
        print(portfolio_stats)
        self.results_['portfolio_stats'] = pd.Series(portfolio_stats, name='Portfolio_Stats')
        self.results_['portfolio_stats']['RSQ_0'] = self.RSQ_0()
        # Regress portfolio against indices returns
        return self.results_['fund_stats'], self.results_['portfolio_stats']

    def append_time_series(self, df_list, name):
        out_stats = self.results_['fund_stats']
        out_series = pd.Series(name=name)
        for df in df_list:
            funds = df.columns
            balances = out_stats.loc[funds, 'Balance']
            weights = balances / balances.sum()
            out_series = out_series.append(df.dot(weights))
        return out_series

    @staticmethod
    def split_time_series(df):
        df_list = []
        nan_count = df.isna().sum(axis=0)
        if not nan_count.sum():
            df_list.append(df)
        else:
            for index, value in nan_count.iteritems():
                if value > 0:
                    # Check if nans at start or beginning
                    is_start = math.isnan(df[index].iloc[0])
                    is_last = math.isnan(df[index].iloc[-1])
                    if is_start:
                        before_df = df.iloc[:value - 1, :].dropna(axis=1)
                        after_df = df.iloc[value:, :]
                        df_list.append(before_df)
                        df_list.append(after_df)
                    elif is_last:
                        del df[index]
                        for df_num in range(len(df_list)):
                            del df_list[df_num][index]

        return df_list

    def residual_correlation(self):
        if self.results_['fund_stats'] is None:
            print("Wait! Must compute linear stats first.")
            return None

        else:
            results = self.results_['fund_stats']
            funds_resid = self.fund_tracking_error_df
            port_resid = self.results_['portfolio_time_series']['tracking_error']

            for fund in results.index:
                # if fund in self.funds_full_data_:
                fund_resid = funds_resid[fund]
                rho = port_resid.corr(fund_resid)
                results.loc[fund, 'Rho'] = rho

            results['Risk_Contribution'] = results['% of All Funds'] * results['TE_StdDev'] * results['Rho']
            self.results_['fund_stats'] = results
            return None

    def zero_correlation(self):
        if self.results_['fund_stats'] is None:
            print("Wait! Must compute linear stats first.")
            return None

        else:
            results = self.results_['fund_stats']
            port_resid = self.results_['portfolio_time_series']['tracking_error']
            port_stdev = port_resid.std()

            for fund in results.index:
                # if fund in self.funds_full_data_:
                fund_stdev = results.loc[fund, 'TE_StdDev']
                weight = results.loc[fund, '% of All Funds']
                rho = weight * fund_stdev / port_stdev
                results.loc[fund, 'Rho_0'] = rho

            results['Risk_Contribution_0'] = results['% of All Funds'] * results['TE_StdDev'] * results['Rho_0']
            self.results_['fund_stats'] = results
            return None

    def fund_sensitivity(self):
        weights = self.results_['fund_stats']['% of All Funds'].dropna()
        funds = self.results_['fund_stats'].index
        rp_known = self.results_['portfolio_time_series']['ground_truth'].copy()
        # Save
        out_stats1 = {}
        out_stats2 = {}
        pred_navs = self.fund_style_returns_df.copy()[funds]
        # Loop through funds
        for fund in self.results_['fund_stats'].index:
            fund_nav_known = self.fund_nav_returns_df.copy()[fund]
            # Perfectly model fund
            fund_nav_pred = self.fund_style_returns_df.copy()
            fund_nav_pred[fund] = fund_nav_known
            # Re compute portfolio style
            new_nav_df_list = self.split_time_series(fund_nav_pred[funds])
            rp_pred1 = self.append_time_series(new_nav_df_list, name='new_style')
            # Re compute weights
            # rp_pred1 = fund_nav_pred.dot(weights.loc[fund_nav_pred.columns])
            # Improve fund RSQ
            rp_pred2 = self.sensitivity_shock_rsq(fund_nav_known, pred_navs[fund], fund, weights)
            # Balance
            balance = self.results_['fund_stats'].loc[fund, 'Balance']
            # Prefix
            prefix1 = "0_Residual_Change_to_Portfolio_"
            prefix2 = "R2_Shock_+0.1_Change_to_Portfolio_"
            # recompute stats
            changes1 = self.change_in_portfolio(rp_known, rp_pred1, balance=balance, prefix=prefix1)
            changes2 = self.change_in_portfolio(rp_known, rp_pred2, balance=balance, prefix=prefix2)

            out_stats1[fund] = changes1
            out_stats2[fund] = changes2
            # value_keys = perc_change.keys()

        out_stats_df1 = pd.DataFrame(out_stats1).T
        out_stats_df2 = pd.DataFrame(out_stats2).T
        self.results_['fund_stats'] = pd.concat([self.results_['fund_stats'], out_stats_df1, out_stats_df2], axis=1)
        return self.results_['fund_stats']

    def group_sensitivity(self):
        rp_known = self.results_['portfolio_time_series']['ground_truth'].copy()
        # Change added funds styles to ground truth
        pred_navs = self.fund_style_returns_df.copy()
        known_navs = self.fund_nav_returns_df.copy()
        added_funds = self.added_funds
        pred_navs.loc[:, added_funds] = known_navs.loc[:, added_funds]
        # Split into two based on NaNs
        style_df_list = self.split_time_series(pred_navs)
        rp_style = self.append_time_series(style_df_list, 'style')
        # New portfolio stats
        portfolio_balance = self.results_['fund_stats']['Balance'].sum()
        portfolio_stats = stats(rp_known, rp_style, balance=portfolio_balance)

        # Regress portfolio against indices returns
        new_portfolio_stats = pd.Series(portfolio_stats, name='Portfolio_Stats')
        return new_portfolio_stats

    def change_in_portfolio(self, known, pred, balance, prefix):
        port_stats = self.results_['portfolio_stats'].copy()
        tmp_stats = stats(known, pred, balance=balance, isPortfolio=True)
        tmp_portfolio_stats = pd.Series(tmp_stats, name='New_Portfolio_Stats')
        change = (tmp_portfolio_stats - port_stats)  # /self.portfolio_stats_
        change = change.to_frame().T.add_prefix(prefix)
        change = change.drop([prefix + 'MSE*Balance',
                              prefix + 'Balance'],
                             axis=1)
        change = change.T[0].to_dict()
        return change

    def add_classification(self):
        qry = """SELECT DISTINCT a.[efa_class_id]
                                   ,mstr.portfolio_name
                              FROM [dbo].[efa_fund_tgt_vol_alias_xref_view] as a
                              INNER JOIN efa_class_master_view as mstr
                                 on mstr.efa_class_id = a.efa_class_id"""

        tvol_funds = oh_query(qry)
        portfolio_names = tvol_funds['portfolio_name']
        for fund in self.results_['fund_stats'].index:
            for tvol_name in portfolio_names:
                # ratio = SequenceMatcher(fund, tvol_name).ratio()
                if fund == tvol_name:
                    self.results_['fund_stats'].loc[fund, 'Classification'] = 'tvol'
                    break
                else:
                    self.results_['fund_stats'].loc[fund, 'Classification'] = 'conventional'
        return None

    def sensitivity_shock_rsq(self, y, f, fund, weights):
        funds = self.results_['fund_stats'].index
        # Change based on rsq
        all_nav_pred = self.fund_style_returns_df.copy()[funds]
        rsq_fund = self.results_['fund_stats'].loc[fund, 'RSQ']
        new_rsq = min(1, 0.1 + rsq_fund)
        sst = np.sum((y - np.average(y)) ** 2)
        ssr = np.sum((y - f) ** 2)
        ssr_shocked = sst * (1 - new_rsq)
        m = ssr_shocked / ssr
        # Get new residual
        resid_shocked = (y - f) * m
        # New reg return
        fund_nav_pred_shocked = y - resid_shocked
        # Redefine nav
        all_nav_pred[fund] = fund_nav_pred_shocked
        # Re compute weights
        all_nav_pred_list = self.split_time_series(all_nav_pred)
        rp_pred = self.append_time_series(all_nav_pred_list, name='new_style2')
        # rp_pred = all_nav_pred.dot(weights.loc[all_nav_pred.columns])
        return rp_pred

    def cvar(self, alpha):
        fund_stats = self.results_['fund_stats']
        fund_te_df = self.fund_tracking_error_df.copy()
        portfolio_data = self.results_['portfolio_time_series']
        portfolio_te_series = portfolio_data['tracking_error']
        # 10% quantile
        var_90 = portfolio_te_series.quantile(q=alpha)
        portfolio_tail = portfolio_te_series[portfolio_te_series <= var_90]
        # CVar
        cvar_90 = portfolio_tail.mean()
        # Downside risk
        downside_risk_90 = portfolio_tail.std()
        # Save stats
        self.results_['portfolio_stats'].loc['CVar_' + str(alpha)] = cvar_90
        self.results_['portfolio_stats'].loc['DR_' + str(alpha)] = downside_risk_90

        for fund in fund_stats.index:
            fund_te_series = fund_te_df[fund]
            fund_tail = fund_te_series.loc[portfolio_tail.index]  # [fund_te_series <= var_90]
            fund_weight = self.results_['fund_stats'].loc[fund, '% of All Funds']
            if not fund_tail.empty:
                # Fund cvar
                fund_cvar = fund_tail.mean() * fund_weight
                # Downside risk
                fund_downside_risk = fund_tail.std()
                # Tail x-sigma-rho
                fund_rho = fund_tail.corr(portfolio_tail)
                fund_dr_contribution = fund_weight * fund_downside_risk * fund_rho
                self.results_['fund_stats'].loc[fund, 'CVar_Contribution_' + str(alpha)] = fund_cvar
                self.results_['fund_stats'].loc[fund, 'DR_' + str(alpha) + '_Portfolio'] = fund_downside_risk
                self.results_['fund_stats'].loc[fund, 'DR_Contribution_' + str(alpha)] = fund_dr_contribution
            else:
                self.results_['fund_stats'].loc[fund, 'CVar_Contribution_' + str(alpha)] = 0
                self.results_['fund_stats'].loc[fund, 'DR_' + str(alpha) + '_Portfolio'] = 0
                self.results_['fund_stats'].loc[fund, 'DR_Contribution_' + str(alpha)] = 0

    def RSQ_0(self):
        var0 = self.results_['portfolio_stats']['TE_StdDev_0'] ** 2
        avg_resid = self.results_['portfolio_stats']['TE_Mean']
        n = len(self.results_['portfolio_time_series']['tracking_error'].index)
        ssr0 = n * (var0 - avg_resid**2)
        sst = ((self.results_['portfolio_time_series']['tracking_error'] - avg_resid)**2).sum()
        rsq_0 = 1 - ssr0/sst
        return rsq_0

class PortfolioRegression(LinearStats):

    def construct_regressors(self, fund_regression_set_id):
        # Pull from BITS and calculate returns
        factors_df = self.fetch_index_history(fund_regression_set_id)
        factors_pivot = self.pivot(factors_df)
        factors_rtn = self.calculate_returns(factors_pivot)
        # Align dates with existing dates of portfolio time series
        portfolio_rtn = self.results_['portfolio_time_series']['ground_truth']
        return factors_rtn

    @staticmethod
    def fetch_index_history(fund_regression_set_id):
        table = 'dbo.reg_index_set_view'
        columns = ['price_date', 'index_name', 'px', 'fund_regression_set_id']
        where = '[fund_regression_set_id] = ' + str(fund_regression_set_id)
        query_obj = SQL(columns, table, where=where)
        factor_df = query_obj.fetch_data(query_obj.query)
        return factor_df

    @staticmethod
    def pivot(df):
        pivot = pd.pivot_table(df,
                               values='px',
                               index='price_date',
                               columns='index_name')
        return pivot

    def calculate_returns(self, pivot_df):
        if self.freq == 'd':
            dt = 1
        elif self.freq == 'w':
            dt = 7

        returns_df = pivot_df.pct_change(dt).dropna()
        returns_df.index = pd.to_datetime(returns_df.index)
        return returns_df


class DBHistory(LinearStats):

    def check_constituents(self, weights):
        portfolio_funds = self.fund_nav_returns_df.columns
        fof_constituents = weights.index
        # Get funds we have mappings for
        tracked = list(set(portfolio_funds).intersection(set(fof_constituents)))
        # Pull data from db
        portfolio_names = "', '".join(tracked)
        portfolio_names_str = "('" + portfolio_names + "')"
        query = """SELECT mstr.portfolio_name
                      ,nav.[price_date]
                      ,AVG(nav.[NAV_GROSS_TR]) as AVG_NAV_GROSS_TR
                  FROM [dbo].[efa_class_total_return_view] AS nav
                  JOIN [dbo].[efa_class_master_view] as mstr 
                     ON   mstr.efa_class_id = nav.efa_class_id
                
                WHERE price_date >= '2007-01-03' AND mstr.portfolio_name in {0}
                GROUP BY mstr.portfolio_name , nav.[price_date]
                ORDER BY mstr.portfolio_name , nav.[price_date]
        
                """.format(portfolio_names_str)
        db_data = oh_query(query)
        # Pivot data
        pivot_data = pd.pivot_table(db_data,
                                    values='AVG_NAV_GROSS_TR',
                                    index=['price_date'],
                                    columns=['portfolio_name'])
        # Get percent of missing data
        percent_missing = pivot_data.isnull().sum(axis=1) / len(pivot_data.columns)
        percent_have = 1 - percent_missing
        return percent_have, pivot_data

    @staticmethod
    def clean_data(df):
        # Keep business days
        isBusinessDay = BDay().onOffset
        match_series = pd.to_datetime(df.index).map(isBusinessDay)
        df_bdays = df.loc[match_series, :]
        # Only include first x % of data
        null_funds = df_bdays.isnull().iloc[0, :].values
        inc_funds_df = df_bdays.loc[:, ~null_funds].dropna()
        # Log returns
        returns = np.log(inc_funds_df / inc_funds_df.shift(1)).dropna()
        return returns

    def construct_time_series(self, weights):
        # Obtain dataframe of returns
        _, pivot_data = self.check_constituents(weights)
        returns = self.clean_data(pivot_data)
        # Obtain weights
        perc_of_fof = weights.iloc[:, 2]
        perc_of_fof = perc_of_fof.loc[returns.columns]
        # Construct series
        series = returns.dot(perc_of_fof)
        # Convert to price
        price = np.exp(series.cumsum())
        return price, returns.columns

    def fetch_set_history(self, set_id, from_date, to_date):
        # Build query
        query = """SELECT [price_date]
                      ,[index_name]
                      ,[px]
                      ,[fund_regression_set_id]
                  FROM [dbo].[reg_index_set_view]
                  WHERE [fund_regression_set_id] = '{0}' AND 
                        [price_date] BETWEEN '{1}' AND '{2}'
                """.format(set_id, from_date, to_date)
        # Pull data
        index_levels = oh_query(query)
        index_levels['price_date'] = pd.to_datetime(index_levels['price_date'])
        # Pivot data
        index_levels_pvt = pd.pivot_table(data=index_levels,
                                          values='px',
                                          index=['price_date'],
                                          columns=['index_name'])
        return index_levels_pvt


class StatsTable(PortfolioRegression, DBHistory):

    def __init__(self, df, level_key, freq, additional_data=[], manual_dates=None):
        Data.__init__(self, df, level_key, freq=freq, additional_data=additional_data, manual_dates=manual_dates)
        self.wrapper_calculations()

    def wrapper_calculations(self):
        self.linear_stats()
        self.fund_sensitivity()
        self.add_classification()
        return None

    def fund_stats_table(self):
        return self.results_['fund_stats']

    def portfolio_stats_table(self):
        return self.results_['portfolio_stats']

    def output_excel(self, path, fname):
        self.results_['fund_stats'] = self.reorder_columns()
        workbook = pd.ExcelWriter(path + fname, engine='xlsxwriter')
        self.results_['fund_stats'].to_excel(workbook, sheet_name='Fund_Stats')
        self.results_['portfolio_stats'].to_excel(workbook, sheet_name='Portfolio_Stats')
        self.fund_tracking_error_df.to_excel(workbook, sheet_name='Fund_Residuals')
        self.fund_style_returns_df.to_excel(workbook, sheet_name='Fund_Styles')
        self.fund_nav_returns_df.to_excel(workbook, sheet_name='Fund_Ground_Truth')
        self.results_['portfolio_time_series'].to_excel(workbook, sheet_name='Portfolio_Time_Series')
        workbook.save()

    def reorder_columns(self, results=None):
        lead_columns = ['Classification', 'Alpha', 'Beta', 'RSQ', '0_Residual_Change_to_Portfolio_TE_Mean',
                        'R2_Shock_+0.1_Change_to_Portfolio_TE_Mean', 'Risk_Contribution',
                        '0_Residual_Change_to_Portfolio_TE_StdDev', 'R2_Shock_+0.1_Change_to_Portfolio_TE_StdDev',
                        '0_Residual_Change_to_Portfolio_RSQ', 'R2_Shock_+0.1_Change_to_Portfolio_RSQ',
                        'CVar_Contribution_0.1', 'CVar', '0_Residual_Change_to_Portfolio_CVar',
                        'R2_Shock_+0.1_Change_to_Portfolio_CVar', 'DR_Contribution_0.1', 'DR', 'DR_0.1_Portfolio']
        if results is None:
            columns = self.results_['fund_stats'].columns
            out_columns = lead_columns
            for col in columns:
                if col not in lead_columns:
                    out_columns.append(col)
                else:
                    continue

            sorted_results = self.results_['fund_stats'].loc[:, out_columns]
            return sorted_results

        else:
            columns = results.columns
            out_columns = lead_columns
            for col in columns:
                if col not in lead_columns:
                    out_columns.append(col)
                else:
                    continue

            sorted_results = results.loc[:, out_columns]
            return sorted_results

    def fof_bottom_up(self, weights, fof_name='Brighthouse Asset Allocation 60 Portfolio', alpha=0.10):
        self.bottom_up_agg(weights, fof_name)
        # Pull original results
        # orig_results = self.results_['fund_stats'].loc[fof_name, :]
        self.wrapper_calculations()
        self.cvar(alpha)
        # Input original results
        name = '**** ' + fof_name
        return self.results_['fund_stats'].loc[fof_name, :]

    def fof_top_down(self, price_paths, end_dates, fof_name='Brighthouse Asset Allocation 60 Portfolio', alpha=0.10):
        print("\nComputing regressions for top-down stats")

        def compute_betas(price_paths):
            out_betas = {}
            for num, price_path in enumerate(price_paths):
                # Run production regression
                results_by_fund, residual_table = run_regression(price_path, end_dates[num])
                # Filter out OLSQ results
                factors = ['US', 'SMALL', 'LTCORP', 'INT', 'INTGOV', 'IBOXHY', 'RU10GRTR', 'RU10VATR', 'XNDX', 'MONEY']
                results = results_by_fund[results_by_fund['CALIBRATION_CODE'] == 'OLSQ']
                effective_date = results['EFFECTIVE_DATETIME'][0]
                # Filter betas
                betas = results[factors].squeeze()
                out_betas[effective_date] = betas
            return out_betas

        betas = compute_betas(price_paths)
        effective_dates = [pd.to_datetime(x) for x in betas.keys()]
        # Get out of sample period levels
        index_levels = self.fetch_set_history('20', '2020-02-28', '2021-05-25')
        # Calculate returns
        index_returns = index_levels.pct_change().dropna()
        # Separate into dataframes
        split_date = effective_dates[1]
        index_returns1 = index_returns.loc[:split_date]
        index_returns2 = index_returns.loc[(split_date + BDay(1)):]
        # Create time series
        style1 = index_returns1.dot(betas['2020-02-28'])
        style2 = index_returns2.dot(betas['2020-06-30'])
        style = style1.append(style2)
        # Replace existing
        if self.freq == 'd':
            dates = self.fund_nav_returns_df[fof_name].index
            style = style.loc[dates]
            self.fund_style_returns_df[fof_name] = style
            self.fund_tracking_error_df[fof_name] = (self.fund_nav_returns_df[fof_name] - style)
        elif self.freq == 'w':
            style = (style + 1).resample('W-Wed').prod() - 1
            self.fund_style_returns_df[fof_name] = style
            self.fund_tracking_error_df[fof_name] = self.fund_nav_returns_df[fof_name] - style

        ## Recompute stats
        self.wrapper_calculations()
        self.cvar(alpha)
        return self.results_['fund_stats'].loc[fof_name, :]

    def fof_bh(self, weights_df, fof_name='Brighthouse Asset Allocation 60 Portfolio', alpha=0.10):
        self.fund_nav_returns_df[fof_name] = self.fund_nav_returns_df.copy().loc[:, weights_df.columns]. \
            mul(weights_df).sum(axis=1)
        self.fund_style_returns_df[fof_name] = self.fund_style_returns_df.copy().loc[:, weights_df.columns]. \
            mul(weights_df).sum(axis=1)
        self.fund_tracking_error_df[fof_name] = self.fund_nav_returns_df[fof_name] - \
                                                self.fund_style_returns_df[fof_name]
        # Re-do calculations
        self.wrapper_calculations()
        self.cvar(alpha)
        return self.results_['fund_stats'].loc[fof_name, :]

    def fof_all(self, weights, price_paths, end_dates,
                fof_name='Brighthouse Asset Allocation 60 Portfolio', alpha=0.10, bh_weights=None,
                bh_weights_path=None):
        # Save original results
        orig_results = self.results_['fund_stats'].loc[fof_name, :]
        port_original = self.results_['portfolio_stats']
        # Get 77% of funds
        _, funds = self.construct_time_series(weights)
        # Redefine weights
        weights = define_weights(weights)
        weights = weights.loc[funds, :]
        # Bottom_up
        bottom_up_fof = self.fof_bottom_up(weights)
        port_bottom_up = self.results_['portfolio_stats']
        bottom_up_style = self.fund_style_returns_df.copy()[fof_name]
        # # Top down
        top_down_fof = self.fof_top_down(price_paths, end_dates)
        port_top_down = self.results_['portfolio_stats']
        top_down_style = self.fund_style_returns_df.copy()[fof_name]
        # Buy and hold weights
        weights.index = "Beta " + weights.index + " 2020-03-02"
        df_bh = self.fund_nav_returns_df.copy().loc[:, funds]
        if bh_weights is None:
            bh_weights_df = define_weights(df_bh,
                                           df_bh.index[0].strftime('%Y-%m-%d'),
                                           how='drifted',
                                           drift_weights=weights)
            suffix = 'BH_weights_' + self.freq + '.pkl'
            bh_weights_df.to_pickle(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\output\pickle\\' + suffix)
        else:
            bh_weights_df = pd.read_pickle(bh_weights_path)

        bh_fof = self.fof_bh(bh_weights_df)
        port_bh = self.results_['portfolio_stats']
        bh_ground_truth = self.fund_nav_returns_df.copy()[fof_name]

        # Aggregate results
        results = pd.concat([orig_results, bottom_up_fof, top_down_fof, bh_fof], axis=1)
        results.columns = ['Original', 'Bottom_Up', 'Top_Down', 'Buy_Hold']

        port_results = pd.concat([port_original, port_bottom_up, port_top_down, port_bh], axis=1)
        port_results.columns = ['Original', 'With_Bottom_Up_' + fof_name, 'With_Top_Down_' + fof_name,
                                'With_BH_' + fof_name]

        # Regressions against buy and hold
        bot_up_v_bh = pd.Series(data=stats(known_y=bh_ground_truth,
                                           pred_y=bottom_up_style,
                                           balance=0),
                                name='BH ~ Bottom_Up')
        top_down_v_bh = pd.Series(data=stats(known_y=bh_ground_truth,
                                             pred_y=top_down_style,
                                             balance=0),
                                  name='BH ~ Top_Down')
        out_versus_stats = pd.concat([bot_up_v_bh, top_down_v_bh],
                                     axis=1)

        return results.T, port_results, out_versus_stats

    def buy_hold_tvol_betas(self, tvol_betas_dict, rebalance_freq='m', alpha=0.1):
        funds = self.results_['fund_stats'][self.results_['fund_stats']['Classification'] == 'tvol'].index
        out_bh_betas = {}
        beta_start_date = self.fund_nav_returns_df.index[0].replace(day=1) - timedelta(days=1)
        returns = tvol_factor_returns(beta_start_date-timedelta(days=1), freq=self.freq)
        # dates to use
        dates = self.fund_nav_returns_df.index
        # Loop through funds by portfolio name
        for portfolio_name in tvol_betas_dict:
            print("Buy-hold for ", portfolio_name)
            if portfolio_name not in funds:
                print("\tOmitting ", portfolio_name)
                continue
            else:
                ## Possible fix to wed-wed betas
                beta_df = tvol_betas_dict[portfolio_name].loc[beta_start_date:]
                out_df = pd.DataFrame()
                # Loop through tvol effective dates (month end dates)
                for index, row in beta_df.iterrows():

                    drift_weights = row.copy()
                    effective_date = drift_weights.name
                    # Account for year end
                    if effective_date.month != 12:
                        df = returns.loc[(returns.index.month == (effective_date.month + 1)) & \
                                         (returns.index.year == effective_date.year)]
                    else:
                        df = returns.loc[(returns.index.month == (1)) & \
                                         (returns.index.year == effective_date.year + 1)]

                    if rebalance_freq == 'm':
                        drift_weights.index = "Beta " + drift_weights.index + " " + df.index[0].strftime(
                            '%Y-%m-%d')
                        bh_weights_df = define_weights(df,
                                                       DateWindow=df.index[0].strftime('%Y-%m-%d'),
                                                       how='drifted',
                                                       drift_weights=drift_weights,
                                                       rebalance=False)
                        out_df = out_df.append(bh_weights_df)
                    elif rebalance_freq == 'w' and self.freq == 'd':
                        wed_in_curr_month = df.loc[df.index.dayofweek == 2]
                        n, _ = wed_in_curr_month.shape
                        df_month = df.copy()
                        bh_weights_df = pd.DataFrame()
                        drift_weights = row.copy()
                        i_override = False
                        stopping_condition = False
                        incomplete_month = False
                        for i in range(-1, n):
                            if i_override:
                                i += 1
                            # Reset row to original weights
                            row = drift_weights.copy()
                            start = i
                            stop = i+1
                            # Check if no more Wednesdays to use as close slice
                            if stop + 1 == n+1:
                                df = df_month.loc[wed_in_curr_month.index[i]+timedelta(days=1):]
                                stopping_condition = True
                                # Check if whole month's data is not populated
                                if n < 3:
                                    incomplete_month = True
                            # Check if first day of month is before first Wednesday
                            elif start == -1 and df_month.index[0] < wed_in_curr_month.index[0]:
                                df = df_month.loc[:wed_in_curr_month.index[0]]
                            else:
                                # Check if first day is a Wednesday
                                if start == -1 and df_month.index[0] == wed_in_curr_month.index[0]:
                                    df = df_month.loc[:wed_in_curr_month.index[1]]
                                    i_override = True
                                # Check if last day of month is a Wednesday
                                elif wed_in_curr_month.index[i+1] == df_month.index[-1]:
                                    df = df_month.loc[
                                         wed_in_curr_month.index[i] + timedelta(days=1):]
                                    stopping_condition = True
                                # All other conditions intra month
                                else:
                                    df = df_month.loc[
                                         wed_in_curr_month.index[i]+timedelta(days=1):wed_in_curr_month.index[i+1]]
                            # Break loop because empty dataframe for incomplete months
                            if incomplete_month:
                                break
                            row.index = "Beta " + row.index + " " + df.index[0].strftime(
                                '%Y-%m-%d')
                            tmp_bh_weights_df = define_weights(df,
                                                           DateWindow=df.index[0].strftime('%Y-%m-%d'),
                                                           how='drifted',
                                                           drift_weights=row,
                                                           rebalance=False)
                            bh_weights_df = bh_weights_df.append(tmp_bh_weights_df)
                            # Exit month loop if stopping condition is met
                            if stopping_condition:
                                break



                        out_df = out_df.append(bh_weights_df)

            out_bh_betas[portfolio_name] = out_df
            # Create new style and tracking error
            new_style = out_df.loc[dates].mul(returns.loc[dates]).sum(axis=1)
            new_tracking_error = self.fund_nav_returns_df[portfolio_name] - new_style
            # Update
            self.fund_style_returns_df[portfolio_name] = new_style
            self.fund_tracking_error_df[portfolio_name] = new_tracking_error

        # Recompute stats
        self.wrapper_calculations()
        self.cvar(alpha)




def tvol_factor_returns(start_date, freq='d'):
    qry = """SELECT price_date,
                   index_name,
                   px
            FROM reg_index_set_view
            WHERE fund_regression_set_id = '19' AND
                  price_date >= '{0}'""".format(start_date)
    levels_df = oh_query(qry)
    levels_pvt = pd.pivot_table(levels_df,
                                 values='px',
                                 index='price_date',
                                 columns='index_name')
    levels_pvt.index = pd.to_datetime(levels_pvt.index)
    levels_pvt = levels_pvt.pct_change().dropna()

    if freq == 'w':
        levels_pvt = (1+levels_pvt).resample('W-Wed').prod()-1
    return levels_pvt

def define_weights(df, DateWindow=None, how='constant', drift_weights=None, rebalance=False, is_fof=False):
    if how == 'constant':
        return df
    elif how == 'drifted':
        # print('Defining buy-hold weights ')
        if is_fof is True:
            drift_weights = drift_weights.iloc[:, 2] / (drift_weights.iloc[:, 2].sum())
        else:
            drift_weights = drift_weights
        df.index.name = 'Date'
        df = df.reset_index()
        # ### ResultsALL
        ResultsAllC = pd.DataFrame(data=drift_weights.values,
                                   index=drift_weights.index).T
        ResultsAllC['Level Fund ' + DateWindow] = 100
        ResultsAllC['LevelBH Fund ' + DateWindow] = 100
        # date_col = 'Date'
        factors = list(ResultsAllC.columns[:-2])
        col_order = ['Level Fund ' + DateWindow, 'LevelBH Fund ' + DateWindow] + factors
        ResultsAllC = ResultsAllC[col_order]

        frequency = 'Daily'
        LevelsDf = LevelCreator(df, frequency, 'Return', 1, 1).dropna().reset_index(drop=True)
        # Align dataframes by removing duplicates
        df, LevelsDf = align_levels(df, LevelsDf)
        AllFactorsDict = {'Empty': 'Empty'}
        FactorSet_Setup = pd.DataFrame()
        filenameRaw = ''
        DoRebalanceMV = False
        DoRebalanceBeta = False
        AllSubFactorList = [x for x in df.columns if x != 'Date']
        Factorsorig = df
        df['Date'] = pd.to_datetime(df['Date']).dt.strftime('%Y-%m-%d')

        for c in range(len(df.index)):

            DateWindow = df['Date'][c]
            ResultsAllC.loc[c, 'DateWindow'] = DateWindow
            if c > 0:
                ResultsAllC.loc[c, ['Beta ' + x + ' ' + DateWindow for x in AllSubFactorList]] = \
                    ResultsAllC.loc[c - 1, factors].values
            ResultsAllC = TradingStats(DoRebalanceMV, DoRebalanceBeta, ResultsAllC, c, DateWindow, AllFactorsDict,
                                       AllSubFactorList,
                                       FactorSet_Setup,
                                       filenameRaw, LevelsDf, Factorsorig)
        date_index = pd.to_datetime(df['Date'])
        starting_weights = drift_weights
        weights_df = parse_bh_weights(ResultsAllC, AllSubFactorList, date_index, starting_weights)

        return weights_df


def align_levels(df, LevelsDf):
    df = df.set_index('Date')
    LevelsDf = LevelsDf.set_index('Date')

    LevelsDf = LevelsDf.drop_duplicates()
    LevelsDf = LevelsDf.loc[~LevelsDf.index.duplicated(), :]

    df = df.loc[~df.index.duplicated(), :]
    return df.reset_index(), LevelsDf.reset_index()


def parse_bh_weights(ResultsAllC, AllSubFactorList, date_index, starting_weights):
    bh_names = ['BetaBH ' + x for x in AllSubFactorList]
    col_names = []
    for col_name in ResultsAllC:
        for bh_name in bh_names:
            if bh_name in col_name:
                col_names.append(col_name)

    bh_weights = ResultsAllC.loc[:, col_names]
    # Make out dataframe
    out_df = pd.DataFrame(index=date_index)
    first_date = date_index.dt.strftime('%Y-%m-%d')[0]
    for factor in bh_names:
        factor_bh_weights = bh_weights.loc[:, bh_weights.columns.str.contains(factor)]
        weights = []
        for col in factor_bh_weights.columns:
            val = factor_bh_weights[col].dropna()
            weights.append(val.values[0])

        out_df[factor.replace('BetaBH ', '')] = weights

    return out_df


def load_model_results(path):
    # Import model output as a list of dataframes
    funds_list = []
    for file in glob.glob(path):
        tmp_df = pd.read_csv(file,
                             index_col=[0],
                             header=[0])
        funds_list.append(tmp_df)
    return funds_list


def run_regression(class_price_path, end_date):
    startDate = '2007-01-03'
    endDate = end_date
    dataSource = 'ALMPRODDB'

    isGAAP = False  # false for STAT
    isCompareToProd = False
    isOutputResiduals = True
    isOutputSourceData = True
    outputPath = r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\output\Regression'

    # if AUDIT_SAMPLE is not empty then limit to these portfolio_uid
    auditSample = []
    # ['11A3CA', 'AB684E']
    # ['DB8D6B']
    # ['F8C754']
    # ['DB8D6B']
    # ['CF4809','830295','F8C754','139F7A','0518DD']

    inputFiles = {
        'CLASSPRICE': class_price_path
    }

    # inputFiles = {}

    # Make sure all the date follows the same format
    result_by_fund, resid_table = fundMappingRegression(startDate, endDate, dataSource, isGAAP, isCompareToProd,
                                                        isOutputResiduals, isOutputSourceData, outputPath,
                                                        Cs['DX_OTHER'], auditSample, inputFiles)
    return result_by_fund, resid_table
