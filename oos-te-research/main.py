

from lib import *
from BuyAndHold import *
import warnings
import glob
warnings.simplefilter(action='ignore', category=FutureWarning)


if __name__ == "__main__":
    ### SQL parameters
    cols = ['[regression_set_id]', '[effective_date]', '[efa_fund_id]', '[from_date]', '[to_date]', '[balance_date]',
            '[calibration_code]', '[valuation_balance]', '[regression_gross_to_balance]', '[nav_gross_return]',
            '[tracking_error_return]', '[portfolio_name]']
    tbl = '[dbo].[te_tracking_error_by_fund_view]'
    where = """[regression_set_id] = '{0}' AND
               [from_date] = '{1}'         AND
               [to_date] = '{2}'           AND
               [balance_date] = '{3}'      AND
               [calibration_code] = '{4}'
             """
    where_tvol = "[regression_set_id] = '{0}' AND [from_date] = '{1}' AND [to_date] = '{2}' AND [balance_date] = '{3}' AND [effective_date] = '{4}'"
    where_invesco = "[regression_set_id] = '{0}' AND [from_date] = '{1}' AND [to_date] = '{2}' AND [balance_date] = '{3}' AND [effective_date] = '{4}' AND [portfolio_name] = 'Invesco Balanced-Risk Allocation Portfolio'"
    # # Regression
    # set_11_time_series_1 = reg_set_time_series('11', '2020-02-28', '2020-09-29',
    #                                            'LEGACY_CLSQ-v2020-02-28_2021-07-27-15-17-09', cols, tbl, where)
    # set_11_time_series_2 = reg_set_time_series('11', '2020-09-30', '2021-03-30',
    #                                            'LEGACY_CLSQ-v2020-09-30_2021-07-27-15-28-23', cols, tbl, where)
    # set_11_time_series_3 = reg_set_time_series('11', '2021-03-31', '2021-07-30',
    #                                            'LEGACY_CLSQ-2021STAT-v2021-03-31_2021-07-22-12-24-', cols, tbl, where)
    # set_18_time_series_1 = reg_set_time_series('18', '2020-02-28', '2020-09-29',
    #                                            'LEGACY_CLSQ-v2020-02-28_2021-07-27-16-06-58', cols, tbl, where)
    # set_18_time_series_2 = reg_set_time_series('18', '2020-09-30', '2021-03-30',
    #                                            'Legacy_CLSQ-v20200930_20201013-1744', cols, tbl, where)
    # set_18_time_series_3 = reg_set_time_series('18', '2021-03-31', '2021-07-30',
    #                                            'LEGACY_CLSQ-v2021-03-31_2021-07-26-15-13-30', cols, tbl, where)
    #
    # # # ### Create dataframe of query dates to pass through where clause
    #
    # values = ['nav_gross_return', 'reg_return', 'tracking_error_return', 'valuation_balance']
    # index = ['from_date', 'to_date']
    # cols = ['portfolio_name', 'efa_fund_id']
    # # # Regression data
    # set_11_rearrange = RearrangeMultiple([set_11_time_series_1, set_11_time_series_2, set_11_time_series_3],
    #                                      calibration_code=['LEGACY_CLSQ-v2020-02-28_2021-07-27-15-17-09',
    #                                                        'LEGACY_CLSQ-v2020-09-30_2021-07-27-15-28-23',
    #                                                        'LEGACY_CLSQ-2021STAT-v2021-03-31_2021-07-22-12-24-'])
    # set_11_time_series = set_11_rearrange.concat(values=values,
    #                                              index=index,
    #                                              cols=cols)
    # set_18_rearrange = RearrangeMultiple([set_18_time_series_1, set_18_time_series_2, set_18_time_series_3],
    #                                      calibration_code=['LEGACY_CLSQ-v2020-02-28_2021-07-27-16-06-58',
    #                                                        'Legacy_CLSQ-v20200930_20201013-1744',
    #                                                        'LEGACY_CLSQ-v2021-03-31_2021-07-26-15-13-30'])
    # set_18_time_series = set_18_rearrange.concat(values=values,
    #                                              index=index,
    #                                              cols=cols)
    # # # # # # Tvol data
    # tvol_time_series = get_tvol_time_series('19', '2020-01-20', '2021-07-30', cols, tbl, where_tvol)
    # tvol_time_series.to_csv(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\tvol_time_series2.csv')
    # tvol_time_series = pd.read_csv(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\tvol_time_series2.csv')
    # tvol_set = Rearrange(tvol_time_series, calibration_code='tvol')
    # tvol_pivot = tvol_set.pivot_dataframe(values, index, cols)
    # #
    # # # # tvol funds
    # tvol_columns = tvol_pivot.columns
    # new_index = tvol_pivot.index.map(lambda x: (pd.to_datetime(x[0]), pd.to_datetime(x[1])))
    # tvol_pivot.index = pd.MultiIndex.from_tuples(new_index, names=['from_date', 'to_date'])
    # tvol_data = tvol_pivot.loc[set_18_time_series.index, :]
    # all_data = set_18_time_series.copy()
    # # # all_data = set_16_time_series
    # all_data[tvol_columns] = tvol_pivot

    # all_data.to_csv(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\output\all_data_set_18_19_through_July.csv')
    all_data = pd.read_csv(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\output\all_data_set_18_19_through_July.csv',
                           index_col=[0, 1],
                           header=[0, 1, 2])
    all_data.index = all_data.index.set_levels(
        [pd.to_datetime(all_data.index.levels[0]), pd.to_datetime(all_data.index.levels[1])])
    #############################
    # # Read in new AQR
    # aqr_new = pd.read_csv(r"C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\Results1-ParameterCaseList_1SubParameterCaseList_'NA'-DSA-Daily-Roll_1-W_52_AQR_Daily_ResultsRCSortedT.csv",
    #                       index_col=[0],
    #                       header=[0])
    # # Invesco
    # inv_new = pd.read_csv(r"C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\Results1-ParameterCaseList_1SubParameterCaseList_'NA'-DSA-Daily-Roll_1-W_52_Invesco_ResultsRCSortedT.csv",
    #                       index_col=[0],
    #                       header=[0])
    # # All other tvol funds
    # path = "C:/Users/bcunha/proj/tracking-error-research/oos-te-research/data/FromModel/*csv"
    # tvol_funds = load_model_results(path)
    # tvol_funds.append(aqr_new)
    # tvol_funds.append(inv_new)

    ###################################
    # PATH
    out_path = 'C:/Users/bcunha/proj/tracking-error-research/oos-te-research/output/'

    # # Original data

    # Method to obtain the betas
    tvol_betas_dict = get_tvol_betas('19', "'2020-01-31'", "'2021-08-31'")

    all_stats = StatsTable(all_data, level_key=1, freq='d')
    all_stats.cvar(0.10)
    all_stats.buy_hold_tvol_betas(tvol_betas_dict, rebalance_freq='w')
    out_fname = 'daily_tvol_BH_new_stats_weekly_rebalance.xlsx'
    all_stats.output_excel(out_path, out_fname)
    # Load in model results
    # df_list = load_model_results(r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\FromModel\AllFunds\*.csv')
    # out_names = ['11', '18', '20']
    # i = 0
    # for df in df_list:
    #     all_stats = StatsTable(all_data, level_key=1, freq='w', additional_data=[df])
    #     all_stats.cvar(0.10)
    #     # fund_results = all_stats.fund_stats_table()
    #     # portfolio_results = all_stats.portfolio_stats_table()
    #     out_fname = 'weekly_FLS_set_{0}_OOS_630_bal.xlsx'.format(out_names[i])
    #     all_stats.output_excel(out_path, out_fname)
    #     all_stats.model_betas.to_csv(out_path + 'set_{0}_portfolio_betas_05312021.csv'.format(out_names[i]))
    #     i += 1
    ### New stats for BHF 60 test
    #####################################
    ##      Weights                    ##
    #####################################
    # weights_path = r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\BHF60_weights.csv'
    # weights = pd.read_csv(weights_path,
    #                       index_col=[0],
    #                       header=[0]).dropna(1)
    #
    # weights_model_path = r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\BHF60_weights_for_model.csv'
    # weights_model = pd.read_csv(weights_model_path,
    #                       index_col=[0],
    #                       header=[0])
    # # new_stats = StatsTable(all_data, level_key=1, freq='w')
    # # new_stats.cvar(0.10)# , manual_dates=new_dates)
    # # new_stats.fof_bottom_up(weights)
    # # #### file name
    # # out_fname = 'weekly_with_bottom_up_BHF60.xlsx'
    # # # Output
    # # new_stats.output_excel(out_path, out_fname)
    # BHF60_0228_path = r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\BHF60_top_down_underlying_data_02292020.csv'
    # BHF60_0630_path = r'C:\Users\bcunha\proj\tracking-error-research\oos-te-research\data\BHF60_top_down_underlying_data_06302020.csv'
    #
    # fund_stats, portfolio_stats, versus_stats = all_stats.fof_all(weights,
    #                                                 price_paths=[BHF60_0228_path, BHF60_0630_path],
    #                                                 end_dates=['2020-02-28', '2020-06-30'])
    # fund_stats = all_stats.reorder_columns(fund_stats)
    #
    # workbook = pd.ExcelWriter(out_path + 'Daily_BHF60_Stats_With_BH.xlsx')
    # fund_stats.to_excel(workbook, sheet_name='Fund_Stats')
    # portfolio_stats.to_excel(workbook, sheet_name='Portfolio_Stats')
    # versus_stats.to_excel(workbook, sheet_name='Versus_Buy_Hold')
    # workbook.save()












