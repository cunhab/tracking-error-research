"""
Utility script that stores useful functions that are not unique to the project; they just help with certain tasks.

"""
import pandas as pd
import numpy as np
import pyodbc
from scipy.optimize import minimize

#######################################
## ALM Database Connection and Pulls ##
#######################################
class AlmDatabaseConnection:
    def __init__(self, server_name):
        self._server_dictionary = {'ALM-DB-PROD': 'use01azvasqlbhf003.database.windows.net',
                                   'ALM-DB-TEST': 'use01azvasqlbhf002.database.windows.net',
                                   'ALM-DB-DEV': 'use01azvasqlbhf001.database.windows.net',
                                   'ALM-DB-SANDBOX': 'alm-sqldb-sb-useast1.database.windows.net'}
        if server_name not in self.valid_server_names():
            raise TypeError(
                "Parameter Value Error: parameter 'server_name' must be one of the following valid Server Names: " + ", ".join(
                    self.valid_server_names()))

        self._server_name = server_name
        self._server = self._server_dictionary[self._server_name]
        self._database = 'alm'
        self._driver = 'ODBC Driver 17 for SQL Server'
        self._conn = pyodbc.connect(
            r'DRIVER=' + self._driver + ';SERVER=' + self._server + ';PORT=1433;DATABASE=' + self._database + ';Trusted_Connection=yes')

    def open_conn(self):
        return self._conn

    def close_conn(self):
        self._conn.close()

    def valid_server_names(self):
        return self._server_dictionary.keys()


class QryProd(AlmDatabaseConnection):
    def __init__(self):
        super(QryProd, self).__init__('ALM-DB-PROD')

    def fetch_data(self, query):
        return pd.read_sql_query(query, self.open_conn())


class SQL(QryProd):
    def __init__(self, columns, table, **kwargs):
        self.columns = columns
        self.table = table
        self.options = {'where': None,  # where clause with multiple conditions
                        'order by': None,
                        'group by': None,
                        'having': None}
        self.options.update(kwargs)
        # print(self.options)
        self.query = self.param_to_string()
        super(SQL, self).__init__()

    def param_to_string(self):
        # Params
        where = self.options['where'] if self.options['where'] is not None else ''
        order_by = self.options['order by'] if self.options['order by'] is not None else ''
        group_by = self.options['group by'] if self.options['group by'] is not None else ''
        having = self.options['having'] if self.options['having'] is not None else ''
        # Cols to string
        cols = ', '.join(self.columns)
        # Construct
        qry = 'SELECT ' + cols + \
              ' FROM ' + self.table + \
              ' WHERE ' + where + ' ' + \
              group_by + ' ' + \
              having + ' ' + \
              order_by
        return qry


def oh_query(query):
    return QryProd().fetch_data(query)


#######################################
##    Analyze known vs. predicted    ##
#######################################
def stats(known_y, pred_y, balance, isPortfolio=False):
    import statistics as st
    from scipy.stats import kurtosis
    from scipy.stats import skew
    from statsmodels.regression.linear_model import OLS
    from statsmodels.tools.tools import add_constant
    from sklearn.metrics import r2_score
    # linear regression
    x = add_constant(pred_y)
    # Exception for funds that started after start of time series
    try:
        lm = OLS(known_y, x)
    except:
        known_y = known_y.dropna()
        pred_y = pred_y.dropna()
        x = x.dropna()
        lm = OLS(known_y, x)
    results = lm.fit()
    alpha = results.params.values[0]
    beta = results.params.values[1]
    # stats
    rsq_from_ss = r2_score(known_y, pred_y)
    mse = st.mean((known_y - pred_y) ** 2)
    min_resid = min(known_y - pred_y)
    max_resid = max(known_y - pred_y)
    mean_resid = st.mean(known_y - pred_y)
    stddev_resid = np.std((known_y - pred_y))
    kurt_resid = kurtosis(known_y - pred_y)
    skew_resid = skew(known_y - pred_y)
    # Percentiles
    tracking_error = known_y - pred_y
    quantiles = tracking_error.quantile([0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95])
    # TE dollars
    te_dollars_mean = np.average(tracking_error)
    # CVar/ES
    var_level = 90
    var_90 = np.percentile(tracking_error, 100 - var_level)
    cvar_90 = tracking_error[tracking_error <= var_90].mean()
    DR = tracking_error[tracking_error <= var_90].std()
    # Store stats
    stats = {'RSQ': rsq_from_ss,
             'Alpha': alpha,
             'Beta': beta,
             'MSE': mse,
             'MSE*Balance': mse * balance,
             'TE_Min': min_resid,
             'TE_Max': max_resid,
             'TE_Mean': mean_resid,
             'TE_StdDev': stddev_resid,
             'TE_Kurt': kurt_resid,
             'TE_Skew': skew_resid,
             'Balance': balance}
    for index, value in quantiles.items():
        key = str(index) + '_quantile'
        stats[key] = value

    stats['CVar'] = cvar_90
    stats['DR'] = DR
    return stats

###########################
## OLS Code Modification ##
###########################
def OLS(data, Params):

    X = np.asmatrix(np.random.rand(20, 5))#np.asmatrix(data.get('X'))
    # Before:
    # Y = np.asmatrix(data.get('Y'))
    # After:
    # Y = data.get('Y')
    # Y = np.asmatrix(np.array(list(Y), dtype=float)).transpose()
    Y = np.asmatrix(np.random.rand(20, 1))#np.asmatrix(data.get('Y'), dtype=float)

    vebose = Params.get('vebose', 0)
    budget = 1 # Params.get('Budget', 0)
    BudgetValue = Params.get('BudgetValue', 1)
    const = Params.get('Constant', 0)
    levereged = Params.get('Leverage', 0)
    n = np.size(X, 1)
    T = np.size(X, 0)
    outPoint = Params.get('outPoint', np.zeros(T))
    if (budget):
        # Xnew = np.zeros([T, n - 1])
        Ynew = Y.copy()
        lastelem = n - 1
        Xlast = np.repeat(X[:, lastelem], lastelem, 1)
        Xnew = X[:, 0:lastelem]
        Xnew = Xnew - Xlast
        Ynew = Ynew - X[:, lastelem]
    else:
        lastelem = n
        Xnew = X.copy()
        Ynew = Y.copy()
    if (const):
        ones = np.ones([T, 1])
        Xnew = np.append(Xnew, ones, 1)
    XTX = np.matmul(np.transpose(Xnew), Xnew)
    XTY = np.matmul(np.transpose(Xnew), Ynew)
    XTXinv = np.linalg.inv(XTX)

    if (levereged):
        beta = hlLeveragedQPSolve(XTX, XTY, Params)
    else:
        # XTX*beta = XTY
        beta = np.linalg.solve(XTX, XTY)
        #### BC - adding code here I think?
        b = 1.0 ## This is BudgetValue
        lb = 0
        ub = 1

        isOutOfBounds = not( sum(beta) <= b and
                             all(x >= lb for x in beta) and
                             all(x <= ub for x in beta) and
                             len(beta) > 0)

        if isOutOfBounds:
            def sse(coef, xdata, ydata):
                y_predict = (xdata @  coef)
                sse = np.power((ydata - y_predict), 2).sum()
                return sse

            w0 = [(x) for x in beta]
            bnds = tuple((lb, ub) for x in beta)
            cons = ({'type': 'ineq',
                     'fun':  lambda w: b - np.sum(w)})
            res = minimize(fun=sse,
                           x0=w0,
                           args=(Xnew, Ynew),
                           constraints=cons,
                           bounds=bnds,
                           method='SLSQP')
            beta = res.x

    if (const):
        alpha = beta[lastelem, 0]
        beta = beta[0:lastelem, 0]
    else:
        alpha = 0
    if (budget):
        newbeta = np.zeros([n, 1])
        newbeta[0:lastelem, :] = beta[0:lastelem, :]
        newbeta[lastelem, 0] = BudgetValue - np.sum(beta[0:lastelem, 0])
        beta = newbeta.copy()

    betas = np.array([x for sublist in beta.tolist() for x in sublist] *
                     outPoint.shape[0]).reshape(outPoint.shape[0], n)
    alphas = np.ones([outPoint.shape[0], 1]) * alpha

    result = {'beta_est': betas, 'alpha_est': alphas, 'betaCov': XTXinv}
    return result
