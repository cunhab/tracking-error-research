'''
    AllFactorsDf: dataframe of returns with dates (rows) and funds (columns)
    n: number of funds
    Factor: name of each fund (currently taken from Dataall, a dataframe of dates (rows) and portfolio, fund returns
            (columns) but doesn't have to be taken from there)
    ResultsAllC: a dataframe of windows (rows) and results (columns). It needs the most recent column of results, no
                need for columns prior to that. At the very beginning, you need to supply the beta (weight) of each
                fund as 'Beta A1 1980-01-28' and the fund level (see right below) as 'Level Fund 1980-01-28'
    Level Fund: ResultsAll needs to contain the level of the portfolio (not the hedge) at the beginning. You can just
                set this to be 100. Given you are not doing rebalancing going forward, just set it always to 100.
                (normally, you would need to increment it based on the liability portfolio's returns)
    c: window index, starting from 0.
    DateWindow: window date, for example '1980-01-29'
    LevelsDf: the level dataframe with dates (rows) and funds (columns)
    AllSubFactorList: List that contains the names of all funds.
'''

''' Create Levels out of fund returns '''

import pandas as pd
from datetime import timedelta

def LevelCreator(df, frequency, DataType, MultiplierIn, MultiplierOut):

    # We assume source data is daily
    df['Date'] = pd.to_datetime(df['Date'])
    # Generate business days
    BDates = pd.to_datetime(pd.DataFrame(DataDatesCreator(df)[1])['Date'])
    df = pd.merge(df, BDates, on='Date', how='right')
    # Proxy missing returns by zero
    if DataType == 'Level':
        df = df.fillna(method='ffill')
    elif DataType =='Return':
        df = df.fillna(0)
        # Transform to Level. By default nan are skipped but the above has proxied those.
        df = pd.concat([df[['Date']], (1 + df.drop('Date', axis=1)/MultiplierIn).cumprod()], axis=1)
        # Add one day prior with level set to 1
        df = pd.concat([df.loc[[0], :], df], axis=0).reset_index(drop=True)
        df.loc[0, 'Date'] = (df.loc[0, 'Date'] - timedelta(days=1)).date().strftime('%Y-%m-%d')
        df['Date'] = pd.to_datetime(df['Date'])
        df.iloc[0, 1:] = 1
    dfReturns = MultiplierOut * df.drop('Date', axis=1)
    df = pd.concat([df['Date'], dfReturns], axis=1)

    return df
def AnalyticTCreator(df, c, SubFactor, TName, AnalyticNameIn, AnalyticNameOut, DateList):

    SpaceSubFactor = ' ' if SubFactor !='' else ''
    AnalyticT = pd.DataFrame(df.loc[c,
        list(set(list(df.columns)) & set([AnalyticNameIn + SpaceSubFactor + x + ' ' + y
                                          for x in [SubFactor] for y in DateList]))]).reset_index()
    AnalyticT.columns = ['FactorDate' + TName, AnalyticNameOut]
    AnalyticT['Factor'] = AnalyticT['FactorDate' + TName].str.replace(AnalyticNameIn + ' ', '')
    for Date in DateList:
        AnalyticT['Factor'] = AnalyticT['Factor'].str.replace(' ' + Date, '')
    AnalyticT['Date' + TName] = \
        AnalyticT['FactorDate' + TName].str.replace(AnalyticNameIn + ' ', '').str.replace(SubFactor + ' ', '')

    return AnalyticT
def TradingStats(DoRebalanceMV, DoRebalanceBeta, ResultsAllC, c, DateWindow, AllFactorsDict, AllSubFactorList,
                 FactorSet_Setup, filenameRaw, LevelsDf, Factorsorig):

    # Beta Traded
    ResultsAllC.loc[c, ['BetaTraded ' + x + ' ' + DateWindow for x in AllSubFactorList]] = \
        ResultsAllC.loc[c - 1, ['BetaTraded ' + x + ' ' + ResultsAllC.loc[c - 1, 'DateWindow']
                                for x in AllSubFactorList]].values if c > 0 and not DoRebalanceBeta else \
            ResultsAllC.loc[c, ['Beta ' + x + ' ' + DateWindow for x in AllSubFactorList]].values

    # Beta Sum
    BetaSumBH = ResultsAllC.loc[c, [x for x in ResultsAllC.columns if 'BetaTraded' in x and DateWindow in x]].sum()
    BetaSum = ResultsAllC.loc[c, [x for x in ResultsAllC.columns if 'Beta' in x and DateWindow in x
                                  and all(sub not in x for sub in ['Predicted', 'Style', 'BH', 'Mean', 'Traded'])]].sum()

    # ReturnBH Fund
    if c > 0:
        Factorsorig['Date'] = pd.to_datetime(Factorsorig['Date']).dt.strftime('%Y-%m-%d')
        for SubFactor in AllSubFactorList:
            ResultsAllC.loc[c, 'Return ' + SubFactor + ' ' + DateWindow] = \
                Factorsorig[Factorsorig['Date']==DateWindow][SubFactor].values[0]
        ResultsAllC.loc[c, 'ReturnBH Fund' + ' ' + DateWindow] = \
            sum([ResultsAllC.loc[c - 1, 'BetaBH ' + x + ' ' + ResultsAllC.loc[c - 1, 'DateWindow']] *
                 ResultsAllC.loc[c, 'Return ' + x + ' ' + DateWindow] for x in AllSubFactorList])
    ResultsAllC.loc[c, 'LevelBH Fund' + ' ' + DateWindow] = \
        ResultsAllC.loc[c - 1, 'LevelBH Fund' + ' ' + ResultsAllC.loc[c - 1, 'DateWindow']] * \
        (1+ ResultsAllC.loc[c, 'ReturnBH Fund' + ' ' + DateWindow]) if c > 0 and not DoRebalanceMV else \
        ResultsAllC.loc[c, 'Level Fund' + ' ' + DateWindow]

    # BetaBH
    for SubFactor in AllSubFactorList:
        # Factor Beta = (1 + Factor Return) * BetaT_1 / (1 + ReturnBH Fund)
        ResultsAllC.loc[c, 'BetaBH ' + SubFactor + ' ' + DateWindow] = \
            (1 + ResultsAllC.loc[c, 'Return ' + SubFactor + ' ' + DateWindow]) * \
            ResultsAllC.loc[c - 1, 'BetaBH ' + SubFactor + ' ' + ResultsAllC.loc[c - 1, 'DateWindow']] / \
            (1 + ResultsAllC.loc[c, 'ReturnBH Fund' + ' ' + DateWindow]) \
        if c > 0 and not DoRebalanceBeta else ResultsAllC.loc[c, 'Beta ' + SubFactor + ' ' + DateWindow]


    for SubFactor in AllSubFactorList:
        # Factor Levels
        BetasT = AnalyticTCreator(ResultsAllC, c, SubFactor, '', 'Beta', 'Beta', [DateWindow])
        BetasT['Date'] = pd.to_datetime(BetasT['Date'])
        for key in AllFactorsDict:
            Levels = pd.concat([AllFactorsDict[key]['Date'], AllFactorsDict[key].drop('Date', axis=1) /
                                list(FactorSet_Setup[(FactorSet_Setup['FactorSet'].astype(str) == filenameRaw + 'P') &
                                                     (FactorSet_Setup['FactorSubset'].astype(str) == key)]['Multiplier'])[
                                    0]], axis=1). \
                reset_index(drop=True) if key != 'Empty' else LevelsDf
            ''' Replace consecutive duplicates, infinite, spike, cross nan values with nan, remove entities
            if (consecutive) missing data is above threshold, remove start or end of consecutive missing data
            across all columns and create Log including for missing values '''
            # (df, LogColumnList, LogColumnValueList, spike, MissingPercentRow, MissingPercentColumn,
            # ConsecutiveMissingColumns, ConsecutiveMissingThreshold)
            # Levels = MissingDataFix(Levels, [], [], 999999, 999, 999, 999, 999)[0]
            Levels = Levels.set_index('Date').stack().reset_index()
            Levels.columns = ['Date', 'Factor', 'Level']
            Levels['Date'] = pd.to_datetime(Levels['Date'])
            BetasT = pd.merge(BetasT, Levels, left_on=['Date', 'Factor'], right_on=['Date', 'Factor'], how='left')
        BetasT['Level ' + SubFactor + ' ' + DateWindow] = BetasT['Level']

        # Quantity = Fund MV * Factor Beta / Factor Level. Continuous beta rebalancing.
        # BetasT['Quantity ' + SubFactor + ' ' + DateWindow] = \
        #     ResultsAllC.loc[c, 'Level Fund' + ' ' + DateWindow] * BetasT['Beta'] / (BetasT['Level'] * BetaSum) \
        #     if DoRebalanceMV else \
        #     ResultsAllC.loc[c, 'LevelMVBH Fund' + ' ' + DateWindow] * BetasT['Beta'] / (BetasT['Level'] * BetaSum)
        BetasT['Quantity ' + SubFactor + ' ' + DateWindow] = \
            ResultsAllC.loc[c, 'Beta ' + SubFactor + ' ' + DateWindow] * \
            ResultsAllC.loc[c, 'LevelBH Fund' + ' ' + DateWindow] / \
            (BetasT['Level'] * BetaSum)
        BetasT['MarketValue ' + SubFactor + ' ' + DateWindow] = \
            BetasT['Quantity ' + SubFactor + ' ' + DateWindow] * BetasT['Level']
        ResultsAllC.loc[c, ['Level ' + SubFactor + ' ' + DateWindow, 'Quantity ' + SubFactor + ' ' + DateWindow,
                           'MarketValue ' + SubFactor + ' ' + DateWindow]] = \
            BetasT[['Level ' + SubFactor + ' ' + DateWindow, 'Quantity ' + SubFactor + ' ' + DateWindow,
                    'MarketValue ' + SubFactor + ' ' + DateWindow]].loc[0]

    # Set QuantityBH equal to the previous one unless this is the first iteration
    ColumnsQuantity = [x for x in ResultsAllC.columns if 'Quantity' in x and DateWindow in x and 'BH' not in x]
    ColumnsQuantity.sort()
    ColumnsQuantityBH = [x.replace('Quantity', 'QuantityBH') for x in ColumnsQuantity]
    ColumnsQuantityT_1 = [x for x in ResultsAllC.columns if 'Quantity' in x and
                          ResultsAllC.loc[max(c - 1, 0), 'DateWindow'] in x and 'BH' not in x]
    ColumnsQuantityT_1.sort()
    ColumnsQuantityBHT_1 = [x.replace('Quantity', 'QuantityBH') for x in ColumnsQuantityT_1] \
        if c > 0 else ColumnsQuantity
    # Support more cases below
    if not DoRebalanceBeta:
        ResultsAllC.loc[c, ColumnsQuantityBH] = ResultsAllC.loc[max(c - 1, 0), ColumnsQuantityBHT_1].values
    else:
        ResultsAllC.loc[c, ColumnsQuantityBH] = ResultsAllC.loc[c, ColumnsQuantity].values

    for SubFactor in AllSubFactorList:
        # Factor MV = Factor Quantity * Factor Level
        ResultsAllC.loc[c, 'MarketValueBH ' + SubFactor + ' ' + DateWindow] = \
            ResultsAllC.loc[c, 'QuantityBH ' + SubFactor + ' ' + DateWindow] * \
            ResultsAllC.loc[c, 'Level ' + SubFactor + ' ' + DateWindow]

    return ResultsAllC
def DataDatesCreator(Data):
    Data = Data.set_index('Date')
    # Populate all days
    start_date = Data.index.min() - pd.DateOffset(day=1)
    end_date = Data.index.max() + pd.DateOffset(day=31)
    dates = pd.bdate_range(start_date, end_date, freq='D', name='Date')
    CDates = pd.DataFrame(dates, columns=['Date'])
    dates = pd.bdate_range(start_date, end_date, freq='B', name='Date')
    BDates = pd.DataFrame(dates, columns=['Date'])

    return CDates, BDates
