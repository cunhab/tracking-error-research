import pandas as pd
import numpy as np
import nltk
import pickle
import pyLDAvis.sklearn
from collections import Counter
from textblob import TextBlob
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from wordcloud import WordCloud, ImageColorGenerator
import matplotlib.pyplot as plt
import seaborn as sns

## Load in the data
data_clean = pd.read_csv(r'C:\Users\bcunha\proj\tracking-error-research\ADW\data_dictionary_cleaned.csv',
                         header=[0])
data_clean = data_clean.drop('Unnamed: 0', axis=1)

## Parse data
df = data_clean[['lemmatized_title', 'lemmatized_description']]
## Convert list to string
df['lemma_str_title'] = [' '.join(map(str, l)) for l in df['lemmatized_title']]
df['lemma_str_description'] = [' '.join(map(str, l)) for l in df['lemmatized_description']]

## Topic modeling
tf_vectorizer_description = CountVectorizer(max_df=0.8, min_df=25, max_features=5000)
tf_description = tf_vectorizer_description.fit_transform(df['lemma_str_title'].values.astype('U'))
tf_feature_names_description = tf_vectorizer_description.get_feature_names()
doc_term_matrix_description = pd.DataFrame(tf_description.toarray(), columns=list(tf_feature_names_description))

lda_model = LatentDirichletAllocation(n_components=5, learning_method='online', max_iter=500, random_state=0).fit(tf_description)
no_top_words = 5


def display_topics(model, feature_names, no_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic %d:" % (topic_idx))
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-no_top_words - 1:-1]]))


display_topics(lda_model, tf_feature_names_description, no_top_words)

# TFIDF
print()
tfidf_vectorizer = TfidfVectorizer(max_df=0.80, min_df =25, max_features=5000, use_idf=True)
tfidf = tfidf_vectorizer.fit_transform(df['lemma_str_title'])
tfidf_feature_names = tfidf_vectorizer.get_feature_names()
doc_term_matrix_tfidf = pd.DataFrame(tfidf.toarray(), columns=list(tfidf_feature_names))

nmf = NMF(n_components=5, random_state=0, alpha=.1, init='nndsvd').fit(tfidf)
display_topics(nmf, tfidf_feature_names, no_top_words)