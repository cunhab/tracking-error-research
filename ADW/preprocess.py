# Import packages
import pandas as pd
import numpy as np
import nltk
# nltk.download('punkt')
# nltk.download('stopwords')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('wordnet')
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords, wordnet
from nltk.stem import WordNetLemmatizer


# Load ADW data dictionary
data_dictionary = pd.read_csv(r'C:\Users\bcunha\proj\tracking-error-research\ADW\1628598827862.csv',
                              header=[0])
# Get data types
data_types = list(data_dictionary['Type'].unique())

######################################
## Data Cleaning and Pre-Processing ##
######################################

data_clean = data_dictionary.copy()
data_clean['Description'] = data_clean['Description'].astype(str)
# Tokenization
data_clean['tokenized_title'] = data_clean['Title'].apply(word_tokenize)
data_clean['tokenized_description'] = data_clean['Description'].apply(word_tokenize)
# Convert all characters to lowercase
data_clean['lower_title'] = data_clean['tokenized_title'].apply(lambda x: [word.lower() for word in x])
data_clean['lower_description'] = data_clean['tokenized_description'].apply(lambda x: [word.lower() for word in x])
# Remove punctuations
punc = string.punctuation
data_clean['no_punc_title'] = data_clean['lower_title'].apply(lambda x: [word for word in x if word not in punc])
data_clean['no_punc_description'] = data_clean['lower_description'].apply(lambda x: [word for word in x if word not in punc])
# Remove stopwords
stop_words = set(stopwords.words('english'))
data_clean['stopwords_removed_description'] = data_clean['no_punc_description'].apply(lambda x: [word for word in x if word not in stop_words])
# Lemmatization
data_clean['pos_tags_title'] = data_clean['no_punc_title'].apply(nltk.tag.pos_tag)
data_clean['pos_tags_description'] = data_clean['stopwords_removed_description'].apply(nltk.tag.pos_tag)

## Convert from NLTK to wordnet format
def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN

data_clean['wordnet_pos_title'] = data_clean['pos_tags_title'].apply(lambda x: [(word, get_wordnet_pos(pos_tag)) for (word, pos_tag) in x])
data_clean['wordnet_pos_description'] = data_clean['pos_tags_description'].apply(lambda x: [(word, get_wordnet_pos(pos_tag)) for (word, pos_tag) in x])

wnl_title = WordNetLemmatizer()
wnl_description = WordNetLemmatizer()

data_clean['lemmatized_title'] = data_clean['wordnet_pos_title'].apply(lambda x: [wnl_title.lemmatize(word, tag) for word, tag in x])
data_clean['lemmatized_description'] = data_clean['wordnet_pos_description'].apply(lambda x: [wnl_title.lemmatize(word, tag) for word, tag in x])


